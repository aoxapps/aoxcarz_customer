import Axios from 'axios';
import {AsyncStorage, Alert} from 'react-native';
import {takeEvery, put} from 'redux-saga/effects';
import {SUPPORT_REQUEST} from './types';
import {supportSuccess, supportFail} from './actions';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';
import {StackActions, NavigationActions} from 'react-navigation';
import {postAPI, get} from '../../utils/api';
import {getConfiguration, setConfiguration} from '../../utils/configuration';
import {PaymentMethodListSuccess} from '../GetPaymentMethodList/actions';
function* onSupportRequest({data, navigation}) {
  yield* showLoader(false);
  try {
    const responseData = yield postAPI(
      Config.supportEmailURL,
      JSON.stringify(data),
    );
    //  console.log('data:   ' + JSON.stringify(responseData));
    if (responseData.data.status === SUCCESS) {
      yield put(supportSuccess(responseData.data.data));
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          'Message sent successfully.',
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.goBack();
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else if (responseData.data.message === Config.authMessage) {
      yield put(supportFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield* hideLoader(false, '');
      yield put(supportFail());
      yield showAlertWithDelay(responseData.data.message);
    }
  } catch (error) {
    // console.log(JSON.stringify(error));
    yield* hideLoader(false, '');
    yield put(supportFail());
    //yield showAlertWithDelay('Invalid  details');
  }
}

function* sagaSupport() {
  yield takeEvery(SUPPORT_REQUEST, onSupportRequest);
}
export default sagaSupport;
