import Axios from 'axios';
import {AsyncStorage, Alert} from 'react-native';
import moment from 'moment-timezone';
import {takeEvery, put} from 'redux-saga/effects';
import {
  ADD_TRIP_REQUEST,
  CANCEL_TRIP_REQUEST,
  GET_TRIP_REQUEST,
  RATING_REQUEST,
  PANIC_REQUEST,
  PREFER_DRIVER_REQUEST,
  COUPON_REQUEST,
  RATING_CANCEL_REQUEST,
  ABORT_TRIP_REQUEST,
} from './types';
import {
  addTripFail,
  addTripSuccess,
  cancelTripSuccess,
  getTripSuccess,
  ratingSuccess,
  panicSuccess,
  preferDriverSuccess,
  cancelRatingSuccess,
  couponSuccess,
  abortTripSuccess,
} from './actions';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';
import {postAPI, get} from '../../utils/api';
import {profileSuccess} from '../GetProfile/actions';

function* onAddTripRequested({
  selectedRide,
  trptyp,
  prefDriverRide,
  country,
  countryCode,
  state,
  stateCode,
  city,
  cityCode,
  pricingType,
  isSurgeTime,
  surgeMultiplier,
  unit,
  distance,
  estimatedTime,
  estimatedCost,
  bikeSelected,
  PaymentVia,
  selectedCardID,
  SourcePick,
  dropOneway,
  TimeZoneSelected,
  dat,
  tim,
  promoCode,
  noOfSeats,
  navigation,
}) {
  yield* showLoader(false);
  try {
    let details = {
      rideType: selectedRide, //standard or child
      trip_type: trptyp,
      prefDriver: prefDriverRide,
      country: country,
      countryCode: countryCode,
      state: state,
      stateCode: stateCode,
      city: city,
      cityCode: cityCode,
      pricingType: pricingType,
      isSurgeTime: isSurgeTime,
      surgeMultiplier: surgeMultiplier,
      unit: unit,
      distance: distance,
      estimatedTime: estimatedTime,
      estimatedCost: estimatedCost,
      carTypeRequired: bikeSelected,
      paymentMethod: PaymentVia,
      paymentSourceRefNo: selectedCardID,
      source: SourcePick,
      destination: dropOneway,
      scheduleTimezone: TimeZoneSelected,
      scheduleDate: dat,
      scheduleTime: tim,
      promoCode: promoCode,
      noOfSeats: noOfSeats,
    };
    // console.log('asdada');
    // const anc = details;
    // console.log('asdada');
    const addTripData = yield postAPI(
      Config.addTripURL,
      JSON.stringify(details),
    );
    // console.log('Add Trip Data:   ' + JSON.stringify(addTripData));
    if (addTripData.data.status === SUCCESS) {
      if (addTripData.data.data.tripStatus != 'Scheduled') {
        var tripId = addTripData.data.data.tripId;
        yield* onGetTripRequest({tripId});
      }
      // yield* onGetTripRequest(addTripData.data.data.tripId);
      yield put(addTripSuccess(addTripData.data.data));
      yield* hideLoader(false, '');
    } else if (addTripData.data.message === Config.authMessage) {
      yield put(addTripFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield* hideLoader(false, '');
      yield put(addTripFail());
      yield showAlertWithDelay(addTripData.data.message);
    }
  } catch (error) {
    // console.log(JSON.stringify(error));
    yield put(addTripFail());
    yield* hideLoader(false, '');
    //yield showAlertWithDelay('Unable to get estimated cost');
  }
}
function* onCancelTripRequest({data, navigation}) {
  yield* showLoader(false);
  try {
    const responseData = yield postAPI(
      Config.cancelTripURL,
      JSON.stringify(data),
    );

    if (responseData.data.status === SUCCESS) {
      yield put(cancelTripSuccess(responseData.data));
      yield* hideLoader(false, '');
    } else if (responseData.data.message === Config.authMessage) {
      yield put(addTripFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield put(addTripFail());
      yield* hideLoader(false, '');
    }
  } catch (error) {
    // console.log(JSON.stringify(error));
    yield put(addTripFail());
    yield* hideLoader(false, '');
  }
}
function* onGetTripRequest({tripId, navigation}) {
  const responseData = yield get(Config.getTripRequestURL + tripId);
  if (responseData.data.status === SUCCESS) {
    yield put(getTripSuccess(responseData.data.data));
  } else if (responseData.data.message === Config.authMessage) {
    yield put(addTripFail());
    yield* hideLoader(false, '');
    setTimeout(() => {
      Alert.alert(
        'Alert',
        Config.authErrorMessage,
        [
          {
            text: 'Ok',
            onPress: () => {
              navigation.navigate(Config.Login);
            },
          },
        ],
        {cancelable: false},
      );
    }, 600);
  }
}
function* onRatingRequest({data, navigation}) {
  yield* showLoader(false);
  try {
    const responseData = yield postAPI(
      Config.ratingTripURL,
      JSON.stringify(data),
    );
    //console.log(responseData);
    if (responseData.data.status === SUCCESS) {
      yield put(ratingSuccess(responseData.data));
      const profileData = yield get(Config.profile);
      if (profileData.data.status === SUCCESS) {
        yield put(profileSuccess(profileData.data.data));
      }
      yield* hideLoader(false, '');
    } else if (responseData.data.message === Config.authMessage) {
      yield put(addTripFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield put(addTripFail());
      yield* hideLoader(false, '');
    }
  } catch (error) {
    //console.log(JSON.stringify(error));
    yield put(addTripFail());
    yield* hideLoader(false, '');
  }
}
function* onPanicRequest({data, navigation}) {
  yield* showLoader(false);
  try {
    const responseData = yield postAPI(Config.panicURL, JSON.stringify(data));
    //console.log('dasdad');
    if (responseData.data.status === SUCCESS) {
      yield put(panicSuccess(responseData.data));
      yield* hideLoader(false, '');
      yield showAlertWithDelay(responseData.data.message);
    } else if (responseData.data.message === Config.authMessage) {
      yield put(addTripFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield put(addTripFail());

      yield* hideLoader(false, '');
      yield showAlertWithDelay(
        'No emergency contacts found. Please add Contacts!',
      );
    }
  } catch (error) {
    // console.log(JSON.stringify(error));
    yield put(addTripFail());
    yield* hideLoader(false, '');
  }
}
function* onPreferDriverRequest({data, navigation}) {
  yield* showLoader(false);
  try {
    const responseData = yield postAPI(
      Config.preferDriverURL,
      JSON.stringify(data),
    );
    if (responseData.data.status === SUCCESS) {
      yield put(preferDriverSuccess(responseData.data));
      yield* hideLoader(false, '');
    } else if (responseData.data.message === Config.authMessage) {
      yield put(addTripFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield put(addTripFail());
      yield* hideLoader(false, '');
    }
  } catch (error) {
    // console.log(JSON.stringify(error));
    yield put(addTripFail());
    yield* hideLoader(false, '');
  }
}
function* onCouponRequest({data, navigation}) {
  yield* showLoader(false);
  try {
    // console.log(JSON.stringify(data));
    const responseData = yield postAPI(
      Config.validatePromoCodeURl,
      JSON.stringify(data),
    );
    // console.log(JSON.stringify(responseData));
    if (responseData.data.status === SUCCESS) {
      yield put(couponSuccess(responseData.data));
      yield* hideLoader(false, '');
    } else if (responseData.data.message === Config.authMessage) {
      yield put(addTripFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield put(couponSuccess(responseData.data));
      yield* hideLoader(false, '');
      // yield showAlertWithDelay(responseData.data.message);
    }
  } catch (error) {
    // console.log(JSON.stringify(error));
    yield put(addTripFail());
    yield* hideLoader(false, '');
  }
}
function* onAbortTripRequest({data, navigation}) {
  yield* showLoader(false);
  try {
    const responseData = yield postAPI(Config.abortTrip, JSON.stringify(data));
    if (responseData.data.status === SUCCESS) {
      //var tripId = responseData.data.data.tripId;
      /// yield* onCurrrentTripRequested({tripId, navigation});
      yield put(abortTripSuccess(responseData.data));
      yield* hideLoader(false, '');
    } else if (responseData.data.message === Config.authMessage) {
      yield put(addTripFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield put(addTripFail());
      yield* hideLoader(false, '');
    }
  } catch (error) {
    // console.log(JSON.stringify(error));
    yield put(addTripFail());
    yield* hideLoader(false, '');
  }
}
function* onRatingCancelRequest(navigation) {
  var data = null;
  // console.log('aasdassda');
  yield put(getTripSuccess(data));
  yield put(cancelRatingSuccess());
}

function* sagaAddTrip() {
  yield takeEvery(ADD_TRIP_REQUEST, onAddTripRequested);
  yield takeEvery(CANCEL_TRIP_REQUEST, onCancelTripRequest);
  yield takeEvery(GET_TRIP_REQUEST, onGetTripRequest);
  yield takeEvery(RATING_REQUEST, onRatingRequest);
  yield takeEvery(PANIC_REQUEST, onPanicRequest);
  yield takeEvery(PREFER_DRIVER_REQUEST, onPreferDriverRequest);
  yield takeEvery(COUPON_REQUEST, onCouponRequest);
  yield takeEvery(RATING_CANCEL_REQUEST, onRatingCancelRequest);
  yield takeEvery(ABORT_TRIP_REQUEST, onAbortTripRequest);
}
export default sagaAddTrip;
