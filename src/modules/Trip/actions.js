import {
  ADD_TRIP_REQUEST,
  GET_TRIP_REQUEST,
  CANCEL_TRIP_REQUEST,
  ABORT_TRIP_REQUEST,
  RATING_REQUEST,
  PANIC_REQUEST,
  PREFER_DRIVER_REQUEST,
  COUPON_REQUEST,
  RATING_CANCEL_REQUEST,
  ADD_TRIP_SUCCESS,
  GET_TRIP_SUCCESS,
  CANCEL_TRIP_SUCCESS,
  ABORT_TRIP_SUCCESS,
  RATING_SUCCESS,
  PANIC_SUCCESS,
  PREFER_DRIVER_SUCCESS,
  COUPON_SUCCESS,
  RATING_CANCEL_SUCCESS,
  ADD_TRIP_FAILURE,
} from './types';

export const addTripRequest = (
  selectedRide,
  trptyp,
  prefDriverRide,
  country,
  countryCode,
  state,
  stateCode,
  city,
  cityCode,
  pricingType,
  isSurgeTime,
  surgeMultiplier,
  unit,
  distance,
  estimatedTime,
  estimatedCost,
  bikeSelected,
  PaymentVia,
  selectedCardID,
  SourcePick,
  dropOneway,
  TimeZoneSelected,
  dat,
  tim,
  promoCode,
  noOfSeats,
  navigation,
) => ({
  type: ADD_TRIP_REQUEST,
  selectedRide,
  trptyp,
  prefDriverRide,
  country,
  countryCode,
  state,
  stateCode,
  city,
  cityCode,
  pricingType,
  isSurgeTime,
  surgeMultiplier,
  unit,
  distance,
  estimatedTime,
  estimatedCost,
  bikeSelected,
  PaymentVia,
  selectedCardID,
  SourcePick,
  dropOneway,
  TimeZoneSelected,
  dat,
  tim,
  promoCode,
  noOfSeats,
  navigation,
});
export const cancelTripRequest = (data, navigation) => ({
  type: CANCEL_TRIP_REQUEST,
  data,
  navigation,
});
export const abortTripRequest = (data, navigation) => ({
  type: ABORT_TRIP_REQUEST,
  data,
  navigation,
});
export const getTripRequest = (tripId, navigation) => ({
  type: GET_TRIP_REQUEST,
  tripId,
  navigation,
});

export const ratingRequest = (data, navigation) => ({
  type: RATING_REQUEST,
  data,
  navigation,
});
export const panicRequest = (data, navigation) => ({
  type: PANIC_REQUEST,
  data,
  navigation,
});
export const preferDriverRequest = (data, navigation) => ({
  type: PREFER_DRIVER_REQUEST,
  data,
  navigation,
});
export const couponRequest = (data, navigation) => ({
  type: COUPON_REQUEST,
  data,
  navigation,
});
export const cancelRatingRequest = navigation => ({
  type: RATING_CANCEL_REQUEST,
  navigation,
});

export const addTripSuccess = data => ({
  type: ADD_TRIP_SUCCESS,
  data,
});
export const getTripSuccess = data => ({
  type: GET_TRIP_SUCCESS,
  data,
});

export const ratingSuccess = data => ({
  type: RATING_SUCCESS,
  data,
});

export const cancelTripSuccess = data => ({
  type: CANCEL_TRIP_SUCCESS,
  data,
});
export const abortTripSuccess = data => ({
  type: ABORT_TRIP_SUCCESS,
  data,
});
export const panicSuccess = data => ({
  type: PANIC_SUCCESS,
  data,
});
export const couponSuccess = data => ({
  type: COUPON_SUCCESS,
  data,
});
export const cancelRatingSuccess = data => ({
  type: RATING_CANCEL_SUCCESS,
  data,
});
export const preferDriverSuccess = data => ({
  type: PREFER_DRIVER_SUCCESS,
  data,
});
export const addTripFail = () => ({
  type: ADD_TRIP_FAILURE,
});
