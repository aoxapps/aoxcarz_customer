import {
  ADD_TRIP_REQUEST,
  CANCEL_TRIP_REQUEST,
  ABORT_TRIP_REQUEST,
  GET_TRIP_REQUEST,
  RATING_REQUEST,
  PANIC_REQUEST,
  PREFER_DRIVER_REQUEST,
  RATING_CANCEL_REQUEST,
  COUPON_REQUEST,
  ADD_TRIP_SUCCESS,
  GET_TRIP_SUCCESS,
  CANCEL_TRIP_SUCCESS,
  ABORT_TRIP_SUCCESS,
  RATING_SUCCESS,
  PANIC_SUCCESS,
  PREFER_DRIVER_SUCCESS,
  COUPON_SUCCESS,
  RATING_CANCEL_SUCCESS,
  ADD_TRIP_FAILURE,
} from './types';

const INITIAL_STATE = {
  tripData: null,
  getTripData: null,
  cancelTripData: null,
  abortTripData: null,
  ratingData: null,
  panicData: null,
  preferDriverData: null,
  couponData: null,
  cancelRatingData: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_TRIP_REQUEST:
      return {
        ...state,
        isBusy: true,
      };
    case GET_TRIP_REQUEST:
      return {
        ...state,
        isBusy: true,
      };
    case CANCEL_TRIP_REQUEST:
      return {
        ...state,
        isBusy: true,
      };
    case ABORT_TRIP_REQUEST:
      return {
        ...state,
      };
    case RATING_REQUEST:
      return {
        ...state,
        isBusy: true,
      };
    case PANIC_REQUEST:
      return {
        ...state,
        isBusy: true,
      };
    case PREFER_DRIVER_REQUEST:
      return {
        ...state,
        isBusy: true,
      };
    case COUPON_REQUEST:
      return {
        ...state,
        isBusy: true,
      };
    case RATING_CANCEL_REQUEST:
      return {
        ...state,
        isBusy: true,
      };
    case ADD_TRIP_SUCCESS:
      return {
        ...state,
        isBusy: false,
        tripData: action.data,
      };
    case GET_TRIP_SUCCESS:
      return {
        ...state,
        isBusy: false,
        getTripData: action.data,
      };
    case CANCEL_TRIP_SUCCESS:
      return {
        ...state,
        isBusy: false,
        cancelTripData: action.data,
      };
    case ABORT_TRIP_SUCCESS:
      return {
        ...state,
        abortTripData: action.data,
      };
    case RATING_SUCCESS:
      return {
        ...state,
        isBusy: false,
        ratingData: action.data,
      };
    case PANIC_SUCCESS:
      return {
        ...state,
        isBusy: false,
        panicData: action.data,
      };
    case PREFER_DRIVER_SUCCESS:
      return {
        ...state,
        isBusy: false,
        preferDriverData: action.data,
      };
    case COUPON_SUCCESS:
      return {
        ...state,
        isBusy: false,
        couponData: action.data,
      };
    case RATING_CANCEL_SUCCESS:
      return {
        ...state,
        isBusy: false,
        cancelRatingData: action.data,
      };
    case ADD_TRIP_FAILURE:
      return {
        ...state,
        isBusy: false,
        tripData: null,
      };
    default:
      return state;
  }
};
