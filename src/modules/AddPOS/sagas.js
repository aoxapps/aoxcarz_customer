import Axios from 'axios';
import {AsyncStorage, Alert} from 'react-native';
import {takeEvery, put} from 'redux-saga/effects';
import {ADDPOS_REQUEST} from './types';
import {posFail, posSuccess} from './actions';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';
import {StackActions, NavigationActions} from 'react-navigation';
import {postAPI, get} from '../../utils/api';
import {getConfiguration, setConfiguration} from '../../utils/configuration';
import {PaymentMethodListSuccess} from '../GetPaymentMethodList/actions';
function* onPOSRequested({
  customerId,
  name,
  token,
  lastd,
  postalCode,
  navigation,
}) {
  yield* showLoader(false);
  try {
    let details = {
      customerId: customerId,
      type: 'Card',
      name: name,
      token: token,
      lastd: lastd,
      postalCode: postalCode,
      detials: 'Payment Details',
    };
    // console.log('data');
    const posData = yield postAPI(Config.userPOS, JSON.stringify(details));
    //console.log('data:   ' + JSON.stringify(posData));
    if (posData.data.status === SUCCESS) {
      const paymentMethodData = yield get(Config.paymentMethodList);
      if (paymentMethodData.data.status === SUCCESS) {
        yield put(PaymentMethodListSuccess(paymentMethodData.data.data));
      }
      yield put(posSuccess());
      yield* hideLoader(false, '');

      // setTimeout(() => {
      //   //navigation.navigate(Config.AccountDetailScreen);
      //   navigation.goBack();
      // }, 800);
    } else if (posData.data.message === Config.authMessage) {
      yield put(posFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield* hideLoader(false, '');
      yield put(posFail());
      yield showAlertWithDelay(JSON.stringify(loginData.data.message));
    }
  } catch (error) {
    // console.log(JSON.stringify(error));
    yield* hideLoader(false, '');
    yield put(posFail());
    yield showAlertWithDelay('Invalid card details');
  }
}

function* sagaAddPOS() {
  yield takeEvery(ADDPOS_REQUEST, onPOSRequested);
}
export default sagaAddPOS;
