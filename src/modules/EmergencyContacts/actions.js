import {
  CONTACT_ADD_REQUEST,
  CONTACT_DELETE_REQUEST,
  CONTACT_SUCCESS,
  CONTACT_FAILURE,
} from './types';

export const addContactRequest = (namedt, number, navigation) => ({
  type: CONTACT_ADD_REQUEST,
  namedt,
  number,
  navigation,
});

export const deleteContactRequest = (contactId, navigation) => ({
  type: CONTACT_DELETE_REQUEST,
  contactId,
  navigation,
});

export const contactSuccess = data => ({
  type: CONTACT_SUCCESS,
  data,
});
export const contactFail = () => ({
  type: CONTACT_FAILURE,
});
