import {takeEvery, put} from 'redux-saga/effects';
import {CONTACT_ADD_REQUEST, CONTACT_DELETE_REQUEST} from './types';
import {contactSuccess, contactFail} from './actions';
import {Alert} from 'react-native';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';

import {postAPI, get} from '../../utils/api';
import {contactListSuccess} from '../GetEmergencyContactList/actions';

function* onContactAddRequest({namedt, number, navigation}) {
  yield* showLoader(false);
  try {
    var details = {
      name: namedt,
      contactNumber: number,
    };
    const addEmergencyContactData = yield postAPI(
      Config.addEmergencyContact,
      JSON.stringify(details),
    );
    // console.log('data:   ' + JSON.stringify(addEmergencyContactData));
    if (addEmergencyContactData.data.status === SUCCESS) {
      const emergencyContactListData = yield get(Config.emergencyContactList);
      if (emergencyContactListData.data.status === SUCCESS) {
        yield put(contactListSuccess(emergencyContactListData.data.data));
      }
      yield put(contactSuccess());
      yield* hideLoader(false, '');
      setTimeout(() => {
        //navigation.navigate(Config.AccountDetailScreen);
        navigation.goBack();
      }, 600);
    } else if (addEmergencyContactData.data.message === Config.authMessage) {
      yield put(contactFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield* hideLoader(false, '');
      yield put(contactFail());
      //yield showAlertWithDelay(JSON.stringify(loginData.data.message));
    }
  } catch (error) {
    // console.log(JSON.stringify(error));
    yield* hideLoader(false, '');
    yield put(contactFail());
    yield showAlertWithDelay('Invalid contact details');
  }
}

function* onContactDeleteRequest({contactId, navigation}) {
  yield* showLoader(false);
  try {
    let details = {
      contactId: contactId,
    };
    const deleteEmergencyContactData = yield postAPI(
      Config.deleteEmergencyContact,
      JSON.stringify(details),
    );
    //console.log('data:   ' + JSON.stringify(deleteEmergencyContactData));
    if (deleteEmergencyContactData.data.status === SUCCESS) {
      const emergencyContactListData = yield get(Config.emergencyContactList);
      // console.log('data:   ' + JSON.stringify(emergencyContactListData));
      if (emergencyContactListData.data.status === SUCCESS) {
        yield put(contactListSuccess(emergencyContactListData.data.data));
      } else {
        yield put(contactListSuccess());
      }
      yield put(contactSuccess());
      yield* hideLoader(false, '');

      setTimeout(() => {
        //navigation.navigate(Config.AccountDetailScreen);
        navigation.goBack();
      }, 600);
    } else if (addEmergencyContactData.data.message === Config.authMessage) {
      yield put(contactFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield* hideLoader(false, '');
      yield put(contactFail());
      //yield showAlertWithDelay(JSON.stringify(loginData.data.message));
    }
  } catch (error) {
    //  console.log(JSON.stringify(error));
    yield* hideLoader(false, '');
    yield put(contactFail());
    yield showAlertWithDelay('Invalid contact details');
  }
}

function* sagaEmergencyContacts() {
  yield takeEvery(CONTACT_ADD_REQUEST, onContactAddRequest);
  yield takeEvery(CONTACT_DELETE_REQUEST, onContactDeleteRequest);
}
export default sagaEmergencyContacts;
