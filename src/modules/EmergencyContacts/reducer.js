import {
  CONTACT_ADD_REQUEST,
  CONTACT_DELETE_REQUEST,
  CONTACT_SUCCESS,
  CONTACT_FAILURE,
} from './types';

const INITIAL_STATE = {
  contactData: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CONTACT_ADD_REQUEST:
      return {
        ...state,
      };
    case CONTACT_DELETE_REQUEST:
      return {
        ...state,
      };
    case CONTACT_SUCCESS:
      return {
        ...state,
        contactData: action.data,
      };
    case CONTACT_FAILURE:
      return {
        ...state,
      };
    default:
      return state;
  }
};
