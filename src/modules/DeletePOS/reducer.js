import {DELETEPOS_SUCCESS, DELETEPOS_REQUEST, DELETEPOS_FAILURE} from './types';

const INITIAL_STATE = {
  posData: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case DELETEPOS_REQUEST:
      return {
        ...state,
      };
    case DELETEPOS_SUCCESS:
      return {
        ...state,
        posData: action.data,
      };
    case DELETEPOS_FAILURE:
      return {
        ...state,
      };
    default:
      return state;
  }
};
