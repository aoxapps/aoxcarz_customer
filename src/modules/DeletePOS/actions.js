import {DELETEPOS_SUCCESS, DELETEPOS_REQUEST, DELETEPOS_FAILURE} from './types';

export const posDeleteRequest = (posID, navigation) => ({
  type: DELETEPOS_REQUEST,
  posID,
  navigation,
});

export const posDeleteSuccess = data => ({
  type: DELETEPOS_SUCCESS,
  data,
});
export const posDeleteFail = () => ({
  type: DELETEPOS_FAILURE,
});
