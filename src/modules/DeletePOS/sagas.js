import Axios from 'axios';
import {AsyncStorage, Alert} from 'react-native';
import {takeEvery, put} from 'redux-saga/effects';
import {DELETEPOS_REQUEST} from './types';
import {posDeleteSuccess, posDeleteFail} from './actions';
import {
  PaymentMethodListSuccess,
  PaymentMethodListFail,
} from '../GetPaymentMethodList/actions';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';
import {postAPI, get} from '../../utils/api';
import {getConfiguration, setConfiguration} from '../../utils/configuration';

function* onDeletePOSRequested({posID, navigation}) {
  yield* showLoader(false);
  try {
    let details = {
      _id: posID,
    };
    const posData = yield postAPI(Config.deletePOS, JSON.stringify(details));
    // console.log('data:   ' + JSON.stringify(posData));
    // console.log('****************');
    if (posData.data.status === SUCCESS) {
      const paymentMethodData = yield get(Config.paymentMethodList);
      if (paymentMethodData.data.status === SUCCESS) {
        yield put(PaymentMethodListSuccess(paymentMethodData.data.data));
      }
      yield put(posDeleteSuccess());
      yield* hideLoader(false, '');
    } else if (posData.data.message === Config.authMessage) {
      yield put(posDeleteFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield* hideLoader(false, '');
      yield put(posDeleteFail());
      yield showAlertWithDelay(posData.data.message);
    }
  } catch (error) {
    // console.log(JSON.stringify(error));

    yield* hideLoader(false, '');
    yield put(posDeleteFail());
  }
}

function* sagaDeletePOS() {
  yield takeEvery(DELETEPOS_REQUEST, onDeletePOSRequested);
}
export default sagaDeletePOS;
