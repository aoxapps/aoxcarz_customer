import Axios from 'axios';
import {Alert} from 'react-native';
import {takeEvery, put, select} from 'redux-saga/effects';
import {
  PROFILEIM_REQUESTED,
  PROFILE_REQUESTED,
  BODYSHOT_REQUESTED,
  RBODYSHOT_REQUESTED,
} from './types';
import {
  profileImSuccess,
  profileSuccess,
  bodyShotSucess,
  removebodyShotSucess,
  apiEPFail,
} from './actions';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';
import {loginSuccess} from '../Password/actions';
import {StackActions, NavigationActions} from 'react-navigation';
import {postAPI} from '../../utils/api';

function showAlert(navigation, message) {
  setTimeout(() => {
    Alert.alert(
      'Updated!!',
      message,
      [
        {
          text: 'Okay',
          onPress: () => {
            navigation.goBack();
          },
        },
      ],
      {cancelable: false},
    );
  }, 600);
}

function* onProfileImageUpdate({profileData, navigation}) {
  const selectorData = yield select();
  var url = '',
    key = '',
    type = '';

  url = Config.URL + Config.profileimage;
  key = 'profileImage';
  type = 'image/jpeg';

  yield* showLoader(false);
  try {
    // console.log(JSON.stringify(profileData));
    const {_id, token} = selectorData.loginReducer.loginData;
    const config = {
      method: Config.apiTypePost,
      headers: {
        customerId: _id,
        token: token,
      },
      url,
      responseType: 'json',
      data: profileData,
    };

    const response = yield Axios(config);
    //console.log('data:   ' + JSON.stringify(response));
    if (response.data.status === SUCCESS) {
      yield put(loginSuccess(response.data.data));
      yield showAlertWithDelay(response.data.message);
      yield put(profileImSuccess(response.data));
      yield* hideLoader(false, '');
    } else if (response.data.message === Config.authMessage) {
      yield put(apiEPFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield put(apiEPFail());
      yield* hideLoader(false, '');
      yield showAlertWithDelay(response.data.message);
    }
  } catch (error) {
    //console.log(JSON.stringify(error));

    yield* hideLoader(false, '');
    yield put(apiEPFail());
  }
}

function* onProfileUpdate({profileData, navigation}) {
  //const selectorData = yield select();
  //var url = Config.URL + Config.updateprofile;

  yield* showLoader(false);
  try {
    // console.log(JSON.stringify(profileData));
    const response = yield postAPI(
      Config.updateprofile,
      JSON.stringify(profileData.profileData),
    );
    //  console.log('data:   ' + JSON.stringify(response));
    if (response.data.status === SUCCESS) {
      yield* hideLoader(false, '');
      yield put(profileSuccess(response.data));
      yield put(loginSuccess(response.data.data));
      if (profileData.isRegister) {
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: Config.SideMenu})],
        });
        navigation.dispatch(resetAction);
      } else {
        showAlert(navigation, response.data.message);
      }
    } else if (response.data.message === Config.authMessage) {
      yield put(apiEPFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield* hideLoader(false, '');
      yield put(apiEPFail());
      yield showAlertWithDelay(response.data.message);
    }
  } catch (error) {
    //console.log(JSON.stringify(error));

    yield* hideLoader(false, '');
    yield put(apiEPFail());
  }
}

function* sagaEditProfile() {
  yield takeEvery(PROFILEIM_REQUESTED, onProfileImageUpdate);
  yield takeEvery(PROFILE_REQUESTED, onProfileUpdate);
}
export default sagaEditProfile;
