import {
  MOBILE_SUCCESS,
  MOBILE_REQUESTED,
  LOGIN_WITH_SOCIAL_REQUEST,
  LOGIN_WITH_SOCIAL_SUCCESS,
  MOBILE_FAIL,
} from './types';

export const mobileRequest = (
  countryCode,
  mobileNumber,
  countryName,
  navigation,
) => ({
  type: MOBILE_REQUESTED,
  countryCode,
  mobileNumber,
  countryName,
  navigation,
});

export const loginWithSocialRequest = (data, navigation) => ({
  type: LOGIN_WITH_SOCIAL_REQUEST,
  data,
  navigation,
});

export const mobileSuccess = data => ({
  type: MOBILE_SUCCESS,
  data,
});
export const loginWithSocialSuccess = data => ({
  type: LOGIN_WITH_SOCIAL_SUCCESS,
  data,
});
export const mobileFail = () => ({
  type: MOBILE_FAIL,
});
