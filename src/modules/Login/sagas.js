import Axios from 'axios';
import {AsyncStorage} from 'react-native';
import {takeEvery, put} from 'redux-saga/effects';
import {MOBILE_REQUESTED, LOGIN_WITH_SOCIAL_REQUEST} from './types';
import {mobileFail, mobileSuccess, loginWithSocialSuccess} from './actions';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS, FAILURE} from '../../constants/Config';
import {postAPI} from '../../utils/api';

function* onMobileRequest({
  countryCode,
  mobileNumber,
  countryName,
  navigation,
}) {
  yield* showLoader(false);
  try {
    let details = {
      countryCode: countryCode,
      mobileNumber: mobileNumber,
    };

    const mobileData = yield postAPI(
      Config.verfiyMobile,
      JSON.stringify(details),
    );
    if (
      mobileData.data.exist === true &&
      mobileData.data.passwordStatus == true
    ) {
      yield put(mobileSuccess(mobileData.data));
      yield* hideLoader(false, '');
      navigation.navigate(Config.Password, {
        mobileNumber: mobileNumber,
        countryCode: countryCode,
      });
    } else if (mobileData.data.status === FAILURE) {
      yield put(mobileFail());
      yield* hideLoader(false, '');
      yield showAlertWithDelay(mobileData.data.message);
    } else if (
      mobileData.data.exist === true &&
      mobileData.data.passwordStatus == false
    ) {
      yield put(mobileFail());
      yield* hideLoader(false, '');
      navigation.navigate(Config.VerifyOTP, {
        otp: mobileData.data.data.OTP,
        mobileNumber,
        countryCode,
        countryName,
        isNew: 'no',
      });
    } else {
      yield put(mobileFail());
      yield* hideLoader(false, '');
      navigation.navigate(Config.VerifyOTP, {
        otp: mobileData.data.data.OTP,
        mobileNumber,
        countryCode,
        countryName,
        isNew: 'yes',
      });
    }
  } catch (error) {
    console.log(JSON.stringify(error));
    var aa = error;
    debugger;
    //console.log(JSON.stringify(error));
    yield put(mobileFail());
    yield* hideLoader(false, '');
  }
}

function* onloginWithSocialRequest({data, navigation}) {
  yield* showLoader(false);
  try {
    // let details = {
    //   fbid: data.fbid,
    //   gid: data.gid,
    //   aid: data.aid,
    // };
    const socialData = yield postAPI(
      Config.loginWithSocialURl,
      JSON.stringify(data),
    );
    //console.log('data:   ' + JSON.stringify(socialData.data));
    //  console.log('****************');
    if (socialData.data.status === SUCCESS) {
      yield put(loginWithSocialSuccess(socialData.data));
      yield* hideLoader(false, '');
    } else {
      yield put(mobileFail());
      yield* hideLoader(false, '');
    }
  } catch (error) {
    yield put(mobileFail());
    yield* hideLoader(false, '');
  }
}

function* sagaMobile() {
  yield takeEvery(MOBILE_REQUESTED, onMobileRequest);
  yield takeEvery(LOGIN_WITH_SOCIAL_REQUEST, onloginWithSocialRequest);
}
export default sagaMobile;
