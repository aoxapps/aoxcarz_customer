import {
  CONTACT_LIST_REQUEST,
  CONTACT_LIST_SUCCESS,
  CONTACT_LIST_FAILURE,
} from './types';

const INITIAL_STATE = {
  contactListData: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case CONTACT_LIST_REQUEST:
      return {
        ...state,
      };
    case CONTACT_LIST_SUCCESS:
      return {
        ...state,
        contactListData: action.data,
      };
    case CONTACT_LIST_FAILURE:
      return {
        ...state,
      };
    default:
      return state;
  }
};
