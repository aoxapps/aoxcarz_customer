import {
  CONTACT_LIST_REQUEST,
  CONTACT_LIST_SUCCESS,
  CONTACT_LIST_FAILURE,
} from './types';

export const contactListRequest = navigation => ({
  type: CONTACT_LIST_REQUEST,
  navigation,
});
export const contactListSuccess = data => ({
  type: CONTACT_LIST_SUCCESS,
  data,
});
export const contactListFail = () => ({
  type: CONTACT_LIST_FAILURE,
});
