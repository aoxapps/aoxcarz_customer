import Axios from 'axios';
import {AsyncStorage, Alert} from 'react-native';
import {takeEvery, put} from 'redux-saga/effects';
import {CONTACT_LIST_REQUEST} from './types';
import {contactListSuccess, contactListFail} from './actions';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';
import {get} from '../../utils/api';

function* onContactListRequested({navigation}) {
  yield* showLoader(false);
  try {
    const emergencyContactListData = yield get(Config.emergencyContactList);
    // console.log(
    //   '------emergencyContact List  data-----:   ' +
    //     JSON.stringify(emergencyContactListData),
    // );
    // console.log('****************');
    if (emergencyContactListData.data.status === SUCCESS) {
      yield put(contactListSuccess(emergencyContactListData.data.data));
      yield* hideLoader(false, '');
    } else if (emergencyContactListData.data.message === Config.authMessage) {
      yield put(contactListFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield put(contactListFail());
      yield* hideLoader(false, '');
      // yield showAlertWithDelay(
      //   JSON.stringify(emergencyContactListData.data.message),
      // );
    }
  } catch (error) {
    // console.log(JSON.stringify(error));
    yield* hideLoader(false, '');
    yield put(contactListFail());
  }
}

function* sagaContactList() {
  yield takeEvery(CONTACT_LIST_REQUEST, onContactListRequested);
}
export default sagaContactList;
