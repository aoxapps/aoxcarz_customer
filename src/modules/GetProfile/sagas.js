import Axios from 'axios';
import {AsyncStorage} from 'react-native';
import {takeEvery, put} from 'redux-saga/effects';
import {GETPROFILE_REQUEST} from './types';
import {profileSuccess, profileFail} from './actions';
import {StackActions, NavigationActions} from 'react-navigation';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';
import {get} from '../../utils/api';
import {loginSuccess} from '../Password/actions';
import {getConfiguration, setConfiguration} from '../../utils/configuration';

function* onProfileRequested({navigation}) {
  //const url = Config.URL + Config.profile;
  yield* showLoader(false);
  try {
    const profileData = yield get(Config.profile);
    //debugger;
    //console.log('data:   ' + JSON.stringify(profileData));
    //console.log('****************');
    if (profileData.data.status === SUCCESS) {
      yield put(loginSuccess(profileData.data.data));
      yield put(profileSuccess(profileData.data.data));
      yield* hideLoader(false, '');
      navigation.navigate(Config.SideMenu);
    } else {
      var token = '';
      yield setConfiguration('token', token);
      var user_id = '';
      yield setConfiguration('user_id', user_id);
      yield AsyncStorage.setItem('user_id', user_id);
      yield AsyncStorage.setItem('token', token);
      yield put(profileFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: Config.SideMenu})],
        });
        navigation.dispatch(resetAction);
      }, 600);
    }
  } catch (error) {
    var token = '';
    yield setConfiguration('token', token);
    var user_id = '';
    yield setConfiguration('user_id', user_id);
    yield AsyncStorage.setItem('user_id', user_id);
    yield AsyncStorage.setItem('token', token);
    // console.log(JSON.stringify(error));
    yield* hideLoader(false, '');
    setTimeout(() => {
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({routeName: Config.Login})],
      });
      navigation.dispatch(resetAction);
    }, 800);
    yield put(profileFail());
  }
}

function* sagaProfile() {
  yield takeEvery(GETPROFILE_REQUEST, onProfileRequested);
}
export default sagaProfile;
