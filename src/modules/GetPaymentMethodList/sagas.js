import Axios from 'axios';
import {AsyncStorage, Alert} from 'react-native';
import {takeEvery, put} from 'redux-saga/effects';
import {PAYMENTLIST_REQUEST} from './types';
import {PaymentMethodListSuccess, PaymentMethodListFail} from './actions';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';
import {get} from '../../utils/api';
import {getConfiguration, setConfiguration} from '../../utils/configuration';

function* onPaymentMethodListRequested({navigation}) {
  yield* showLoader(false);
  try {
    const paymentMethodData = yield get(Config.paymentMethodList);
    // console.log(
    //   '------Payment list data-----:   ' + JSON.stringify(paymentMethodData),
    // );
    // console.log('****************');
    if (paymentMethodData.data.status === SUCCESS) {
      yield put(PaymentMethodListSuccess(paymentMethodData.data.data));
      yield* hideLoader(false, '');
    } else if (paymentMethodData.data.message === Config.authMessage) {
      yield put(PaymentMethodListFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield put(PaymentMethodListFail());
      yield* hideLoader(false, '');
      yield showAlertWithDelay(JSON.stringify(paymentMethodData.data.message));
    }
  } catch (error) {
    //console.log(JSON.stringify(error));
    yield* hideLoader(false, '');
    yield put(PaymentMethodListFail());
  }
}

function* sagaPaymentMethodList() {
  yield takeEvery(PAYMENTLIST_REQUEST, onPaymentMethodListRequested);
}
export default sagaPaymentMethodList;
