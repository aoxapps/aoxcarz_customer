import Axios from 'axios';
import {takeEvery, put} from 'redux-saga/effects';
import {REQUEST_RESET} from './types';
import {resetSuccess, resetFail} from './actions';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';

function* onResetRequested({countryCode, mobileNumber}) {
  const url = Config.URL + Config.resendotp;
  yield* showLoader(false);
  try {
    const config = {
      method: Config.kMethodPostKey,
      headers: Config.kHeaderValues,
      url,
      responseType: 'json',
      data: JSON.stringify({
        countryCode,
        mobileNumber,
      }),
    };

    const mobileData = yield Axios(config);

    if (mobileData.data.status === SUCCESS) {
      yield put(resetSuccess(mobileData.data));
      yield* hideLoader(false, '');
      // console.log('SUCESS');
      // yield showAlertWithDelay(`OTP is ${mobileData.data.data.OTP}`);
    } else {
      yield* hideLoader(false, '');
      yield put(resetFail());
    }
  } catch (error) {
    console.log(JSON.stringify(error));

    yield* hideLoader(false, '');
    yield put(resetFail());
  }
}

function* sagaVerifyOtp() {
  yield takeEvery(REQUEST_RESET, onResetRequested);
}
export default sagaVerifyOtp;
