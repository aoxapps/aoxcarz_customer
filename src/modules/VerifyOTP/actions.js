import { RESET_SUCCESS, REQUEST_RESET, RESET_FAIL } from './types';

export const resetRequest = (countryCode, mobileNumber) => ({
    type: REQUEST_RESET,
    countryCode,
    mobileNumber,
    
});

export const resetSuccess = data => (
    {
        type: RESET_SUCCESS,
        data
    }
);
export const resetFail = () => (
    {
        type: RESET_FAIL
    }
);
