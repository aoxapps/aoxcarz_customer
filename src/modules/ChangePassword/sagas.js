import Axios from 'axios';
import {AsyncStorage, Alert} from 'react-native';
import {takeEvery, put, select} from 'redux-saga/effects';
import {PASSRESET_REQUESTED, PASSCHANGE_REQUESTED} from './types';
import {apiFail, apiSuccess} from './actions';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';
import {StackActions, NavigationActions} from 'react-navigation';
import {postAPI} from '../../utils/api';

function* onResetRequested({password, navigation}) {
  yield* showLoader(false);
  try {
    let details = {
      password,
      confirmPassword: password,
      mobileNumber: navigation.getParam('mobileNumber'),
      OTP: navigation.getParam('otp'),
    };
    const loginData = yield postAPI(
      Config.resetpassword,
      JSON.stringify(details),
    );

    // console.log('data:   ' + JSON.stringify(loginData.data));
    if (loginData.data.status === SUCCESS) {
      yield* hideLoader(false, '');
      yield put(apiSuccess(loginData.data));

      setTimeout(() => {
        const resetAction = StackActions.reset({
          index: 0,
          actions: [NavigationActions.navigate({routeName: Config.Login})],
        });
        navigation.dispatch(resetAction);
      }, 800);
    } else {
      yield* hideLoader(false, '');
      yield put(apiFail());
      // eslint-disable-next-line no-undef
      yield showAlertWithDelay(JSON.stringify(loginData.data.message));
    }
  } catch (error) {
    // console.log(JSON.stringify(error));
    yield* hideLoader(false, '');
    yield put(apiFail());
    yield showAlertWithDelay('Invalid Mobile Number/Password');
  }
}
function* onChangeRequested({password, currentPassword, navigation}) {
  yield* showLoader(false);
  try {
    let details = {
      password: password,
      confirmPassword: password,
      currentPassword: currentPassword,
    };
    const loginData = yield postAPI(
      Config.changepassword,
      JSON.stringify(details),
    );
    // console.log('data:   ' + JSON.stringify(loginData.data));
    if (loginData.data.status === SUCCESS) {
      yield* hideLoader(false, '');
      yield put(apiSuccess(loginData.data));

      setTimeout(() => {
        navigation.goBack();
      }, 800);
      //yield showAlertWithDelay(JSON.stringify(loginData.data.message));
    } else if (loginData.data.message === Config.authMessage) {
      yield put(apiFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield* hideLoader(false, '');
      yield put(apiFail());
      // eslint-disable-next-line no-undef
      yield showAlertWithDelay(loginData.data.message);
    }
  } catch (error) {
    // console.log(JSON.stringify(error));

    yield* hideLoader(false, '');
    yield put(apiFail());
    yield showAlertWithDelay('Something went wrong!!');
  }
}

function* sagaChangeP() {
  yield takeEvery(PASSRESET_REQUESTED, onResetRequested);
  yield takeEvery(PASSCHANGE_REQUESTED, onChangeRequested);
}
export default sagaChangeP;
