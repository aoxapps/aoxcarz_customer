import { PASSRESET_REQUESTED,PASSCHANGE_REQUESTED, PASSRESET_SUCCESS, PASSRESET_FAIL } from './types';

export const resetPRequest = (password, navigation) => ({
    type: PASSRESET_REQUESTED,
    password,
    navigation,
});
export const changeRequest = (password, currentPassword, navigation) => ({
    type: PASSCHANGE_REQUESTED,
    password,
    currentPassword,
    navigation,
});

export const apiSuccess = data => (
    {
        type: PASSRESET_SUCCESS,
        data
    }
);
export const apiFail = () => (
    {
        type: PASSRESET_FAIL
    }
);
