import {
  ESTIMATE_COST_REQUEST,
  ESTIMATE_COST_SUCCESS,
  ESTIMATE_COST_FAILURE,
} from './types';

export const estimateRequest = (sourceLoc, destLoc, promoCode, navigation) => ({
  type: ESTIMATE_COST_REQUEST,
  sourceLoc,
  destLoc,
  promoCode,
  navigation,
});

export const estimateSuccess = data => ({
  type: ESTIMATE_COST_SUCCESS,
  data,
});
export const estimateFail = () => ({
  type: ESTIMATE_COST_FAILURE,
});
