import {
  ESTIMATE_COST_REQUEST,
  ESTIMATE_COST_SUCCESS,
  ESTIMATE_COST_FAILURE,
} from './types';

const INITIAL_STATE = {
  estimateData: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ESTIMATE_COST_REQUEST:
      return {
        ...state,
      };
    case ESTIMATE_COST_SUCCESS:
      return {
        ...state,
        estimateData: action.data,
      };
    case ESTIMATE_COST_FAILURE:
      return {
        ...state,
      };
    default:
      return state;
  }
};
