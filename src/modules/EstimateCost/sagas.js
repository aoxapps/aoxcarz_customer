import Axios from 'axios';
import {AsyncStorage} from 'react-native';
import {takeEvery, put} from 'redux-saga/effects';
import {ESTIMATE_COST_REQUEST} from './types';
import {estimateFail, estimateSuccess} from './actions';
import {Alert} from 'react-native';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';
import {postAPI, get} from '../../utils/api';

function* onEstimateRequested({sourceLoc, destLoc, promoCode, navigation}) {
  yield* showLoader(false);
  try {
    let details = {
      source: sourceLoc,
      destination: destLoc,
      promoCode: promoCode,
    };

    const estimateData = yield postAPI(
      Config.estimatedCost,
      JSON.stringify(details),
    );

    // console.log(
    //   'Parameters Estimation cost:   ' + JSON.stringify(estimateData),
    // );
    if (estimateData.data.status === SUCCESS) {
      yield put(estimateSuccess(estimateData.data.data));
      yield* hideLoader(false, '');
    } else if (estimateData.data.message === Config.authMessage) {
      yield put(estimateFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield put(estimateFail());
      yield* hideLoader(false, '');
      //yield showAlertWithDelay(JSON.stringify(stimateData.data.message));
    }
  } catch (error) {
    // console.log(JSON.stringify(error));
    yield* hideLoader(false, '');
    yield put(estimateFail());
    //yield showAlertWithDelay('Unable to get estimated cost');
  }
}

function* sagaEstimateCost() {
  yield takeEvery(ESTIMATE_COST_REQUEST, onEstimateRequested);
}
export default sagaEstimateCost;
