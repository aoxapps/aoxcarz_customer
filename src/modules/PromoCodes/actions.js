import {PROMO_LIST_REQUEST, PROMO_LIST_SUCCESS, PROMO_FAILURE} from './types';

export const promoListRequest = navigation => ({
  type: PROMO_LIST_REQUEST,
  navigation,
});

export const promoListSuccess = data => ({
  type: PROMO_LIST_SUCCESS,
  data,
});
export const promoFail = () => ({
  type: PROMO_FAILURE,
});
