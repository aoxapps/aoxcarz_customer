import Axios from 'axios';
import {AsyncStorage, Alert} from 'react-native';
import {takeEvery, put} from 'redux-saga/effects';
import {PROMO_LIST_REQUEST} from './types';
import {promoListSuccess, promoFail} from './actions';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';
import {StackActions, NavigationActions} from 'react-navigation';
import {postAPI, get} from '../../utils/api';
import {getConfiguration, setConfiguration} from '../../utils/configuration';
import {PaymentMethodListSuccess} from '../GetPaymentMethodList/actions';
function* onPromoListRequest({navigation}) {
  yield* showLoader(false);
  try {
    const responseData = yield get(Config.allPromosURL);
    // console.log('data:   ' + JSON.stringify(responseData));
    if (responseData.data.status === SUCCESS) {
      yield put(promoListSuccess(responseData.data.data));
      yield* hideLoader(false, '');
    } else if (responseData.data.message === Config.authMessage) {
      yield put(promoFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield* hideLoader(false, '');
      yield put(promoFail());
      yield showAlertWithDelay(responseData.data.message);
    }
  } catch (error) {
    // console.log(JSON.stringify(error));
    yield* hideLoader(false, '');
    yield put(promoFail());
    //yield showAlertWithDelay('Invalid  details');
  }
}

function* sagaPromoCodes() {
  yield takeEvery(PROMO_LIST_REQUEST, onPromoListRequest);
}
export default sagaPromoCodes;
