import {MY_TRIPS_REQUEST, MY_TRIPS_SUCCESS, MY_TRIPS_FAIL} from './types';

export const myTripsRequest = navigation => ({
  type: MY_TRIPS_REQUEST,
  navigation,
});

export const myTripsSuccess = data => ({
  type: MY_TRIPS_SUCCESS,
  data,
});
export const myTripsFail = () => ({
  type: MY_TRIPS_FAIL,
});
