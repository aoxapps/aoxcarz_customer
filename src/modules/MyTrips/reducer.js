import {MY_TRIPS_REQUEST, MY_TRIPS_SUCCESS, MY_TRIPS_FAIL} from './types';

const INITIAL_STATE = {
  data: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case MY_TRIPS_REQUEST:
      return {
        ...state,
      };
    case MY_TRIPS_SUCCESS:
      return {
        ...state,
        data: action.data,
      };
    case MY_TRIPS_FAIL:
      return {
        ...state,
      };
    default:
      return state;
  }
};
