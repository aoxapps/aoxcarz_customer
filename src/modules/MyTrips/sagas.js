import {takeEvery, put} from 'redux-saga/effects';
import {AsyncStorage, Alert} from 'react-native';
import {MY_TRIPS_REQUEST} from './types';
import {myTripsSuccess, myTripsFail} from './actions';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';
import {postAPI, get} from '../../utils/api';

function* onTripsRequest({navigation}) {
  yield* showLoader(false);
  try {
    const responseData = yield get(Config.allTripsURL);
    // console.log(responseData);
    if (responseData.data.status === SUCCESS) {
      yield put(myTripsSuccess(responseData.data.data));
      yield* hideLoader(false, '');
    } else if (responseData.data.message === Config.authMessage) {
      yield put(myTripsFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield put(myTripsFail());
      yield* hideLoader(false, '');
    }
  } catch (error) {
    //  console.log(JSON.stringify(error));
    yield put(myTripsFail());
    yield* hideLoader(false, '');
  }
}

function* sagaMyTrips() {
  yield takeEvery(MY_TRIPS_REQUEST, onTripsRequest);
}
export default sagaMyTrips;
