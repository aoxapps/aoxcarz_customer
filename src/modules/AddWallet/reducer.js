import {
  ADD_WALLET_BALANCE_REQUEST,
  ADD_WALLET_BALANCE_SUCCESS,
  ADD_WALLET_BALANCE_FAILURE,
} from './types';

const INITIAL_STATE = {
  addWalletBalanceData: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_WALLET_BALANCE_REQUEST:
      return {
        ...state,
        isBusy: true,
        // addWalletBalanceData: null,
      };
    case ADD_WALLET_BALANCE_SUCCESS:
      return {
        ...state,
        isBusy: false,
        addWalletBalanceData: action.data,
      };
    case ADD_WALLET_BALANCE_FAILURE:
      return {
        ...state,
        isBusy: false,
        //  addWalletBalanceData: null,
      };
    default:
      return state;
  }
};
