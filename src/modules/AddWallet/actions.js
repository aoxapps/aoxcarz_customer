import {
  ADD_WALLET_BALANCE_REQUEST,
  ADD_WALLET_BALANCE_SUCCESS,
  ADD_WALLET_BALANCE_FAILURE,
} from './types';

export const addWalletBalanceRequest = (count, item_id, navigation) => ({
  type: ADD_WALLET_BALANCE_REQUEST,
  count,
  item_id,
  navigation,
});

export const addWalletBalanceSuccess = data => ({
  type: ADD_WALLET_BALANCE_SUCCESS,
  data,
});
export const addWalletBalanceFail = () => ({
  type: ADD_WALLET_BALANCE_FAILURE,
});
