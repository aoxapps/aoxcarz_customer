import Axios from 'axios';
import {AsyncStorage, Alert} from 'react-native';
import {takeEvery, put} from 'redux-saga/effects';
import {ADD_WALLET_BALANCE_REQUEST} from './types';
import {addWalletBalanceFail, addWalletBalanceSuccess} from './actions';
import {profileSuccess} from '../GetProfile/actions';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';
import {postAPI, get} from '../../utils/api';

function* onAddWalletBalanceRequested({count, item_id, navigation}) {
  yield* showLoader(false);
  try {
    let details = {
      walletCredit: count,
      paymentSourceRefNo: item_id,
    };
    const addWalletBalanceData = yield postAPI(
      Config.addMoney,
      JSON.stringify(details),
    );
    // console.log(
    //   'Wallet Balance Data:   ' + JSON.stringify(addWalletBalanceData),
    // );
    if (addWalletBalanceData.data.status === SUCCESS) {
      const profileData = yield get(Config.profile);
      if (profileData.data.status === SUCCESS) {
        yield put(profileSuccess(profileData.data.data));
      }
      yield put(addWalletBalanceSuccess(addWalletBalanceData.data));
      yield* hideLoader(false, '');
    } else if (addWalletBalanceData.data.message === Config.authMessage) {
      yield put(addWalletBalanceFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield* hideLoader(false, '');
      yield put(addWalletBalanceFail());
      //yield showAlertWithDelay(JSON.stringify(stimateData.data.message));
    }
  } catch (error) {
    //console.log(JSON.stringify(error));
    yield* hideLoader(false, '');
    yield put(addWalletBalanceFail());
    //yield showAlertWithDelay('Unable to get estimated cost');
  }
}

function* sagaAddWalletBalance() {
  yield takeEvery(ADD_WALLET_BALANCE_REQUEST, onAddWalletBalanceRequested);
}
export default sagaAddWalletBalance;
