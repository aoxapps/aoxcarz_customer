import Axios from 'axios';
import {AsyncStorage} from 'react-native';
import {takeEvery, put} from 'redux-saga/effects';
import {LOGIN_REQUESTED} from './types';
import {loginFail, loginSuccess} from './actions';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';
import {StackActions, NavigationActions} from 'react-navigation';

import {postAPI, get} from '../../utils/api';
import {getConfiguration, setConfiguration} from '../../utils/configuration';
import {PaymentMethodListSuccess} from '../GetPaymentMethodList/actions';
import {profileSuccess} from '../GetProfile/actions';
import {contactListSuccess} from '../GetEmergencyContactList/actions';
function* onLoginRequested({
  countryCode,
  mobileNumber,
  password,
  fcmToken,
  navigation,
}) {
  yield* showLoader(false);
  try {
    let details = {
      //countryCode: countryCode,
      mobileNumber: mobileNumber,
      password: password,
      firebaseToken: fcmToken,
    };

    const loginData = yield postAPI(
      Config.loginMobile,
      JSON.stringify(details),
    );

    //console.log('data:   ' + JSON.stringify(loginData.data));
    if (loginData.data.status === SUCCESS) {
      var token = loginData.data.data.token;
      yield setConfiguration('token', token);
      var user_id = loginData.data.data._id;
      yield setConfiguration('user_id', user_id);

      yield AsyncStorage.setItem('user_id', user_id);
      yield AsyncStorage.setItem('token', token);
      yield put(profileSuccess(loginData.data.data));
      const paymentMethodData = yield get(Config.paymentMethodList);
      if (paymentMethodData.data.status === SUCCESS) {
        yield put(PaymentMethodListSuccess(paymentMethodData.data.data));
      }
      const emergencyContactListData = yield get(Config.emergencyContactList);
      if (emergencyContactListData.data.status === SUCCESS) {
        yield put(contactListSuccess(emergencyContactListData.data.data));
      }
      yield put(loginSuccess(loginData.data.data));
      yield* hideLoader(false, '');
      //navigation.navigate(Config.SideMenu);
      // setTimeout(() => {
      //   const resetAction = StackActions.reset({
      //     index: 0,
      //     actions: [NavigationActions.navigate({routeName: Config.SideMenu})],
      //   });
      //   navigation.dispatch(resetAction);
      // }, 100);

      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({routeName: Config.SideMenu})],
      });
      navigation.dispatch(resetAction);
    } else {
      yield put(loginFail());
      yield* hideLoader(false, '');
      // eslint-disable-next-line no-undef
      yield showAlertWithDelay(loginData.data.message);
    }
  } catch (error) {
    var aa = error;
    // console.log(JSON.stringify(error));
    yield put(loginFail());
    yield* hideLoader(false, '');
    yield showAlertWithDelay('Invalid Mobile Number/Password');
  }
}

function* sagaLogin() {
  yield takeEvery(LOGIN_REQUESTED, onLoginRequested);
}
export default sagaLogin;
