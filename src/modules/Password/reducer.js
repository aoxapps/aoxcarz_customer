import {LOGIN_REQUESTED, REQUEST_SUCCESS, REQUEST_FAIL} from './types';

const INITIAL_STATE = {
  loginData: null,
  isBusy: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN_REQUESTED:
      return {
        ...state,
        isBusy: true,
        loginData: null,
      };
    case REQUEST_SUCCESS:
      return {
        ...state,
        isBusy: false,
        loginData: action.data,
      };
    case REQUEST_FAIL:
      return {
        ...state,
        isBusy: false,
        loginData: null,
      };
    default:
      return state;
  }
};
