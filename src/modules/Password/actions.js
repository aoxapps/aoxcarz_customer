import {LOGIN_REQUESTED, REQUEST_SUCCESS, REQUEST_FAIL} from './types';

export const loginRequest = (
  countryCode,
  mobileNumber,
  password,
  fcmToken,
  navigation,
) => ({
  type: LOGIN_REQUESTED,
  countryCode,
  mobileNumber,
  password,
  fcmToken,
  navigation,
});

export const loginSuccess = data => ({
  type: REQUEST_SUCCESS,
  data,
});
export const loginFail = () => ({
  type: REQUEST_FAIL,
});
