import Axios from 'axios';
import {AsyncStorage} from 'react-native';
import {takeEvery, put, call} from 'redux-saga/effects';
import {REGISTER} from './types';
import {registerFail, registerSuccess} from './actions';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';
import {NavigationActions, StackActions} from 'react-navigation';
import {loginSuccess} from '../Password/actions';
import {getConfiguration, setConfiguration} from '../../utils/configuration';
import {postAPI, get} from '../../utils/api';
import {PaymentMethodListSuccess} from '../GetPaymentMethodList/actions';
import {profileSuccess} from '../GetProfile/actions';
function* onRegisterRequested({data, navigation}) {
  yield* showLoader(false);
  try {
    const response = yield postAPI(Config.register, JSON.stringify(data))
      .then(response => {
        return response.data;
      })
      .catch(error => {
        //  console.log(error);
      });
    // console.log('data:   ' + JSON.stringify(response.data));
    if (response.status == SUCCESS) {
      var token = response.data.token;
      var user_id = response.data._id;
      yield setConfiguration('token', token);
      yield setConfiguration('user_id', user_id);
      yield put(loginSuccess(response.data));
      yield put(profileSuccess(response.data));
      const paymentMethodData = yield get(Config.paymentMethodList);
      // console.log('data:   ' + JSON.stringify(paymentMethodData));
      if (paymentMethodData.data.status === SUCCESS) {
        yield put(PaymentMethodListSuccess(paymentMethodData.data.data));
      }
      yield put(registerSuccess(response.data));
      yield* hideLoader(false, '');
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({routeName: Config.SideMenu})],
      });
      navigation.dispatch(resetAction);
    } else {
      yield* hideLoader(false, '');
      yield put(registerFail());
      yield showAlertWithDelay(response.message);
    }
  } catch (error) {
    //console.log(JSON.stringify(error));
    yield* hideLoader(false, '');
    yield put(registerFail());
  }
}

function* sagaRegister() {
  yield takeEvery(REGISTER, onRegisterRequested);
}
export default sagaRegister;
