import {PREFERRED_REQUEST, PREFERRED_SUCCESS, PREFERRED_FAIL} from './types';

const INITIAL_STATE = {
  data: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PREFERRED_REQUEST:
      return {
        ...state,
      };
    case PREFERRED_SUCCESS:
      return {
        ...state,
        data: action.data,
      };
    case PREFERRED_FAIL:
      return {
        ...state,
      };
    default:
      return state;
  }
};
