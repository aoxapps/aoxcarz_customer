import {PREFERRED_REQUEST, PREFERRED_SUCCESS, PREFERRED_FAIL} from './types';

export const preferredRequest = navigation => ({
  type: PREFERRED_REQUEST,
  navigation,
});

export const preferredSuccess = data => ({
  type: PREFERRED_SUCCESS,
  data,
});
export const preferredFail = () => ({
  type: PREFERRED_FAIL,
});
