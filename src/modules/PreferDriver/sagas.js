import {takeEvery, put} from 'redux-saga/effects';
import {AsyncStorage, Alert} from 'react-native';
import {PREFERRED_REQUEST} from './types';
import {preferredSuccess, preferredFail} from './actions';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';
import {postAPI, get} from '../../utils/api';

function* onPreferredRequest({navigation}) {
  yield* showLoader(false);
  try {
    const responseData = yield get(Config.preferDriverListURL);
    //  console.log(responseData);
    if (responseData.data.status === SUCCESS) {
      yield put(preferredSuccess(responseData.data.data));
      yield* hideLoader(false, '');
    } else if (responseData.data.message === Config.authMessage) {
      yield put(preferredFail());
      yield* hideLoader(false, '');
      setTimeout(() => {
        Alert.alert(
          'Alert',
          Config.authErrorMessage,
          [
            {
              text: 'Ok',
              onPress: () => {
                navigation.navigate(Config.Login);
              },
            },
          ],
          {cancelable: false},
        );
      }, 600);
    } else {
      yield put(preferredFail());
      yield* hideLoader(false, '');
    }
  } catch (error) {
    console.log(JSON.stringify(error));
    yield put(preferredFail());
    yield* hideLoader(false, '');
  }
}

function* sagaPreferredDriver() {
  yield takeEvery(PREFERRED_REQUEST, onPreferredRequest);
}
export default sagaPreferredDriver;
