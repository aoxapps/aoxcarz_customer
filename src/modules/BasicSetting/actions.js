import {SETTING_SUCCESS, SETTING_REQUEST, SETTING_FAILURE} from './types';

export const settingRequest = navigation => ({
  type: SETTING_REQUEST,
  navigation,
});

export const settingSuccess = data => ({
  type: SETTING_SUCCESS,
  data,
});
export const settingFail = () => ({
  type: SETTING_FAILURE,
});
