import Axios from 'axios';
import {AsyncStorage} from 'react-native';
import {takeEvery, put} from 'redux-saga/effects';
import {SETTING_REQUEST} from './types';
import {settingSuccess, settingFail} from './actions';
import {StackActions, NavigationActions} from 'react-navigation';
import {
  showLoader,
  hideLoader,
  showAlertWithDelay,
} from '../../utils/CommonFunctions';
import Config, {SUCCESS} from '../../constants/Config';
import {get} from '../../utils/api';
import {loginSuccess} from '../Login/actions';
import {getConfiguration, setConfiguration} from '../../utils/configuration';

function* onSettingRequest({navigation}) {
  yield* showLoader(false);
  try {
    const settingData = yield get(Config.basicSettingURL);
    // console.log('data:   ' + JSON.stringify(settingData));
    //  console.log('****************');
    // debugger;
    if (settingData.data.status === SUCCESS) {
      yield put(settingSuccess(settingData.data));
      yield* hideLoader(false, '');
    } else {
    }
  } catch (error) {
    // console.log(JSON.stringify(error));
    yield* hideLoader(false, '');
    yield put(settingFail());
  }
}

function* sagaBasicSetting() {
  yield takeEvery(SETTING_REQUEST, onSettingRequest);
}
export default sagaBasicSetting;
