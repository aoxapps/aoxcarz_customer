import {SETTING_SUCCESS, SETTING_REQUEST, SETTING_FAILURE} from './types';

const INITIAL_STATE = {
  settingData: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SETTING_REQUEST:
      return {
        ...state,
      };
    case SETTING_SUCCESS:
      return {
        ...state,
        settingData: action.data,
      };
    case SETTING_FAILURE:
      return {
        ...state,
      };
    default:
      return state;
  }
};
