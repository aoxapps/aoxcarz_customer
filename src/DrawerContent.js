import React from 'react';
import {NavigationActions} from 'react-navigation';
import {
  Text,
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  ScrollView,
  Button,
  ImageBackground,
} from 'react-native';
import {setConfiguration} from './utils/configuration';
import {DrawerActions} from 'react-navigation-drawer';
import {AsyncStorage} from 'react-native';

import Images from './constants/Images';
import Fonts from './constants/Fonts';
import Colors from './constants/Colors';
import Config from './constants/Config';
import {connect} from 'react-redux';
import {logoutRequest} from './redux/reducers';
import {strings} from './constants/languagesString';
import {moderateScale, scale} from 'react-native-size-matters';
import {RFValue} from 'react-native-responsive-fontsize';

class DrawerContent extends React.PureComponent {
  constructor(props) {
    super(props);
  }
  navigateToScreen = route => async () => {
    if (route == Config.Login) {
      setConfiguration('user_id', '');
      setConfiguration('fcmToken', '');
      setConfiguration('token', '');
      try {
        await AsyncStorage.setItem('user_id', '');
        await AsyncStorage.setItem('fcmToken', '');
        await AsyncStorage.setItem('token', '');
      } catch (e) {
        // saving error
      }
    }

    const navigateAction = NavigationActions.navigate({
      routeName: route,
    });
    this.props.navigation.dispatch(DrawerActions.closeDrawer());
    this.props.navigation.dispatch(navigateAction);
  };
  componentDidMount() {}

  render() {
    var {name, profileImage, customerAvgRating, mobileNumber, countryCode} = '';
    if (this.props.loginData != null && this.props.loginData != '') {
      name = this.props.loginData.name;
      profileImage = this.props.loginData.profileImage;
      customerAvgRating = this.props.loginData.customerAvgRating;
      mobileNumber = this.props.loginData.mobileNumber;
      countryCode = this.props.loginData.countryCode;
      if (countryCode != null && countryCode != '') {
        countryCode = this.props.loginData.countryCode + '-';
      } else {
        countryCode = '';
      }
    }
    return (
      <View style={styles.container}>
        <View
          style={{
            height: 180,
            backgroundColor: '#0082cb',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            resizeMode="cover"
            style={{
              width: '100%',
              height: '100%',
              position: 'absolute',
              top: 0,
              left: 0,
            }}
            source={Images.sidemenuBG}
          />

          <View
            style={{
              height: 'auto',
              overflow: 'hidden',
              width: '100%',
              marginTop: 20,
              backgroundColor: 'transparent',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View style={{alignItems: 'center', marginTop: 10}}>
              <View
                style={{
                  height: 70,
                  overflow: 'hidden',
                  width: 70,
                  borderWidth: 1,
                  borderColor: Colors.White,
                  borderRadius: 35,
                  marginLeft: 30,
                  right: 20,
                  backgroundColor: 'transparent',
                }}>
                {profileImage == '' ||
                profileImage == 'null' ||
                profileImage == null ||
                profileImage == 'none' ? (
                  <Image
                    resizeMode="cover"
                    style={{width: '100%', height: '100%'}}
                    source={Images.drawerProfileBG}
                  />
                ) : (
                  <Image
                    resizeMode="cover"
                    style={{width: '100%', height: '100%'}}
                    source={{
                      uri: profileImage,
                    }}
                  />
                )}
              </View>
              <View style={{alignItems: 'center'}}>
                <Text style={{color: '#ffffff', fontWeight: 'bold'}}>
                  {name}
                </Text>
                <Text style={{color: '#ffffff', fontWeight: 'bold'}}>
                  {countryCode + mobileNumber}
                </Text>
              </View>
            </View>

            <View
              style={{
                justifyContent: 'space-around',
                flexDirection: 'row',
                paddingBottom: 20,
              }}>
              <Text
                style={{
                  color: 'white',
                  fontWeight: 'bold',
                  fontSize: 16,
                }}>
                {customerAvgRating == 0 ||
                customerAvgRating == '' ||
                customerAvgRating == null ||
                typeof customerAvgRating == undefined
                  ? '0.0'
                  : customerAvgRating.toString().length == 1
                  ? customerAvgRating + '.0'
                  : customerAvgRating}
              </Text>
              <View
                style={{
                  width: '37%',
                  height: '37%',
                  right: 12,
                }}>
                <Image
                  resizeMode="contain"
                  style={{
                    width: '50%',
                    height: '50%',
                    right: 10,
                  }}
                  source={Images.starImage}
                />
              </View>
            </View>
          </View>
        </View>

        <View style={styles.screenContainer}>
          <ScrollView style={{width: '100%', height: '100%'}}>
            <TouchableOpacity
              style={styles.tile}
              onPress={() =>
                this.props.navigation.navigate(Config.MapSearchScreen, {
                  driverId: '',
                  prefDriverRide: 'no',
                })
              }>
              <Image
                resizeMode="contain"
                style={styles.tileIcon}
                source={Images.rideIcon}
              />
              <Text style={styles.tileTitle}> {strings.Rides} </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.tile}
              onPress={this.navigateToScreen(Config.PreferredScreen)}>
              <Image
                resizeMode="contain"
                style={styles.tileIcon}
                source={Images.preferedIcon}
              />
              <Text style={styles.tileTitle}> {strings.PreferredDrivers} </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.tile}
              onPress={this.navigateToScreen(Config.MyTripScreen)}>
              <Image
                resizeMode="contain"
                style={styles.tileIcon}
                source={Images.yourTripIcon}
              />
              <Text style={styles.tileTitle}> {strings.YourTrips} </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.tile}
              onPress={this.navigateToScreen(Config.Invite)}>
              <Image
                resizeMode="contain"
                style={styles.tileIcon}
                source={Images.shareIcon}
              />
              <Text style={styles.tileTitle}> {strings.InviteFriend} </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.tile}
              onPress={this.navigateToScreen(Config.CouponScreen)}>
              <Image
                resizeMode="contain"
                style={styles.tileIcon}
                source={Images.promotionIcon}
              />
              <Text style={styles.tileTitle}> {strings.Promotions} </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.tile}
              onPress={this.navigateToScreen(Config.AccountDetailScreen)}>
              <Image
                resizeMode="contain"
                style={styles.tileIcon}
                source={Images.paymentMethodIcon}
              />
              <Text style={styles.tileTitle}>{strings.PaymentMethod}</Text>
            </TouchableOpacity>

            {/* <TouchableOpacity
              style={styles.tile}
              onPress={this.navigateToScreen(Config.WalletScreen)}>
              <Image
                resizeMode="contain"
                style={styles.tileIcon}
                source={Images.walletIcon}
              />
              <Text style={styles.tileTitle}>{strings.Wallet}</Text>
            </TouchableOpacity> */}

            <TouchableOpacity
              style={styles.tile}
              onPress={this.navigateToScreen(Config.Emergency)}>
              <Image
                resizeMode="contain"
                style={styles.tileIcon}
                source={Images.emergencyIcon}
              />
              <Text style={styles.tileTitle}>
                {' '}
                {strings.EmergencyContacts}{' '}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.tile}
              onPress={this.navigateToScreen(Config.ProfileScreen)}>
              <Image
                resizeMode="contain"
                style={styles.tileIcon}
                source={Images.profileIcon}
              />
              <Text style={styles.tileTitle}> {strings.Profile} </Text>
            </TouchableOpacity>

            {/* <TouchableOpacity
              style={styles.tile}
              onPress={this.navigateToScreen(Config.About)}>
              <Image
                resizeMode="contain"
                style={styles.tileIcon}
                source={Images.about_usIcon}
              />
              <Text style={styles.tileTitle}> {strings.AboutUs} </Text>
            </TouchableOpacity> */}

            <TouchableOpacity
              style={styles.tile}
              onPress={this.navigateToScreen(Config.Support)}>
              <Image
                resizeMode="contain"
                style={styles.tileIcon}
                source={Images.supportIcon}
              />
              <Text style={styles.tileTitle}> {strings.HelpSupport} </Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  loginData: state.loginReducer.loginData,
});
const mapDispatchToProps = dispatch => ({
  logoutRequest: () => dispatch(logoutRequest()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DrawerContent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerContainer: {
    height: 150,
  },
  headerText: {
    color: '#fff8f8',
  },
  screenContainer: {
    paddingTop: 0,
    marginBottom: 180,
    backgroundColor: '#ffffff',
  },
  screenStyle: {
    height: 30,
    marginTop: 2,
    flexDirection: 'row',
    alignItems: 'center',
  },
  screenTextStyle: {
    fontSize: 20,
    marginLeft: 20,
  },
  tile: {
    height: 40,
    width: '100%',
    marginTop: 8,
    backgroundColor: 'transparent',
    flexDirection: 'row',
  },
  tileIcon: {
    width: 25,
    height: 25,
    marginTop: 11,
    marginLeft: 30,
    tintColor: Colors.placeholderColor,
  },
  tileTitle: {
    marginTop: 9,
    fontSize: RFValue(14),
    fontFamily: Fonts.Regular,
    color: 'gray',
    marginLeft: 20,
  },
  tileArrow: {
    position: 'absolute',
    width: 15,
    height: 15,
    top: 15,
    right: 18,
  },
});
