import React, {Component} from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  TextInput,
  View,
  Alert,
  StatusBar,
  PermissionsAndroid,
  BackHandler,
  Linking,
} from 'react-native';
import Navigation from './Navigation';
import Axios from 'axios';
import LoadingView from './components/CLoader';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';
import SplashScreen from 'react-native-splash-screen';
import configureStore from './redux/configureStore';
import {setConfiguration} from '../src/utils/configuration';
import OfflineNotice from './components/OfflineNotice';
import LocationNotice from './components/LocationNotice';
import VersionInfo from './components/VersionInfo';
import {API_ROOT} from './env';
//import firebase from 'react-native-firebase';
import {AsyncStorage} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];
console.disableYellowBox = true;
const {store, persistor} = configureStore();
import {fcmService} from './components/Notification/FCMService';
import {localNotificationService} from './components/Notification/LocalNotificationService';
import Config from './constants/Config';
import request from './utils/request';
import DeviceInfo from 'react-native-device-info';
import RNExitApp from 'react-native-exit-app';
import Colors from './constants/Colors';

export default class App extends Component {
  constructor() {
    super();
    if (Text.defaultProps == null) {
      Text.defaultProps = {};
      Text.defaultProps.allowFontScaling = false;
    }
    if (TextInput.defaultProps == null) {
      TextInput.defaultProps = {};
    }
    TextInput.defaultProps.allowFontScaling = false;
    this.notificationListener = null;
  }

  componentDidMount() {
    //this.checkAppVersion();

    setConfiguration('API_ROOT', API_ROOT);
    setTimeout(() => SplashScreen.hide(), 2000);
    fcmService.registerAppWithFCM();
    fcmService.register(onRegister);

    //localNotificationService.configure(onOpenNotification);
    // this.checkPermission();
    if (Platform.OS === 'android') {
      // this.requestLocationPermission();
      this.requestCameraAndAudioAndLocationPermission();
    }

    function onRegister(token) {
      // console.log('[App] onRegister: ', token);
      if (token) {
        AsyncStorage.setItem('fcmToken', token);
        setConfiguration('fcmToken', token);
      }
    }
  }

  checkAppVersion = async () => {
    var url = API_ROOT + Config.basicSettingURL;
    var response = await request.get(url);
    let buildNumber = DeviceInfo.getVersion();
    console.log('sadadaaaaaaaaa', response);
    var appVersion = '';
    var appURL = '';
    if (Platform.OS === 'android') {
      appVersion = response.data.Android_User_App_Force_Version.toString();
      appURL = response.App_Url.Android_App_URL.Android_Client_App_URL;
      if (appVersion != buildNumber) {
        setTimeout(() => {
          Alert.alert(
            'Please Update',
            Config.applicationUpdate,
            [
              {
                text: 'Update',
                onPress: () => {
                  // BackHandler.exitApp();
                  Linking.openURL(appURL);
                },
              },
            ],
            {cancelable: false},
          );
        }, 600);
      }
    } else {
      appVersion = response.data.IOS_User_App_Force_Version.toString();
      appURL = response.App_Url.IOS_App_URL.IOS_Client_App_URL;
      if (appVersion != buildNumber) {
        setTimeout(() => {
          Alert.alert(
            'Please Update',
            Config.applicationUpdate,
            [
              {
                text: 'Update',
                onPress: () => {
                  Linking.openURL(appURL);
                  // RNExitApp.exitApp();
                },
              },
            ],
            {cancelable: false},
          );
        }, 600);
      }
    }
  };

  // getContact() {

  //   const api = create({
  //     baseURL: apiRoot,

  //   });

  //   api
  //     .get('/api/v1/admin/getbasicinfo', {})
  //     .then(res => this.uploadSuccess(res));
  // }

  // uploadSuccess(res) {
  //   if (res.status == 200) {
  //     this.setState({
  //       isBusyUploadImage: false,
  //       data_email: res.data.data.email,
  //       data_mobile: res.data.data.phoneNumber,
  //     });
  //   } else {
  //     this.setState({
  //       isBusyUploadImage: false,
  //     });
  //     this.showAlert(res.message, 300);
  //   }
  // }
  //
  requestCameraAndAudioAndLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.requestMultiple([
        PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
        PermissionsAndroid.PERMISSIONS.CAMERA,
        // PermissionsAndroid.PERMISSIONS.READ_SMS,
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
      ]);
      if (
        granted['android.permission.RECORD_AUDIO'] ===
          PermissionsAndroid.RESULTS.GRANTED &&
        granted['android.permission.CAMERA'] ===
          PermissionsAndroid.RESULTS.GRANTED &&
        // granted['android.permission.READ_SMS'] ===
        //   PermissionsAndroid.RESULTS.GRANTED &&
        granted['android.permission.ACCESS_FINE_LOCATION'] ===
          PermissionsAndroid.RESULTS.GRANTED &&
        granted['android.permission.READ_CONTACTS'] ===
          PermissionsAndroid.RESULTS.GRANTED
      ) {
        //console.log('You can use the cameras & mic');
      } else {
        // console.log('Permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  async requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: '',
          message: 'Allow to access current location',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        //console.log('You can use the camera');
      } else {
        //console.log('Camera permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  componentWillUnmount() {}

  render() {
    return (
      // <SafeAreaProvider>
      <Provider store={store}>
        {Platform.OS === 'ios' ? (
          <StatusBar
            backgroundColor={Colors.PrimaryColor}
            barStyle="dark-content"
          />
        ) : (
          <StatusBar
            backgroundColor={Colors.PrimaryColor}
            barStyle="dark-content"
          />
        )}

        <PersistGate loading={null} persistor={persistor}>
          <Navigation />
          <OfflineNotice />
          <LocationNotice />
          <LoadingView />
        </PersistGate>
      </Provider>
      // </SafeAreaProvider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
