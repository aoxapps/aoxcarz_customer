import React, {PureComponent} from 'react';
import {View, Text, Dimensions, StyleSheet, Image} from 'react-native';
import NetInfo, {
  NetInfoSubscription,
  NetInfoState,
} from '@react-native-community/netinfo';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Colors from '../constants/Colors';
import Images from '../constants/Images';
import Fonts from '../constants/Fonts';
const {width} = Dimensions.get('window');

function MiniOfflineSign() {
  return (
    <View style={styles.offlineContainer}>
      <Image
        resizeMode="contain"
        style={{
          width: wp('30%'),
          height: hp('30%'),
          backgroundColor: 'transparent',
          tintColor: 'white',
        }}
        source={Images.noInternet}
      />
      <Text style={styles.offlineText}>Check your internet connection</Text>
    </View>
  );
}

class OfflineNotice extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
    };
  }
  componentDidMount() {
    //To get the network state once
    NetInfo.fetch().then(state => {
      // console.log(
      //   'Connection type: ' +
      //     state.type +
      //     ', Is connected?: ' +
      //     state.isConnected,
      // );
      this.setState({isConnected: state.isConnected});
    });
    //Subscribe to network state updates
    const unsubscribe = NetInfo.addEventListener(state => {
      // console.log(
      //   'Connection type: ' +
      //     state.type +
      //     ', Is connected?: ' +
      //     state.isConnected,
      // );
      this.setState({isConnected: state.isConnected});
    });
  }

  componentWillUnmount() {
    //   this.unsubscribe();
  }

  render() {
    if (!this.state.isConnected) {
      return <MiniOfflineSign />;
    }
    return null;
  }
}

const styles = StyleSheet.create({
  offlineContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: Colors.PrimaryColor,
    height: hp('100%'),
    justifyContent: 'center',
    alignItems: 'center',
    // flexDirection: 'row',
    width,
    position: 'absolute',
    //top: 30,
  },
  offlineText: {color: '#ffffff', fontFamily: Fonts.Regular, fontSize: 20},
});

export default OfflineNotice;
