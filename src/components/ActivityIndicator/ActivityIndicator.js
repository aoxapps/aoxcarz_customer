import React from 'react';
import {
  Container,
  ActivityIndicator,
  Content,
  Text,
  View,
  Image,
} from 'react-native';

const Activity = props => (
  <View
    style={{
      flex: 1,
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'black',
      opacity: 0.5,
    }}>
    <ActivityIndicator size="large" color="#ffffff" />
  </View>
);

export default Activity;
