import React, {PureComponent} from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  Image,
  Platform,
  Alert,
  BackHandler,
  Linking,
} from 'react-native';
import Config from './../constants/Config';
import request from './../utils/request';
import DeviceInfo from 'react-native-device-info';
import RNExitApp from 'react-native-exit-app';

const {width} = Dimensions.get('window');

class VersionInfo extends PureComponent {
  constructor(props) {
    super(props);
  }
  async componentDidMount() {
    var url = API_ROOT + Config.basicSettingURL;
    var response = await request.get(url);
    let buildNumber = DeviceInfo.getVersion();
    //  console.log('sadadaaaaaaaaa', response);
    var appVersion = '';
    var appURL = '';
    if (Platform.OS === 'android') {
      appVersion = response.data.Android_User_App_Force_Version.toString();
      appURL = response.App_Url.Android_App_URL.Android_Client_App_URL;
      if (appVersion != buildNumber) {
        setTimeout(() => {
          Alert.alert(
            'Please Update',
            Config.applicationUpdate,
            [
              {
                text: 'Update',
                onPress: () => {
                  // BackHandler.exitApp();
                  Linking.openURL(appURL);
                },
              },
            ],
            {cancelable: false},
          );
        }, 600);
      }
    } else {
      appVersion = response.data.IOS_User_App_Force_Version.toString();
      appURL = response.App_Url.IOS_App_URL.IOS_Client_App_URL;
      if (appVersion != buildNumber) {
        setTimeout(() => {
          Alert.alert(
            'Please Update',
            Config.applicationUpdate,
            [
              {
                text: 'Update',
                onPress: () => {
                  Linking.openURL(appURL);
                  // RNExitApp.exitApp();
                },
              },
            ],
            {cancelable: false},
          );
        }, 600);
      }
    }
  }
  render() {
    return null;
  }
}

export default VersionInfo;
