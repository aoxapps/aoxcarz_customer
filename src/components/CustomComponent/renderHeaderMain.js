import React from 'react';
import {
  View,
  TouchableOpacity,
  ImageBackground,
  Platform,
  Text,
  Image,
} from 'react-native';
import Fonts from '../../constants/Fonts';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { getStatusBarHeight } from '../../utils/IPhoneXHelper';
import VectorIcon from '../../utils/vectorIcons';
import Images from '../../constants/Images';
import { moderateScale, scale } from 'react-native-size-matters';
import { RFValue } from 'react-native-responsive-fontsize';
import Colors from '../../constants/Colors';
const RenderHeader = props => {
  const { title, navigation, back } = props;

  return (
    <ImageBackground
      style={{
        height: Platform.select({
          ios: moderateScale(40) + getStatusBarHeight(),
          android: moderateScale(50),
        }),
        width: '100%',
        // borderColor: '#475d72',
        // borderWidth: 0,
        alignItems: 'center',
        paddingTop: Platform.select({
          ios: getStatusBarHeight(),
          android: 0,
        }),
        backgroundColor: Colors.White,
        flexDirection: 'row',
      }}

    >
      <TouchableOpacity
        style={{ flex: 2 }}
        onPress={() => navigation.openDrawer()}>
        {/* <VectorIcon
          style={{alignSelf: 'center'}}
          name={'ios-menu'}
          groupName={'Ionicons'}
          size={35}
          color={'white'}
        /> */}
        <Image
          source={Images.menuIcon}
          style={{
            width: moderateScale(55),
            height: moderateScale(55),
            marginLeft: moderateScale(5),
          }}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <Text
        style={{
          fontSize: RFValue(18),
          color: Colors.Black,
          flex: 6,
          fontFamily: Fonts.Semibold,
          textAlign: 'center',
        }}>
        {title}
      </Text>
      <View style={{ flex: 2 }} />
    </ImageBackground>
  );
};
export default RenderHeader;
