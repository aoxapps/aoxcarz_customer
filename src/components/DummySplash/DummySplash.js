import React from 'react';
import {
  Container,
  ActivityIndicator,
  Content,
  Text,
  View,
  Image,
} from 'react-native';
import {connect} from 'react-redux';

const DummySplash = props => (
  <View
    style={{
      flex: 1,
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: 'white',
    }}>
    <Image
      resizeMode="cover"
      style={{width: '100%', height: '100%'}}
      source={require('../../../assets/Splash.png')}
    />
  </View>
);

export default DummySplash;
