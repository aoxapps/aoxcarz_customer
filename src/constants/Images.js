const Images = {
  googleIcon: require('../../assets/googleIcon.png'),
  fbIcon: require('../../assets/fbIcon.png'),
  menuBlack: require('../../assets/Menu.png'),
  appleIcon: require('../../assets/appleIcon.png'),
  loginBG: require('../../assets/login-bg.png'),
  menuIcon: require('../../assets/menu-btn.png'),
  backIcon: require('../../assets/back-btn.png'),
  otpIcon: require('../../assets/otp-bg.png'),
  sidemenuBG: require('../../assets/sidemenu-bg.png'),
  nextIcon: require('../../assets/next.png'),
  profileIcon: require('../../assets/profile.png'),
  callIcon: require('../../assets/call.png'),
  emailIcon: require('../../assets/email.png'),
  addressIcon: require('../../assets/address.png'),
  pinIcon: require('../../assets/current-location.png'),
  addIcon: require('../../assets/Add-Icon.png'),
  passwordIcon: require('../../assets/password.png'),
  dummyUser: require('../../assets/dummyUser.png'),
  menuWhite: require('../../assets/menuWhite.png'),
  drawerProfileBG: require('../../assets/Victoria-Backgrnd.png'),
  rightImg: require('../../assets/right.png'),
  starImage: require('../../assets/Star.png'),
  rideIcon: require('../../assets/rides.png'),
  preferedIcon: require('../../assets/prefered_driver.png'),
  yourTripIcon: require('../../assets/your_trip.png'),
  shareIcon: require('../../assets/invite_friends.png'),
  inviteTmage: require('../../assets/invite.png'),
  promotionIcon: require('../../assets/Promotion.png'),
  walletIcon: require('../../assets/wallet.png'),
  paymentMethodIcon: require('../../assets/payment_method.png'),
  emergencyIcon: require('../../assets/emergency.png'),
  profileIcon: require('../../assets/profile.png'),
  about_usIcon: require('../../assets/about_us.png'),
  supportIcon: require('../../assets/support.png'),
  profileBackround: require('../../assets/Profile-Backround.png'),
  editProfile: require('../../assets/editProfile.png'),
  logout: require('../../assets/logout.png'),
  backWhite: require('../../assets/backwhite.png'),
  startDestination: require('../../assets/start_Destination.png'),
  locationVertical: require('../../assets/location-Pin-vertical.png'),
  // locationPin: require('../../assets/'),
  starBlack: require('../../assets/starBlack.png'),
  starBlank: require('../../assets/starBlank.png'),
  perferDriver: require('../../assets/Perfer-Driver.png'),
  selected: require('../../assets/Selected.png'),
  unselected: require('../../assets/unselected.png'),
  startingPoint: require('../../assets/StartingPoint.png'),
  calender: require('../../assets/calender.png'),
  cancel: require('../../assets/cancel.png'),
  cancelWhite: require('../../assets/Cross-white.png'),
  dropLocation: require('../../assets/drop_location.png'),
  pickupLocation: require('../../assets/pickupIcon.png'),
  aliceMonic: require('../../assets/AliceMonic.png'),
  cross: require('../../assets/Cross.png'),
  cancelNew: require('../../assets/cancelNew.png'),
  noDriver: require('../../assets/noDriver.png'),
  carTop: require('../../assets/carTop.png'),
  pickupIcon: require('../../assets/pickupIcon.png'),
  marker: require('../../assets/markerr.png'),
  locationPin2: require('../../assets/loction-Pin.png'),
  pickShow: require('../../assets/pickShow.png'),
  leftBlackArrow: require('../../assets/Left-Black-Arrow.png'),
  iconCan: require('../../assets/iconCan.png'),
  gprs2: require('../../assets/GPRS2.png'),
  redPanic: require('../../assets/redPanic.png'),
  greyPanic: require('../../assets/greyPanic.png'),
  online: require('../../assets/Online.png'),
  cash: require('../../assets/Cash.png'),
  visa: require('../../assets/Visa.png'),
  downArrow: require('../../assets/Down-Arrow.png'),
  addIcon: require('../../assets/Add-Icon.png'),
  noInternet: require('../../assets/no_internet.png'),
};
export default Images;
