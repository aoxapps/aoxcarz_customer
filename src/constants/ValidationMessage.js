const ValidationMessage = {
  mobileRequired: 'Please enter phone number!!',
  mobileValid: 'Please enter valid phone number!!',
  otpValid: 'Please enter valid OTP',
};
export default ValidationMessage;
