const Colors = {
  PrimaryColor: '#f9d142',
  primary1: '#f9d142',
  background: '#ffffff',
  background1: '#f7f8f9',
  buttonGreen: '#40BA52',
  buttonRed: '#E93F18',
  placeholderColor: '#818e97',
  photoBorder: '#FC68A7',
  bottomBorder: '#c6c6c6', //'#d1d1d1'
  placeholderTextColor: '#818e97',
  White: 'white',
  Black: '#000000',
  buttonGreen: '#40BA52',
  iYellow: '#e1b300',
};
export default Colors;
