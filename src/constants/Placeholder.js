const placeholder = {
  mobileNumber: 'Enter Your Mobile Number',
  password: 'Password',
  emailAddress: 'Email Address',
  address: 'Address',
};
export default placeholder;
