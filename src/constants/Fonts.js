const Fonts = {
  Light: 'CharlieDisplay-Light',
  Regular: 'CharlieDisplay-Regular',
  Semibold: 'CharlieDisplay-Semibold',
  Thin: 'CharlieDisplay-Thin',
};
export default Fonts;
