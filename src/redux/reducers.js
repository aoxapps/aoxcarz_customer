import {combineReducers} from 'redux';
import loginReducer from './../modules/Password/reducer';
import mobileReducer from './../modules//Login/reducer';
import resetotpReducer from './../modules/VerifyOTP/reducer';
import registerReducer from './../modules/Register/reducer';
import changePReducer from './../modules/ChangePassword/reducer';
import editPReducer from './../modules/EditProfile/reducer';
import GetProfileReducer from './../modules/GetProfile/reducer';
import AddPOSReducer from './../modules/AddPOS/reducer';
import paymentListReducer from './../modules/GetPaymentMethodList/reducer';
import deletePOSReducer from './../modules/DeletePOS/reducer';
import estimateCostReducer from './../modules/EstimateCost/reducer';
import tripReducer from './../modules/Trip/reducer';
import addWalletBalanceReducer from './../modules/AddWallet/reducer';
import addEmergencyContactReducer from './../modules/EmergencyContacts/reducer';
import emergencyContactListReducer from './../modules/GetEmergencyContactList/reducer';
import preferredDriverReducer from './../modules/PreferDriver/reducer';
import myTripsReducer from './../modules/MyTrips/reducer';
import promoCodesReducer from './../modules/PromoCodes/reducer';
import supportReducer from './../modules/Support/reducer';
import loadingReducer from './../components/CLoader/reducer';
import basicSettingReducer from './../modules/BasicSetting/reducer';
import storage from '@react-native-community/async-storage';
export const logoutRequest = () => ({
  type: 'LOG_OUT',
});

const appReducer = combineReducers({
  loginReducer,
  loadingReducer,
  mobileReducer,
  resetotpReducer,
  registerReducer,
  changePReducer,
  editPReducer,
  GetProfileReducer,
  AddPOSReducer,
  paymentListReducer,
  deletePOSReducer,
  estimateCostReducer,
  tripReducer,
  addWalletBalanceReducer,
  addEmergencyContactReducer,
  supportReducer,
  emergencyContactListReducer,
  preferredDriverReducer,
  myTripsReducer,
  promoCodesReducer,
  basicSettingReducer,
  //eventDReducer,
  //chatReducer,
});
const initialState = appReducer({}, {});
const rootReducer = (state, action) => {
  if (action.type === 'LOG_OUT') {
    Object.keys(state).forEach(key => {
      storage.removeItem(`persist:${key}`);
    });
    state = Object.assign({}, initialState);
  }

  return appReducer(state, action);
};

export default rootReducer;
