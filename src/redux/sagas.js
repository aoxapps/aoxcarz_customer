import {all} from 'redux-saga/effects';
import sagaLogin from './../modules/Password/sagas';
import sagaMobile from './../modules/Login/sagas';
import sagaVerifyOtp from './../modules/VerifyOTP/sagas';
import sagaRegister from './../modules/Register/sagas';
import sagaChangeP from './../modules/ChangePassword/sagas';
import sagaEditProfile from './../modules/EditProfile/sagas';
import sagaAddPOS from './../modules/AddPOS/sagas';
import sagaPaymentMethodList from './../modules/GetPaymentMethodList/sagas';
import sagaDeletePOS from './../modules/DeletePOS/sagas';
import sagaEstimateCost from './../modules/EstimateCost/sagas';
import sagaAddTrip from './../modules/Trip/sagas';
import sagaAddWalletBalance from './../modules/AddWallet/sagas';
import sagaEmergencyContacts from './../modules/EmergencyContacts/sagas';
import sagaContactList from './../modules/GetEmergencyContactList/sagas';
import sagaPreferredDriver from './../modules/PreferDriver/sagas';
import sagaMyTrips from './../modules/MyTrips/sagas';
import sagaPromoCodes from './../modules/PromoCodes/sagas';
import sagaSupport from './../modules/Support/sagas';
import sagaProfile from './../modules/GetProfile/sagas';
import sagaBasicSetting from './../modules/BasicSetting/sagas';
export default function* rootSaga() {
  yield all([
    sagaLogin(),
    sagaMobile(),
    sagaVerifyOtp(),
    sagaChangeP(),
    sagaRegister(),
    sagaEditProfile(),
    sagaProfile(),
    sagaAddPOS(),
    sagaPaymentMethodList(),
    sagaDeletePOS(),
    sagaEstimateCost(),
    sagaAddTrip(),
    sagaAddWalletBalance(),
    sagaEmergencyContacts(),
    sagaContactList(),
    sagaPreferredDriver(),
    sagaMyTrips(),
    sagaPromoCodes(),
    sagaSupport(),
    sagaBasicSetting(),
  ]);
}
