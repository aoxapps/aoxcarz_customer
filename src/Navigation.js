import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createDrawerNavigator} from 'react-navigation-drawer';
import Login from './containers/Login';
import Password from './containers/Password';
import VerifyOTP from './containers/VerifyOTP';
import Register from './containers/Register';
import Profile from './containers/Profile';
import EditProfile from './containers/EditProfile';

// import UpcomingEvents from './container/Events/UpcomingEvents';
// import PastEvents from './container/Events/PastEvents';

import ChangePassword from './containers/ChangePassword';
import MyTrip from './containers/MyTrip';
//import PaymentMethod from './containers/PaymentMethod';
import Emergency from './containers/Emergency';
import Support from './containers/Support';
import SupportMessage from './containers/SupportMessage';
import About from './containers/About';
import Invite from './containers/Invite';

import MapSearch from './containers/Map';
//import MapDetails from './containers/MapDetails';
import AddCard from './containers/AddCard';
import AccountDetail from './containers/AccountDetail';
// import History from './containers/History';
import Thankyou from './containers/Thankyou';
//import NoDriver from './containers/NoDriver';
import AddContacts from './containers/AddContacts';
import ChangePasswordProfile from './containers/ChangePasswordProfile';
import CouponScreen from './containers/Coupon';
import DrawerContent from './DrawerContent';
import Wallet from './containers/Wallet';
import PreferredDriver from './containers/PreferredDriver';

// import EventDetails from './container/EventDetail';
// import RequestForm from './container/RequestForm';
// import Review from './container/Review';
// import InRoute from './container/InRoute';
// import Chat from './container/Chat';
// import ModelList from './container/ModelList';

const PreferredNavigation = createStackNavigator(
  {
    PreferredDriver: {
      screen: PreferredDriver,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
  },
  {
    headerMode: 'none',
  },
);

const ProfileNavigation = createStackNavigator(
  {
    ProfileScreen: {
      screen: Profile,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },

    ChangePasswordProfile: {
      screen: ChangePasswordProfile,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
    EditProfile: {
      screen: EditProfile,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
  },
  {
    headerMode: 'none',
  },
);

const PaymentNavigation = createStackNavigator(
  {
    AccountDetailScreen: {
      screen: AccountDetail,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
    // PaymentMethod: {
    //   screen: PaymentMethod,
    //   navigationOptions: {
    //     gesturesEnabled: false,
    //   },
    // },
    AddCard: {
      screen: AddCard,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
  },
  {
    headerMode: 'none',
  },
);

const MyTripNavigation = createStackNavigator(
  {
    // Search_LocationScreen: {
    //     screen: Search_Location,
    //     navigationOptions: {
    //         gesturesEnabled: false,
    //     },
    // },

    MapSearchScreen: {
      screen: MapSearch,
      navigationOptions: {
        gesturesEnabled: false,
        // header: 'none',
      },
    },

    // MapDetailsScreen: {
    //   screen: MapDetails,
    //   navigationOptions: {
    //     gesturesEnabled: false,
    //   },
    // },
    AddCard: {
      screen: AddCard,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
    // NoDriverScreen: {
    //   screen: NoDriver,
    //   navigationOptions: {
    //     gesturesEnabled: false,
    //   },
    // },

    ThankyouScreen: {
      screen: Thankyou,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
    // PastDetail: {
    //     screen: PastDetail,
    //     navigationOptions: {
    //         gesturesEnabled: false,
    //     },
    // },
    // Track: {
    //     screen: Track,
    //     navigationOptions: {
    //         gesturesEnabled: false,
    //     },
    // },
    // Chat: {
    //     screen: Chat,
    //     navigationOptions: {
    //         gesturesEnabled: false,
    //     },
    // },
    // Dispute: {
    //     screen: Dispute,
    //     navigationOptions: {
    //         gesturesEnabled: false,
    //     },
    // },
  },
  {
    headerMode: 'none',
  },
);

const EmergencyNavigation = createStackNavigator(
  {
    EmergencyScreen: {
      screen: Emergency,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
    AddContacts: {
      screen: AddContacts,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
  },
  {
    headerMode: 'none',
  },
);

const SupportNavigation = createStackNavigator(
  {
    SupportScreen: {
      screen: Support,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
    SupportMessage: {
      screen: SupportMessage,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
  },
  {
    headerMode: 'none',
  },
);

// const PromotionNavigation = createStackNavigator({

//     Coupon: {
//         screen: Coupon,
//         navigationOptions: {
//             gesturesEnabled: false,
//         },
//     }

// }, {
//         headerMode: 'none'
//     }
// );

const PastTripNavigation = createStackNavigator(
  {
    MyTrip: {
      screen: MyTrip,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
    // PastDetail: {
    //     screen: PastDetailp,
    //     navigationOptions: {
    //         gesturesEnabled: false,
    //     },
    // },
  },
  {
    headerMode: 'none',
  },
);

const DrawerNav = createDrawerNavigator(
  {
    // Profile: {
    //     screen: ProfileNavigation,
    // },
    MyTrip: {
      screen: MyTripNavigation,
    },
    PreferredScreen: {
      screen: PreferredNavigation,
    },
    MyTripScreen: {
      screen: MyTrip,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
    CouponScreen: {
      screen: CouponScreen,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
    AccountDetail: {
      screen: PaymentNavigation,
    },
    Emergency: {
      screen: EmergencyNavigation,
    },
    Support: {
      screen: SupportNavigation,
    },
    About: {
      screen: About,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
    ProfileScreen: {
      screen: ProfileNavigation,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
    Invite: {
      screen: Invite,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
    WalletScreen: {
      screen: Wallet,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
  },
  {
    contentComponent: DrawerContent,
    contentOptions: {
      tintColor: '#a6a5ab',
    },
  },
);

const AppNavigator = createStackNavigator(
  {
    // SelectLanguage: {
    //     screen: SelectLanguage,
    //     navigationOptions: {
    //         gesturesEnabled: false,
    //     },
    // },
    Login: {
      screen: Login,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
    Password: {
      screen: Password,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
    VerifyOTP: {
      screen: VerifyOTP,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
    ChangePassword: {
      screen: ChangePassword,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
    Register: {
      screen: Register,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
    SideMenu: {
      screen: DrawerNav,
      navigationOptions: {
        gesturesEnabled: false,
      },
    },
  },
  {
    headerMode: 'none',
  },
);

export default createAppContainer(AppNavigator);
