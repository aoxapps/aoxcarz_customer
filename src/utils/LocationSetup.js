import {PermissionsAndroid, Platform, AsyncStorage} from 'react-native';
import Geolocation from 'react-native-geolocation-service';

var watchID = null;
var latlng = {};
export const fetchLocation = async () => {
  const granted =
    Platform.OS === 'android'
      ? await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Location Access Required',
            message: 'This App needs to Access your location',
          },
        )
      : true;

  if (granted) {
    Geolocation.getCurrentPosition(
      position => {
        console.log(
          'LatLng :  ' +
            position.coords.latitude +
            '  ' +
            position.coords.longitude,
        );

        const latlng = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        //AsyncStorage.setItem('latlng', JSON.stringify(latlng));
        // setinitialPosition(initialPosition)
      },
      error => alert(error.message),
      {enableHighAccuracy: true, timeout: 10000, maximumAge: 1000},
    );
  }
};

export const watchLocation = async () => {
  const granted =
    Platform.OS === 'android'
      ? await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Location Access Required',
            message: 'This App needs to Access your location',
          },
        )
      : true;

  if (granted) {
    watchID = Geolocation.watchPosition(
      position => {
        console.log(
          'watch LatLng :  ' +
            position.coords.latitude +
            '  ' +
            position.coords.longitude,
        );
        const latlng = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        //AsyncStorage.setItem('latlng', JSON.stringify(latlng));
      },
      error => alert(error.message),
      {interval: 1000, enableHighAccuracy: true, distanceFilter: 0},
    );
  }
};

export const clearLocation = () => {
  Geolocation.clearWatch(watchID);
  Geolocation.stopObserving();
};
