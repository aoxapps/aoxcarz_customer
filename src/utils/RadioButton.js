import React, {Component} from 'react';
import {Text, View, TouchableOpacity, StyleSheet} from 'react-native';
import Colors from './Colors';

export default class RadioButtons extends Component {
  state = {
    value: null,
  };

  render() {
    const {options} = this.props;
    const {value} = this.state;

    return (
      <View style={{flex: 1, flexDirection: 'row'}}>
        {options.map(item => {
          return (
            <View key={item.key} style={styles.buttonContainer}>
              <TouchableOpacity
                style={{flexDirection: 'row'}}
                onPress={() => {
                  this.setState({
                    value: item.key,
                  });
                }}>
                <View
                  style={[
                    styles.circle,
                    {
                      borderColor:
                        value === item.key ? Colors.PrimaryColor : '#ACACAC',
                    },
                  ]}
                  onPress={() => {
                    this.setState({
                      value: item.key,
                    });
                  }}>
                  {value === item.key && <View style={styles.checkedCircle} />}
                </View>
                <Text
                  style={{
                    paddingLeft: 8,
                    color: value === item.key ? Colors.PrimaryColor : null,
                  }}>
                  {item.text}
                </Text>
              </TouchableOpacity>
            </View>
          );
        })}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection: 'row',

    alignItems: 'center',

    marginRight: 20,
  },

  circle: {
    height: 20,
    width: 20,
    borderRadius: 10,
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  checkedCircle: {
    width: 14,
    height: 14,
    borderRadius: 7,
    backgroundColor: Colors.PrimaryColor,
  },
});
