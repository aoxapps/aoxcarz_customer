import MapSearch from './MapSearch';
import {estimateRequest} from './../../modules/EstimateCost/actions';
import {PaymentMethodListRequest} from './../../modules/GetPaymentMethodList/actions';
import {
  addTripRequest,
  ratingRequest,
  panicRequest,
  preferDriverRequest,
  cancelTripRequest,
  couponRequest,
  getTripRequest,
  cancelRatingRequest,
  abortTripRequest,
} from '../../modules/Trip/actions';
import {connect} from 'react-redux';
const mapStateToProps = state => ({
  PaymentListData: state.paymentListReducer.paymentMethodData,
  EstimateCostData: state.estimateCostReducer.estimateData,
  ratingData: state.tripReducer.ratingData,
  panicData: state.tripReducer.panicData,
  preferDriverData: state.tripReducer.preferDriverData,
  cancelTripData: state.tripReducer.cancelTripData,
  abortTripData: state.tripReducer.abortTripData,
  couponData: state.tripReducer.couponData,
  getTripData: state.tripReducer.getTripData,
  addTripData: state.tripReducer.tripData,
  ProfileData: state.GetProfileReducer.profileData,
});
const mapDispatchToProps = dispatch => ({
  getTripRequest: (tripId, navigation) =>
    dispatch(getTripRequest(tripId, navigation)),
  ratingRequest: (data, navigation) =>
    dispatch(ratingRequest(data, navigation)),
  estimateRequest: (oneWayitem, dropOnewayitem, promoCode, navigation) =>
    dispatch(
      estimateRequest(oneWayitem, dropOnewayitem, promoCode, navigation),
    ),
  panicRequest: (data, navigation) => dispatch(panicRequest(data, navigation)),
  preferDriverRequest: (data, navigation) =>
    dispatch(preferDriverRequest(data, navigation)),
  cancelTripRequest: (data, navigation) =>
    dispatch(cancelTripRequest(data, navigation)),
  abortTripRequest: (data, navigation) =>
    dispatch(abortTripRequest(data, navigation)),
  couponRequest: (data, navigation) =>
    dispatch(couponRequest(data, navigation)),
  cancelRatingRequest: navigation => dispatch(cancelRatingRequest(navigation)),
  PaymentMethodListRequest: navigation =>
    dispatch(PaymentMethodListRequest(navigation)),
  addTripRequest: (
    selectedRide,
    trptyp,
    prefDriverRide,
    country,
    countryCode,
    state,
    stateCode,
    city,
    cityCode,
    pricingType,
    isSurgeTime,
    surgeMultiplier,
    unit,
    distance,
    estimatedTime,
    estimatedCost,
    bikeSelected,
    PaymentVia,
    selectedCardID,
    SourcePick,
    dropOneway,
    TimeZoneSelected,
    dat,
    tim,
    promoCode,
    noOfSeats,
    navigation,
  ) =>
    dispatch(
      addTripRequest(
        selectedRide,
        trptyp,
        prefDriverRide,
        country,
        countryCode,
        state,
        stateCode,
        city,
        cityCode,
        pricingType,
        isSurgeTime,
        surgeMultiplier,
        unit,
        distance,
        estimatedTime,
        estimatedCost,
        bikeSelected,
        PaymentVia,
        selectedCardID,
        SourcePick,
        dropOneway,
        TimeZoneSelected,
        dat,
        tim,
        promoCode,
        noOfSeats,
        navigation,
      ),
    ),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(MapSearch);
//export default MapSearch;
