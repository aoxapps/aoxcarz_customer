import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  Keyboard,
  Platform,
  StatusBar,
  SafeAreaView,
  TouchableHighlight,
  Animated,
  PermissionsAndroid,
  Share,
  KeyboardAvoidingView,
  Dimensions,
  FlatList,
  Alert,
  NativeModules,
  ScrollView,
  BackHandler,
  AppState,
  Linking,
} from 'react-native';

const keyboardVerticalOffset = Platform.OS === 'ios' ? 260 : 0;
const widthCal = Dimensions.get('window').width;
const heightCal = Dimensions.get('window').height;
import {getStatusBarHeight} from '../../utils/IPhoneXHelper';
import DatePicker from 'react-native-datepicker';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {DrawerActions} from 'react-navigation-drawer';
import MapView, {Callout, PROVIDER_GOOGLE} from 'react-native-maps';
import {isEmpty} from '../../utils/CommonFunctions';
import Geolocation from 'react-native-geolocation-service';
import Geocoder from 'react-native-geocoding';
Geocoder.init('AIzaSyAK_2EtQ6DCp_4L69cuVm0sm3nnA1sV1xs');
import SocketIOClient from 'socket.io-client';
import BackgroundFetch from 'react-native-background-fetch';
import BackgroundTimer from 'react-native-background-timer';
import {getConfiguration, setConfiguration} from '../../utils/configuration';
import MapViewDirections from 'react-native-maps-directions';
import {strings} from '../../constants/languagesString';
import Fonts from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import Images from '../../constants/Images';
import VectorIcon from '../../utils/vectorIcons';
import {
  BallIndicator,
  WaveIndicator,
  DotIndicator,
} from 'react-native-indicators';
import moment from 'moment-timezone';
import {RtcEngine, AgoraView} from 'react-native-agora';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Config from '../../constants/Config';
import {fcmService} from '../../components/Notification/FCMService';
import {localNotificationService} from '../../components/Notification/LocalNotificationService';
import {moderateScale, scale, verticalScale} from 'react-native-size-matters';
import {RFValue} from 'react-native-responsive-fontsize';

//import firebase from 'react-native-firebase';
const {Agora} = NativeModules; //Define Agora object as a native module

const {FPS30, AudioProfileDefault, AudioScenarioDefault, Adaptative} = Agora; //Set defaults for Stream

let dimensions = {
  //get dimensions of the device to use in view styles
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};

const ASPECT_RATIO = dimensions.width / dimensions.height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
var markers = [
  {
    title: 'vikas car',
    coordinates: {
      latitude: 30.6828,
      longitude: 76.7091,
    },
  },
  {
    title: 'Prince car',
    coordinates: {
      latitude: 30.7277,
      longitude: 76.7062,
    },
  },
  {
    title: 'Ashu car',
    coordinates: {
      latitude: 30.696,
      longitude: 76.6965,
    },
  },
];
export default class MapSearch extends React.Component {
  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.backHandler = null;
    this.state = {
      showSuggestion: false,
      predictions: [],
      predictionsdest: [],
      selSourcePlaceId: '',
      showSuggestionDest: false,
      curLatitude: 0,
      curLongitude: 0,

      sourceLocation: '',
      destinationLocation: '',
      destinationPlaceID: '',
      estimatedTime: 0,
      distance: 0,

      destLong: 0,
      destLat: 0,
      tripStatus: 'fresh',
      ratingValue: 0,
      FindDriver: false,
      NoDriverFoundView: false,
      panicStatus: false,
      driverLatitude: 0,
      driverLongitude: 0,
      shareMessage: '',
      endLocationAddr: '',
      preferredClick: false,
      driverId: '',
      preferredDone: false,
      prefDriverRide: 'none',
      driverIdPref: '',
      driverMobileCode: '',
      driverMobileNumber: '',
      showcoupon: true,
      Schedule: false,
      showitem: false,
      driverdetails: false,
      curLat: 0,
      curLong: 0,
      curAddress: '',

      destAddress: '',
      bikeSelected: 'S',
      estimationCostArr: [],
      paymentTap: false,
      cardList: [],
      selectedCardNum: strings.Selectcard,
      selectedCardImage: '',
      selectedCardID: '',
      promoCode: '',
      date: '',
      Time: '',
      tripId: '',
      estimatedCost: '',
      ScheduleSetup: false,
      minDateSel: '',
      walletBalance: 0,
      walletView: false,
      estimateComparision: '',
      PaymentVia: '',

      selectedRide: 'standard',
      RideDes: false,
      descriptionRideString: '',

      SourcePick: {
        address_components: '',
        formatted_address: '',
        place_id: '',
        startLocation: {
          lat: '',
          long: '',
        },
      },
      dropOneway: {
        address_components: '',
        formatted_address: '',
        place_id: '',
        startLocation: {
          lat: '',
          long: '',
        },
      },
      isShared: false,
      isSingle: true,
      isMultiple: false,
      shareView: false,
      AddTripArray: [],
      rideTyp: 'standard',
      TimeZoneSelected: moment.tz.guess(),
      showCurLat: 0,
      showCurLng: 0,
      showdestLat: 0,
      ShowdestLng: 0,
      ChildVideo: false,

      addressList: [],
      sourceSearchMap: false,
      destSearchMap: false,
      sourceLatDelta: LATITUDE_DELTA,
      sourceLngDelta: LONGITUDE_DELTA,
      destLatDelta: LATITUDE_DELTA,
      destLngDelta: LONGITUDE_DELTA,
      sourceLatitude: 0,
      sourceLongitude: 0,
      destLatitude: 0,
      destLongitude: 0,

      peerIds: [], //Array for storing ctcted peers
      uid: Math.floor(Math.random() * 100), //Generate a UID for local user
      appid: 'c9104d217f5e437691c9b8aaa40aab58', //Enter the App ID generated from the Agora Website
      // appid: '78c6b199638444d49aa47b2fcf7e25db', //Enter the App ID generated from the Agora Website
      channelName: '1111122222', //Channel Name for the current session
      vidMute: true, //State variable for Video Mute
      audMute: true, //State variable for Audio Mute
      joinSucceed: false, //State variable for storing success

      estimateCost: [],
      getTripData: '',
      addTripData: '',
      ratingData: '',
      panicData: '',
      preferDriverData: '',
      cancelTripData: '',
      couponData: '',
      PaymentListData: [],
      driverLocation: {
        latitude: 30.71335,
        longitude: 76.71701,
      },
      appState: AppState.currentState,
      distanceToStartLocation: 0,
      timeToStartLocation: 0,
    };
    this.intervalId = '';
    this.watchID = '';
    // if (Platform.OS === 'android') {
    const configi = {
      //Setting config of the app
      appid: 'c9104d217f5e437691c9b8aaa40aab58', //App ID
      channelProfile: 0, //Set channel profile as 0 for RTC
      videoEncoderConfig: {
        //Set Video feed encoder settings
        width: 720,
        height: 1080,
        bitrate: 1,
        frameRate: FPS30,
        orientationMode: Adaptative,
      },
      audioProfile: AudioProfileDefault,
      audioScenario: AudioScenarioDefault,
    };
    RtcEngine.init(configi);
    // }
    // setTimeout(() => this.getCurrentLocation(), 2000);

    // const { navigation } = props;

    //console.log('moment TimeZone:', moment.tz.guess());
    const {navigation} = this.props;
    this.didFocusListener = navigation.addListener(
      'didFocus',
      this.componentDidFocus,
    );
  }
  componentDidFocus = payload => {
    // setTimeout(() => this.getCurrentLocation(), 2000);
    setTimeout(() => this.getCurrentLocationInstant(false), 2000);
    // this.setState({PaymentListData: this.props.PaymentListData}, () => {
    //   var pl = this.props.PaymentListData;

    //   // debugger;
    //   if (
    //     pl != null &&
    //     // pl != '' &&
    //     typeof pl != undefined &&
    //     pl != 'undefined'
    //   ) {
    //     this.getData();
    //   } else {
    //     this.props.PaymentMethodListRequest(this.props.navigation);
    //   }
    // });
    this.props.PaymentMethodListRequest(this.props.navigation);
  };

  focus = () => {
    this.ref.focus();
  };
  // componentDidFocus(){

  //     // setTimeout(() => this.uiUpdate() , 2000);

  // }

  uiUpdate() {
    // var screenStatus = getConfiguration('findingDriver');
    // if (screenStatus == true) {
    //   setTimeout(() => this.uiUpdateCancel(), 50000);
    // }
    // this.setState({FindDriver: screenStatus});
    // setTimeout(() => this.getCurrentLocation(), 2000);

    if (this.state.FindDriver == true) {
      setTimeout(() => this.uiUpdateCancel(), 50000);
    }
    setTimeout(() => this.getCurrentLocationInstant(false), 2000);
  }

  uiUpdateCancel() {
    //  setConfiguration('findingDriver', false);
    // this.setState({FindDriver: false, NoDriverFoundView: true});
    if (this.state.FindDriver == true) {
      this.setState({
        FindDriver: false,
        NoDriverFoundView: true,
        promoCode: '',
        shareView: false,
        showcoupon: true,
        isShared: false,
        isSingle: true,
        isMultiple: false,
      });
      setTimeout(() => this.getCurrentLocationInstant(false), 2000);
      Keyboard.dismiss();
    }
  }

  /**
   * @name toggleAudio
   * @description Function to toggle local user's audio
   */
  toggleAudio() {
    let mute = this.state.audMute;
    // console.log('Audio toggle', mute);
    RtcEngine.muteLocalAudioStream(!mute);
    this.setState({
      audMute: !mute,
    });
  }
  /**
   * @name toggleVideo
   * @description Function to toggle local user's video
   */
  toggleVideo() {
    let mute = this.state.vidMute;
    //console.log('Video toggle', mute);
    this.setState({
      vidMute: !mute,
    });
    RtcEngine.muteLocalVideoStream(!this.state.vidMute);
  }
  /**
   * @name endCall
   * @description Function to end the call
   */
  endCall() {
    //RtcEngine.destroy();
    RtcEngine.leaveChannel();
    this.setState({ChildVideo: false, peerIds: [], joinSucceed: false});
    // Actions.home();
  }
  /**
   * @name peerClick
   * @description Function to swap the main peer videostream with a different peer videostream
   */
  peerClick(data) {
    let peerIdToSwap = this.state.peerIds.indexOf(data);
    this.setState(prevState => {
      let currentPeers = [...prevState.peerIds];
      let temp = currentPeers[peerIdToSwap];
      currentPeers[peerIdToSwap] = currentPeers[0];
      currentPeers[0] = temp;
      return {peerIds: currentPeers};
    });
  }
  /**
   * @name videoView
   * @description Function to return the view for the app
   */
  videoView = () => {
    return (
      <View style={{flex: 1, backgroundColor: Colors.PrimaryColor}}>
        {this.state.peerIds.length > 1 ? (
          <View style={{flex: 1}}>
            <View style={{height: (dimensions.height * 3) / 4 - 50}}>
              <AgoraView
                style={{flex: 1}}
                remoteUid={this.state.peerIds[0]}
                mode={1}
                key={this.state.peerIds[0]}
              />
            </View>
            <View style={{height: dimensions.height / 4}}>
              <ScrollView
                horizontal={true}
                decelerationRate={0}
                snapToInterval={dimensions.width / 2}
                snapToAlignment={'center'}
                style={{
                  width: dimensions.width,
                  height: dimensions.height / 4,
                }}>
                {this.state.peerIds.slice(1).map(data => (
                  <TouchableOpacity
                    style={{
                      width: dimensions.width / 2,
                      height: dimensions.height / 4,
                    }}
                    onPress={() => this.peerClick(data)}
                    key={data}>
                    <AgoraView
                      style={{
                        width: dimensions.width / 2,
                        height: dimensions.height / 4,
                      }}
                      remoteUid={data}
                      mode={1}
                      key={data}
                    />
                  </TouchableOpacity>
                ))}
              </ScrollView>
            </View>
          </View>
        ) : this.state.peerIds.length > 0 ? (
          <View style={{height: dimensions.height - 50}}>
            <AgoraView
              style={{flex: 1}}
              remoteUid={this.state.peerIds[0]}
              mode={1}
            />
          </View>
        ) : (
          <View
            style={{
              justifyContent: 'center',
              flex: 1,
              alignSelf: 'center',
              alignContent: 'center',
            }}>
            <Text style={{color: '#ffffff'}}>Please wait for a while...</Text>
          </View>
        )}
        {!this.state.vidMute ? ( //view for local video
          <AgoraView
            style={styles.localVideoStyle}
            zOrderMediaOverlay={true}
            showLocalVideo={true}
            mode={1}
          />
        ) : (
          <View />
        )}
        <View style={styles.buttonBar}>
          <Icon.Button
            style={styles.iconStyle}
            backgroundColor="#0093E9"
            name={this.state.audMute ? 'mic-off' : 'mic'}
            onPress={() => this.toggleAudio()}
          />
          <Icon.Button
            style={styles.iconStyle}
            //backgroundColor="#0093E9"
            // color="red"
            // name="call-end"
            backgroundColor="#0093E9"
            name="clear"
            onPress={() => this.endCall()}
          />
          {/* <Icon.Button
            style={styles.iconStyle}
            backgroundColor="#0093E9"
            name={this.state.vidMute ? 'videocam-off' : 'videocam'}
            onPress={() => this.toggleVideo()}
          /> */}
        </View>
      </View>
    );
  };

  handleBackButtonClick = () => {
    if (this.props.navigation.isFocused()) {
      Alert.alert(
        'Exit',
        'Are you sure you want to Exit App?',
        [
          {text: 'Cancel', onPress: () => {}, style: 'cancel'},
          {text: 'Exit', onPress: () => BackHandler.exitApp()},
        ],
        {cancelable: false},
      );
      return true;
    } else {
      return false;
    }
  };

  async requestCameraPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: '',
          message: 'Allow to access current location',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // console.log('You can use the camera');
      } else {
        //  console.log('Camera permission denied');
      }
    } catch (err) {
      console.log(err);
    }
  }

  async checkPermissionForLocatin() {
    const granted = await PermissionsAndroid.check(
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
    );

    if (granted) {
      console.log('You can use the ACCESS_FINE_LOCATION');
    } else {
      console.log('ACCESS_FINE_LOCATION permission denied');
    }
  }

  onShare = async () => {
    try {
      const result = await Share.share({
        message: this.state.shareMessage,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  async createNotificationListeners() {
    this.notificationListener = firebase
      .notifications()
      .onNotification(notification => {
        const {title, body} = notification;
        // console.log(notification);
        //  this.showAlert(title, body);
        const channelId = new firebase.notifications.Android.Channel(
          'Default1',
          'Default',
          firebase.notifications.Android.Importance.High,
        );
        firebase.notifications().android.createChannel(channelId);

        let notification_to_be_displayed = new firebase.notifications.Notification(
          {
            data: notification.data,
            sound: 'default',
            show_in_foreground: true,
            title: notification.title,
            body: notification.body,
          },
        );

        if (Platform.OS == 'android') {
          notification_to_be_displayed.android
            .setPriority(firebase.notifications.Android.Priority.Max)
            .android.setChannelId('Default1')
            .android.setVibrate(1000);
        }
        firebase
          .notifications()
          .displayNotification(notification_to_be_displayed);
      });

    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(notificationOpen => {
        const {title, body} = notificationOpen.notification;

        // const messageType = notificationOpen.notification.data.type;
        // if (
        //   typeof messageType !== 'undefined' &&
        //   messageType !== '' &&
        //   messageType !== null
        // ) {
        //   var bookingId = notificationOpen.notification.data.bookingId;
        //   if (messageType === 'jobStarted') {
        //     var booking = {bookingId: bookingId, bookingType: 'jobStarted'};
        //     firebase
        //       .notifications()
        //       .removeDeliveredNotification(
        //         notificationOpen.notification.notificationId,
        //       );
        //     this.moveToAppointments(booking);
        //   } else if (messageType === 'jobCancelled') {
        //     firebase
        //       .notifications()
        //       .removeDeliveredNotification(
        //         notificationOpen.notification.notificationId,
        //       );
        //     this.moveToDashboard();
        //   } else if (messageType === 'jobCompleted') {
        //     var booking = {bookingId: bookingId, bookingType: 'jobCompleted'};
        //     firebase
        //       .notifications()
        //       .removeDeliveredNotification(
        //         notificationOpen.notification.notificationId,
        //       );
        //     this.moveToAppointments(booking);
        //   }
        // }

        // this.showAlert(title, body);
      });

    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      const {title, body} = notificationOpen.notification;

      //this.showAlert(title, body);
    }

    this.messageListener = firebase.messaging().onMessage(message => {
      //process data message
      //this.showAlert('Alert', message);
      //console.log(JSON.stringify(message));
    });
  }

  // startTripStatusUpdate = () => {
  //   let tripId = this.state.tripId;
  //   let tripStatus = this.state.tripStatus;
  //   this.statusUpdateId = BackgroundTimer.runBackgroundTimer(() => {
  //     if (
  //       tripId != null &&
  //       tripId != '' &&
  //       typeof tripId != undefined &&
  //       tripStatus != Config.TRIP_FINDING
  //     ) {

  //     }
  //     this.getLastestLocation();
  //   }, 10000);
  // };
  async componentDidMount() {
    if (Platform.OS === 'android') {
      this.requestCameraPermission();
    }
    this.getCurrentLocation();
    setTimeout(() => this.getCurrentLocationInstant(false), 2000);

    RtcEngine.on('userJoined', data => {
      const {peerIds} = this.state; //Get currrent peer IDs
      if (peerIds.indexOf(data.uid) === -1) {
        //If new user has joined
        this.setState({
          peerIds: [...peerIds, data.uid], //add peer ID to state array
        });
      }
    });
    RtcEngine.on('userOffline', data => {
      //If user leaves
      this.setState({
        //  peerIds: [],
        peerIds: this.state.peerIds.filter(uid => uid !== data.uid), //remove peer ID from state array
      });
    });
    RtcEngine.on('joinChannelSuccess', data => {
      //If Local user joins RTC channel
      // console.log('data');
      RtcEngine.startPreview(); //Start RTC preview
      this.setState({
        joinSucceed: true, //Set state variable to true
      });
    });
    var url = getConfiguration('API_ROOT');
    this.socket = SocketIOClient(url);
    this.socket.on('connect', () => {
      this.socket.emit(
        'customersocket',
        {
          customerId: getConfiguration('user_id'),
          firebase_token: 'none',
        },
        data => {
          // console.log('------customersocket-------', data); // data will be 'tobi says woot'
          if (data.customerStatus === Config.TRIP_ON_TRIP) {
            this.setState({
              tripId: data.tripId,
            });
            this.getTripDataRequest(data.tripId);
          } else if (data.customerStatus === Config.TRIP_WAITING_DRIVER) {
            this.getTripDataRequest(data.tripId);
          } else if (data.customerStatus === Config.TRIP_FINDING) {
            var currentTripId = this.props.ProfileData.currentTripId;
            if (
              currentTripId != null &&
              currentTripId != '' &&
              typeof currentTripId != undefined
            ) {
              this.getTripDataRequest(currentTripId);
            }
            // console.log('dadadsadasd');
          }
        },
      );
    });

    setConfiguration('Socket', this.socket);

    this.socket.on('trip_customer_socket', data => {
      // console.log('Data recieved from server', data);
      if (
        data.tripStatus == Config.TRIP_COMPLETED ||
        data.tripStatus == Config.TRIP_CANCELLED
      ) {
        this.setState({
          tripStatus: data.tripStatus,
          sourceLocation: '',
          destinationLocation: '',
          ChildVideo: false,
          panicStatus: false,
        });
      } else {
        // if (data.tripStatus === Config.TRIP_DESTINATION_INROUTE) {
        //   if (!data.prefDriver) {
        //   }
        // }
        //debugger;
        var driverLat = data.driverLocation.coordinates[1];
        var driverLng = data.driverLocation.coordinates[0];
        this.setState({
          driverLocation: {latitude: driverLat, longitude: driverLng},
          driverAngle: data.angle,
          driverLatitude: driverLat,
          driverLongitude: driverLng,
          tripId: data.tripId,
        });
      }

      // if (data.customerStatus === Config.TRIP_ON_TRIP) {
      //   //call function with tripId : data.tripId
      //   this.getTripDataRequest(data.tripId);
      // } else if (data.customerStatus === Config.TRIP_WAITING_DRIVER) {
      //   //call function with tripId : data.tripId
      //   this.getTripDataRequest(data.tripId);
      // }
      this.getTripDataRequest(data.tripId);
    });
    this.setState({PaymentListData: this.props.PaymentListData}, () => {
      var pl = this.props.PaymentListData;
      if (
        pl != null &&
        pl != '' &&
        typeof pl != undefined &&
        pl != 'undefined'
      ) {
        this.getData();
      } else {
        this.props.PaymentMethodListRequest(this.props.navigation);
      }
    });
    // this.createNotificationListeners();

    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
    AppState.addEventListener('change', this.getUserState);

    fcmService.notificationListeners(
      onRegister,
      onNotification,
      onOpenNotification,
    );
    localNotificationService.configure(onOpenNotification);

    function onRegister(token) {
      // console.log('[App] onRegister: ', token);
    }

    function onNotification(notify) {
      //  console.log('[App] onNotification: ', notify);
      const options = {
        soundName: 'default',
        playSound: true,
      };
      localNotificationService.showNotification(
        0,
        notify.title,
        notify.body,
        notify,
        options,
      );
    }

    function onOpenNotification(notify) {
      //console.log('[App] onOpenNotification: ', notify);
      //  alert('Open Notification: ' + notify.body);
    }
  }

  getTripDataRequest = tripId => {
    this.props.getTripRequest(tripId, this.props.navigation);
  };
  openDrawerClick() {
    Keyboard.dismiss();
    this.props.navigation.dispatch(DrawerActions.openDrawer());
  }
  BackAction() {
    this.setState({
      tripStatus: 'fresh',
      selectedRide: 'standard',
      showitem: false,
      promoCode: '',
      showcoupon: true,
      shareView: false,
      isShared: false,
      shareView: false,
      Schedule: false,
      ScheduleSetup: false,
    });
    setTimeout(() => this.getCurrentLocationInstant(false), 2000);
  }

  // mapdetails() {
  //   this.props.navigation.navigate('MapDetailsScreen');
  // }

  googleAutocomplete = async (Location, curLat, curLong) => {
    const apiUrl =
      'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' +
      Location +
      '&key=' +
      Config.GOOGLE_MAPS_APIKEY +
      // '&components=country:in|country:ca' +
      '&location=' +
      curLat +
      ',' +
      curLong +
      '&radius=' +
      1000;
    // console.log(apiUrl);
    const result = await fetch(apiUrl);
    const json = await result.json();
    return json;
  };

  async onChangeSource(sourceLocation) {
    try {
      this.setState({
        tentativePrice: '',
        showSuggestionDest: false,
        sourceLocation,
      });
      var result = await this.googleAutocomplete(
        sourceLocation,
        this.state.curLatitude,
        this.state.curLongitude,
      );
      this.setState({
        predictions: result.predictions,
        showSuggestion: true,
      });
      // var adress_data = result.predictions;
      // this.setState({
      //   myaddress_list: adress_data,
      // });
      if (result.predictions.length == 0) {
        this.setState({
          showSuggestion: false,
        });
      }
    } catch (err) {
      // console.error(err);
    }
  }

  async onChangeDestination(destinationLocation) {
    try {
      this.setState({
        tentativePrice: '',
        showSuggestion: false,
        destinationLocation,
      });
      var result = await this.googleAutocomplete(
        destinationLocation,
        this.state.curLatitude,
        this.state.curLongitude,
      );

      this.setState({
        predictionsdest: result.predictions,
        showSuggestionDest: true,
      });

      if (result.predictions.length == 0) {
        this.setState({
          showSuggestionDest: false,
        });
      }
    } catch (err) {
      // console.error(err);
    }
  }

  setSourceLocation = (placeId, description) => {
    Keyboard.dismiss();
    this.setState({
      sourceLocation: description,
      showSuggestion: false,
      selSourcePlaceId: placeId,
    });
    //  console.log('asdadasd');
    Geocoder.from(description)
      .then(json => {
        var location = json.results[0].geometry.location;
        // console.log('Source location : ', location);
        this.setState({
          SourcePick: {
            address_components: json.results[0].address_components,
            formatted_address: json.results[0].formatted_address,
            place_id: json.results[0].place_id,
            startLocation: {
              lat: json.results[0].geometry.location.lat,
              lng: json.results[0].geometry.location.lng,
            },
          },
        });
        // this.setState({curLatitude: location.lat, curLongitude: location.lng});
      })
      .catch(error => console.log(error));
  };

  componentWillUnmount = () => {
    RtcEngine.destroy();
    Geolocation.clearWatch(this.watchID);
    Geolocation.stopObserving();
    this.didFocusListener.remove();
    AppState.removeEventListener('change', this.getUserState);
    fcmService.unRegister();
    localNotificationService.unregister();
  };

  getUserState = async nextAppState => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      //var currentTripId = this.props.ProfileData.currentTripId;
      var TripId = this.state.tripId;
      //console.log('adasdadadsadsas');
      if (TripId != null && TripId != '' && typeof TripId != undefined) {
        this.getTripDataRequest(TripId);
      }
    }
    this.setState({appState: nextAppState});
  };

  getCurrentLocation = async () => {
    this.watchID = await Geolocation.watchPosition(
      position => {
        let angle = position.coords.heading;
        this.setState({
          curLatitude: position.coords.latitude,
          curLongitude: position.coords.longitude,
          sourceLatitude: position.coords.latitude,
          sourceLongitude: position.coords.longitude,
          destLatitude: position.coords.latitude,
          destLongitude: position.coords.longitude,
          angle: angle,
          error: null,
        });
        setConfiguration('lat', position.coords.latitude);
        setConfiguration('long', position.coords.longitude);
        Geocoder.from(position.coords.latitude, position.coords.longitude)
          .then(json => {
            var addressComponent = json.results[0].formatted_address;
            this.setState({
              sourceLocation: addressComponent,
              selSourcePlaceId: json.results[0].place_id,
            });
          })
          .catch(error => console.log(error));
      },
      error => this.setState({error: error.message}),
      {
        enableHighAccuracy: false,
        timeout: 20000,
        maximumAge: 10000,
        distanceFilter: 100,
      },
    );
  };

  clearPickUpAddress() {
    this.setState({sourceLocation: '', showSuggestion: false});
  }

  cleardestinationAddress() {
    this.setState({destinationLocation: '', showSuggestionDest: false});
  }

  async setDestinationLocation(placeId, description) {
    Keyboard.dismiss();
    this.setState({
      destinationLocation: description,
      showSuggestionDest: false,
      destinationPlaceID: placeId,
    });

    Geocoder.from(description)
      .then(json => {
        var location = json.results[0].geometry.location;
        //  console.log('Destination location : ', location);

        this.setState(
          {
            dropOneway: {
              address_components: json.results[0].address_components,
              formatted_address: json.results[0].formatted_address,
              place_id: json.results[0].place_id,
              startLocation: {
                lat: json.results[0].geometry.location.lat,
                lng: json.results[0].geometry.location.lng,
              },
            },
          },
          () => {
            if (this.state.selSourcePlaceId.length > 0 && placeId.length > 0) {
              this.updateDropState();
            }
          },
        );
        this.setState({
          destlatt: location.lat,
          destlongi: location.lng,
        });
      })
      .catch(error => console.log(error));
  }

  async getData() {
    var pl = this.state.PaymentListData;

    if (pl != null && pl != '') {
      if (pl.length == 0) {
      } else {
        this.setState({
          cardList: pl,
          selectedCardNum: 'card',
          selectedCardImage: pl[0].logo,
          selectedCardID: pl[0]._id,
          PaymentVia: 'stripe',
        });

        // if (pl[0].type === 'Cash') {
        //   this.setState({
        //     cardList: pl,
        //     selectedCardNum: 'cash',
        //     selectedCardImage: pl[0].logo,
        //     selectedCardID: pl[0]._id,
        //     PaymentVia: 'cash',
        //   });
        // } else if (pl[1].type === 'Cash') {
        //   this.setState({
        //     cardList: pl,
        //     selectedCardNum: 'cash',
        //     selectedCardID: pl[1]._id,
        //     selectedCardImage: pl[1].logo,
        //     PaymentVia: 'cash',
        //   });
        // } else if (pl[0].type === 'Wallet') {
        //   this.setState({
        //     cardList: pl,
        //     selectedCardNum: 'wallet',
        //     selectedCardID: pl[0]._id,
        //     selectedCardImage: pl[0].logo,
        //     PaymentVia: 'wallet',
        //   });
        // }
      }
    }
  }

  getEstimateCost = (oneWayitem, dropOnewayitem, promoCode) => {
    // debugger;
    this.props.estimateRequest(
      oneWayitem,
      dropOnewayitem,
      promoCode,
      this.props.navigation,
    );
  };

  componentDidUpdate(prevProps, prevState) {
    // if (this.state.handlefocus == false) {
    //   this.ref.focus();
    // }
    if (prevProps.addTripData != this.props.addTripData) {
      this.setState({addTripData: this.props.addTripData}, () => {
        if (
          this.state.addTripData != '' &&
          this.state.addTripData != 'null' &&
          this.state.addTripData != null
        ) {
          var data = this.state.addTripData;
          //console.log(data);
          this.afterAddtripAp(data);
        } else {
        }
      });
    }
    if (prevProps.PaymentListData != this.props.PaymentListData) {
      this.setState({PaymentListData: this.props.PaymentListData}, () => {
        this.getData();
      });
    }

    if (prevProps.EstimateCostData != this.props.EstimateCostData) {
      this.setState({estimateCost: this.props.EstimateCostData}, () =>
        this.afterEstimateCost(),
      );
    }
    if (prevProps.getTripData != this.props.getTripData) {
      this.setState({getTripData: this.props.getTripData}, () => {
        var getTripData = this.state.getTripData;
        // console.log(getTripData);
        if (getTripData != null && getTripData != '') {
          this.GetUpdatedTripData(this.state.getTripData);
        }
      });
    }

    if (prevProps.ratingData != this.props.ratingData) {
      this.setState({ratingData: this.props.ratingData}, () => {
        var ratingData = this.state.ratingData;

        if (this.state.ratingData.status === 'success') {
          this.afterRating();
        }
      });
    }
    if (prevProps.abortTripData != this.props.abortTripData) {
      if (this.props.abortTripData.status === 'success') {
        // this.afterRating();
        this.afterAbortTrip();
      }
    }
    if (prevProps.panicData != this.props.panicData) {
      this.setState({panicData: this.props.panicData}, () => {
        // var aa = this.props.panicData;
        //   console, log('sdfsfsfs');
        if (this.state.panicData.status === 'success') {
          this.afterPanicSubmit(this.props.panicData);
        }
      });
    }
    if (prevProps.preferDriverData != this.props.preferDriverData) {
      this.setState({preferDriverData: this.props.preferDriverData}, () => {
        if (this.state.preferDriverData.status === 'success') {
          this.afterAddPreferDriver();
        }
      });
    }
    if (prevProps.cancelTripData != this.props.cancelTripData) {
      this.setState({cancelTripData: this.props.cancelTripData}, () => {
        if (this.state.cancelTripData.status === 'success') {
          this.afterCancelTrip();
        }
      });
    }
    if (prevProps.couponData != this.props.couponData) {
      this.setState({couponData: this.props.couponData}, () => {
        //console.log('dsasdasad');
        if (this.props.couponData.status === 'success') {
          this.afterValidateCoupon();
        } else if (this.props.couponData.status === 'failure') {
          this.invalidCoupon(this.props.couponData);
        }
      });
    }
  }

  GetUpdatedTripData = data => {
    //  console.log(data);
    //debugger;
    if (data.tripStatus == Config.TRIP_COMPLETED) {
      if (data.review.driverRating === 0) {
        this.setState(
          {
            tripStatus: data.tripStatus,
            ratingValue: 0,
            tripId: data._id,
            estimatedCost: data.estimatedCost,
            sourceLocation: '',
            destinationLocation: '',
            ChildVideo: false,
            joinSucceed: false,
            panicStatus: false,
            peerIds: [],
            ScheduleSetup: false,
            isShared: false,
            shareView: false,
          },
          () => {
            this.getCurrentLocation();
          },
        );
      }
    }
    if (
      data.tripStatus == Config.TRIP_PICKUP_INROUTE ||
      data.tripStatus == Config.TRIP_ARRIVED ||
      data.tripStatus == Config.TRIP_DESTINATION_INROUTE
    ) {
      var tripId = data._id;
      var sourceLat = data.startLocation.coordinates[1];
      var sourceLng = data.startLocation.coordinates[0];
      var destinationLat = data.endLocation.coordinates[1];
      var destinationLng = data.endLocation.coordinates[0];
      var driverImage = data.driverImage;
      var driverName = data.driverName;
      var tripOTP = data.tripOTP;
      console.log('----OTP----', tripOTP);
      var driveId = data.driverId;
      var prefferedDriver = data.prefDriver;
      var isPreff = false;
      if (
        prefferedDriver == 'no' ||
        typeof prefferedDriver == undefined ||
        prefferedDriver == null ||
        prefferedDriver == false
      ) {
      } else {
        isPreff = true;
      }
      this.setState({
        sourceLocationCord: {latitude: sourceLat, longitude: sourceLng},
        destinationLocationCord: {
          latitude: destinationLat,
          longitude: destinationLng,
        },
        endLocationAddr: data.endLocationAddr,
        driverImage: driverImage,
        driverName: driverName,
        carNumber: '',
        tripOTP: tripOTP,
        driverMobileCode: data.countryCode,
        tripId: tripId,
        estimatedCost: data.estimatedCost,
        driverMobileNumber: data.driverNumber,
        tripStatus: data.tripStatus,
        FindDriver: false,
        NoDriverFoundView: false,
        shareMessage:
          'I am riding with ' +
          driverName +
          ' with car number : ' +
          data.driverRefId.selectedCar.plateNumber,
        driverId: driveId,
        preferredDone: isPreff,
        showitem: false,
        plateNumber: data.driverRefId.selectedCar.plateNumber,
      });
      if (data.tripStatus == Config.TRIP_DESTINATION_INROUTE) {
        this.setState({
          rideTyp: data.rideType,
        });
      }
    }
    if (data.tripStatus == Config.TRIP_CANCELLED) {
      //  var aa = this.state.tripStatus;
      if (this.state.tripStatus != 'fresh') {
        setTimeout(() => {
          Alert.alert(
            strings.CancelTrip,
            strings.canceledByDriver,
            [{text: strings.Ok, onPress: () => {}}],
            {cancelable: false},
          );
        }, 600);
      }
      this.setState(
        {
          ChildVideo: false,
          joinSucceed: false,
          peerIds: [],
          tripData: '',
          tripStatus: 'fresh',
          selectedRide: 'standard',
          promoCode: '',
          showcoupon: true,
          ratingValue: 0,
          NoDriverFoundView: false,
          FindDriver: false,
          sourceLocation: '',
          ScheduleSetup: false,
          isShared: false,
          shareView: false,
        },
        () => {
          setTimeout(() => this.getCurrentLocation(), 2000);
          this.getCurrentLocationInstant(false);
        },
      );
      //  console.log('asdadasd');
    }
  };
  afterEstimateCost() {
    var date = new Date().getDate(); //Current Date
    var month = new Date().getMonth() + 1; //Current Month
    var year = new Date().getFullYear(); //Current Year
    var dateNew = date + '-' + month + '-' + year;

    var ar = this.state.estimateCost;
    this.setState({
      minDateSel: dateNew,
      curLat: this.state.curLatitude,
      curLong: this.state.curLongitude,
      curAddress: this.state.sourceLocation,
      destLat: this.state.destlatt,
      destLong: this.state.destlongi,
      destAddress: this.state.destinationLocation,
      distance: this.state.distance,
      estimatedTime: this.state.estimatedTime,
      driverId: this.state.driverIdPref,
      prefDriverRide: this.state.prefDriverRide,
      showitem: true,
      tripStatus: 'booking',
      AddTripArray: ar,
      showCurLat: ar[0].startLocation.lat,
      showCurLng: ar[0].startLocation.lng,
      showdestLat: ar[0].endLocation.lat,
      ShowdestLng: ar[0].endLocation.lng,
    });
    this.state.estimationCostArr = [];
    const portfolioArray = ar.map(banner => {
      this.state.estimationCostArr.push(banner);
    });

    //debugger;
    // console.log('asdad');
    this.setState({
      bikeSelected: this.state.estimationCostArr[0]._id,
      estimatedCost: this.state.estimationCostArr[0].estimatedCost,
    });
    setTimeout(
      () =>
        this.setState({
          sourceLocation: '',
          destinationLocation: '',
          showSuggestion: false,
          showSuggestionDest: false,
          panicStatus: false,
        }),
      2000,
    );
  }

  // messageDriver() {}

  openSmsUrl = (phone, body) => {
    return this.openUrl(`sms:${phone}${this.getSMSDivider()}body=${body}`);
  };
  openUrl = url => {
    return Linking.openURL(url);
  };
  getSMSDivider = () => {
    return Platform.OS === 'ios' ? '&' : '?';
  };
  messageDriver = () => {
    let numberWithCode =
      this.state.driverMobileCode + this.state.driverMobileNumber;
    let driverMessage = 'Hello ' + this.state.driverName + ', ';
    this.openSmsUrl(numberWithCode, driverMessage);
  };

  // messageDriver = () => {
  //   let numberWithCode =
  //     this.state.driverMobileCode + this.state.driverMobileNumber;
  //   let driverMessage = 'Hello ' + this.state.driverName + ', ';
  //   // console.log(numberWithCode);
  //   SendSMS.send(
  //     {
  //       body: driverMessage,
  //       recipients: [numberWithCode],
  //       successTypes: ['sent', 'queued'],
  //     },

  //     (completed, cancelled, error) => {
  //       if (completed) {
  //         Alert.alert('SMS Sent Successfully.');
  //       } else if (cancelled) {
  //         //console.log('SMS Sent Cancelled.');
  //       } else if (error) {
  //         // console.log('Some error occured.');
  //       }
  //     },
  //   );
  // };

  cancelRide() {
    this.callCancelTrip();
  }

  callCancelTrip() {
    let data = {
      tripId: this.state.tripId,
    };
    this.props.cancelTripRequest(data, this.props.navigation);
  }

  afterAbortTrip = () => {
    this.setState(
      {
        ChildVideo: false,
        joinSucceed: false,
        panicStatus: false,
        peerIds: [],
        tripData: '',
        tripStatus: 'fresh',
        promoCode: '',
        showcoupon: true,
        selectedRide: 'standard',
        ratingValue: 0,
        NoDriverFoundView: false,
        FindDriver: false,
        sourceLocation: '',
      },
      () => {
        setTimeout(() => this.getCurrentLocation(), 2000);
        this.getCurrentLocationInstant(false);
      },
    );
  };

  afterCancelTrip() {
    this.setState({
      tripData: '',
      tripStatus: 'fresh',
      promoCode: '',
      showcoupon: true,
      selectedRide: 'standard',
    });
    this.getCurrentLocationInstant(false);
  }

  shareDetail() {
    // console.log('share detail');
    this.onShare();
  }

  submitReview() {
    this.submitRating();
  }

  rating(value) {
    this.setState({ratingValue: value});
  }

  submitRating() {
    var rating = this.state.ratingValue;
    if (!isEmpty(rating) || rating == 0) {
      return false;
    }
    let data = {
      tripId: this.state.tripId,
      driverRating: rating,
    };
    if (
      data.tripId != null &&
      data.tripId != undefined &&
      data.tripId != 'undefined' &&
      data.tripId != ''
    ) {
      //console.log(data);
      this.props.ratingRequest(data, this.props.navigation);
    } else {
      // this.afterRating();
      // this.props.cancelRatingRequest();
    }
  }

  afterRating() {
    //RtcEngine.destroy();
    RtcEngine.leaveChannel();

    this.setState(
      {
        ChildVideo: false,
        joinSucceed: false,
        panicStatus: false,
        peerIds: [],
        tripData: '',
        tripStatus: 'fresh',
        promoCode: '',
        showcoupon: true,
        selectedRide: 'standard',
        ratingValue: 0,
        NoDriverFoundView: false,
        FindDriver: false,
        sourceLocation: '',
      },
      () => {
        setTimeout(() => this.getCurrentLocation(), 2000);
        this.getCurrentLocationInstant(false);
      },
    );
  }
  goToDashBoard() {
    this.setState({
      NoDriverFoundView: false,
      tripStatus: 'fresh',
      FindDriver: false,
      showitem: false,
      promoCode: '',
      showcoupon: true,
      location: '',
      destinationLocation: '',
      destinationPlaceID: '',
    });
    setTimeout(() => this.getCurrentLocation(), 2000);
    setTimeout(() => this.getCurrentLocationInstant(), 2000);
    //this.focus();
  }

  panicButtonAction() {
    if (this.state.panicStatus == false) {
      this.submitPanic();
    }
  }
  // refreshUser = () => {
  //   var TripId = this.state.tripId;
  //   if (TripId != null && TripId != '' && typeof TripId != undefined) {
  //     this.getTripDataRequest(TripId);
  //   }
  // };

  refreshUser = () => {
    // var aa = this.state.tripId;
    // debugger;
    var TripId = this.state.tripId;
    if (TripId != null && TripId != '' && typeof TripId != undefined) {
      var data = {tripId: TripId};
      this.props.abortTripRequest(data, this.props.navigation);
    }
  };

  refreshUserAction() {
    Alert.alert(
      'Alert!',
      'Are you sure you want to abort trip?',
      [
        {text: 'Ok', onPress: () => this.refreshUser()},
        {text: 'Cancel', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
  }
  submitPanic() {
    let data = {
      tripId: this.state.tripId,
    };
    this.props.panicRequest(data, this.props.navigation);
  }

  afterPanicSubmit(data) {
    this.setState({panicStatus: true});
  }

  noAction() {
    this.setState({preferredClick: false});
  }

  yesAction = () => {
    let data = {
      driverId: this.state.driverId,
    };
    this.props.preferDriverRequest(data, this.props.navigation);
  };

  afterAddPreferDriver() {
    this.setState({
      preferredClick: false,
      preferredDone: true,
    });
  }

  preferredAction() {
    this.setState({preferredClick: true});
  }

  getCurrentLocationInstant = status => {
    let sourceLoc = this.state.sourceLocation;
    let destLoc = this.state.destinationLocation;
    Geocoder.from(this.state.curLatitude, this.state.curLongitude)
      .then(json => {
        if (
          sourceLoc == '' ||
          sourceLoc == null ||
          sourceLoc == 'null' ||
          typeof sourceLoc == undefined ||
          !status
        ) {
          this.setState({
            SourcePick: {
              address_components: json.results[0].address_components,
              formatted_address: json.results[0].formatted_address,
              place_id: json.results[0].place_id,
              startLocation: {
                lat: json.results[0].geometry.location.lat,
                lng: json.results[0].geometry.location.lng,
              },
            },
            sourceLocation: json.results[0].formatted_address,
            selSourcePlaceId: json.results[0].place_id,
          });
        } else if (
          destLoc == '' ||
          destLoc == null ||
          destLoc == 'null' ||
          typeof destLoc == undefined
        ) {
          if (status) {
            var location = json.results[0].geometry.location;
            this.setState(
              {
                dropOneway: {
                  address_components: json.results[0].address_components,
                  formatted_address: json.results[0].formatted_address,
                  place_id: json.results[0].place_id,
                  startLocation: {
                    lat: json.results[0].geometry.location.lat,
                    lng: json.results[0].geometry.location.lng,
                  },
                },
                destinationLocation: json.results[0].formatted_address,
                destinationPlaceID: json.results[0].place_id,
                destlatt: location.lat,
                destlongi: location.lng,
              },
              () => {
                if (
                  this.state.selSourcePlaceId.length > 0 &&
                  json.results[0].place_id.length > 0
                ) {
                  this.updateDropState();
                }
              },
            );
          }
        }
      })
      .catch(error => error);
  };

  ApplyCoupon = () => {
    if (this.state.promoCode.trim() == '') {
      this.showAlert(strings.EnterPromocode, 300);
    } else {
      this.validateCoupon();
    }
  };

  ApplyConfirm = () => {
    if (this.state.ScheduleSetup == true) {
      var sdate = this.state.date;
      var stime = this.state.Time;

      this.AddtripAp(sdate, stime, 'schedule');
    } else {
      if (this.state.selectedCardNum == 'wallet') {
        var walletBalance = this.props.ProfileData.walletCredit;
        if (
          walletBalance == null ||
          walletBalance == '' ||
          typeof walletBalance == undefined
        ) {
          walletBalance = 0;
        }
        var addTripBalance = this.state.AddTripArray[0].estimatedCost;
        if (addTripBalance > walletBalance) {
          setTimeout(() => {
            Alert.alert(
              strings.Alert,
              strings.insufficientBalance,
              [{text: strings.Ok, onPress: () => {}}],
              {cancelable: false},
            );
          }, 600);
          return false;
        }
      }
      this.AddtripAp('03-04-2020', '04:45', 'instant');
    }
  };
  applyShare = () => {
    this.AddtripAp('03-04-2020', '04:45', 'pool');
  };
  AddtripAp(dat, tim, trptyp) {
    var addTrip = this.state.AddTripArray[0];
    var estCost = this.state.estimatedCost;
    //debugger;
    const {
      selectedRide,
      prefDriverRide,
      bikeSelected,
      PaymentVia,
      selectedCardID,
      SourcePick,
      dropOneway,
      TimeZoneSelected,
    } = this.state;
    let promoCode = '';
    if (!this.state.showcoupon) {
      promoCode = this.state.promoCode;
    }
    let noOfSeats = 0;
    if (this.state.isShared && trptyp === 'pool') {
      if (this.state.isSingle) {
        noOfSeats = 1;
      } else if (this.state.isMultiple) {
        noOfSeats = 2;
      }
    }

    if (trptyp == 'instant') {
      this.props.addTripRequest(
        selectedRide,
        trptyp,
        prefDriverRide,
        addTrip.country,
        addTrip.countryCode,
        addTrip.state,
        addTrip.stateCode,
        addTrip.city,
        addTrip.cityCode,
        addTrip.pricingType,
        addTrip.isSurgeTime,
        addTrip.surgeMultiplier,
        addTrip.unit,
        addTrip.distance,
        addTrip.estimatedTime,
        estCost,
        bikeSelected,
        PaymentVia,
        selectedCardID,
        SourcePick,
        dropOneway,
        TimeZoneSelected,
        '',
        '',
        promoCode,
        noOfSeats,
        this.props.navigation,
      );
    } else {
      this.props.addTripRequest(
        selectedRide,
        trptyp,
        prefDriverRide,
        addTrip.country,
        addTrip.countryCode,
        addTrip.state,
        addTrip.stateCode,
        addTrip.city,
        addTrip.cityCode,
        addTrip.pricingType,
        addTrip.isSurgeTime,
        addTrip.surgeMultiplier,
        addTrip.unit,
        addTrip.distance,
        addTrip.estimatedTime,
        estCost,
        bikeSelected,
        PaymentVia,
        selectedCardID,
        SourcePick,
        dropOneway,
        TimeZoneSelected,
        dat,
        tim,
        promoCode,
        noOfSeats,
        this.props.navigation,
      );
    }
  }

  // errorAddingTrip(e) {
  //  // console.log('hi here is error:', e);
  //   if (e.message == 'No Drivers Found!') {
  //     this.setState({NoDriverFoundView: true});
  //   } else {
  //     this.showAlert(e.message);
  //   }
  // }

  scheduleDone() {
    this.setState({
      showitem: false,
      tripStatus: 'fresh',
      promoCode: '',
      showcoupon: true,
    });
    this.getCurrentLocationInstant(false);
  }
  startRequestTimer = () => {
    var count = '0';
    this.intervalId = BackgroundTimer.runBackgroundTimer(() => {
      count = (Number(count) + 1).toString();
      if (count === '30') {
        this.stopRequestTimer();
      }
    }, 1000);
  };

  stopRequestTimer = async () => {
    await BackgroundTimer.stopBackgroundTimer(this.intervalId);
    this.uiUpdateCancel();
  };

  afterAddtripAp = data => {
    //  console.log(data);
    if (data.trip_type === 'instant' || data.trip_type === 'pool') {
      if (data.isDriverFound === 'yes') {
        this.startRequestTimer();
        this.setState({
          showitem: false,
          shareView: false,
          FindDriver: true,
          tripStatus: 'fresh',
          promoCode: '',
          showcoupon: true,
          isShared: false,
          isSingle: true,
          isMultiple: false,
        });
        //setTimeout(() => this.uiUpdateCancel(), 20000);
      } else {
        // this.uiUpdateCancel();
        this.setState({
          FindDriver: false,
          NoDriverFoundView: true,
          promoCode: '',
          showcoupon: true,
          shareView: false,
          isShared: false,
          isSingle: true,
          isMultiple: false,
        });
      }
    } else {
      if (this.state.ScheduleSetup == true) {
        setTimeout(() => {
          Alert.alert(
            strings.Success,
            strings.yourRideSuccess,
            [{text: strings.Ok, onPress: () => this.scheduleDone()}],
            {cancelable: false},
          );
        }, 600);
        this.ApplyScheduleDone();
      }
    }
  };

  ApplySchedule = () => {
    this.setState({
      showitem: true,
      Schedule: true,
      ScheduleSetup: false,
    });
  };

  ApplyScheduleDone = () => {
    //  console.log('asda');
    this.setState({
      Schedule: false,
      ScheduleSetup: false,
      date: '',
      Time: '',
      tripStatus: 'fresh',
      showitem: false,
    });
  };

  CallDriver = () => {
    this.setState({
      driverdetails: true,
    });
  };

  bikeSel(item) {
    this.setState({
      bikeSelected: item._id,
      estimatedCost: item.estimatedCost,
      isShared: false,
    });
  }
  shareSel(item) {
    this.setState({
      bikeSelected: item._id,
      estimatedCost: item.estimatedCost,
      isShared: true,
    });
  }
  tapCard() {
    this.setState({paymentTap: !this.state.paymentTap});
    //console.log('payment tap:', this.state.paymentTap);
  }
  cardSelectAction = item => {
    // console.log('---item---', item);
    this.setState({
      selectedCardNum: item.lastd,
      selectedCardID: item._id,
      selectedCardImage: item.logo,
      paymentTap: false,
      PaymentVia: 'stripe',
    });
  };
  payFromCashAction = item => {
    this.setState({
      selectedCardNum: 'cash',
      PaymentVia: 'cash',
      paymentTap: false,
      selectedCardImage: item.logo,
      selectedCardID: item._id,
    });
  };
  payFromWalletAction = item => {
    this.setState({
      selectedCardNum: 'wallet',
      PaymentVia: 'wallet',
      paymentTap: false,
      selectedCardImage: item.logo,
      selectedCardID: item._id,
    });
  };

  cardCancelHide() {
    this.setState({paymentTap: false});
  }

  showAlert(message, duration) {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      alert(message);
    }, duration);
  }
  validateCoupon() {
    let data = {
      promocode: this.state.promoCode,
    };
    if (this.state.showcoupon) {
      // this.showAlert(strings.alreadyApplied, 300);
      this.props.couponRequest(data, this.props.navigation);
    } else {
    }
  }

  invalidCoupon = data => {
    this.setState({promoCode: ''});
    this.showAlert(data.message, 600);
  };

  afterValidateCoupon() {
    this.setState(
      {
        showcoupon: false,
      },
      () => {
        var oneWayitem = this.state.SourcePick;
        var dropOnewayitem = this.state.dropOneway;
        var promoCode = this.state.promoCode;
        this.getEstimateCost(oneWayitem, dropOnewayitem, promoCode);
      },
    );
  }

  closeRide = () => {
    this.setState({
      showitem: true,
      Schedule: false,
      ScheduleSetup: false,
      date: '',
      Time: '',
    });
  };
  closeShare = () => {
    this.setState({
      showitem: true,
      shareView: false,
      ScheduleSetup: false,
    });
  };

  setPickUp() {
    // console.log('Time Selected:', this.state.Time);
    // console.log('Date Selected:', this.state.date);

    if (this.state.date == '') {
      Alert.alert(strings.SelectDate);
    } else if (this.state.Time == '') {
      Alert.alert('Select Time');
    } else {
      this.setState({
        showitem: true,
        Schedule: false,
        ScheduleSetup: true,
      });
    }
  }

  standardRideAction() {
    this.setState({selectedRide: 'standard', isShared: false});
  }

  childRideAction() {
    var item = this.state.estimationCostArr[0];
    this.setState({
      selectedRide: 'child',
      bikeSelected: item._id,
      isShared: false,
    });
  }

  rideDescriptionAction() {
    if (this.state.RideDes == true) {
      this.setState({RideDes: false});
    } else this.setState({RideDes: true});
  }
  submitDescription() {
    if (this.state.descriptionRideString == '') {
      Alert.alert(strings.Alert, strings.EnterDescription, [
        {text: strings.Ok},
      ]);
    } else {
      this.setState({RideDes: false});
    }
  }

  updateDropState = () => {
    if (
      this.state.sourceLocation == '' ||
      this.state.sourceLocation == null ||
      this.state.sourceLocation == 'null' ||
      typeof this.state.sourceLocation == undefined
    ) {
      setTimeout(() => {
        Alert.alert(
          strings.Alert,
          'Select pickup address',
          [{text: strings.Ok, onPress: () => this.refSource.focus()}],
          {cancelable: false},
        );
      }, 600);

      return false;
    }

    var oneWayitem = {
      startLocationAddr: this.state.SourcePick.formatted_address,
      startLocation: this.state.SourcePick.startLocation,
      addressComponents: this.state.SourcePick.address_components,
    };

    var dropOnewayitem = {
      endLocationAddr: this.state.dropOneway.formatted_address,
      endLocation: this.state.dropOneway.startLocation,
      addressComponents: this.state.dropOneway.address_components,
    };

    if (
      oneWayitem.addressComponents != null &&
      oneWayitem.addressComponents != '' &&
      typeof oneWayitem.addressComponents != undefined &&
      dropOnewayitem.endLocationAddr != '' &&
      dropOnewayitem.endLocationAddr != null &&
      typeof dropOnewayitem.endLocationAddr != undefined
    ) {
      if (
        oneWayitem.startLocation.lat == dropOnewayitem.endLocation.lat &&
        oneWayitem.startLocation.lng == dropOnewayitem.endLocation.lng
      ) {
        alert('Pickup address and destination cannot be same');
        return false;
      }
      var promoCode = '';
      this.setState({SourcePick: oneWayitem, dropOneway: dropOnewayitem}, () =>
        this.getEstimateCost(oneWayitem, dropOnewayitem, promoCode),
      );
    }
  };

  liveFeedAction() {
    this.makeConnection(this.state.tripId, this.state.uid);
  }

  makeConnection = (channel, id) => {
    RtcEngine.joinChannel(channel, id);
    RtcEngine.enableAudio(); //Join Channel
    this.setState({ChildVideo: true});
  };

  noOfSeats = (isSingle, isMultiple) => {
    if (isSingle) {
      this.setState({
        isSingle: true,
        isMultiple: false,
      });
    } else if (isMultiple) {
      this.setState({
        isSingle: false,
        isMultiple: true,
      });
    }
  };

  calculateDistance(objDistance) {
    var distance = objDistance.distance;
    var duration = parseFloat(objDistance.duration).toFixed(0);
    if (duration.length > 3) {
      duration = 0;
    }
    //  console.log('calculated distance is --- ', distance);
    setTimeout(
      () =>
        this.setState({
          distanceToStartLocation: parseFloat(distance).toFixed(2),
          timeToStartLocation: duration,
        }),
      500,
    );
  }

  onSourceRegionChange(sourceLocation) {
    Geocoder.from(sourceLocation.latitude, sourceLocation.longitude).then(
      json => {
        if (
          sourceLocation.latitude.toFixed(4) !=
            this.state.sourceLatitude.toFixed(4) &&
          sourceLocation.longitude.toFixed(4) !=
            this.state.sourceLatitude.toFixed(4)
        ) {
          this.setState({
            SourcePick: {
              address_components: json.results[0].address_components,
              formatted_address: json.results[0].formatted_address,
              place_id: json.results[0].place_id,
              startLocation: {
                lat: json.results[0].geometry.location.lat,
                lng: json.results[0].geometry.location.lng,
              },
            },
            sourceLocation: json.results[0].formatted_address,
            selSourcePlaceId: json.results[0].place_id,
            sourceLatitude: json.results[0].geometry.location.lat,
            sourceLongitude: json.results[0].geometry.location.lng,
            sourceLatDelta: sourceLocation.latitudeDelta,
            sourceLngDelta: sourceLocation.longitudeDelta,
            curLatitude: json.results[0].geometry.location.lat,
            curLongitude: json.results[0].geometry.location.lng,
          });
        }
      },
    );
  }

  onDestinationRegionChange(sourceLocation) {
    Geocoder.from(sourceLocation.latitude, sourceLocation.longitude).then(
      json => {
        var location = json.results[0].geometry.location;
        if (
          sourceLocation.latitude.toFixed(4) !=
            this.state.destLatitude.toFixed(4) &&
          sourceLocation.longitude.toFixed(4) !=
            this.state.destLongitude.toFixed(4)
        ) {
          this.setState({
            dropOneway: {
              address_components: json.results[0].address_components,
              formatted_address: json.results[0].formatted_address,
              place_id: json.results[0].place_id,
              startLocation: {
                lat: json.results[0].geometry.location.lat,
                lng: json.results[0].geometry.location.lng,
              },
            },
            destinationLocation: json.results[0].formatted_address,
            destLatitude: json.results[0].geometry.location.lat,
            destLongitude: json.results[0].geometry.location.lng,
            destLatDelta: sourceLocation.latitudeDelta,
            destLngDelta: sourceLocation.longitudeDelta,
            destlatt: location.lat,
            destlongi: location.lng,
          });
        }
      },
    );
  }

  setDestination = () => {
    var des = this.state.destinationLocation;
    if (des) {
      this.updateDropState();
    } else {
      this.setState({
        dropOffLocationModal: true,
      });
    }
  };
  serachSourceLocationOnMap() {
    return (
      <React.Fragment>
        <MapView
          zoomControlEnabled
          zoomEnabled={true}
          style={styles.mapView}
          onRegionChangeComplete={region => this.onSourceRegionChange(region)}
          region={{
            latitude: this.state.sourceLatitude,
            longitude: this.state.sourceLongitude,
            latitudeDelta: this.state.sourceLatDelta,
            longitudeDelta: this.state.sourceLngDelta,
          }}>
          <MapView.Marker
            style={{backgroundColor: 'transparent'}}
            ref={marker => {
              this.marker = marker;
            }}
            flat={true}
            coordinate={{
              latitude: 30.123456, //this.state.sourceLatitude,
              longitude: 76.123456, //this.state.sourceLongitude,
            }}>
            {/* <Image
              resizeMode="contain"
              source={Images.ic_address}
              style={{ height: 40, width: 40, tintColor: 'red' }}
            /> */}
          </MapView.Marker>
          <Image
            resizeMode="contain"
            source={Images.pinIcon}
            style={{
              height: 40,
              width: 40,
              position: 'absolute',
              top: heightCal / 2 - 52,
              left: widthCal / 2 - 20,
            }}
          />
        </MapView>
        {this.state.tripStatus == 'fresh' || this.state.tripStatus != 'booking'
          ? this._showHeaderView()
          : null}
        <Callout style={styles.sourceDestMain}>
          <Image
            resizeMode="contain"
            style={styles.pickupLocationImg}
            source={Images.selected}
          />
          <TouchableOpacity
            onPress={() => {
              this.setSearchModal();
              // this.setState({pickUpLocationModal: true});
            }}
            style={styles.searchMain}>
            <Text
              style={{
                color: Colors.PrimaryColor,
                fontFamily: Fonts.Regular,
                fontSize: 16,
              }}>
              {strings.pickup_location}
            </Text>
            <View style={styles.searchSection}>
              <Text numberOfLines={1}>{this.state.sourceLocation}</Text>
            </View>
          </TouchableOpacity>
        </Callout>
        <Callout style={styles.locationGPRSMain}>
          <View style={{backgroundColor: 'white', width: '100%', padding: 20}}>
            <View style={{height: 20}} />
            <TouchableOpacity
              onPress={() => {
                this.state.sourceLocation != ''
                  ? this.setState({
                      sourceSearchMap: false,
                    })
                  : null;
              }}
              style={{
                padding: 10,
                backgroundColor:
                  this.state.sourceLocation != '' ? Colors.primary1 : 'grey',
                width: '90%',
                borderRadius: moderateScale(20),
                alignSelf: 'center',
              }}>
              <Text
                style={{
                  color: Colors.White,
                  fontSize: RFValue(16),
                  fontFamily: Fonts.Regular,
                  textAlign: 'center',
                }}>
                {strings.done}
              </Text>
            </TouchableOpacity>
          </View>
        </Callout>
      </React.Fragment>
    );
  }

  serachDestinationLocationOnMap() {
    return (
      <React.Fragment>
        <MapView
          zoomControlEnabled
          zoomEnabled={true}
          style={styles.mapView}
          onRegionChangeComplete={region =>
            this.onDestinationRegionChange(region)
          }
          region={{
            latitude: this.state.destLatitude,
            longitude: this.state.destLongitude,
            latitudeDelta: this.state.destLatDelta,
            longitudeDelta: this.state.destLngDelta,
          }}>
          <MapView.Marker
            style={{backgroundColor: 'transparent'}}
            ref={marker => {
              this.marker = marker;
            }}
            flat={true}
            coordinate={{
              latitude: 30.123456, //this.state.sourceLatitude,
              longitude: 76.123456, //this.state.sourceLongitude,
            }}>
            {/* <Image
              resizeMode="contain"
              source={Images.ic_address}
              style={{ height: 40, width: 40, tintColor: 'red' }}
            /> */}
          </MapView.Marker>
          <Image
            resizeMode="contain"
            source={Images.pinIcon}
            style={{
              height: 40,
              width: 40,
              position: 'absolute',
              top: heightCal / 2 - 52,
              left: widthCal / 2 - 20,
            }}
          />
        </MapView>
        {this.state.tripStatus == 'fresh' || this.state.tripStatus != 'booking'
          ? this._showHeaderView()
          : null}
        <Callout style={styles.sourceDestMain}>
          <Image
            resizeMode="contain"
            style={styles.pickupLocationImg}
            source={Images.selected}
          />
          <TouchableOpacity
            onPress={() => {
              this.setSearchModal();
              //  this.setState({pickUpLocationModal: true});
            }}
            style={styles.searchMain}>
            <Text
              style={{
                color: Colors.PrimaryColor,
                fontFamily: Fonts.Regular,
                fontSize: 16,
              }}>
              Drop Off
            </Text>
            <View style={styles.searchSection}>
              <Text numberOfLines={1}>{this.state.destinationLocation}</Text>
            </View>
          </TouchableOpacity>
        </Callout>
        <Callout style={styles.locationGPRSMain}>
          <View style={{backgroundColor: 'white', width: '100%', padding: 20}}>
            <View style={{height: 20}} />
            <TouchableOpacity
              disabled={this.state.destinationLocation != '' ? false : true}
              onPress={() => {
                this.state.destinationLocation != ''
                  ? this.setState({
                      destSearchMap: false,
                      //dropOffLocationModal: true,
                    })
                  : null;
              }}
              style={{
                padding: 10,
                backgroundColor:
                  this.state.destinationLocation != ''
                    ? Colors.primary1
                    : 'grey',
                width: '90%',
                borderRadius: moderateScale(20),
                alignSelf: 'center',
              }}>
              <Text
                style={{
                  color: Colors.White,
                  fontSize: RFValue(16),
                  fontFamily: Fonts.Regular,
                  textAlign: 'center',
                }}>
                {strings.done}
              </Text>
            </TouchableOpacity>
          </View>
        </Callout>
      </React.Fragment>
    );
  }
  setSearchModal = () => {
    this.setState({
      sourceSearchMap: false,
      destSearchMap: false,
    });
  };
  _showHeaderView() {
    return (
      <View
        style={{
          paddingTop: Platform.select({
            ios: getStatusBarHeight(),
            android: 20,
          }),
          flexDirection: 'row',
          justifyContent: 'space-between',
          position: 'absolute',
          width: '100%',
          paddingBottom: 20,
          paddingHorizontal: 20,
        }}>
        <TouchableOpacity
          onPress={() => {
            this.setSearchModal();
          }}
          style={{
            height: 40,
            width: 40,
            borderRadius: 20,
            backgroundColor: 'transparent',
            justifyContent: 'center',
          }}>
          {/* <VectorIcon
            style={{alignSelf: 'center'}}
            name={'menu'}
            groupName={'Entypo'}
            size={40}
            color={'black'}
          /> */}
          <Image
            source={Images.backIcon}
            style={{
              width: moderateScale(55),
              height: moderateScale(55),
              // marginLeft: moderateScale(5),
            }}
            resizeMode="contain"
          />
        </TouchableOpacity>

        {/* <TouchableOpacity
          onPress={() => this.openDrawerClick()}
          style={{
            height: 40,
            width: 40,
            borderRadius: 20,
            // borderWidth: 2,
            borderColor: 'black',
          }}>
          {this.props.loginData?.profileImage == '' ||
          this.props.loginData?.profileImage == 'null' ||
          this.props.loginData?.profileImage == null ||
          this.props.loginData?.profileImage == 'none' ? (
            <Image
              style={{height: '100%', width: '100%'}}
              resizeMode={'contain'}
              source={Images.dummyUser}
            />
          ) : (
            <Image
              style={{height: 40, width: 40, borderRadius: 20}}
              resizeMode={'cover'}
              source={{uri: this.props.loginData?.profileImage}}
            />
          )}
         
        </TouchableOpacity> */}
      </View>
    );
  }
  _showPickUpHeaderView() {
    return (
      <View
        style={{
          backgroundColor: 'white',
          shadowColor: '#000',
          width: '100%',
        }}>
        <View
          style={{
            paddingTop: Platform.select({
              ios: getStatusBarHeight(),
              //  android: 20,
            }),
            flexDirection: 'row',
            justifyContent: 'space-between',
            // paddingHorizontal: 10,
            alignItems: 'center',
          }}>
          <TouchableOpacity
            style={{alignSelf: 'center'}}
            onPress={() => this.setState({pickUpLocationModal: false})}>
            <Image
              style={{
                width: moderateScale(55),
                height: moderateScale(55),
                marginLeft: moderateScale(5),
              }}
              resizeMode={'contain'}
              source={Images.backIcon}
            />
          </TouchableOpacity>
          <Text
            style={{
              fontFamily: Fonts.Semibold,
              fontSize: RFValue(18),
              marginLeft: moderateScale(-15),
            }}>
            {strings.addPickup}
          </Text>
          <View />
        </View>
        <View style={{height: 20}} />
      </View>
    );
  }

  _showDropOffHeaderView() {
    return (
      <View
        style={{
          backgroundColor: 'white',
          width: '100%',
        }}>
        <View
          style={{
            paddingTop: Platform.select({
              ios: getStatusBarHeight(),
              // android: 20,
            }),
            flexDirection: 'row',
            justifyContent: 'space-between',
            // paddingHorizontal: 10,
            alignItems: 'center',
          }}>
          <TouchableOpacity
            style={{alignSelf: 'center'}}
            onPress={() => this.setState({dropOffLocationModal: false})}>
            <Image
              style={{
                width: moderateScale(55),
                height: moderateScale(55),
                marginLeft: moderateScale(5),
              }}
              resizeMode={'contain'}
              source={Images.backIcon}
            />
          </TouchableOpacity>
          <Text
            style={{
              fontFamily: Fonts.Semibold,
              fontSize: RFValue(18),
              marginLeft: moderateScale(-15),
            }}>
            {strings.addDropOff}
          </Text>
          <View />
        </View>
        <View style={{height: 20}} />
      </View>
    );
  }

  _showPickLocationModal() {
    const predictions = this.state.predictions.map((prediction, index) => (
      <TouchableOpacity
        style={styles.prediction}
        key={index}
        onPress={() =>
          this.setSourceLocation(prediction.place_id, prediction.description)
        }>
        <Text style={{margin: 10}} key={prediction.id}>
          {prediction.description}
        </Text>
      </TouchableOpacity>
    ));
    return (
      <React.Fragment>
        <View
          style={{
            flex: 1,
            backgroundColor: 'white',
            height: '100%',
            width: '100%',
            position: 'absolute',
          }}>
          <View style={styles.sourceDestMainShadow}>
            {this._showPickUpHeaderView()}
            <View
              style={{
                flexDirection: 'row',
                backgroundColor: 'white',
                width: '100%',
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: Colors.PrimaryColor,
                borderBottomWidth: 1.0,
                paddingLeft: moderateScale(10),
              }}>
              <Image
                resizeMode="contain"
                style={styles.pickupLocationImg}
                source={Images.selected}
              />
              <View style={styles.searchMain}>
                <Text
                  style={{
                    color: Colors.PrimaryColor,
                    fontFamily: Fonts.Regular,
                    fontSize: RFValue(16),
                  }}>
                  {strings.pickup_location}
                </Text>
                <View style={styles.searchSection}>
                  <TextInput
                    ref={refSource => {
                      this.refSource = refSource;
                    }}
                    placeholder={strings.Enterpickupaddress}
                    maxLength={35}
                    placeholderTextColor="gray"
                    onChangeText={sourceLocation =>
                      this.onChangeSource(sourceLocation)
                    }
                    value={this.state.sourceLocation}
                    style={styles.pickuplocation}
                  />
                  {this.state.sourceLocation.length == 0 ? null : (
                    <TouchableOpacity
                      style={styles.clearLocation}
                      onPress={() => this.clearPickUpAddress()}>
                      <Image
                        resizeMode="contain"
                        style={styles.searchIcon}
                        source={Images.iconCan}
                      />
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            </View>
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  sourceSearchMap: true,
                  pickUpLocationModal: false,
                })
              }
              style={{padding: 5}}>
              <Text
                style={{
                  textAlign: 'center',
                  marginTop: 5,
                  fontWeight: 'bold',
                  fontFamily: Fonts.Semibold,
                }}>
                {strings.select_on_map}
              </Text>
            </TouchableOpacity>
          </View>
          {this.state.showSuggestion ? (
            <View style={styles.suggestionSourcePickup}>{predictions}</View>
          ) : null}
          {/* {this.state.addressList.length > 0 ? ( */}
          {!this.state.showSuggestion ? (
            <View style={{marginTop: 10, padding: 30}}>
              {/* <TouchableOpacity
                onPress={() => this.props.navigation.navigate('AddAddress')}>
                <Text
                  style={{
                    fontSize: 22,
                    fontFamily: Fonts.Semibold,
                    color: 'rgba(0,0,0,0.85)',
                  }}>
                  Add Address +
                </Text>
              </TouchableOpacity> */}
              <FlatList
                data={this.state.addressList}
                renderItem={({item, index}) => {
                  return (
                    <TouchableOpacity
                      onPress={() => this.selectSourceAddressFromList(item)}
                      style={{
                        backgroundColor: 'white',
                        paddingVertical: 10,
                        borderColor: 'rgba(0,0,0,0.65)',
                        alignItems: 'center',
                        marginVertical: 5,
                        flexDirection: 'row',
                        borderBottomWidth: 0.5,
                      }}>
                      <Image
                        style={{height: 25, width: 25}}
                        resizeMode={'contain'}
                        source={Images.ic_address}
                      />
                      <View style={{marginLeft: 10}}>
                        <Text
                          style={{
                            fontSize: 16,
                            color: Colors.PrimaryColor,
                            fontFamily: Fonts.Regular,
                          }}>
                          {item.name}
                        </Text>
                        <Text
                          style={{
                            fontSize: 16,
                            color: 'rgba(0,0,0,0.65)',
                            fontFamily: Fonts.Regular,
                          }}>
                          {item.address.formatted_address}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  );
                }}
              />
            </View>
          ) : null}
          {/*  ) : null} */}
        </View>
        <TouchableOpacity
          style={styles.bottom}
          disabled={this.state.sourceLocation != '' ? false : true}
          onPress={() => {
            this.state.sourceLocation != ''
              ? this.setState({
                  pickUpLocationModal: false,
                })
              : null;
          }}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            backgroundColor:
              this.state.sourceLocation != '' ? Colors.primary1 : 'grey',
            height: moderateScale(45),
            marginHorizontal: moderateScale(20),
            borderRadius: moderateScale(30),
            bottom: moderateScale(25),
            width: '90%',
          }}>
          <Text style={styles.button}>{strings.done}</Text>
        </TouchableOpacity>
      </React.Fragment>
    );
  }

  _showDropOffLocationModal() {
    const predictionsDest = this.state.predictionsdest.map(
      (predictionsDest, index) => (
        <TouchableOpacity
          style={styles.prediction}
          key={index}
          onPress={() =>
            this.setDestinationLocation(
              predictionsDest.place_id,
              predictionsDest.description,
            )
          }>
          <Text style={{margin: 10}} key={predictionsDest.id}>
            {predictionsDest.description}
          </Text>
        </TouchableOpacity>
      ),
    );
    return (
      <React.Fragment>
        <View
          style={{
            flex: 1,
            backgroundColor: 'white',
            height: '100%',
            width: '100%',
            position: 'absolute',
          }}>
          <View style={styles.sourceDestMainShadow}>
            {this._showDropOffHeaderView()}
            <View
              style={{
                flexDirection: 'row',
                backgroundColor: 'white',
                width: '100%',
                alignItems: 'center',
                borderColor: Colors.PrimaryColor,
                borderBottomWidth: 1.0,
                paddingLeft: moderateScale(10),
              }}>
              <Image
                resizeMode="contain"
                style={styles.dropLocationImg}
                source={Images.selected}
              />
              <View
                style={{
                  width: wp('80%'),
                  marginLeft: 10,
                  backgroundColor: 'white',
                }}>
                <Text
                  style={{
                    color: Colors.PrimaryColor,
                    fontFamily: Fonts.Regular,
                    fontSize: RFValue(16),
                    // top: 10,
                  }}>
                  {strings.dropOff_location}
                </Text>
                <View style={styles.searchSection}>
                  <TextInput
                    ref={refSource => {
                      this.refSource = refSource;
                    }}
                    placeholder={'Enter drop off location...'}
                    maxLength={35}
                    placeholderTextColor="gray"
                    onChangeText={sourceLocation =>
                      this.onChangeDestination(sourceLocation)
                    }
                    value={this.state.destinationLocation}
                    style={styles.pickuplocation}
                  />
                  {this.state.destinationLocation.length == 0 ? null : (
                    <TouchableOpacity
                      style={styles.clearLocation}
                      onPress={() => this.cleardestinationAddress()}>
                      <Image
                        resizeMode="contain"
                        style={styles.searchIcon}
                        source={Images.iconCan}
                      />
                    </TouchableOpacity>
                  )}
                </View>
              </View>
              {/* <TouchableOpacity onPress={() => this.setState({addDrop: true})}>
                <Image
                  resizeMode="contain"
                  style={{height: 25, width: 25}}
                  source={Images.addIcon}
                />
              </TouchableOpacity> */}
            </View>
            <TouchableOpacity
              onPress={() =>
                this.setState({
                  destSearchMap: true,
                  dropOffLocationModal: false,
                })
              }
              style={{padding: 5}}>
              <Text
                style={{
                  textAlign: 'center',
                  marginTop: 5,
                  fontWeight: 'bold',
                  fontFamily: Fonts.Semibold,
                }}>
                {strings.select_on_map}
              </Text>
            </TouchableOpacity>

            {/* <FlatList
              data={this.state.stopages}
              renderItem={({item, index}) => {
                return (
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      height: 'auto',
                      height: 60,
                      marginHorizontal: 10,
                      flex: 1,
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        width: '90%',
                      }}>
                      <Image
                        style={{width: 20, width: 20}}
                        resizeMode={'contain'}
                        source={Images.ic_address}
                      />
                      <View style={{marginLeft: 10}}>
                        <Text
                          style={{
                            color: Colors.PrimaryColor,
                            fontFamily: Fonts.Semibold,
                          }}>
                          Drop Off {index + 1}
                        </Text>
                        <Text>{item.endLocationAddr}</Text>
                      </View>
                    </View>
                    <TouchableOpacity
                      style={{marginLeft: 10, alignSelf: 'center'}}
                      onPress={() => this._removeStopage(item, index)}>
                      <Image
                        style={{width: 18, width: 18, tintColor: 'red'}}
                        resizeMode={'contain'}
                        source={Images.cancel}
                      />
                    </TouchableOpacity>
                  </View>
                );
              }}
            /> */}
          </View>
          {this.state.showSuggestionDest ? (
            <View style={styles.suggestionSourcePickup}>{predictionsDest}</View>
          ) : null}
          {this.state.addressList.length > 0 ? (
            !this.state.showSuggestionDest ? (
              <View style={{marginTop: 10, padding: 30}}>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate('AddAddress')}>
                  <Text
                    style={{
                      fontSize: 22,
                      fontFamily: Fonts.Semibold,
                      color: 'rgba(0,0,0,0.85)',
                    }}>
                    Add Address +{/* Select from my places */}
                  </Text>
                </TouchableOpacity>
                <FlatList
                  data={this.state.addressList}
                  renderItem={({item, index}) => {
                    return (
                      <TouchableOpacity
                        onPress={() => this.selectDestAddressFromList(item)}
                        style={{
                          backgroundColor: 'white',
                          paddingVertical: 10,
                          borderColor: 'rgba(0,0,0,0.65)',
                          alignItems: 'center',
                          marginVertical: 5,
                          flexDirection: 'row',
                          borderBottomWidth: 0.5,
                        }}>
                        <Image
                          style={{height: 25, width: 25}}
                          resizeMode={'contain'}
                          source={Images.ic_address}
                        />
                        <View style={{marginLeft: 10}}>
                          <Text
                            style={{
                              fontSize: 16,
                              color: Colors.PrimaryColor,
                              fontFamily: Fonts.Regular,
                            }}>
                            {item.name}
                          </Text>
                          <Text
                            style={{
                              fontSize: 16,
                              color: 'rgba(0,0,0,0.65)',
                              fontFamily: Fonts.Regular,
                            }}>
                            {item.address.formatted_address}
                          </Text>
                        </View>
                      </TouchableOpacity>
                    );
                  }}
                />
              </View>
            ) : null
          ) : null}
          {/* {this.state.addDrop ? (
            <View
              style={{
                position: 'absolute',
                width: '100%',
                backgroundColor: 'transparent',
                bottom: 0,
                padding: 30,
              }}>
              <TouchableOpacity
                onPress={() => this.goToBeforeBooking()}
                style={{
                  alignSelf: 'center',
                  padding: 10,
                  alignItems: 'center',
                  backgroundColor: Colors.PrimaryColor,
                  width: '90%',
                  borderRadius: 10,
                }}>
                <Text
                  style={{
                    color: Colors.White,
                    fontSize: 18,
                    fontFamily: Fonts.Thin,
                    textAlign: 'center',
                  }}>
                  NEXT
                </Text>
              </TouchableOpacity>
            </View>
          ) : null} */}
        </View>
        <TouchableOpacity
          style={styles.bottom}
          disabled={this.state.destinationLocation != '' ? false : true}
          onPress={() => {
            this.state.destinationLocation != ''
              ? this.setState({
                  dropOffLocationModal: false,
                })
              : null;
          }}
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            position: 'absolute',
            backgroundColor:
              this.state.destinationLocation != '' ? Colors.primary1 : 'grey',
            height: moderateScale(45),
            marginHorizontal: moderateScale(20),
            borderRadius: moderateScale(30),
            bottom: moderateScale(25),
            width: '90%',
          }}>
          <Text style={styles.button}>{strings.done}</Text>
        </TouchableOpacity>
      </React.Fragment>
    );
  }
  freshRenderView = () => {
    return (
      <React.Fragment>
        <MapView
          zoomControlEnabled
          zoomEnabled={true}
          style={styles.mapView}
          showsUserLocation={true}
          region={{
            latitude: this.state.curLatitude,
            longitude: this.state.curLongitude,
            latitudeDelta: LATITUDE_DELTA,
            longitudeDelta: LONGITUDE_DELTA,
          }}>
          {markers.map((marker, index) => (
            <MapView.Marker
              key={index}
              coordinate={marker.coordinates}
              // coordinate={marker.coordinate}
              title={marker.title}>
              <Animated.Image
                resizeMode="contain"
                source={Images.dummyCar}
                style={{height: 40, width: 40}}
              />
            </MapView.Marker>
          ))}
        </MapView>
        {/* {this.state.immediateRideDialogue
          ? this.showImmediateRideAlert()
          : null} */}
        {/* {this.state.tripStatus == 'fresh' || this.state.tripStatus != 'booking'
          ? this._showHeaderView()
          : null} */}
        <Callout
          style={{
            width: '100%',
            marginTop:
              Platform.OS == 'android' ? moderateScale(75) : moderateScale(100),
          }}>
          <View style={styles.freshSourceDestMain}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                width: '100%',
              }}>
              <Image
                resizeMode="contain"
                style={styles.pickupLocationImg}
                source={Images.selected}
              />
              <TouchableOpacity
                onPress={() => {
                  this.setState({
                    pickUpLocationModal: true,
                  });
                }}
                style={styles.searchMain}>
                <Text style={styles.freshMapAddressStaticTxt}>
                  {strings.pickup_location}
                </Text>
                <View style={styles.searchSection}>
                  <Text numberOfLines={1}>{this.state.sourceLocation}</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.line} />
            {this.state.destinationLocation ? (
              <View style={{marginTop: moderateScale(20)}}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: '100%',
                  }}>
                  <Image
                    resizeMode="contain"
                    style={styles.dropLocationImg}
                    source={Images.selected}
                  />
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        dropOffLocationModal: true,
                      });
                    }}
                    style={styles.searchMain}>
                    <Text style={styles.freshMapAddressStaticTxt}>
                      {strings.dropOff_location}
                    </Text>
                    <View style={styles.searchSection}>
                      <Text numberOfLines={1}>
                        {this.state.destinationLocation}
                      </Text>
                    </View>
                  </TouchableOpacity>
                </View>
                <View style={styles.line} />
              </View>
            ) : null}
          </View>
        </Callout>
        <Callout style={styles.locationGPRSMain}>
          <TouchableOpacity
            style={styles.locationGPRS}
            onPress={() => {
              this.setState({immediateRideDialogue: false});
              this.getCurrentLocationInstant(true);
            }}>
            <Image
              resizeMode="stretch"
              style={styles.gprsImg}
              source={Images.gprs2}
            />
          </TouchableOpacity>
          {!this.state.FindDriver ? (
            <View
              style={{backgroundColor: 'white', width: '100%', padding: 20}}>
              <View style={{height: 20}} />
              <TouchableOpacity
                // onPress={() =>
                //   this.setState({
                //     dropOffLocationModal: true,
                //   })
                // }
                onPress={() => {
                  this.setDestination();
                }}
                style={{
                  padding: 10,
                  backgroundColor: Colors.PrimaryColor,
                  width: '90%',
                  borderRadius: moderateScale(30),
                  alignSelf: 'center',
                }}>
                <Text
                  style={{
                    color: Colors.White,
                    fontSize: RFValue(16),
                    fontFamily: Fonts.Regular,
                    textAlign: 'center',
                  }}>
                  {strings.next}
                </Text>
              </TouchableOpacity>
            </View>
          ) : null}
        </Callout>
      </React.Fragment>
    );
  };

  showItemMapRenderView = () => {
    return (
      <React.Fragment>
        <MapView
          //  provider={PROVIDER_GOOGLE}
          style={styles.mapView}
          zoomEnabled={true}
          initialRegion={{
            latitude: 30.704649,
            longitude: 76.717873,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
          region={{
            latitude: this.state.showCurLat,
            longitude: this.state.showCurLng,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}>
          <MapView.Marker
            style={{backgroundColor: 'transparent'}}
            ref={marker => {
              this.markerdriver = marker;
            }}
            flat={false}
            rotation={this.state.angle}
            coordinate={{
              latitude: this.state.showCurLat,
              longitude: this.state.showCurLng,
            }}>
            <Animated.Image
              tintColor="#15243a"
              resizeMode="contain"
              source={Images.pickShow}
              style={{height: 20, width: 16}}
            />
          </MapView.Marker>

          <MapView.Marker
            style={{backgroundColor: 'transparent'}}
            ref={marker => {
              this.markerdriver = marker;
            }}
            flat={false}
            rotation={this.state.angle}
            coordinate={{
              latitude: this.state.showdestLat,
              longitude: this.state.ShowdestLng,
            }}>
            <Animated.Image
              resizeMode="contain"
              source={Images.locationPin2}
              style={{height: 20, width: 16}}
            />
          </MapView.Marker>

          <MapViewDirections
            origin={{
              latitude: this.state.showCurLat,
              longitude: this.state.showCurLng,
            }}
            destination={{
              latitude: this.state.showdestLat,
              longitude: this.state.ShowdestLng,
            }}
            apikey={Config.GOOGLE_MAPS_APIKEY}
            strokeColor="black"
            strokeWidth={1}
            resetOnChange={true}
          />
        </MapView>
        <KeyboardAvoidingView
          behavior="padding"
          keyboardVerticalOffset={keyboardVerticalOffset}
          style={styles.showItemAvoidingView}>
          <View style={styles.showItemMainView}>
            {/* <TouchableOpacity
              style={styles.standardRide}
              onPress={() => this.standardRideAction()}>
              {this.state.selectedRide == 'standard' ? (
                <Image
                  resizeMode="contain"
                  style={styles.rideSelectIcon}
                  source={Images.selected}
                />
              ) : (
                <Image
                  resizeMode="contain"
                  style={styles.rideSelectIcon}
                  source={Images.unselected}
                />
              )}
              <Text style={styles.standardRideText}>
                {strings.StandaradRide}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.childRide}
              onPress={() => this.childRideAction()}>
              {this.state.selectedRide == 'child' ? (
                <Image
                  resizeMode="contain"
                  style={styles.rideSelectIcon}
                  source={Images.selected}
                />
              ) : (
                <Image
                  resizeMode="contain"
                  style={styles.rideSelectIcon}
                  source={Images.unselected}
                />
              )}
              <Text style={styles.childRideText}>{strings.ChildRide}</Text>
            </TouchableOpacity>
            {this.state.selectedRide == 'child' ? (
              <TouchableOpacity
                style={styles.rideDescription}
                onPress={() => this.rideDescriptionAction()}>
                <Text style={styles.rideDescriptionText}>
                  {strings.RideDescription}
                </Text>
              </TouchableOpacity>
            ) : null} */}
          </View>
          <View style={styles.showItemListMain}>
            <FlatList
              style={styles.showItemList}
              data={this.state.estimationCostArr}
              horizontal={true}
              scrollEnabled={false}
              renderItem={({item, index}) =>
                item.carType == 'Share' ? (
                  <TouchableOpacity
                    onPress={() => this.shareSel(item)}
                    disabled={
                      this.state.selectedRide === 'child' ||
                      this.state.ScheduleSetup
                        ? true
                        : false
                    }
                    style={{
                      width: widthCal / 3,
                      height: moderateScale(100),
                      opacity:
                        this.state.selectedRide === 'child' ||
                        this.state.ScheduleSetup
                          ? 0.3
                          : 0.9,
                      flexDirection: 'column',
                      backgroundColor: 'transparent',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    {this.state.bikeSelected == item._id &&
                    this.state.selectedRide != 'child' &&
                    !this.state.ScheduleSetup ? (
                      <Image
                        resizeMode="contain"
                        style={styles.checkedCarImg}
                        source={Images.rightImg}
                      />
                    ) : null}
                    <Image
                      source={{uri: item.carImage}}
                      resizeMode="cover"
                      style={styles.carImg}
                    />
                    <Text style={styles.carType}>{item.carType}</Text>
                    <Text style={styles.estimatedCostText}>
                      $ {item.estimatedCost}
                    </Text>
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity
                    onPress={() => this.bikeSel(item)}
                    style={{
                      width: widthCal / 3,
                      height: moderateScale(100),
                      flexDirection: 'column',
                      backgroundColor:
                        index == 0 ? 'transparent' : 'transparent',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    {this.state.bikeSelected == item._id ? (
                      <Image
                        resizeMode="contain"
                        style={styles.checkedCarImg}
                        source={Images.rightImg}
                      />
                    ) : null}
                    <Image
                      source={{uri: item.carImage}}
                      resizeMode="cover"
                      style={styles.carImg}
                    />
                    <Text style={styles.carType}>{item.carType}</Text>
                    <Text style={styles.estimatedCostText}>
                      $ {item.estimatedCost}
                    </Text>
                  </TouchableOpacity>
                )
              }
              keyExtractor={item => item._id}
            />
          </View>

          <View
            style={{
              borderBottomWidth: moderateScale(2),
              marginTop: moderateScale(5),
              borderBottomColor: '#CACDD4',
            }}
          />

          <View
            style={{
              height: moderateScale(40),
              marginTop: moderateScale(10),
              backgroundColor: 'white',
              alignItems: 'center',
              justifyContent: 'space-around',
              flexDirection: 'row',
            }}>
            <TouchableOpacity
              onPress={() => this.tapCard()}
              style={{
                flexDirection: 'row',
                backgroundColor: 'transparent',
              }}>
              {this.state.selectedCardNum === 'cash' ? (
                <Image
                  resizeMode="contain"
                  style={{
                    height: moderateScale(35),
                    width: moderateScale(35),
                    marginRight: moderateScale(20),
                    marginLeft: moderateScale(20),
                  }}
                  //source={Images.cash}
                  source={{uri: this.state.selectedCardImage}}
                />
              ) : this.state.selectedCardNum === 'wallet' ? (
                <Image
                  resizeMode="contain"
                  style={{
                    height: moderateScale(35),
                    width: moderateScale(35),
                    marginRight: moderateScale(20),
                    marginLeft: moderateScale(20),
                  }}
                  source={{uri: this.state.selectedCardImage}}
                />
              ) : (
                <Image
                  resizeMode="contain"
                  style={{
                    height: moderateScale(35),
                    width: moderateScale(35),
                    marginRight: moderateScale(20),
                    marginLeft: moderateScale(20),
                  }}
                  source={{uri: this.state.selectedCardImage}}
                />
              )}
              <Text style={{top: moderateScale(8), right: moderateScale(8)}}>
                {this.state.selectedCardNum}
              </Text>
              <Image
                resizeMode="contain"
                style={{
                  top: moderateScale(10),
                  width: moderateScale(12),
                  height: moderateScale(10),
                }}
                source={Images.downArrow}
              />
            </TouchableOpacity>

            {this.state.showcoupon ? (
              <React.Fragment>
                <View style={{backgroundColor: 'white'}}>
                  <TextInput
                    placeholder="Promo Code"
                    onChangeText={promoCode => this.setState({promoCode})}
                    value={this.state.promoCode}
                    autoCorrect={false}
                    autoCapitalize="none"
                    keyboardType="default"
                    returnKeyType="done"
                  />
                  <View
                    style={{
                      borderBottomWidth: moderateScale(2),
                      marginTop: moderateScale(5),
                      borderBottomColor: '#CACDD4',
                    }}
                  />
                </View>
                <View>
                  <TouchableOpacity onPress={this.ApplyCoupon}>
                    <Text style={{color: Colors.buttonGreen}}>
                      {strings.Apply}
                    </Text>
                  </TouchableOpacity>
                </View>
              </React.Fragment>
            ) : (
              <View>
                <Text style={{color: '#CACDD4', fontWeight: 'bold'}}>
                  {strings.PromoCodeApplied}
                </Text>
              </View>
            )}
          </View>

          <View
            style={{
              borderBottomWidth: moderateScale(2),
              marginTop: moderateScale(5),
              borderBottomColor: '#CACDD4',
            }}
          />

          <View
            style={{
              flexDirection: 'row',
              marginLeft: moderateScale(25),
              marginRight: moderateScale(25),
              marginTop: moderateScale(25),
              justifyContent: 'space-between',
              alignItems: 'center',
            }}>
            {this.state.isShared ? (
              <TouchableOpacity
                onPress={() => {
                  this.setState({shareView: true});
                }}
                style={{
                  width: '50%',
                  height: wp('12%'),
                  marginTop: 0,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginHorizontal: moderateScale(10),
                  borderRadius: moderateScale(27),
                  backgroundColor: Colors.Black,
                }}>
                <Text style={{color: 'white'}}>Next</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                onPress={this.ApplyConfirm}
                style={{
                  width: '50%',
                  height: wp('12%'),
                  marginTop: 0,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginHorizontal: moderateScale(10),
                  borderRadius: moderateScale(27),
                  backgroundColor: Colors.Black,
                }}>
                {this.state.ScheduleSetup == true ? (
                  <Text style={{color: 'white'}}>{strings.Schedule}</Text>
                ) : (
                  <Text style={{color: 'white'}}>Confirm</Text>
                )}
              </TouchableOpacity>
            )}

            {this.state.ScheduleSetup == false ? (
              <TouchableOpacity
                style={{
                  flexDirection: 'row',
                  opacity: this.state.isShared ? 0.3 : 0.9,
                }}
                disabled={this.state.isShared ? true : false}
                onPress={this.ApplySchedule}>
                <Image
                  resizeMode="contain"
                  style={{width: moderateScale(30), height: moderateScale(15)}}
                  source={Images.calender}
                />
                <Text>{strings.Schedule}</Text>
              </TouchableOpacity>
            ) : (
              <TouchableOpacity
                style={{flexDirection: 'row'}}
                onPress={this.ApplySchedule}>
                <Image
                  resizeMode="contain"
                  style={{width: moderateScale(30), height: moderateScale(15)}}
                  source={Images.calender}
                />
                <Text>{this.state.date + ' ' + this.state.Time}</Text>
              </TouchableOpacity>
            )}
          </View>
        </KeyboardAvoidingView>
      </React.Fragment>
    );
  };

  showItemRenderView = () => {
    return (
      <KeyboardAvoidingView
        behavior="padding"
        keyboardVerticalOffset={keyboardVerticalOffset}
        style={{
          height: moderateScale(360),
          width: '100%',
          backgroundColor: 'white',
          position: 'absolute',
          bottom: 0,
        }}>
        <View
          style={{
            width: '100%',
            // height: moderateScale(70),
            height: moderateScale(50),
            backgroundColor: 'white',
            flexDirection: 'row',
            // marginTop: moderateScale(10),
            marginTop: moderateScale(30),
          }}>
          <Text style={styles.standardRideText}>Select Car Type</Text>
          {/* <TouchableOpacity
            style={{
              width: '32%',
              marginLeft: moderateScale(10),
              height: moderateScale(40),
              backgroundColor: 'transparent',
              justifyContent: 'center',
              flexDirection: 'row',
              alignItems: 'center',
            }}
            onPress={() => this.standardRideAction()}>
            {this.state.selectedRide == 'standard' ? (
              <Image
                resizeMode="contain"
                style={{width: moderateScale(15), height: moderateScale(15)}}
                source={Images.selected}
              />
            ) : (
              <Image
                resizeMode="contain"
                style={{width: moderateScale(15), height: moderateScale(15)}}
                source={Images.unselected}
              />
            )}
            <Text
              style={{
                color: 'black',
                //fontWeight: 'bold',
                fontFamily: Fonts.Semibold,
                fontSize: RFValue(14),
                backgroundColor: 'transparent',
                marginLeft: moderateScale(10),
              }}>
              {strings.StandaradRide}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: '30%',
              marginLeft: moderateScale(10),
              height: moderateScale(40),
              backgroundColor: 'transparent',
              justifyContent: 'center',
              flexDirection: 'row',
              alignItems: 'center',
            }}
            onPress={() => this.childRideAction()}>
            {this.state.selectedRide == 'child' ? (
              <Image
                resizeMode="contain"
                style={{width: moderateScale(15), height: moderateScale(15)}}
                source={Images.selected}
              />
            ) : (
              <Image
                resizeMode="contain"
                style={{width: moderateScale(15), height: moderateScale(15)}}
                source={Images.unselected}
              />
            )}
            <Text
              style={{
                color: 'black',
                fontFamily: Fonts.Semibold,
                fontSize: RFValue(14),
                backgroundColor: 'transparent',
                marginLeft: moderateScale(10),
              }}>
              {strings.ChildRide}
            </Text>
          </TouchableOpacity>
          {this.state.selectedRide == 'child' ? (
            <TouchableOpacity
              style={{
                width: '32%',
                marginLeft: moderateScale(5),
                height: moderateScale(40),
                backgroundColor: 'transparent',
                justifyContent: 'center',
                flexDirection: 'row',
                alignItems: 'center',
              }}
              onPress={() => this.rideDescriptionAction()}>
              <Text
                style={{
                  color: Colors.iRed,
                  fontFamily: Fonts.Semibold,
                  fontWeight: 'bold',
                  fontSize: RFValue(14),
                  backgroundColor: 'transparent',
                  marginLeft: moderateScale(10),
                }}>
                {strings.RideDescription}
              </Text>
            </TouchableOpacity>
          ) : null} */}
        </View>
        <View
          style={{
            width: '100%',
            height: moderateScale(120),
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'row',
            marginLeft: '0%',
          }}>
          <FlatList
            style={{width: '100%', height: moderateScale(110)}}
            data={this.state.estimationCostArr}
            horizontal={true}
            scrollEnabled={false}
            renderItem={({item, index}) =>
              item.carType == 'Share' ? (
                <TouchableOpacity
                  onPress={() => this.shareSel(item)}
                  disabled={
                    this.state.selectedRide === 'child' ||
                    this.state.ScheduleSetup
                      ? true
                      : false
                  }
                  style={{
                    width: widthCal / 3,
                    height: moderateScale(100),
                    opacity:
                      this.state.selectedRide === 'child' ||
                      this.state.ScheduleSetup
                        ? 0.3
                        : 0.9,
                    flexDirection: 'column',
                    backgroundColor: index == 0 ? 'transparent' : 'transparent',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  {this.state.bikeSelected == item._id &&
                  this.state.selectedRide != 'child' &&
                  !this.state.ScheduleSetup ? (
                    <Image
                      resizeMode="contain"
                      style={{
                        width: moderateScale(50),
                        height: moderateScale(15),
                        left: moderateScale(25),
                        top: moderateScale(25),
                        zIndex: moderateScale(2),
                      }}
                      source={Images.rightImg}
                    />
                  ) : (
                    <Image
                      resizeMode="contain"
                      style={{
                        width: moderateScale(50),
                        height: moderateScale(15),
                        left: moderateScale(25),
                        top: moderateScale(25),
                        zIndex: moderateScale(2),
                      }}
                      // source={Images.startingPoint}
                    />
                  )}
                  <Image
                    source={{uri: item.carImage}}
                    resizeMode="cover"
                    style={{
                      width: moderateScale(60),
                      height: moderateScale(60),
                      borderRadius: moderateScale(30),
                      borderWidth: 0.5,
                      borderColor: 'grey',
                    }}
                  />
                  <Text style={styles.carType}>{item.carType}</Text>
                  <Text
                    style={{
                      color: 'black',
                      fontSize: RFValue(12),
                      backgroundColor: 'transparent',
                      fontWeight: 'bold',
                    }}>
                    $ {item.estimatedCost}
                  </Text>
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => this.bikeSel(item)}
                  style={{
                    width: widthCal / 3,
                    height: moderateScale(100),
                    flexDirection: 'column',
                    backgroundColor: index == 0 ? 'transparent' : 'transparent',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  {this.state.bikeSelected == item._id ? (
                    <Image
                      resizeMode="contain"
                      style={{
                        width: moderateScale(50),
                        height: moderateScale(15),
                        left: moderateScale(25),
                        top: moderateScale(25),
                        zIndex: moderateScale(2),
                      }}
                      source={Images.rightImg}
                    />
                  ) : (
                    <Image
                      resizeMode="contain"
                      style={{
                        width: moderateScale(50),
                        height: moderateScale(15),
                        left: moderateScale(25),
                        top: moderateScale(25),
                        zIndex: moderateScale(2),
                      }}
                      // source={Images.startingPoint}
                    />
                  )}
                  <Image
                    source={{uri: item.carImage}}
                    resizeMode="cover"
                    style={{
                      width: moderateScale(60),
                      height: moderateScale(60),
                      borderRadius: moderateScale(30),
                      borderWidth: 0.5,
                      borderColor: 'grey',
                    }}
                  />
                  <Text style={styles.carType}>{item.carType}</Text>
                  <Text
                    style={{
                      color: 'black',
                      fontSize: RFValue(12),
                      backgroundColor: 'transparent',
                      fontWeight: 'bold',
                    }}>
                    $ {item.estimatedCost}
                  </Text>
                </TouchableOpacity>
              )
            }
            //   keyExtractor={item => item._id}
          />
        </View>

        <View
          style={{
            borderBottomWidth: moderateScale(2),
            marginTop: moderateScale(5),
            borderBottomColor: '#CACDD4',
          }}
        />

        <View
          style={{
            height: moderateScale(40),
            marginTop: moderateScale(10),
            backgroundColor: 'white',
            alignItems: 'center',
            justifyContent: 'space-around',
            flexDirection: 'row',
          }}>
          <TouchableOpacity
            onPress={() => this.tapCard()}
            style={{
              flexDirection: 'row',
              backgroundColor: 'transparent',
            }}>
            {this.state.selectedCardNum === 'cash' ? (
              <Image
                resizeMode="contain"
                style={{
                  height: moderateScale(35),
                  width: moderateScale(25),
                  marginRight: moderateScale(20),
                  marginLeft: moderateScale(20),
                }}
                source={{uri: this.state.selectedCardImage}}

                // source={Images.cash}
              />
            ) : this.state.selectedCardNum === 'wallet' ? (
              <Image
                resizeMode="contain"
                style={{
                  height: moderateScale(35),
                  width: moderateScale(35),
                  marginRight: moderateScale(20),
                  marginLeft: moderateScale(20),
                }}
                //source={Images.walletNew}
                source={{uri: this.state.selectedCardImage}}
              />
            ) : (
              <Image
                resizeMode="contain"
                style={{
                  height: moderateScale(35),
                  width: moderateScale(35),
                  marginRight: moderateScale(20),
                  marginLeft: moderateScale(20),
                }}
                //source={Images.walletNew}
                source={{uri: this.state.selectedCardImage}}
              />

              // <Image
              //   resizeMode="contain"
              //   style={{height: 30}}
              //   // source={Images.visa}
              //   source={{uri: this.state.selectedCardImage}}
              // />
            )}
            <Text style={{top: 8, right: 8}}>{this.state.selectedCardNum}</Text>
            <Image
              resizeMode="contain"
              style={{
                top: moderateScale(10),
                width: moderateScale(12),
                height: moderateScale(10),
              }}
              source={Images.downArrow}
            />
          </TouchableOpacity>

          {this.state.showcoupon ? (
            <React.Fragment>
              <View style={{backgroundColor: 'white'}}>
                <TextInput
                  placeholder="Promo Code"
                  onChangeText={promoCode => this.setState({promoCode})}
                  value={this.state.promoCode}
                  autoCorrect={false}
                  autoCapitalize="none"
                  keyboardType="default"
                  returnKeyType="done"
                />
                <View
                  style={{
                    borderBottomWidth: moderateScale(2),
                    marginTop: moderateScale(5),
                    borderBottomColor: '#CACDD4',
                  }}
                />
              </View>
              <View>
                <TouchableOpacity onPress={this.ApplyCoupon}>
                  <Text style={{color: Colors.buttonGreen}}>
                    {strings.Apply}
                  </Text>
                </TouchableOpacity>
              </View>
            </React.Fragment>
          ) : (
            <View>
              <Text style={{color: '#CACDD4', fontWeight: 'bold'}}>
                {strings.PromoCodeApplied}
              </Text>
            </View>
          )}
        </View>

        <View
          style={{
            borderBottomWidth: moderateScale(2),
            marginTop: moderateScale(5),
            borderBottomColor: '#CACDD4',
          }}
        />

        <View
          style={{
            flexDirection: 'row',
            marginLeft: moderateScale(25),
            marginRight: moderateScale(25),
            marginTop: moderateScale(25),
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          {this.state.isShared ? (
            <TouchableOpacity
              onPress={() => {
                this.setState({shareView: true});
              }}
              style={{
                width: '50%',
                height: wp('12%'),
                marginTop: 0,
                alignItems: 'center',
                justifyContent: 'center',
                marginHorizontal: moderateScale(10),
                borderRadius: moderateScale(27),
                backgroundColor: Colors.Black,
              }}>
              <Text style={{color: 'white'}}>Next</Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={this.ApplyConfirm}
              style={{
                width: '50%',
                height: wp('12%'),
                marginTop: 0,
                alignItems: 'center',
                justifyContent: 'center',
                marginHorizontal: moderateScale(10),
                borderRadius: moderateScale(27),
                backgroundColor: Colors.Black,
              }}>
              {this.state.ScheduleSetup == true ? (
                <Text style={{color: 'white'}}>{strings.Schedule}</Text>
              ) : (
                <Text style={{color: 'white'}}>Confirm</Text>
              )}
            </TouchableOpacity>
          )}
          {this.state.ScheduleSetup == false ? (
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                opacity: this.state.isShared ? 0.3 : 0.9,
              }}
              disabled={this.state.isShared ? true : false}
              onPress={this.ApplySchedule}>
              <Image
                resizeMode="contain"
                style={{width: moderateScale(30), height: moderateScale(15)}}
                source={Images.calender}
              />
              <Text>{strings.Schedule}</Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={{flexDirection: 'row'}}
              onPress={this.ApplySchedule}>
              <Image
                resizeMode="contain"
                style={{width: moderateScale(30), height: moderateScale(15)}}
                source={Images.calender}
              />
              <Text>{this.state.date + ' ' + this.state.Time}</Text>
            </TouchableOpacity>
          )}
        </View>
      </KeyboardAvoidingView>
    );
  };

  otpScreenRenderView = () => {
    return (
      <View style={{flex: 0.4}}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'white',
            padding: moderateScale(10),
          }}>
          <View
            style={{
              flex: 0.6,
              flexDirection: 'row',
              alignItems: 'center',
            }}>
            <View style={{flex: 0.5}}>
              {this.state.driverImage == '' ||
              this.state.driverImage == null ||
              this.state.driverImage == 'null' ||
              this.state.driverImage == 'none' ? (
                <Image
                  style={{
                    width: moderateScale(50),
                    height: moderateScale(50),
                    borderRadius: moderateScale(25),
                  }}
                  source={Images.dummyUser}
                  resizeMode={'cover'}
                />
              ) : (
                <Image
                  style={{
                    width: moderateScale(50),
                    height: moderateScale(50),
                    borderRadius: moderateScale(25),
                  }}
                  source={{uri: this.state.driverImage}}
                  resizeMode={'cover'}
                />
              )}
            </View>
            <View
              style={{
                flex: moderateScale(2),
                marginHorizontal: moderateScale(5),
              }}>
              <Text style={{fontFamily: Fonts.Regular, fontSize: RFValue(14)}}>
                {this.state.driverName}
              </Text>
              {/* <Text  style={{color: 'grey'}}>
              {strings.Company} :{' '}
              <Text
               
                style={{color: '#000', fontFamily: Fonts.Semibold}}>
                {this.state.plateNumber}
              </Text>
            </Text> */}
              <Text style={{color: 'grey'}}>
                {strings.carNumber} :
                <Text style={{color: '#000', fontFamily: Fonts.Semibold}}>
                  {this.state.plateNumber}
                </Text>
              </Text>
              <Text style={{color: 'grey'}}>
                Time Remaining :
                {/* <Text style={{color: '#000', fontFamily: Fonts.Semibold}}>
                  {this.state.timeToStartLocation} min
                </Text> */}
              </Text>
              {/* <Text style={{color: 'grey'}}>
                Distance Remaining :
                <Text style={{color: '#000', fontFamily: Fonts.Semibold}}>
                  {this.state.distanceToStartLocation > 0
                    ? this.state.distanceToStartLocation
                    : 0.0}{' '}
                  km
                </Text>
              </Text> */}
            </View>
            <View
              style={{
                flexDirection: 'column',
                alignItems: 'center',
              }}>
              <View
                style={{
                  backgroundColor: Colors.PrimaryColor,
                  paddingHorizontal: moderateScale(10),
                  borderRadius: moderateScale(20),
                  height: moderateScale(30),
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    textAlign: 'center',
                    color: Colors.Black,
                    fontFamily: Fonts.Semibold,
                  }}>
                  {strings.OTP}: {this.state.tripOTP}
                </Text>
              </View>
              <View style={{marginTop: 5, alignItems: 'center'}}>
                <View
                  style={{
                    backgroundColor: Colors.White,
                    borderRadius: moderateScale(45),
                    height: moderateScale(45),
                    width: moderateScale(45),
                    justifyContent: 'center',
                    marginTop: moderateScale(10),
                    borderWidth: moderateScale(4),
                    borderColor: Colors.buttonRed,
                  }}>
                  <Text
                    style={{
                      textAlign: 'center',
                      color: Colors.Black,
                      fontSize: RFValue(16),
                      fontFamily: Fonts.Semibold,
                    }}>
                    {this.state.timeToStartLocation}
                  </Text>
                </View>
                <Text>min</Text>
              </View>
            </View>
          </View>
          <View
            style={{
              flex: 0.5,
              flexDirection: 'row',
              alignItems: 'center',
              backgroundColor: 'white',
              justifyContent: 'center',
            }}>
            <TouchableOpacity
              style={{
                flexDirection: 'row',
                backgroundColor: Colors.PrimaryColor,
                paddingHorizontal: moderateScale(10),
                borderRadius: moderateScale(20),
                width: '34%',
                height: moderateScale(40),
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: Colors.PrimaryColor,
                borderWidth: moderateScale(1),
              }}
              onPress={() => this.messageDriver()}>
              <VectorIcon
                name={'message1'}
                groupName={'AntDesign'}
                size={moderateScale(18)}
                color={Colors.Black}
              />
              <Text
                style={{
                  marginLeft: moderateScale(2),
                  textAlign: 'center',
                  color: Colors.Black,
                  fontFamily: Fonts.Regular,
                  fontSize: RFValue(12),
                }}>
                {strings.messageDriver}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                width: '29%',
                flexDirection: 'row',
                marginHorizontal: moderateScale(10),
                backgroundColor: Colors.White,
                paddingHorizontal: moderateScale(10),
                borderRadius: moderateScale(20),
                height: moderateScale(40),
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: Colors.Black,
                borderWidth: 1,
              }}
              onPress={() => this.cancelRide()}>
              {/* <VectorIcon
                name={'close'}
                groupName={'AntDesign'}
                size={moderateScale(18)}
                color={Colors.PrimaryColor}
              /> */}
              <Text
                style={{
                  marginLeft: moderateScale(2),
                  textAlign: 'center',
                  color: Colors.Black,
                  fontFamily: Fonts.Regular,
                  fontSize: RFValue(12),
                }}>
                {strings.cancelRide}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                flex: 1,
                flexDirection: 'row',
                backgroundColor: Colors.White,
                paddingHorizontal: moderateScale(10),
                borderRadius: moderateScale(20),
                height: moderateScale(40),
                alignItems: 'center',
                justifyContent: 'center',
                borderColor: Colors.Black,
                borderWidth: moderateScale(1),
              }}
              onPress={() => this.shareDetail()}>
              <VectorIcon
                name={'share'}
                groupName={'Entypo'}
                size={moderateScale(18)}
                color={Colors.Black}
              />
              <Text
                style={{
                  marginLeft: moderateScale(2),
                  textAlign: 'center',
                  color: Colors.Black,
                  fontFamily: Fonts.Regular,
                  fontSize: RFValue(12),
                }}>
                {strings.shareDetails}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  tripCompleteRenderView = () => {
    return (
      <View
        style={{
          position: 'absolute',
          justifyContent: 'center',
          alignItems: 'center',
          left: 0,
          right: 0,
          top: 0,
          bottom: 0,
          backgroundColor: 'transparent',
        }}>
        <View
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            backgroundColor: '#000000',
            opacity: 0.9,
          }}
        />

        <View
          style={{
            backgroundColor: '#ffffff',
            borderRadius: moderateScale(5),
            width: wp('80%'),
            height: wp('100%'),
            alignItems: 'center',
          }}>
          <Text
            style={{
              width: 'auto',
              fontFamily: 'Arial',
              color: 'gray',
              textAlign: 'center',
              marginHorizontal: moderateScale(25),
              marginTop: moderateScale(20),
              fontSize: RFValue(16),
            }}>
            {strings.thankYouForRide}
          </Text>

          <Text
            style={{
              width: 'auto',
              fontFamily: 'Arial',
              textAlign: 'center',
              marginHorizontal: moderateScale(25),
              marginTop: moderateScale(20),
              color: Colors.Black,
              fontSize: RFValue(18),
            }}>
            {strings.Total}
          </Text>

          <Text
            style={{
              width: 'auto',
              fontFamily: 'Arial',
              textAlign: 'center',
              marginHorizontal: moderateScale(25),
              marginTop: moderateScale(20),
              color: Colors.Black,
              fontSize: RFValue(16),
            }}>
            $ {this.state.estimatedCost}
          </Text>

          <View
            style={{
              height: moderateScale(35),
              backgroundColor: 'transparent',
              marginTop: wp('2%'),
              width: 'auto',
              flexDirection: 'row',
            }}>
            <TouchableOpacity onPress={() => this.rating(1)}>
              {this.state.ratingValue >= 1 ? (
                <Image
                  resizeMode="contain"
                  style={{width: wp('9%'), height: wp('9%')}}
                  source={Images.starBlack}
                />
              ) : (
                <Image
                  resizeMode="contain"
                  style={{width: wp('8%'), height: wp('8%')}}
                  source={Images.starBlank}
                />
              )}
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.rating(2)}>
              {this.state.ratingValue >= 2 ? (
                <Image
                  resizeMode="contain"
                  style={{width: wp('8%'), height: wp('8%')}}
                  source={Images.starBlack}
                />
              ) : (
                <Image
                  resizeMode="contain"
                  style={{width: wp('8%'), height: wp('8%')}}
                  source={Images.starBlank}
                />
              )}
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.rating(3)}>
              {this.state.ratingValue >= 3 ? (
                <Image
                  resizeMode="contain"
                  style={{width: wp('8%'), height: wp('8%')}}
                  source={Images.starBlack}
                />
              ) : (
                <Image
                  resizeMode="contain"
                  style={{width: wp('8%'), height: wp('8%')}}
                  source={Images.starBlank}
                />
              )}
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.rating(4)}>
              {this.state.ratingValue >= 4 ? (
                <Image
                  resizeMode="contain"
                  style={{width: wp('8%'), height: wp('8%')}}
                  source={Images.starBlack}
                />
              ) : (
                <Image
                  resizeMode="contain"
                  style={{width: wp('8%'), height: wp('8%')}}
                  source={Images.starBlank}
                />
              )}
            </TouchableOpacity>

            <TouchableOpacity onPress={() => this.rating(5)}>
              {this.state.ratingValue >= 5 ? (
                <Image
                  resizeMode="contain"
                  style={{width: wp('8%'), height: wp('8%')}}
                  source={Images.starBlack}
                />
              ) : (
                <Image
                  resizeMode="contain"
                  style={{width: wp('8%'), height: wp('8%')}}
                  source={Images.starBlank}
                />
              )}
            </TouchableOpacity>
          </View>

          <TouchableOpacity
            onPress={() => this.submitReview()}
            style={{
              width: '80%',
              height: wp('13%'),
              borderRadius: wp('6.5%'),
              marginTop: wp('8%'),
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: Colors.Black,
            }}>
            <Text
              style={{
                fontSize: RFValue(20),
                color: 'white',
                fontFamily: 'Arial',
              }}>
              {strings.Submit}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  findingDriverRenderView = () => {
    return (
      <View
        needsOffscreenAlphaCompositing={true}
        style={{
          flex: 1,
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'black',
          opacity: 0.7,
          position: 'absolute',
        }}>
        <View
          style={{
            width: '90%',
            height: moderateScale(75),
            backgroundColor: 'white',
            alignItems: 'center',
            justifyContent: 'center',
            opacity: 1,
          }}>
          <Text
            style={{
              fontSize: RFValue(16),
              fontWeight: 'bold',
              color: 'black',
              top: moderateScale(10),
            }}>
            {strings.findingDriver}
          </Text>
          <DotIndicator color="black" size={moderateScale(10)} />
        </View>
      </View>
    );
  };

  descriptionRenderView = () => {
    return (
      <View
        style={{
          width: '100%',
          height: '100%',
          backgroundColor: 'rgba(0, 0, 0, 0.8)',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View
          style={{
            width: '80%',
            height: moderateScale(350),
            backgroundColor: '#E9E9E9',
            alignItems: 'center',
            top: moderateScale(-30),
            opacity: moderateScale(1),
            borderRadius: moderateScale(5),
          }}>
          <View
            style={{
              backgroundColor: Colors.PrimaryColor,
              borderTopRightRadius: moderateScale(5),
              borderTopLeftRadius: moderateScale(5),
              height: moderateScale(48),
              width: '100%',
            }}>
            <View style={{alignItems: 'center', justifyContent: 'center'}}>
              <Text
                style={{
                  fontFamily: Fonts.Semibold,
                  fontWeight: 'bold',
                  color: Colors.Black,
                  fontSize: RFValue(18),
                  marginTop: moderateScale(10),
                }}>
                {strings.enterDriverDescription}
              </Text>
            </View>
            <TouchableOpacity
              onPress={() => this.rideDescriptionAction()}
              style={{
                width: moderateScale(45),
                height: moderateScale(45),
                backgroundColor: 'transparent',
                right: 0,
                top: moderateScale(-35),
                left: '87%',
                alignItems: 'center',
                justifyContent: 'center',
                opacity: 1,
              }}>
              <Image
                resizeMode="contain"
                style={{
                  width: moderateScale(15),
                  height: moderateScale(15),
                  tintColor: Colors.Black,
                }}
                source={Images.cancelWhite}
              />
            </TouchableOpacity>
          </View>
          <View
            style={[
              styles.tileFeedback,
              {width: '80%', top: moderateScale(20)},
            ]}>
            <TextInput
              style={[
                styles.searchTextInput,
                {
                  height: moderateScale(200),
                  width: '100%',
                  backgroundColor: 'transparent',
                },
              ]}
              placeholder="Enter message"
              placeholderTextColor={'#000000'}
              multiline={true}
              numberOfLines={4}
              autoCorrect={false}
              returnKeyType={'done'}
              blurOnSubmit={true}
              onSubmitEditing={() => {
                Keyboard.dismiss();
              }}
              //autoFocus={true}
              onChangeText={descriptionRideString =>
                this.setState({descriptionRideString})
              }
              value={this.state.descriptionRideString}
            />
          </View>

          <TouchableOpacity
            onPress={() => this.submitDescription()}
            style={{
              width: moderateScale(170),
              height: moderateScale(45),
              backgroundColor: Colors.primary1,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: moderateScale(25),
              position: 'absolute',
              bottom: moderateScale(20),
            }}>
            <Text
              style={{
                fontWeight: 'bold',
                color: Colors.Black,
                fontSize: RFValue(16),
              }}>
              {strings.Submit}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  scheduleRenderView = () => {
    return (
      <View
        style={{
          height: 360,
          backgroundColor: 'white',
          position: 'absolute',
          bottom: 0,
        }}>
        <View
          style={{
            flexDirection: 'row',
            backgroundColor: 'white',
            width: '100%',
            height: 70,
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: RFValue(16),
              position: 'absolute',
              left: 10,
              top: 15,
            }}>
            {strings.scheduleARide}
          </Text>
          <TouchableOpacity
            style={{position: 'absolute', right: 20, top: 15}}
            onPress={this.closeRide}>
            <Image
              resizeMode="contain"
              style={{width: wp('5%'), height: wp('5%')}}
              source={Images.cancel}
            />
          </TouchableOpacity>
        </View>

        <View
          style={{
            borderWidth: 1,
            borderColor: '#D3D3D3',
            padding: 0,
            marginLeft: 10,
            paddingLeft: 10,
            width: widthCal - 20,
            marginTop: 15,
            // marginBottom: 10,
          }}>
          <Text style={{color: Colors.Black, marginLeft: 10, marginTop: 5}}>
            {strings.PickupTime}
          </Text>
          <DatePicker
            style={{width: 100, marginLeft: 10}}
            date={this.state.Time}
            mode="time"
            placeholder={strings.SelectTime}
            placeholderColor="black"
            format="hh:mm A"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            showIcon={false}
            customStyles={{
              dateIcon: {
                position: 'absolute',
                left: 0,
                top: 4,
                marginLeft: 0,
              },
              dateInput: {
                // marginLeft: -25,
                borderColor: 'white',
                color: 'black',
                //textAlign: 'left',
              },
              // ... You can check the source to find the other keys.
            }}
            onDateChange={Time => {
              var newTime = moment(Time, ['h:mm A']).format('HH:mm');
              this.setState({Time: newTime});
              //this.setState({Time: Time});
            }}
          />
        </View>
        <View
          style={{
            borderWidth: 1,
            borderColor: '#D3D3D3',
            padding: 0,
            marginLeft: 10,
            paddingLeft: 10,
            width: widthCal - 20,
            marginTop: 20,
          }}>
          <Text style={{color: Colors.Black, marginLeft: 10, marginTop: 5}}>
            {strings.PickupDate}
          </Text>
          <DatePicker
            style={{width: 100, marginLeft: 10}}
            date={this.state.date}
            mode="date"
            placeholder={strings.SelectDate}
            placeholderColor="black"
            format="DD-MM-YYYY"
            // format="MMMM Do YYYY"
            minDate={new Date()}
            //maxDate="01-01-2021"
            confirmBtnText="Confirm"
            cancelBtnText="Cancel"
            showIcon={false}
            customStyles={{
              dateIcon: {
                position: 'absolute',
                left: 0,
                top: 4,
                marginLeft: 0,
              },
              dateInput: {
                // marginLeft: -25,
                borderColor: 'white',
                color: 'black',
                // textAlign: 'left',
              },
              // ... You can check the source to find the other keys.
            }}
            onDateChange={date => {
              // setDate(moment(date).format('DD-MM-YYYY'));
              // var aa = moment(date).format('DD-MM-YYYY');
              // console, log('asdasd');
              this.setState({
                date: date,
              });
              //this.setState({date: date});
            }}
          />
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            //alignSelf: 'center',
            marginHorizontal: 30,
            marginTop: 35,
          }}>
          <TouchableOpacity
            onPress={() => this.setPickUp()}
            style={{
              width: '50%',
              height: wp('12%'),
              marginTop: 15,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 27,
              backgroundColor: Colors.Black,
            }}>
            <Text style={[styles.title, {color: 'white'}]}>
              {' '}
              {strings.SetPickupTime}{' '}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={this.closeRide}
            style={{
              width: '50%',
              height: 45,
              marginTop: 15,
              alignItems: 'flex-end',
              justifyContent: 'center',
            }}>
            <Text style={{color: Colors.buttonRed, fontWeight: 'bold'}}>
              {strings.cancelSchedule}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  shareRenderView = () => {
    return (
      <View
        style={{
          height: 340,
          backgroundColor: 'white',
          position: 'absolute',
          bottom: 0,
        }}>
        <View
          style={{
            flexDirection: 'row',
            backgroundColor: 'white',
            width: '100%',
            height: 40,
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              fontSize: RFValue(16),
              position: 'absolute',
              left: 20,
              //top: 15,
            }}>
            {strings.shareRide}
          </Text>
          <View style={{position: 'absolute', right: 20}}>
            <Text>${this.state.estimatedCost}</Text>
          </View>
        </View>
        <View
          style={{
            borderBottomWidth: 2,
            borderBottomColor: '#CACDD4',
          }}
        />

        <View
          style={{
            flexDirection: 'row',
            backgroundColor: 'white',
            width: '100%',
            marginTop: wp('18%'),
          }}>
          <View style={{flexDirection: 'row'}}>
            <Text
              style={{
                fontFamily: Fonts.Regular,
                fontSize: RFValue(16),
                left: 30,
              }}>
              {strings.seatsAvailable}
            </Text>
          </View>

          <View style={styles.arrowTile1}>
            <TouchableOpacity
              style={{
                backgroundColor: this.state.isSingle
                  ? Colors.Black
                  : Colors.PrimaryColor,
                marginLeft: 20,
                height: wp('8%'),
                width: wp('8%'),
                borderRadius: wp('8%'),
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => this.noOfSeats(true, false)}>
              <Text style={{color: 'white'}}>1</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={{
                backgroundColor: this.state.isMultiple
                  ? Colors.Black
                  : Colors.PrimaryColor,
                marginLeft: 20,
                height: wp('8%'),
                width: wp('8%'),
                borderRadius: wp('8%'),
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => this.noOfSeats(false, true)}>
              <Text style={{color: 'white'}}>2</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            backgroundColor: 'white',
            width: '100%',
            marginTop: wp('10%'),
          }}>
          <View style={{flexDirection: 'column'}}>
            <Text
              style={{
                fontFamily: Fonts.Regular,
                fontSize: RFValue(14),
                left: 30,
              }}>
              {strings.seatSelected}
            </Text>
          </View>

          <View style={{position: 'absolute', right: 50}}>
            <Text>{this.state.isSingle ? 1 : 2}</Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'center',
            marginHorizontal: 30,
            marginTop: Platform.OS === 'ios' ? wp('14%') : wp('17%'),
          }}>
          <TouchableOpacity
            onPress={() => this.applyShare()}
            style={{
              width: '50%',
              height: wp('12%'),
              //marginTop: 15,
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 27,
              marginRight: 10,
              backgroundColor: Colors.Black,
            }}>
            <Text style={[styles.title, {color: 'white'}]}>
              {strings.confirmShare}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.closeShare()}
            style={{
              width: '50%',
              height: wp('12%'),
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: 27,
              // marginLeft: 10,
              // marginRight: 10,
              backgroundColor: Colors.buttonRed,
            }}>
            <Text style={[styles.title, {color: 'white'}]}>
              {strings.cancelShare}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  paymentMethodRenderView = () => {
    return (
      <View
        style={{
          width: '100%',
          height: '100%',
          backgroundColor: 'rgba(0, 0, 0, 0.8)',
          position: 'absolute',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View
          style={{
            width: '80%',
            height: 260,
            backgroundColor: 'white',
            marginTop: 8,
            borderRadius: 5,
          }}>
          <View
            style={{
              backgroundColor: Colors.PrimaryColor,
              borderTopRightRadius: 5,
              borderTopLeftRadius: 5,
              height: 50,
              width: '100%',
              marginBottom: 10,
            }}>
            <Text
              style={{
                marginLeft: 8,
                fontFamily: Fonts.Thin,
                fontWeight: 'bold',
                fontSize: RFValue(18),
                marginTop: 10,
                color: Colors.Black,
              }}>
              {strings.SelectPaymentMethod}
            </Text>
          </View>
          <ScrollView>
            <FlatList
              data={this.state.cardList}
              renderItem={({item}) =>
                item.type === 'Card' ? (
                  <TouchableOpacity
                    onPress={() => this.cardSelectAction(item)}
                    style={styles.loadDetail}>
                    <View style={{width: '20%', marginLeft: 7}}>
                      <Image
                        resizeMode="contain"
                        style={{height: 30, width: 30}}
                        source={{uri: item.logo}}
                      />
                    </View>
                    <Text style={{fontSize: RFValue(14), fontWeight: '500'}}>
                      ***********{item.lastd}
                    </Text>
                  </TouchableOpacity>
                ) : item.type === 'Wallet' ? (
                  <TouchableOpacity
                    onPress={() => this.payFromWalletAction(item)}
                    style={{
                      marginLeft: '5%',
                      marginRight: '5%',
                      height: 50,
                      marginTop: 15,
                      marginBottom: -5,
                      flexDirection: 'row',
                      padding: 15,
                      backgroundColor: '#E9E9E9',
                      alignItems: 'center',
                    }}>
                    <View style={{width: '20%'}}>
                      <Image
                        resizeMode="contain"
                        style={{height: 30, width: 30}}
                        //source={Images.walletNew}
                        source={{uri: item.logo}}
                      />
                    </View>
                    <Text
                      style={{
                        // marginLeft: 8,
                        // marginTop: 8,
                        fontWeight: '500',
                        fontSize: RFValue(16),
                      }}>
                      PEI TAXI Wallet
                    </Text>
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity
                    onPress={() => this.payFromCashAction(item)}
                    style={{
                      marginLeft: '5%',
                      marginRight: '5%',
                      height: 50,
                      marginTop: 15,
                      marginBottom: 10,
                      flexDirection: 'row',
                      padding: 15,
                      backgroundColor: '#E9E9E9',
                    }}>
                    <View style={{width: '20%'}}>
                      <Image
                        resizeMode="contain"
                        style={{height: 30, width: 30}}
                        //source={Images.cash}
                        source={{uri: item.logo}}
                      />
                    </View>
                    <Text
                      style={{
                        // marginLeft: 8,
                        // marginTop: 8,
                        fontWeight: '500',
                        fontSize: RFValue(16),
                      }}>
                      Pay Cash
                    </Text>
                  </TouchableOpacity>
                )
              }
              keyExtractor={item => item._id}
            />
          </ScrollView>
          <TouchableOpacity
            onPress={() => this.cardCancelHide()}
            style={{
              position: 'absolute',
              right: 2,
              top: 8,
              width: 30,
              height: 30,
              borderRadius: 15,
              backgroundColor: 'white',
              alignItems: 'center',
              justifyContent: 'center',
              backgroundColor: 'transparent',
            }}>
            <Image
              source={Images.cancelWhite}
              style={{height: 15, width: 15, tintColor: Colors.Black}}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  };
  preferredRenderView = () => {
    return (
      <View
        style={{
          backgroundColor: 'white',
          width: '100%',
          height: 160,
          position: 'absolute',
          bottom: 0,
          flexDirection: 'column',
          alignItems: 'center',
        }}>
        <Text style={{fontSize: RFValue(24), fontWeight: '500', marginTop: 15}}>
          {strings.Wanttoadd}
        </Text>
        <Text style={{fontSize: RFValue(24), fontWeight: '500', marginTop: 5}}>
          {strings.Preferredlist}
        </Text>
        <View
          style={{
            width: '100%',
            height: 50,
            backgroundColor: 'white',
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <TouchableOpacity
            style={{
              flexDirection: 'row',
              backgroundColor: Colors.PrimaryColor,
              paddingHorizontal: 10,
              borderRadius: 20,
              width: '30%',
              height: 32,
              alignItems: 'center',
              justifyContent: 'center',
              borderColor: Colors.PrimaryColor,
              borderWidth: 1,
            }}
            onPress={() => this.yesAction()}>
            <Text
              style={{
                marginLeft: 2,
                textAlign: 'center',
                color: Colors.White,
                fontFamily: Fonts.Regular,
                fontSize: RFValue(16),
              }}>
              {strings.Yes}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={{
              width: '30%',
              flexDirection: 'row',
              marginHorizontal: 10,
              backgroundColor: Colors.buttonRed,
              paddingHorizontal: 10,
              borderRadius: 20,
              height: 32,
              alignItems: 'center',
              justifyContent: 'center',
              borderColor: Colors.buttonRed,
              borderWidth: 1,
            }}
            onPress={() => this.noAction()}>
            <Text
              style={{
                marginLeft: 2,
                textAlign: 'center',
                color: Colors.White,
                fontFamily: Fonts.Regular,
                fontSize: RFValue(16),
              }}>
              {strings.No}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  noDriverRenderView = () => {
    return (
      <View
        needsOffscreenAlphaCompositing={true}
        style={{
          flex: 1,
          top: 0,
          left: 0,
          right: 0,
          bottom: 0,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'white',
          position: 'absolute',
        }}>
        <View
          style={{
            width: '100%',
            height: 200,
            backgroundColor: 'white',
            alignItems: 'center',
            opacity: 1,
          }}>
          <Image
            style={{width: 70, height: 70, borderRadius: 35}}
            source={Images.noDriver}
            resizeMode={'cover'}
          />
          <Text
            style={{
              fontSize: RFValue(18),
              fontWeight: '500',
              color: Colors.buttonRed,
              top: 10,
            }}>
            {strings.OOPS}
          </Text>
          <Text
            style={{
              fontSize: RFValue(16),
              fontWeight: 'bold',
              color: 'black',
              top: 10,
            }}>
            {strings.NoDriverFound}
          </Text>
          <Text
            style={{
              fontSize: RFValue(16),
              fontWeight: 'bold',
              color: 'black',
              top: 10,
            }}>
            {strings.NearYourLocation}
          </Text>
        </View>
        <View
          style={{
            width: '100%',
            height: 100,
            backgroundColor: 'white',
            alignItems: 'center',
            justifyContent: 'center',
            opacity: 1,
          }}>
          <TouchableOpacity
            onPress={() => this.goToDashBoard()}
            style={{
              width: '60%',
              height: 50,
              borderRadius: 25,
              backgroundColor: Colors.PrimaryColor,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontSize: RFValue(16),
                fontWeight: 'bold',
                color: Colors.Black,
              }}>
              {strings.GotoDashboard}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  beforeTripStartRender = () => {
    return (
      <>
        <MapView.Marker
          identifier={'Source'}
          style={{backgroundColor: 'transparent'}}
          ref={marker => {
            this.marker = marker;
          }}
          flat={false}
          coordinate={{
            latitude: this.state.sourceLocationCord.latitude,
            longitude: this.state.sourceLocationCord.longitude,
          }}>
          <Image
            resizeMode="contain"
            source={Images.pickupIcon}
            style={{height: 20, width: 20}}
          />
        </MapView.Marker>
        <MapViewDirections
          origin={{
            latitude: this.state.driverLatitude,
            longitude: this.state.driverLongitude,
          }}
          destination={{
            latitude: this.state.sourceLocationCord.latitude,
            longitude: this.state.sourceLocationCord.longitude,
          }}
          apikey={Config.GOOGLE_MAPS_APIKEY}
          strokeColor="black"
          strokeWidth={2.5}
          resetOnChange={false}
          onReady={distance => this.calculateDistance(distance)}
        />
      </>
    );
  };

  afterTripStartRender = () => {
    return (
      <React.Fragment>
        <MapView.Marker
          title={this.state.endLocationAddr}
          identifier={'Destination'}
          style={{backgroundColor: 'transparent'}}
          ref={marker => {
            this.marker = marker;
          }}
          flat={false}
          coordinate={{
            latitude: this.state.destinationLocationCord.latitude,
            longitude: this.state.destinationLocationCord.longitude,
          }}>
          <Image
            resizeMode="contain"
            source={Images.marker}
            style={{height: 20, width: 20}}
          />
        </MapView.Marker>
        <MapViewDirections
          origin={{
            latitude: this.state.driverLatitude,
            longitude: this.state.driverLongitude,
          }}
          destination={{
            latitude: this.state.destinationLocationCord.latitude,
            longitude: this.state.destinationLocationCord.longitude,
          }}
          apikey={Config.GOOGLE_MAPS_APIKEY}
          strokeColor="black"
          strokeWidth={2.5}
          resetOnChange={false}
        />
      </React.Fragment>
    );
  };

  destinationInRouteRender = () => {
    return (
      <View
        style={{
          backgroundColor: 'white',
          width: '100%',
          height: 160,
          position: 'absolute',
          bottom: 0,
          flexDirection: 'column',
        }}>
        <View
          style={{
            backgroundColor: 'white',
            width: '100%',
            height: 100,
            flexDirection: 'row',
          }}>
          <View
            style={{
              backgroundColor: 'white',
              width: 60,
              height: '100%',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            {this.state.driverImage == '' ||
            this.state.driverImage == null ||
            this.state.driverImage == 'null' ||
            this.state.driverImage == 'none' ? (
              <Image
                style={{width: 50, height: 50, borderRadius: 25}}
                source={Images.dummyUser}
                resizeMode={'cover'}
              />
            ) : (
              <Image
                style={{width: 50, height: 50, borderRadius: 25}}
                source={{uri: this.state.driverImage}}
                resizeMode={'cover'}
              />
            )}
          </View>
          <View
            style={{
              backgroundColor: 'transparent',
              flex: 1,
              flexDirection: 'row',
            }}>
            <View
              style={{
                backgroundColor: 'white',
                flex: 2,
                justifyContent: 'center',
              }}>
              <Text
                style={{fontSize: RFValue(20), fontWeight: 'bold', margin: 2}}>
                {this.state.driverName}
              </Text>
              <Text
                style={{fontSize: RFValue(16), fontWeight: '500', margin: 5}}>
                {strings.carNumber} :
                <Text style={{color: '#000', fontFamily: Fonts.Semibold}}>
                  {this.state.plateNumber}
                </Text>
              </Text>
            </View>
            <TouchableOpacity
              onPress={() => this.panicButtonAction()}
              style={{
                backgroundColor: 'white',
                width: 60,
                height: '100%',
                justifyContent: 'center',
              }}>
              {this.state.panicStatus == false ? (
                <Image
                  style={{width: 50, height: 50, borderRadius: 25}}
                  source={Images.redPanic}
                  resizeMode={'cover'}
                />
              ) : (
                <Image
                  style={{width: 50, height: 50, borderRadius: 25}}
                  source={Images.greyPanic}
                  resizeMode={'cover'}
                />
              )}
            </TouchableOpacity>
            {/* <TouchableOpacity
              onPress={() => this.refreshUserAction()}
              style={{
                backgroundColor: 'white',
                width: 60,
                height: '100%',
                justifyContent: 'center',
              }}>
              <Image
                style={{width: 51, height: 51, borderRadius: 25}}
                source={Images.refreshIcon}
                resizeMode={'cover'}
              />
            </TouchableOpacity> */}
          </View>
        </View>
        {this.state.preferredDone == false && this.state.rideTyp != 'child' ? (
          <TouchableOpacity
            onPress={() => this.preferredAction()}
            style={{
              width: '100%',
              height: 50,
              backgroundColor: '#f2f2f2',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Image
              style={{width: 25, height: 25, borderRadius: 25}}
              source={Images.perferDriver}
              resizeMode={'cover'}
            />
            <Text style={{fontSize: RFValue(16), fontWeight: '500', margin: 5}}>
              {strings.PreferredDriver}
            </Text>
          </TouchableOpacity>
        ) : null}

        {this.state.preferredDone == true && this.state.rideTyp == 'child' ? (
          <TouchableOpacity
            onPress={() => this.liveFeedAction()}
            style={{
              width: '100%',
              height: 60,
              backgroundColor: '#f2f2f2',
              flexDirection: 'row',
              justifyContent: 'center',
              //marginBottom: 50,
              paddingBottom: 10,
              alignItems: 'center',
            }}>
            <Image
              style={{width: 25, height: 25, borderRadius: 25}}
              source={Images.online}
              resizeMode={'cover'}
            />
            <Text style={{fontSize: RFValue(16), fontWeight: '500', margin: 5}}>
              {strings.LiveFeed}
            </Text>
          </TouchableOpacity>
        ) : null}

        {this.state.preferredDone == false && this.state.rideTyp == 'child' ? (
          <View
            style={{
              borderColor: 'transparent',
              borderWidth: 1,
              width: '100%',
              height: 60,
              backgroundColor: '#f2f2f2',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <TouchableOpacity
              onPress={() => this.preferredAction()}
              style={{
                width: '48%',
                height: 50,
                backgroundColor: '#f2f2f2',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Image
                style={{width: 25, height: 25, borderRadius: 25}}
                source={Images.perferDriver}
                resizeMode={'cover'}
              />
              <Text
                style={{fontSize: RFValue(16), fontWeight: '500', margin: 5}}>
                {strings.PreferredDriver}
              </Text>
            </TouchableOpacity>
            <View
              style={{
                backgroundColor: '#dfdfdf',
                width: 3,
                height: '100%',
              }}
            />
            <TouchableOpacity
              onPress={() => this.liveFeedAction()}
              style={{
                width: '48%',
                height: 60,
                backgroundColor: '#f2f2f2',
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                paddingBottom: 10,
              }}>
              <Image
                style={{width: 25, height: 25, borderRadius: 25}}
                source={Images.online}
                resizeMode={'cover'}
              />
              <Text
                style={{fontSize: RFValue(16), fontWeight: '500', margin: 5}}>
                {strings.LiveFeed}
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  };

  render() {
    if (this.state.ChildVideo == true) {
      return this.videoView();
    }
    return (
      <View
        style={{
          flex: 1,
        }}>
        <View style={{flex: 1}}>
          <View style={styles.container}>
            {this.state.tripStatus == Config.TRIP_PICKUP_INROUTE ||
            this.state.tripStatus == Config.TRIP_ARRIVED ||
            this.state.tripStatus == Config.TRIP_DESTINATION_INROUTE ||
            this.state.tripStatus == Config.TRIP_COMPLETED ? (
              <MapView
                // provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                style={styles.mapView}
                //showsUserLocation={true}
                showsMyLocationButton={true}
                zoomEnabled={true}
                region={{
                  latitude: this.state.driverLatitude,
                  longitude: this.state.driverLongitude,
                  latitudeDelta: 0.009,
                  longitudeDelta: 0.001,
                }}>
                <MapView.Marker
                  style={{
                    transform: [
                      {
                        rotate:
                          this.state.driverAngle === undefined
                            ? '0deg'
                            : `${this.state.driverAngle}deg`,
                      },
                    ],
                  }}
                  ref={marker => {
                    this.marker = marker;
                  }}
                  flat={false}
                  //  rotation = {200}
                  coordinate={{
                    latitude: this.state.driverLatitude,
                    longitude: this.state.driverLongitude,
                  }}>
                  <Image
                    resizeMode="contain"
                    source={Images.carTop}
                    style={{height: 30, width: 30}}
                  />
                </MapView.Marker>

                {this.state.tripStatus == Config.TRIP_PICKUP_INROUTE ||
                this.state.tripStatus == Config.TRIP_ARRIVED
                  ? this.beforeTripStartRender()
                  : null}

                {this.state.tripStatus == Config.TRIP_DESTINATION_INROUTE
                  ? this.afterTripStartRender()
                  : null}
              </MapView>
            ) : null}

            {this.state.tripStatus == 'fresh' &&
            (!this.state.sourceSearchMap && !this.state.destSearchMap)
              ? this.freshRenderView()
              : null}

            {this.state.pickUpLocationModal
              ? this._showPickLocationModal()
              : null}
            {this.state.dropOffLocationModal
              ? this._showDropOffLocationModal()
              : null}
            {this.state.sourceSearchMap
              ? this.serachSourceLocationOnMap()
              : null}
            {this.state.destSearchMap
              ? this.serachDestinationLocationOnMap()
              : null}
            {this.state.showitem == true ? this.showItemMapRenderView() : null}
            {(this.state.tripStatus == 'fresh' ||
              this.state.tripStatus != 'booking') &&
            !this.state.pickUpLocationModal &&
            !this.state.destSearchMap &&
            !this.state.sourceSearchMap &&
            !this.state.dropOffLocationModal ? (
              <TouchableOpacity
                style={styles.backTouchable}
                onPress={() => this.openDrawerClick()}>
                <Image
                  source={Images.menuIcon}
                  style={{
                    width: moderateScale(55),
                    height: moderateScale(55),
                    // marginLeft: moderateScale(5),
                  }}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            ) : null}
            {this.state.tripStatus == 'booking' ? (
              <TouchableOpacity
                style={styles.backTouchable}
                onPress={() => this.BackAction()}>
                <Image
                  source={Images.backIcon}
                  style={{
                    width: moderateScale(55),
                    height: moderateScale(55),
                    marginLeft: moderateScale(5),
                  }}
                  resizeMode="contain"
                />
              </TouchableOpacity>
            ) : null}
          </View>

          {this.state.tripStatus == Config.TRIP_PICKUP_INROUTE ||
          this.state.tripStatus == Config.TRIP_ARRIVED
            ? this.otpScreenRenderView()
            : null}

          {this.state.tripStatus == Config.TRIP_COMPLETED
            ? this.tripCompleteRenderView()
            : null}

          {this.state.FindDriver == true
            ? this.findingDriverRenderView()
            : null}

          {this.state.tripStatus == Config.TRIP_DESTINATION_INROUTE
            ? this.destinationInRouteRender()
            : null}

          {this.state.preferredClick ? this.preferredRenderView() : null}
          {this.state.showitem ? this.showItemRenderView() : null}
          {this.state.RideDes ? this.descriptionRenderView() : null}
          {this.state.Schedule ? this.scheduleRenderView() : null}
          {this.state.shareView ? this.shareRenderView() : null}

          {this.state.paymentTap ? this.paymentMethodRenderView() : null}
          {this.state.NoDriverFoundView ? this.noDriverRenderView() : null}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  mapView: {
    width: '100%',
    height: '100%',
  },
  prediction: {
    paddingVertical: moderateScale(5),
    borderBottomWidth: 1.0,
    borderColor: 'gray',
  },
  // sourceDestMain: {
  //   marginHorizontal: wp('10%'),
  //   // marginLeft: wp('10%'),
  //   // marginRight: wp('10%'),
  //   marginTop: wp('20%'),
  //   borderRadius: wp('2%'),
  //   backgroundColor: 'white',
  //   height: moderateScale(140),
  //   width: wp('85%'),
  //   flexDirection: 'row',
  // },
  sourceDestMain: {
    marginTop: moderateScale(75),
    borderRadius: 10,
    backgroundColor: 'white',
    width: wp('90%'),
    flexDirection: 'row',
    position: 'absolute',
    alignSelf: 'center',
    padding: moderateScale(10),
    alignItems: 'center',
  },
  sourceDestImg: {
    width: wp('5%'),
    marginLeft: wp('2%'),
    marginTop: wp('3%'),
    alignItems: 'center',
  },
  buttonBar: {
    height: moderateScale(70),
    backgroundColor: '#0093E9',
    display: 'flex',
    width: '100%',
    position: 'absolute',
    bottom: 0,
    left: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    paddingBottom: moderateScale(20),
  },
  localVideoStyle: {
    width: moderateScale(140),
    height: moderateScale(160),
    position: 'absolute',
    top: moderateScale(5),
    right: moderateScale(5),
    zIndex: moderateScale(100),
  },
  iconStyle: {
    fontSize: RFValue(34),
    paddingTop: moderateScale(15),
    paddingLeft: moderateScale(40),
    paddingRight: moderateScale(40),
    paddingBottom: moderateScale(15),
    borderRadius: 0,
  },

  loadDetail: {
    backgroundColor: '#E9E9E9',
    borderColor: '#E9E9E9',
    marginBottom: moderateScale(8),
    marginLeft: '5%',
    marginRight: '5%',
    flexDirection: 'row',
    alignItems: 'center',
    padding: moderateScale(10),
    shadowRadius: moderateScale(5),
    borderBottomWidth: 0.0,
  },
  // searchMain: {
  //   width: wp('75%'),
  //   marginLeft: wp('2%'),
  //   marginTop: wp('3%'),
  // },
  // searchSection: {
  //   flexDirection: 'row',
  //   justifyContent: 'flex-start',
  //   alignItems: 'center',
  //   backgroundColor: 'transparent',
  // },
  // pickupLocationImg: {
  //   width: wp('4%'),
  //   height: wp('5%'),
  // },
  verticalImg: {
    width: wp('10%'),
    height: wp('14%'),
  },
  dropLocationImg: {
    width: 25,
    height: 25,
    tintColor: 'tomato',
  },
  // pickuplocation: {
  //   fontSize: RFValue(14),
  //   height: wp('10%'),
  //   backgroundColor: 'white',
  //   width: '89%',
  // },

  pickuplocation: {
    width: '92%',
    marginBottom: 10,
  },
  dropLocation: {
    fontSize: RFValue(14),
    marginTop: wp('2%'),
    height: wp('10%'),
    width: '89%',
  },
  sectionDivider: {
    position: 'absolute',
    top: moderateScale(60),
    height: 1,
    width: '100%',
    backgroundColor: 'gray',
  },
  clearLocation: {
    alignItems: 'center',
    justifyContent: 'center',
    width: moderateScale(30),
    height: moderateScale(30),
  },

  gprsImg: {
    width: moderateScale(50),
    height: moderateScale(50),
  },
  suggestionSource: {
    height: 'auto',
    width: wp('85%'),
    backgroundColor: 'white',
    position: 'absolute',
    top: moderateScale(145),
    left: wp('10%'),
  },
  showItemAvoidingView: {
    height: moderateScale(360),
    width: '100%',
    backgroundColor: 'white',
    position: 'absolute',
    bottom: 0,
  },
  showItemMainView: {
    width: '100%',
    height: moderateScale(70),
    backgroundColor: 'white',
    flexDirection: 'row',
    marginTop: moderateScale(10),
  },
  standardRide: {
    width: '32%',
    marginLeft: moderateScale(10),
    height: moderateScale(40),
    backgroundColor: 'transparent',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  standardRideText: {
    color: 'black',
    fontWeight: 'bold',
    fontFamily: Fonts.Semibold,
    fontSize: RFValue(16),
    backgroundColor: 'transparent',
    marginLeft: moderateScale(10),
  },
  childRide: {
    width: '30%',
    marginLeft: moderateScale(10),
    height: moderateScale(40),
    backgroundColor: 'transparent',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  childRideText: {
    color: 'black',
    fontFamily: Fonts.Semibold,
    fontSize: RFValue(14),
    backgroundColor: 'transparent',
    marginLeft: moderateScale(10),
  },
  rideDescription: {
    width: '32%',
    marginLeft: moderateScale(5),
    height: moderateScale(40),
    backgroundColor: 'transparent',
    justifyContent: 'center',
    flexDirection: 'row',
    alignItems: 'center',
  },
  rideDescriptionText: {
    //color: Colors.iRed,
    fontFamily: Fonts.Semibold,
    fontWeight: 'bold',
    fontSize: RFValue(14),
    backgroundColor: 'transparent',
    marginLeft: moderateScale(10),
  },
  showItemListMain: {
    width: '100%',
    height: moderateScale(120),
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    marginLeft: '0%',
  },
  showItemList: {
    width: '100%',
    height: moderateScale(110),
  },
  checkedCarImg: {
    width: moderateScale(50),
    height: moderateScale(15),
    left: moderateScale(25),
    top: moderateScale(25),
    zIndex: moderateScale(2),
  },
  carImg: {
    width: moderateScale(60),
    height: moderateScale(60),
    borderRadius: moderateScale(30),
    borderWidth: 0.5,
    borderColor: 'grey',
  },
  carType: {
    color: '#CACDD4',
    fontWeight: 'bold',
    backgroundColor: 'transparent',
  },
  estimatedCostText: {
    color: 'black',
    fontSize: RFValue(14),
    backgroundColor: 'transparent',
    fontWeight: 'bold',
  },
  rideSelectIcon: {
    width: moderateScale(15),
    height: moderateScale(15),
  },
  suggestionDest: {
    height: 'auto',
    width: wp('85%'),
    backgroundColor: 'white',
    position: 'absolute',
    top: moderateScale(202),
    left: wp('10%'),
  },
  // locationGPRS: {
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   width: moderateScale(60),
  //   height: moderateScale(60),
  //   backgroundColor: 'transparent',
  // },

  searchIcon: {
    padding: moderateScale(10),
    width: moderateScale(10),
    height: moderateScale(10),
  },

  backTouchable: {
    position: 'absolute',
    width: wp('15%'),
    height: moderateScale(50),
    top: Platform.OS === 'ios' ? wp('9%') : 0,
    backgroundColor: 'transparent',
    marginLeft: Platform.OS === 'ios' ? 5 : 0,
  },
  backIcon: {
    position: 'absolute',
    width: moderateScale(22),
    height: moderateScale(20),
    top: moderateScale(20),
    left: moderateScale(15),
    backgroundColor: 'transparent',
  },
  arrowTile: {
    backgroundColor: 'transparent',
    flexDirection: 'row',
    position: 'absolute',
    right: moderateScale(20),
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 0,
    borderColor: '#818e97',
  },
  arrowTile1: {
    backgroundColor: 'transparent',
    flexDirection: 'row',
    position: 'absolute',
    right: moderateScale(40),
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 0,
    borderColor: '#818e97',
  },

  freshSourceDestMain: {
    borderRadius: 10,
    backgroundColor: 'white',
    width: wp('90%'),
    alignSelf: 'center',
    padding: 10,
    paddingBottom: moderateScale(15),
  },
  pickupLocationImg: {
    width: 25,
    height: 25,
    tintColor: Colors.primary1,
  },
  searchMain: {
    width: wp('85%'),
    marginLeft: 20,
    paddingRight: moderateScale(10),
  },
  searchSection: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'transparent',
    width: '90%',
  },
  locationGPRSMain: {
    width: '100%',
    backgroundColor: 'transparent',
    position: 'absolute',
    bottom: 0,
  },
  freshMapAddressStaticTxt: {
    color: Colors.PrimaryColor,
    fontFamily: Fonts.Regular,
    fontSize: 16,
  },
  line: {
    height: 1,
    width: '100%',
    backgroundColor: Colors.PrimaryColor,
    marginTop: 10,
  },
  locationGPRS: {
    alignSelf: 'flex-end',
    width: moderateScale(60),
    height: moderateScale(60),
    backgroundColor: 'transparent',
    marginRight: moderateScale(20),
  },
  button: {
    color: Colors.Black,
    fontFamily: Fonts.Semibold,
    fontSize: RFValue(16),
  },
});
