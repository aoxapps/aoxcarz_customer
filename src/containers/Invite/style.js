import {StyleSheet} from 'react-native';
import Fonts from '../../constants/Fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Colors from '../../constants/Colors';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.white,
  },

  inviteImageView: {
    width: '100%',
    height: hp('40%'),
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  inviteImage: {
    width: '80%',
    height: '80%',
  },
  inviteTextView: {
    width: '100%',
    height: 'auto',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    marginBottom: 30,
  },
  inviteText: {
    fontSize: 17,
    color: 'gray',
    marginTop: 10,
  },
  inviteRefer: {
    color: '#3a68d9',
    fontWeight: 'normal',
    textAlign: 'center',
    fontSize: 16,
  },

  title: {
    fontSize: 20,
    color: 'white',
  },

  gridViewBackground: {
    height: '100%',
    width: '100%',
    flex: 1,
    borderColor: '#f5f6f8',
    borderWidth: 0,
    backgroundColor: 'white',
  },

  arrowTile: {
    width: '90%',
    marginLeft: '5%',
    height: 45,
    borderRadius: 27,
    marginTop: 0,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 10,
    backgroundColor: Colors.Black,
  },

  loadDetail: {
    justifyContent: 'center',
  },
  bottomView: {position: 'absolute', bottom: 40, width: '100%'},
});
export default styles;
