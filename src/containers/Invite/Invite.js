import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  FlatList,
  Share,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  ScrollView,
  Keyboard,
  Alert,
  Linking,
} from 'react-native';
import {DrawerActions} from 'react-navigation-drawer';
import {strings} from '../../constants/languagesString';
import RenderHeader from './../../components/CustomComponent/renderHeaderMain';
import Images from '../../constants/Images';
import styles from './style';

export default class Invite extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      message: '',
      shareMessage: strings.peiTaxiRefer + 'http://www.maxicarz.com',
    };
  }

  componentDidMount() {}

  goToNextScreen = async () => {
    // Linking.canOpenURL('https://play.google.com/store/apps?hl=en').then(
    //   supported => {
    //     if (supported) {
    //       Linking.openURL('https://play.google.com/store/apps?hl=en');
    //     } else {
    //       console.log(
    //         "Don't know how to open URI: " +
    //           'https://play.google.com/store/apps?hl=en',
    //       );
    //     }
    //   },
    // );
    try {
      const result = await Share.share({
        message: this.state.shareMessage,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  openDrawerClick() {
    this.props.navigation.dispatch(DrawerActions.openDrawer());
  }

  render() {
    return (
      <View style={styles.container}>
        <RenderHeader
          back={true}
          title={strings.InviteFriend}
          navigation={this.props.navigation}
        />

        <View style={styles.gridViewBackground}>
          <View style={styles.inviteImageView}>
            <Image
              resizeMode="contain"
              style={styles.inviteImage}
              source={Images.inviteTmage}
            />
          </View>

          <View style={styles.inviteTextView}>
            <Text style={styles.inviteText}>{strings.InviteYourFriends}</Text>
          </View>

          <View style={styles.inviteTextView} />

          <View style={styles.loadDetail}>
            <Text style={styles.inviteRefer}>
              {strings.maxiRefer} {'\n'}
              http://www.maxicarz.com
            </Text>
          </View>
          <View style={styles.bottomView}>
            <TouchableOpacity
              onPress={() => this.goToNextScreen()}
              style={styles.arrowTile}>
              <Text style={styles.title}> {strings.InviteFriend} </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
