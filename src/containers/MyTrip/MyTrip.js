import React from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  SafeAreaView,
  View,
  FlatList,
  ScrollView,
  ImageBackground,
} from 'react-native';
import moment from 'moment';
import { getStatusBarHeight } from '../../utils/IPhoneXHelper';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import VectorIcon from '../../utils/vectorIcons';
import { navigateTo } from '../../utils/CommonFunctions';
import { DrawerActions } from 'react-navigation-drawer';

import { strings } from '../../constants/languagesString';
import RenderHeader from './../../components/CustomComponent/renderHeaderMain';
import Fonts from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import Images from '../../constants/Images';
import { moderateScale, scale, verticalScale } from 'react-native-size-matters';
import { RFValue } from 'react-native-responsive-fontsize';

class MyTrips extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      type: 1,
      upcomingData: [],
      pastBokking: [],
      detailView: false,
      itemDetail: '',
      myTripsData: '',
    };

    const { navigation } = props;
    this.didFocusListener = navigation.addListener(
      'didFocus',
      this.componentDidFocus,
    );
  }

  componentDidFocus = payload => {
    // const {params} = payload.action;
    // console.log('hurrayyyyyy');
    this.getTripsData();
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.myTripsData !== this.props.myTripsData) {
      this.setState({ myTripsData: this.props.myTripsData }, () => {
        var data = this.state.myTripsData;
        this.afterGetTripsData(data);
      });
    }
    if (prevProps.cancelTripData !== this.props.cancelTripData) {
      this.setState({ cancelTripData: this.props.cancelTripData }, () => {
        if (this.state.cancelTripData.status === 'success') {
          this.afterCancelTrip();
        }
      });
    }
  }
  getTripsData = () => {
    this.props.myTripsRequest(this, this.props.navigation);
  };
  afterGetTripsData = data => {
    var upcomingData = data.upcomingBooking;
    var pastData = data.pastBooking;

    //console.log('upcomingData value --- ', upcomingData);
    // console.log('pastData value --- ', pastData);
    this.setState({
      upcomingData: upcomingData,
      pastBokking: pastData,
    });
  };
  showAlert(message, duration) {
    this.setState({ autoLogin: false });
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      alert(message);
    }, duration);
  }

  goToDetails(item, type) {
    if (item.tripStatus == 'schedule') {
      this.setState({ detailView: true, itemDetail: item, itemType: type });
    } else {
      this.setState({ detailView: true, itemDetail: item, itemType: type });
    }
  }

  upcomingClick() {
    this.setState({ type: 1 });
    this.getTripsData();
  }

  pastClick() {
    this.setState({ type: 2 });
    this.getTripsData();
  }

  cancelTrip(item) {
    this.callCancelTrip(item);
  }

  callCancelTrip(item) {
    let data = {
      tripId: item._id,
    };
    this.props.cancelTripRequest(data);
  }

  afterCancelTrip() {
    this.getTripsData();
  }

  getDateAndTime(dateTime) {
    // var newDate = moment(dateTime).format('DD-MM-YYYY');
    var newDate = moment(dateTime).format('MM-DD-YYYY');
    var newTime = moment(dateTime).format('hh:mm A');
    return newDate + ' ' + 'at' + ' ' + newTime;
  }

  openDrawerClick() {
    // this.props.navigation.dispatch(DrawerActions.openDrawer());
    this.setState({ detailView: false });
  }

  renderItems = (item, type) => {
    // debugger;
    return (
      <TouchableOpacity
        style={{
          flex: 1,
          flexDirection: 'row',
          paddingHorizontal: 20,
          paddingVertical: 10,
        }}
        onPress={() => this.goToDetails(item, type)}>
        <View style={{ flex: 1, padding: 10 }}>
          <Text style={{ fontFamily: Fonts.Regular, fontSize: wp(4) }}>
            {/* {item.scheduleDate + ' at ' + item.scheduleTime} */}
            {item.trip_type === 'schedule'
              ? item.scheduleDate + ' at ' + item.scheduleTime
              : this.getDateAndTime(item.tripEndedAt)}
          </Text>
          <View
            style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
            <VectorIcon
              name={'radio-btn-active'}
              groupName={'Fontisto'}
              size={10}
              color={Colors.PrimaryColor}
            />
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              <Text
                numberOfLines={1}
                style={{
                  marginLeft: 10,
                  fontFamily: Fonts.Regular,
                  color: 'grey',
                }}>
                {item.startLocationAddr}
              </Text>
            </ScrollView>
          </View>
          <View
            style={{
              width: 1,
              height: 10,
              padding: 0,
              marginLeft: 4,
              backgroundColor: Colors.bottomBorder,
            }}
          />
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              marginLeft: -3,
            }}>
            <VectorIcon
              name={'location-on'}
              groupName={'MaterialIcons'}
              size={14}
              color={'red'}
            />
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              <Text
                numberOfLines={1}
                style={{
                  marginLeft: 10,
                  fontFamily: Fonts.Regular,
                  color: 'grey',
                }}>
                {item.endLocationAddr}
              </Text>
            </ScrollView>
          </View>
        </View>
        <View
          style={{
            flex: 0.4,
            justifyContent: 'space-around',
            alignItems: 'center',
          }}>
          <Text
            style={{
              fontFamily: Fonts.Semibold,
              fontSize: wp(4),
              textAlign: 'center',
              color: Colors.PrimaryColor,
            }}>
            ${item.estimatedCost}
            {type === 1 && (
              <Text
                style={{
                  fontFamily: Fonts.Regular,
                  fontSize: wp(3),
                  color: 'grey',
                }}>{`\nEstimated Price`}</Text>
            )}
          </Text>

          {type === 1 ? (
            <TouchableOpacity
              onPress={() => this.cancelTrip(item)}
              style={{
                borderWidth: 1,
                borderRadius: 20,
                borderColor: 'red',
                padding: 2,
                //paddingVertical: 2,
              }}>
              <Text style={{ color: 'red' }}> {strings.Cancel} </Text>
            </TouchableOpacity>
          ) : item.tripStatus === strings.Completed ? (
            <Text style={{ color: 'green' }}> {strings.Completed}</Text>
          ) : (
            <Text style={{ color: 'red' }}> {strings.Cancelled}</Text>
          )}
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    const { type } = this.state;

    return (
      <View style={{ flex: 1, backgroundColor: 'white' }}>
        <RenderHeader
          back={true}
          title={strings.MyTrips}
          navigation={this.props.navigation}
        />
        <View style={{ flex: 1 }}>
          <View style={{ flexDirection: 'row', height: wp(10) }}>
            <TouchableOpacity
              style={{
                flex: 1,
                borderBottomWidth: 2,
                borderColor:
                  type === 1 ? Colors.PrimaryColor : Colors.bottomBorder,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => this.upcomingClick()}>
              <Text
                style={{
                  color: type === 1 ? Colors.PrimaryColor : Colors.bottomBorder,
                  fontFamily: Fonts.Semibold,
                  fontSize: wp(4),
                }}>
                {strings.Upcoming}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                flex: 1,
                borderBottomWidth: 2,
                borderColor:
                  type === 2 ? Colors.PrimaryColor : Colors.bottomBorder,
                justifyContent: 'center',
                alignItems: 'center',
              }}
              onPress={() => this.pastClick()}>
              <Text
                style={{
                  color: type === 2 ? Colors.PrimaryColor : Colors.bottomBorder,
                  fontFamily: Fonts.Semibold,
                  fontSize: wp(4),
                }}>
                {strings.Past}
              </Text>
            </TouchableOpacity>
          </View>

          {this.state.type == 1 ? (
            <View style={{ flex: 1 }}>
              {this.state.upcomingData.length > 0 ? (
                <FlatList
                  style={{ flex: 1 }}
                  data={this.state.upcomingData}
                  renderItem={({ item }) => this.renderItems(item, type)}
                  ItemSeparatorComponent={() => (
                    <View
                      style={{
                        width: '100%',
                        height: 1,
                        backgroundColor: Colors.bottomBorder,
                      }}
                    />
                  )}
                />
              ) : (
                <Text
                  style={{
                    width: wp('100%'),
                    marginTop: 20,
                    textAlign: 'center',
                  }}>
                  {' '}
                  {strings.Nobookingsavailable}{' '}
                </Text>
              )}
            </View>
          ) : (
            <View style={{ flex: 1 }}>
              {this.state.pastBokking.length > 0 ? (
                <FlatList
                  style={{ flex: 1 }}
                  data={this.state.pastBokking}
                  renderItem={({ item }) => this.renderItems(item, type)}
                  ItemSeparatorComponent={() => (
                    <View
                      style={{
                        width: '100%',
                        height: 1,
                        backgroundColor: Colors.bottomBorder,
                      }}
                    />
                  )}
                />
              ) : (
                <Text
                  style={{
                    width: wp('100%'),
                    marginTop: 20,
                    textAlign: 'center',
                  }}>
                  {' '}
                  {strings.Nobookingsavailable}{' '}
                </Text>
              )}
            </View>
          )}
        </View>

        {this.state.detailView ? (
          <View
            style={{
              width: '100%',
              height: '100%',
              backgroundColor: 'white',
              position: 'absolute',
            }}>
            <ImageBackground
              style={{
                height: Platform.select({
                  ios: 40 + getStatusBarHeight(),
                  android: 50,
                }),
                width: '100%',
                //  borderColor: '#0082cb',
                borderWidth: 0,
                alignItems: 'center',
                paddingTop: Platform.select({
                  ios: getStatusBarHeight(),
                  android: 0,
                }),
                flexDirection: 'row',
              }}

            >
              <TouchableOpacity
                style={{ flex: 2 }}
                onPress={() => this.openDrawerClick()}>
                <Image
                  source={Images.backIcon}
                  style={{ width: moderateScale(55), height: moderateScale(55) }}
                  resizeMode="contain"
                />
              </TouchableOpacity>
              <Text
                style={{
                  fontSize: RFValue(18),
                  color: Colors.Black,
                  flex: 6,
                  fontFamily: Fonts.Semibold,
                  textAlign: 'center',
                }}>
                {strings.myTripDetail}
              </Text>
              <View style={{ flex: 2 }} />
            </ImageBackground>
            <View
              style={{ width: '100%', height: 200, backgroundColor: 'white' }}>
              <View style={{ flex: 1, padding: 10, backgroundColor: 'white' }}>
                <View style={{ width: '100%', height: 50, flexDirection: 'row' }}>
                  <Text style={{ fontFamily: Fonts.Regular, fontSize: wp(4) }}>
                    {this.state.itemDetail.trip_type === 'schedule'
                      ? this.state.itemDetail.scheduleDate +
                      ' at ' +
                      this.state.itemDetail.scheduleTime
                      : this.getDateAndTime(this.state.itemDetail.tripEndedAt)}

                    {/* {this.getDateAndTime(this.state.itemDetail.tripEndedAt)} */}
                    {/* {this.state.itemDetail.scheduleDate +
                      ' at ' +
                      this.state.itemDetail.scheduleTime} */}
                  </Text>

                  <Text
                    style={{
                      fontFamily: Fonts.Semibold,
                      fontSize: wp(4),
                      textAlign: 'center',
                      position: 'absolute',
                      right: 0,
                    }}>
                    ${this.state.itemDetail.estimatedCost}
                    {this.state.itemType === 1 && (
                      <Text
                        style={{
                          fontFamily: Fonts.Regular,
                          fontSize: wp(3),
                          color: 'grey',
                        }}>{`\nEstimated Price`}</Text>
                    )}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginTop: 10,
                  }}>
                  <VectorIcon
                    name={'radio-btn-active'}
                    groupName={'Fontisto'}
                    size={10}
                    color={Colors.PrimaryColor}
                  />
                  <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    <Text
                      numberOfLines={1}
                      style={{
                        marginLeft: 10,
                        fontFamily: Fonts.Regular,
                        color: 'grey',
                      }}>
                      {this.state.itemDetail.startLocationAddr}
                    </Text>
                  </ScrollView>
                </View>
                <View
                  style={{
                    width: 1,
                    height: 10,
                    padding: 0,
                    marginLeft: 4,
                    backgroundColor: Colors.bottomBorder,
                  }}
                />
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    marginLeft: -3,
                  }}>
                  <VectorIcon
                    name={'location-on'}
                    groupName={'MaterialIcons'}
                    size={14}
                    color={'red'}
                  />
                  <ScrollView horizontal showsHorizontalScrollIndicator={false}>
                    <Text
                      numberOfLines={1}
                      style={{
                        marginLeft: 10,
                        fontFamily: Fonts.Regular,
                        color: 'grey',
                      }}>
                      {this.state.itemDetail.endLocationAddr}
                    </Text>
                  </ScrollView>
                </View>
              </View>
              <View
                style={{
                  flex: 0.4,
                  justifyContent: 'space-around',
                  alignItems: 'center',
                }}>
                {this.state.itemType === 1 ? (
                  <TouchableOpacity
                    onPress={() => this.cancelTrip(this.state.itemDetail)}
                    style={{
                      borderWidth: 1,
                      borderRadius: 20,
                      borderColor: 'red',
                      padding: 2,
                      //paddingVertical: 2,
                    }}>
                    <Text style={{ color: 'red' }}> {strings.Cancel} </Text>
                  </TouchableOpacity>
                ) : (
                  <View
                    style={{
                      backgroundColor: 'transparent',
                      alignItems: 'center',
                      justifyContent: 'center',
                      width: '100%',
                      height: 40,
                    }}>
                    <Text
                      style={{
                        color: Colors.Black,
                        fontSize: 18,
                        fontWeight: '500',
                      }}>
                      {strings.youRideWith} {this.state.itemDetail.driverName}
                    </Text>
                  </View>
                )}
              </View>
            </View>
          </View>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  headerView: {
    height: 80,
    width: '100%',
    backgroundColor: Colors.PrimaryColor,
    borderColor: '#0082cb',
    borderWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backTouchable: {
    position: 'absolute',
    width: 60,
    height: 50,
    top: 0,
    left: 0,
  },
  backIcon: {
    position: 'absolute',
    width: 22,
    height: 22,
    top: 30,
    left: 15,
    backgroundColor: 'transparent',
    tintColor: 'white',
  },
  title: {
    fontSize: 20,
    color: 'white',
    marginTop: 10,
  },
  statusTile: {
    flex: 1,
    marginHorizontal: 10,
    backgroundColor: 'transparent',
    height: 'auto',
    width: 'auto',
  },
  statusTile1: {
    backgroundColor: 'transparent',
    height: 'auto',
    width: 'auto',
    marginRight: wp('5.5%'),
  },
  statusText: {
    fontFamily: Fonts.Semibold,
    fontSize: wp('4%'),
    marginBottom: wp('1%'),
    color: 'green',
    textAlign: 'right',
    alignSelf: 'stretch',
  },
  statusCancelText: {
    fontFamily: Fonts.Semibold,
    fontSize: wp('4%'),
    marginBottom: wp('1%'),
    color: 'red',
    textAlign: 'right',
    alignSelf: 'stretch',
  },
});

export default MyTrips;
