import MyTrip from './MyTrip';
import {myTripsRequest} from './../../modules/MyTrips/actions';
import {cancelTripRequest} from '../../modules/Trip/actions';
import {connect} from 'react-redux';
const mapStateToProps = state => ({
  myTripsData: state.myTripsReducer.data,
  cancelTripData: state.tripReducer.cancelTripData,
});

const mapDispatchToProps = dispatch => ({
  myTripsRequest: navigation => dispatch(myTripsRequest(navigation)),
  cancelTripRequest: (data, navigation) =>
    dispatch(cancelTripRequest(data, navigation)),
});
export default connect(mapStateToProps, mapDispatchToProps)(MyTrip);
//export default MyTrip;
