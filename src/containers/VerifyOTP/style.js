import {StyleSheet} from 'react-native';
import Fonts from '../../constants/Fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Colors from '../../constants/Colors';
import {RFValue} from 'react-native-responsive-fontsize';
import {moderateScale, scale} from 'react-native-size-matters';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.background,
  },
  otpHeading: {
    backgroundColor: 'transparent',
    width: 'auto',
    height: 'auto',
    marginTop: moderateScale(20),
    marginHorizontal: moderateScale(20),
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  otpText: {
    fontFamily: Fonts.Light,
    fontSize: RFValue(16),
  },
  phoneText: {
    fontFamily: Fonts.Regular,
    fontSize: RFValue(16),
    marginTop: moderateScale(5),
  },
  otp: {
    backgroundColor: Colors.background,
    width: 'auto',
    height: moderateScale(50),
    marginTop: moderateScale(20),
    marginHorizontal: moderateScale(20),
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  otpInput: {
    width: moderateScale(40),
    height: moderateScale(50),
    paddingBottom: 0,
    borderBottomWidth: 1,
    fontSize: RFValue(16),
    marginHorizontal: 5,
  },
  gridViewBackground: {
    width: '100%',
    marginTop: moderateScale(10),
    borderColor: '#f5f6f8',
    backgroundColor: Colors.background,
  },
  arrowIcon: {
    width: '100%',
    height: '100%',
  },
  touchableArrow: {
    height: wp('16%'),
    width: wp('16%'),
    marginRight: moderateScale(20),
  },
  arrowTile: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginVertical: moderateScale(20),
  },
  resendView: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    marginVertical: moderateScale(20),
    marginTop: moderateScale(40),
  },
  resendTile: {
    width: '100%',
    height: wp('9%'),
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
  },
  txtResend: {
    fontFamily: Fonts.Semibold,
    color: 'red',
    fontSize: RFValue(16),
  },

  bottom: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    backgroundColor: Colors.primary1,
    height: moderateScale(45),
    marginHorizontal: moderateScale(20),
    borderRadius: moderateScale(30),
    bottom: moderateScale(25),
    width: '90%',
  },
});
export default styles;
