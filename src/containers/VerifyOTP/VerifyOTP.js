import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  FlatList,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  ScrollView,
  Keyboard,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {showAlert} from '../../utils/CommonFunctions';
import RenderHeader from './../../components/CustomComponent/renderHeader';
import {connect} from 'react-redux';
import {resetRequest} from '././../../modules/VerifyOTP/actions';
import Fonts from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import Images from '../../constants/Images';
import Config from '../../constants/Config';
import Constants from '../../constants/Constants';
import ValidationMessage from '../../constants/ValidationMessage';
import styles from './style';
import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {moderateScale} from 'react-native-size-matters';
import {RFValue} from 'react-native-responsive-fontsize';
import strings from '../../constants/languagesString';
class VerifyOTP extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      mainViewTop: 0,
      code: '',
      serverOTP: this.props.navigation.getParam('otp'),
      mobileNumber: this.props.navigation.getParam('mobileNumber'),
      isNew: this.props.navigation.getParam('isNew'),
    };
    this.pinInput = React.createRef();
  }

  componentDidMount() {
    const {navigation} = this.props;

    if (navigation.getParam('forgot')) {
      this.requestReset();
    }
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow.bind(this),
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide.bind(this),
    );
  }
  _keyboardDidShow() {
    this.setState({
      mainViewTop: -200,
    });
  }

  _keyboardDidHide() {
    this.setState({
      mainViewTop: 0,
    });
  }
  goBack() {
    this.props.navigation.goBack();
  }
  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  goToNextScreen = () => {
    const {code, serverOTP, mobileNumber} = this.state;
    const {navigation, resetData} = this.props;
    var otp = code;
    var isNew = navigation.getParam('isNew');
    if ((resetData && otp == resetData.data.OTP) || otp == serverOTP) {
      if (navigation.getParam('forgot') || isNew === 'no') {
        this.props.navigation.navigate(Config.ChangePassword, {
          mobileNumber: mobileNumber,
          otp: otp,
          forgot: true,
        });
      } else {
        this.props.navigation.navigate(Config.Register, {
          mobileNumber: mobileNumber,
          countryCode: navigation.getParam('countryCode', ''),
          countryName: navigation.getParam('countryName', ''),
          otp: otp,
        });
      }
    } else {
      this.pinInput.current.shake().then(() => this.setState({code: ''}));
      showAlert(ValidationMessage.otpValid);
    }
  };

  requestReset = () => {
    const {navigation, resetRequest} = this.props;
    resetRequest(
      navigation.getParam('countryCode'),
      navigation.getParam('mobileNumber'),
    );
  };

  render() {
    const {code} = this.state;
    return (
      <View style={[styles.container, {}]}>
        <RenderHeader
          back={true}
          title={'Verify OTP'}
          navigation={this.props.navigation}
        />

        <View style={styles.gridViewBackground}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Image
              resizeMode="contain"
              style={{
                width: moderateScale(150),
                height: moderateScale(150),
              }}
              source={Images.otpIcon}
            />
          </View>
          <View style={styles.otpHeading}>
            <Text style={styles.otpText}>{strings.otpScreenText}</Text>
            <Text style={styles.phoneText}>
              {this.props.navigation.getParam('countryCode', '')}{' '}
              {this.props.navigation.getParam('mobileNumber', '')}{' '}
              {strings.via_sms}
            </Text>
          </View>

          <View style={styles.otp}>
            <SmoothPinCodeInput
              ref={this.pinInput}
              cellStyle={{
                borderBottomWidth: 2,
                borderColor: 'gray',
              }}
              cellStyleFocused={{
                borderColor: 'black',
              }}
              keyboardType="numbers-and-punctuation"
              value={code}
              onTextChange={code => this.setState({code})}
            />
          </View>
        </View>

        <View style={styles.resendView}>
          <TouchableOpacity
            style={styles.resendTile}
            onPress={this.requestReset}>
            <Text style={styles.txtResend}>Resend code</Text>
          </TouchableOpacity>
        </View>

        <TouchableOpacity style={styles.bottom} onPress={this.goToNextScreen}>
          <Text
            style={{
              color: Colors.Black,
              fontFamily: Fonts.Semibold,
              fontSize: RFValue(16),
            }}>
            {strings.Submit}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
const mapStateToProps = state => ({
  resetData: state.resetotpReducer.resetData,
});
const mapDispatchToProps = dispatch => ({
  resetRequest: (countryCode, mobileNumber) =>
    dispatch(resetRequest(countryCode, mobileNumber)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(VerifyOTP);
