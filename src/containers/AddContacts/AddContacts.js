import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  FlatList,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  ScrollView,
  Platform,
  Keyboard,
  Alert,
} from 'react-native';
import {DrawerActions} from 'react-navigation';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import Contacts from 'react-native-contacts';
import {PermissionsAndroid} from 'react-native';
import {strings} from '../../constants/languagesString';
import RenderHeader from './../../components/CustomComponent/renderHeader';
import Fonts from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import Images from '../../constants/Images';

import styles from './style';
export default class AddContacts extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      contacts: [],
    };
    const {navigation} = props;
    this.didFocusListener = navigation.addListener(
      'didFocus',
      this.componentDidFocus,
    );
  }

  componentWillUnmount() {
    this.didFocusListener.remove();
  }

  componentDidFocus = payload => {
    const {params} = payload.action;
  };

  async requestCameraPermission() {
    try {
      if (Platform.OS === 'ios') {
        Contacts.getAll((err, contacts) => {
          if (err) {
            if (err == 'denied') {
              Alert.alert(
                strings.AccessDenied,
                strings.Pleasegrantpermissiontoaccesscontactssettings,
                [
                  {
                    text: strings.Ok,
                    onPress: () => this.props.navigation.goBack(),
                  },
                ],
              );
            } else {
              throw err;
            }
          }
          contacts.sort((a, b) =>
            a.givenName !== b.givenName
              ? a.givenName < b.givenName
                ? -1
                : 1
              : 0,
          );
          this.setState({contacts});
        });
      } else if (Platform.OS === 'android') {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
          {
            title: strings.Contacts,
            message: strings.Thisappwouldliketoviewyourcontacts,
            buttonNegative: 'Cancel',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          // console.log('You can use the camera');
          alert('dasdsadada');
          Contacts.getAll((err, contacts) => {
            if (err === 'denied') {
            } else {
              contacts.sort((a, b) =>
                a.givenName !== b.givenName
                  ? a.givenName < b.givenName
                    ? -1
                    : 1
                  : 0,
              );
              this.setState({contacts});
            }
          });
        } else {
          //  console.log('Camera permission denied');
        }
      }
    } catch (err) {
      console.log(err);
    }
  }

  componentDidMount() {
    // this.requestCameraPermission();
    if (Platform.OS === 'ios') {
      Contacts.getAll((err, contacts) => {
        if (err) {
          if (err == 'denied') {
            Alert.alert(
              strings.AccessDenied,
              strings.Pleasegrantpermissiontoaccesscontactssettings,
              [
                {
                  text: strings.Ok,
                  onPress: () => this.props.navigation.goBack(),
                },
              ],
            );
          } else {
            throw err;
          }
        }
        contacts.sort((a, b) =>
          a.givenName !== b.givenName
            ? a.givenName < b.givenName
              ? -1
              : 1
            : 0,
        );
        this.setState({contacts});
      });
    } else if (Platform.OS === 'android') {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
        title: strings.Contacts,
        message: strings.Thisappwouldliketoviewyourcontacts,
      }).then(() => {
        // alert('dasdsadada');
        Contacts.getAll((err, contacts) => {
          if (err === 'denied') {
          } else {
            contacts.sort((a, b) =>
              a.givenName !== b.givenName
                ? a.givenName < b.givenName
                  ? -1
                  : 1
                : 0,
            );
            this.setState({contacts});
          }
        });
      });
    }
  }

  showAlert(message, duration) {
    this.setState({autoLogin: false});
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      alert(message);
    }, duration);
  }

  addContact(namestr, number) {
    this.props.addContactRequest(namestr, number, this.props.navigation);
  }

  goBack() {
    this.props.navigation.goBack();
  }
  openDrawerClick() {
    this.props.navigation.dispatch(DrawerActions.openDrawer());
  }

  selectContact(item) {
    var number = item.phoneNumbers[0].number;
    var namedt =
      item.familyName != '' &&
      item.familyName != null &&
      item.familyName != undefined
        ? item.givenName + ' ' + item.familyName
        : item.givenName;
    this.addContact(namedt, number);
    // if (Platform.OS === 'ios') {
    //   var number = item.phoneNumbers[0].number;
    //   var namedt = item.familyName + ' ' + item.givenName;
    //   this.addContact(namedt, number);
    // }
  }

  render() {
    return (
      <View style={styles.container}>
        <RenderHeader
          back={true}
          title={strings.addNewContact}
          navigation={this.props.navigation}
        />
        <View style={styles.gridViewBackground}>
          <View style={{flex: 1}}>
            <FlatList
              data={this.state.contacts}
              renderItem={({item}) => (
                <TouchableOpacity
                  onPress={() => this.selectContact(item)}
                  style={{borderBottomWidth: 1, borderColor: 'gray'}}>
                  <Text style={styles.contact_details}>
                    {`${item.givenName} `} {item.familyName}
                  </Text>
                </TouchableOpacity>
              )}
              numColumns={1}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>
        </View>
      </View>
    );
  }
}
