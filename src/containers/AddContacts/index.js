import AddContacts from './AddContacts';
import {connect} from 'react-redux';
import {addContactRequest} from './../../modules/EmergencyContacts/actions';
const mapDispatchToProps = dispatch => ({
  addContactRequest: (namestr, number, navigation) =>
    dispatch(addContactRequest(namestr, number, navigation)),
});
export default connect(null, mapDispatchToProps)(AddContacts);
//export default AddContacts;
