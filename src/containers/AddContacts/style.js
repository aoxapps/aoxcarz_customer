import {StyleSheet} from 'react-native';
import Fonts from '../../constants/Fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Colors from '../../constants/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  headerView: {
    height: 60,
    width: '100%',
    backgroundColor: '#1c1c1a',
    borderColor: '#0082cb',
    borderWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    color: 'white',
    fontFamily: 'Arial',
  },

  gridViewBackground: {
    flex: 1,
    marginTop: 0,
    marginBottom: -50,
    borderColor: 'black',
    borderWidth: 0.0,
    backgroundColor: 'white',
  },
  backTouchable: {
    position: 'absolute',
    width: 60,
    height: 60,
    top: 0,
    left: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backIcon: {
    width: 22,
    height: 22,
    top: 0,
    left: 0,
    backgroundColor: 'transparent',
  },
  loadDetail: {
    width: 'auto',
    height: 'auto',
    backgroundColor: 'white',
    marginVertical: 7,
    marginHorizontal: 5,
    alignItems: 'center',
    flexDirection: 'row',
    borderColor: 'gray',
    borderBottomWidth: 0.6,
    paddingVertical: 5,
  },
  sourceAddressBG: {
    marginTop: 20,
    marginHorizontal: 20,
    height: 30,
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  destAddressBG: {
    marginTop: 0,
    marginHorizontal: 20,
    height: 30,
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  sourceIcon: {
    width: 15,
    height: 15,
  },
  equipmentIcon: {
    width: 0,
    height: 0,
  },
  arrowTile: {
    backgroundColor: 'transparent',
    height: wp('16%'),
    position: 'absolute',
    right: wp('5.33%'),
    left: wp('5.33%'),
    bottom: wp('18%'),
    alignItems: 'center',
    flexDirection: 'row',
    borderWidth: 0,
    borderColor: 'blue',
  },
  touchableArrow: {
    backgroundColor: '#2a4359',
    position: 'absolute',
    right: 0,
    height: wp('16%'),
    width: wp('16%'),
    bottom: 0,
    borderRadius: wp('8%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  arrowIcon: {
    width: wp('6.66%'),
    height: wp('6.66%'),
  },
  txtNoLoads: {
    fontFamily: 'Arial',
    marginTop: 50,
    width: '100%',
    textAlign: 'center',

    fontSize: wp('5.86%'),
  },
  tabViewBG: {
    flexDirection: 'row',
    marginHorizontal: 0,
    backgroundColor: '#ffffff',
    height: wp('10.66%'),
    width: '100%',
  },
  tab1BG: {
    backgroundColor: 'transparent',
    height: '100%',
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tab2BG: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    height: '100%',
    width: '50%',
  },
  txtUpcoming: {
    fontFamily: 'Arial',
    fontSize: wp('4.8%'),
    color: 'white',
  },
  phones: {
    fontSize: 20,
    textAlign: 'center',
    marginTop: 0,
    marginBottom: 10,
  },
  contact_details: {
    fontFamily: 'Arial',
    color: 'black',
    marginVertical: 10,
    marginLeft: 15,
    fontSize: wp('4.8%'),
  },
});

export default styles;
