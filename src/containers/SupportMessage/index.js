import SupportMessage from './SupportMessage';
import {supportRequest} from './../../modules/Support/actions';

import {connect} from 'react-redux';
const mapStateToProps = state => ({
  supportData: state.supportReducer.supportData,
});
const mapDispatchToProps = dispatch => ({
  supportRequest: (data, navigation) =>
    dispatch(supportRequest(data, navigation)),
});
export default connect(mapStateToProps, mapDispatchToProps)(SupportMessage);
//export default SupportMessage;
