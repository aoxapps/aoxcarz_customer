import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  FlatList,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  ScrollView,
  Keyboard,
  Alert,
} from 'react-native';
import {DrawerActions} from 'react-navigation-drawer';
//import { getConfiguration } from '../../utils/configuration';
import Activity from '../../components/ActivityIndicator';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {strings} from '../../constants/languagesString';
import RenderHeader from './../../components/CustomComponent/renderHeader';
import Fonts from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import Images from '../../constants/Images';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class SupportMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      message: '',
      supportData: '',
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.supportData !== this.props.supportData) {
      this.setState({supportData: this.props.supportData}, () => {
        var data = this.props.supportData;
        this.afterGetSupportData(data);
      });
    }
  }
  afterGetSupportData = data => {
    this.setState({name: '', message: ''});
  };
  componentDidMount() {}

  sendMessage() {
    let data = {
      name: this.state.name,
      msg: this.state.message,
    };
    this.props.supportRequest(data, this.props.navigation);
  }

  showAlert(message, duration) {
    setTimeout(() => {
      alert(message);
    }, 300);
  }

  goToNextScreen() {
    if (this.state.name.trim() != '' && this.state.message.trim() != '') {
      this.sendMessage();
    } else {
      this.showAlert(strings.Pleasefillallrequiredfields, 300);
    }
  }

  openDrawerClick() {
    this.props.navigation.goBack();
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <RenderHeader
          back={true}
          title={strings.HelpSupport}
          navigation={this.props.navigation}
        />

        <View style={{flex: 1, backgroundColor: 'white'}}>
          <KeyboardAwareScrollView>
            <View style={{flex: 0.4}}>
              <View
                style={{
                  width: '100%',
                  height: wp('50%'),
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'transparent',
                }}>
                <Image
                  resizeMode="contain"
                  style={{width: wp('50%'), height: wp('50%')}}
                  source={require('../../../assets/help_support.png')}
                />
              </View>

              <View
                style={{
                  width: '100%',
                  height: 'auto',
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: 'transparent',
                }}>
                <Text
                  style={{
                    fontSize: wp('5%'),
                    color: 'black',
                    //fontFamily: "CharlieDisplay-Semibold"
                  }}>
                  {strings.NeedSomeHelp}
                </Text>

                <Text
                  style={{
                    fontSize: wp('5%'),
                    marginTop: wp('5%'),
                    color: 'gray',
                    // fontFamily: "CharlieDisplay-Light"
                  }}>
                  {strings.Feelfreetogetintouchwithus}
                </Text>
              </View>
            </View>

            <View style={{flex: 0.6}}>
              <View style={styles.tile}>
                <TextInput
                  style={styles.searchTextInput}
                  placeholder="Enter Name"
                  placeholderTextColor={'#818e97'}
                  autoCorrect={false}
                  value={this.state.name}
                  onChangeText={name => {
                    if (name.length == 1 && name == ' ') {
                    } else {
                      this.setState({name});
                    }
                  }}
                  onSubmitEditing={() => this.message.focus()}
                />
              </View>

              <View style={styles.tileFeedback}>
                <Text
                  style={{
                    fontSize: 17,
                    color: 'gray',
                    //fontFamily: "CharlieDisplay-Light"
                  }}>
                  {strings.Message}
                </Text>
                <TextInput
                  style={[styles.searchTextInput, {height: 90}]}
                  placeholder=""
                  placeholderTextColor={'#000000'}
                  multiline={true}
                  numberOfLines={4}
                  autoCorrect={false}
                  returnKeyType={'done'}
                  blurOnSubmit={true}
                  onSubmitEditing={() => {
                    Keyboard.dismiss();
                  }}
                  onChangeText={message => {
                    if (message.length == 1 && message == ' ') {
                    } else {
                      this.setState({message});
                    }
                  }}
                  value={this.state.message}
                  ref={message => (this.message = message)}
                />
              </View>
            </View>
          </KeyboardAwareScrollView>
          <View style={styles.bottomView}>
            <View style={styles.arrowTile}>
              <TouchableOpacity
                onPress={() => this.goToNextScreen()}
                style={{
                  width: wp('95%'),
                  height: 45,
                  borderRadius: 20,
                  alignItems: 'center',
                  justifyContent: 'center',
                  marginHorizontal: 10,
                  backgroundColor: Colors.Black,
                }}>
                <Text style={styles.title}> {strings.Submit} </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },

  title: {
    fontSize: wp('4.8%'),
    color: 'white',
  },

  gridViewBackground: {
    height: '100%',
    width: '100%',
    //  marginTop: 10,
    borderColor: '#f5f6f8',
    borderWidth: 0,
    backgroundColor: 'white',
  },
  backTouchable: {
    position: 'absolute',
    width: 60,
    height: 60,
    top: 0,
    left: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backIcon: {
    width: 22,
    height: 22,
    top: 0,
    left: 0,
    backgroundColor: 'transparent',
  },

  tileIcon: {
    width: 20,
    height: 40,
    marginLeft: 20,
    marginTop: -5,
  },
  tile: {
    backgroundColor: 'transparent',
    width: 'auto',
    height: wp('13.33%'),
    marginTop: wp('5%'),
    marginHorizontal: 20,
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 1.0,
    borderColor: '#818e97',
  },
  searchTextInput: {
    height: 'auto',
    width: '100%',
    //paddingHorizontal: 10,
    backgroundColor: 'transparent',
    borderColor: 'gray',
    borderRadius: 0,
    fontSize: wp('4.8%'),
  },
  arrowIcon: {
    width: '100%',
    height: '100%',
  },
  touchableArrow: {
    backgroundColor: '#375064',
    position: 'absolute',
    right: 0,
    height: wp('16%'),
    width: wp('16%'),
    borderRadius: wp('8%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  arrowTile: {
    backgroundColor: 'transparent',
    width: '100%',
    height: 50,
    marginTop: wp('5%'),
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 0,
    borderColor: '#818e97',
  },
  // bottomView: {position: 'absolute', bottom: 40, width: '100%'},
  tileFeedback: {
    backgroundColor: 'transparent',
    width: 'auto',
    height: 120,
    marginTop: 30,
    marginHorizontal: 20,

    borderBottomWidth: 1.0,
    borderColor: '#818e97',
    borderRadius: 5,
  },
});
