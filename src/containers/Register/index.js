import Register from './Register';
import {connect} from 'react-redux';
import {registerRequest} from './../../modules/Register/actions';

const mapStateToProps = state => ({
  registerData: state.registerReducer.registerData,
});
const mapDispatchToProps = dispatch => ({
  regRequest: (data, navigation) => dispatch(registerRequest(data, navigation)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Register);
//export default Register;
