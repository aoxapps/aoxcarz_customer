import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  ImageBackground,
  TouchableOpacity,
  Keyboard,
  Modal,
  TouchableHighlight,
  BackHandler,
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
// import {
//   widthPercentageToDP as wp,
//   heightPercentageToDP as hp,
// } from 'react-native-responsive-screen';
import { Platform } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import Geolocation from 'react-native-geolocation-service';
import { NavigationActions, StackActions } from 'react-navigation';
import { isEmpty } from '../../utils/CommonFunctions';
import { getStatusBarHeight } from '../../utils/IPhoneXHelper';
import VectorIcon from '../../utils/vectorIcons';
import Fonts from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import Images from '../../constants/Images';
import Config from '../../constants/Config';
import Placeholder from '../../constants/Placeholder';
import PhoneInput from 'react-native-phone-input';
import ValidationMessage from '../../constants/ValidationMessage';
import ValidationRules from '../../utils/validation-rules';
import styles from './style';
import { moderateScale, scale } from 'react-native-size-matters';
import { RFValue } from 'react-native-responsive-fontsize';
import RenderHeader from './../../components/CustomComponent/renderHeader';

const regEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import strings from '../../constants/languagesString';
import { colors } from 'react-native-elements';

export default class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: this.props.navigation.getParam('mobileNumber', ''),
      password: '',
      address: '',
      name: this.props.navigation.getParam('name', ''),
      email: this.props.navigation.getParam('email', ''),
      registerType: this.props.navigation.getParam('registerType', ''),
      countryCode:
        this.props.navigation.getParam('countryCode', '').length > 0
          ? this.props.navigation.getParam('countryCode', '')
          : '+1',
      countryName:
        this.props.navigation.getParam('countryName', '').length > 0
          ? this.props.navigation.getParam('countryName', '')
          : 'ca',
      predictions: [],
      showSuggestion: false,
      sourceLocation: '',
      addressModel: false,
      modalVisible: false,
      curLatitude: '',
      curLongitude: '',
    };
  }
  componentDidMount() {
    // let countryCode = this.props.navigation.getParam('countryCode', '');
    // let countryName = this.props.navigation.getParam('countryName', '');
    // if (countryCode != null && countryCode != '') {
    //   this.setState({countryCode: countryCode, countryName: countryName});
    // }

    if (this.props.navigation.getParam('registerType', '') === 'Social') {
      this.setState({ countryCode: '+1', initialCountry: 'ca' });
    }
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );
    this.getCurrentLocation();
  }

  getCurrentLocation = async () => {
    await Geolocation.getCurrentPosition(
      position => {
        console.log('dasdada');
        this.setState({
          curLatitude: position.coords.latitude,
          curLongitude: position.coords.longitude,
        });
      },
      error => {
        this.setState({ error: error.message });
      },
      {
        enableHighAccuracy: false,
        timeout: 20000,
        maximumAge: 10000,
        distanceFilter: 100,
        // distanceFilter: 10,
      },
    );
  };

  handleBackButtonClick = () => {
    return true;
  };

  goToLogin = () => {
    const resetAction = StackActions.reset({
      index: 0,
      actions: [NavigationActions.navigate({ routeName: Config.Login })],
    });
    this.props.navigation.dispatch(resetAction);
  };

  verifyCredentials = () => {
    const { name, email, password, address } = this.state;
    if (isEmpty(name.trim())) {
      alert(strings.name_alert);
      return false;
    } else if (isEmpty(email.trim())) {
      alert(strings.email_alert);
      return false;
    } else if (!regEmail.test(email.trim())) {
      alert(strings.valid_email);
      return false;
    } else if (isEmpty(address.trim())) {
      alert(strings.address_alert);
      return false;
    } else if (isEmpty(password.trim())) {
      alert(strings.password_alert);
      return false;
    }
    return true;
  };

  goToNextScreen = () => {
    if (this.verifyCredentials()) {
      const { name, phone, email, password, address } = this.state;
      var data = {
        name: name,
        mobileNumber: phone,
        email: email,
        address: address,
        password: password,
        countryCode: this.props.navigation.getParam('countryCode', ''),
        gid: this.props.navigation.getParam('googleId', ''),
        fbid: this.props.navigation.getParam('facebookId', ''),
        appleid: this.props.navigation.getParam('appleId', ''),
        OTP: this.props.navigation.getParam('otp', ''),
        profileImage: this.props.navigation.getParam('photo', 'none'),
      };
      //  console.log('aaa');
      this.props.regRequest(data, this.props.navigation);
    }
  };

  googleAutocomplete = async (Location, curLat, curLong) => {
    const apiUrl =
      'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' +
      Location +
      '&key=' +
      Config.GOOGLE_MAPS_APIKEY +
      // '&components=country:in|country:ca' +
      '&location=' +
      curLat +
      ',' +
      curLong +
      '&radius=' +
      1000;
    const result = await fetch(apiUrl);
    const json = await result.json();
    return json;
  };

  async onChangeSource(sourceLocation) {
    this.setState({
      tentativePrice: '',
      showSuggestionDest: false,
      sourceLocation: sourceLocation,
    });
    var json = await this.googleAutocomplete(
      sourceLocation,
      this.state.curLatitude,
      this.state.curLongitude,
    );
    try {
      this.setState({
        predictions: json.predictions,
        showSuggestion: true,
      });
      var adress_data = json.predictions;

      this.setState({
        myaddress_list: adress_data,
      });

      if (json.predictions.length == 0) {
        this.setState({
          showSuggestion: false,
        });
      }
    } catch (err) {
      this.showAlert(err.message, 300);
    }
  }

  showAlert(message, duration) {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      alert(message);
    }, duration);
  }

  async setSourceLocation(placeId, description) {
    Keyboard.dismiss();
    this.setState({
      sourceLocation: description,
      showSuggestion: false,
      selSourcePlaceId: placeId,
      address: description,
      modalVisible: false,
    });

    if (this.state.destinationPlaceID.length > 0 && placeId.length > 0) {
      //console.log('source place1 id', this.state.selSourcePlaceId);
      // console.log('destination1 place id', this.state.destinationPlaceID);
      // this.getDirections();
      // this.getEstimateCost();
      // this.props.navigation.navigate('CarSchedule');
      // this.setState({
      // destinationLocation: '',
      // destinationPlaceID: ''
      // });
    }
  }

  selectCountry(country) {
    this.phone.selectCountry(country.iso2);
  }

  openDrawerClick() {
    this.setState({ modalVisible: false });
  }

  getCountryCode() {
    var country = this.phone.getCountryCode();
    var name = this.phone.getISOCode();
    this.setState({ countryCode: '+' + country, countryName: name });
  }

  openAddressModel(visible) {
    this.setState({ modalVisible: visible });
  }

  render() {
    const { address, filePath } = this.state;
    const predictions = this.state.predictions.map(prediction => (
      <TouchableHighlight
        style={{
          paddingVertical: moderateScale(5),
          borderBottomWidth: 0.7,
          borderColor: 'gray',
          backgroundColor: Colors.White,
          height: 'auto',
          marginHorizontal: moderateScale(20),
          // marginTop:10,
        }}
        onPress={() =>
          this.setSourceLocation(prediction.place_id, prediction.description)
        }>
        <Text style={{ margin: 10 }} key={prediction.id}>
          {prediction.description}
        </Text>
      </TouchableHighlight>
    ));
    return (
      <View
        style={{
          flex: 1,
          marginTop: this.state.mainViewTop,
          backgroundColor: Colors.White,
        }}>
        <RenderHeader
          isRegister={true}
          title={strings.register}
          navigation={this.props.navigation}
        />
        {/* <View style={styles.HeaderImage}>
          <ImageBackground
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              flex: 1,
              paddingTop: Platform.select({
                ios: getStatusBarHeight(),
                android: 0,
              }),
              backgroundColor: 'transparent',
            }}
            resizeMode="center">
            <Image
              resizeMode="contain"
              style={{
                width: '55%',
                height: '55%',
                backgroundColor: 'transparent',
              }}
              source={Images.logo}
            />
          </ImageBackground>
        </View> */}
        <KeyboardAwareScrollView
          style={[
            styles.lowerView,
            {
              paddingTop: this.state.scrollPadding,
              backgroundColor: Colors.background,
            },
          ]}>
          <View style={styles.tile}>
            <Image
              resizeMode="contain"
              style={styles.tileIcon}
              source={Images.profileIcon}
            />
            <TextInput
              style={styles.searchTextInput}
              placeholder="Name"
              placeholderTextColor={Placeholder.placeholderTextColor}
              autoCorrect={false}
              //onChangeText={name => this.setState({name})}
              onChangeText={name => {
                if (name.length == 1 && name == ' ') {
                } else {
                  this.setState({ name });
                }
              }}
              value={this.state.name}
              returnKeyType="next"
              onSubmitEditing={() => this.email.focus()}
            // this.refs.firstdigit.focus();
            />
          </View>

          <View style={styles.tile11}>
            <Text
              style={{
                position: 'absolute',
                left: 70,
                color: 'black',
                marginTop: wp('2.5%'),
                fontSize: RFValue(16),
              }}>
              {this.state.countryCode}
            </Text>
            <View
              style={{
                height: '100%',
                width: 0,
                top: -5,
                left: 100,
              }}
            />

            <PhoneInput
              ref={phone => {
                this.phone = phone;
              }}
              //returnKeyType="next"
              //ref={phone => (this.phone = phone)}
              textProps={{
                returnKeyType: 'done',
                placeholder: 'Mobile Number',
                placeholderTextColor: Placeholder.placeholderTextColor,
                value: this.state.phone,
              }}
              textStyle={styles.searchTextInput}
              disabled={this.state.registerType == 'Social' ? false : true}
              // editable={this.state.type == 'social' ? true : false}
              onChangePhoneNumber={phone => {
                if (phone.length == 1 && phone == ' ') {
                } else {
                  this.setState({ phone });
                }
              }}
              onSelectCountry={countryCode => this.getCountryCode(countryCode)}
              offset={60}
              flagStyle={{ marginLeft: wp('2%') }}
              initialCountry={this.state.countryName}
            />
          </View>

          <View style={styles.tile}>
            <Image
              resizeMode="contain"
              style={styles.tileIcon}
              source={Images.emailIcon}
            />
            <TextInput
              style={styles.searchTextInput}
              placeholder={Placeholder.emailAddress}
              placeholderTextColor={Placeholder.placeholderTextColor}
              autoCorrect={false}
              //onChangeText={email => this.setState({email})}
              onChangeText={email => {
                if (email.length == 1 && email == ' ') {
                } else {
                  this.setState({ email });
                }
              }}
              value={this.state.email}
              autoCapitalize="none"
              keyboardType="email-address"
              returnKeyType="next"
              onSubmitEditing={() => this.address.focus()}
              ref={email => (this.email = email)}
            />
          </View>
          <View style={styles.tile}>
            <Image
              resizeMode="contain"
              style={styles.tileIcon}
              source={Images.addressIcon}
            />
            <TextInput
              style={styles.searchTextInput}
              onTouchStart={() => this.openAddressModel()}
              placeholder={Placeholder.address}
              placeholderTextColor={Placeholder.placeholderTextColor}
              autoCorrect={false}
              //onChangeText={address => this.setState({address})}
              onChangeText={address => {
                if (address.length == 1 && address == ' ') {
                } else {
                  this.setState({ address });
                }
              }}
              value={this.state.address}
              returnKeyType="next"
              ref={address => (this.address = address)}
              onSubmitEditing={() => this.password.focus()}
            />
          </View>
          <View style={styles.tile}>
            <Image
              resizeMode="contain"
              style={styles.tileIcon}
              source={Images.passwordIcon}
            />
            <TextInput
              style={styles.searchTextInput}
              placeholder={Placeholder.password}
              placeholderTextColor={Placeholder.placeholderTextColor}
              autoCorrect={false}
              secureTextEntry={true}
              //onChangeText={password => this.setState({password})}
              onChangeText={password => {
                if (password.length == 1 && password == ' ') {
                } else {
                  this.setState({ password });
                }
              }}
              value={this.state.password}
              ref={password => (this.password = password)}
            />
          </View>

          <TouchableOpacity style={styles.bottom} onPress={this.goToNextScreen}>
            <Text
              style={{
                color: Colors.Black,
                fontFamily: Fonts.Semibold,
                fontSize: RFValue(16),
              }}>
              {strings.Submit}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            onPress={() => this.goToLogin()}
            style={styles.orTile}>
            <Text style={styles.txtAlready}>
              {' '}
              {strings.already_have_account}
            </Text>
            <Text style={styles.txtSignIn}> {strings.sign_in}</Text>
          </TouchableOpacity>
          <View style={styles.orTile} />

          <View style={{ height: '75%', width: '90%' }}>
            <Modal
              animationType="slide"
              transparent={false}
              visible={this.state.modalVisible}
              onRequestClose={() => {
                this.openAddressModel(false);
                // Alert.alert('Modal has been closed.');
              }}>
              <ImageBackground
                style={{
                  height: Platform.select({
                    ios: moderateScale(40) + getStatusBarHeight(),
                    android: moderateScale(50),
                  }),
                  width: '100%',
                  borderColor: Colors.White,
                  borderWidth: 0,
                  alignItems: 'center',
                  paddingTop: Platform.select({
                    ios: getStatusBarHeight(),
                    android: 0,
                  }),
                  flexDirection: 'row',
                }}

              >
                <TouchableOpacity
                  style={{ flex: 2 }}
                  onPress={() => this.openDrawerClick()}>
                  <Image
                    source={Images.backIcon}
                    style={{
                      width: moderateScale(55),
                      height: moderateScale(55),
                      marginLeft: moderateScale(5),
                    }}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
                <Text
                  style={{
                    fontSize: moderateScale(18),
                    color: 'black',
                    flex: 6,
                    fontFamily: Fonts.Semibold,
                    textAlign: 'center',
                  }}>
                  {strings.enter_address}
                </Text>
                <View style={{ flex: 2 }} />
              </ImageBackground>
              <View>
                <View style={styles.tile}>
                  <Image
                    resizeMode="contain"
                    style={styles.tileIcon}
                    source={Images.addressIcon}
                  />
                  <TextInput
                    onTouchStart={() => this.openAddressModel()}
                    placeholder="Address"
                    placeholderTextColor="grey"
                    onChangeText={sourceLocation =>
                      this.onChangeSource(sourceLocation)
                    }
                    value={this.state.sourceLocation}
                    style={styles.searchTextInput}
                  />
                </View>

                {this.state.showSuggestion ? (
                  <View
                    style={{
                      height: wp('55%'),
                      width: wp('100%'),
                      backgroundColor: 'white',
                      marginTop: moderateScale(20),
                    }}>
                    {predictions}
                  </View>
                ) : null}
              </View>
            </Modal>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
