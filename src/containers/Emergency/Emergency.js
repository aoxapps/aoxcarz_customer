import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  FlatList,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  ScrollView,
  Keyboard,
  Alert,
  PermissionsAndroid,
} from 'react-native';
import {DrawerActions} from 'react-navigation-drawer';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import Contacts from 'react-native-contacts';
import {strings} from '../../constants/languagesString';
import RenderHeader from './../../components/CustomComponent/renderHeaderMain';
import Fonts from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import Images from '../../constants/Images';
import styles from './style';
import {moderateScale, scale} from 'react-native-size-matters';
import {RFValue} from 'react-native-responsive-fontsize';

export default class Emergency extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
    };
    const {navigation} = props;
    this.didFocusListener = navigation.addListener(
      'didFocus',
      this.componentDidFocus,
    );
  }

  componentWillUnmount() {
    this.didFocusListener.remove();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.contactListData !== this.props.contactListData) {
      // if (
      //   this.props.contactListData != null &&
      //   this.props.contactListData != ''
      // ) {
      this.setState({dataSource: this.props.contactListData}, () => {
        console.log(this.state.dataSource);
        this.afterGetContactList();
      });
      // }
    }
  }
  afterGetContactList = () => {
    this.setState({
      dataSource: this.props.contactListData,
    });
  };

  componentDidFocus = payload => {
    const {params} = payload.action;
    //  this.getEmergencyContactList();
    this.setState({dataSource: this.props.contactListData});
  };

  getAndroidContacts() {
    Contacts.getAll((err, contacts) => {
      // console.log('per:', err);
      if (err === 'denied') {
      } else {
        this.setState({contacts});
      }
    });
  }

  componentDidMount() {
    if (Platform.OS === 'ios') {
      Contacts.getAll((err, contacts) => {
        if (err) {
          if (err == 'denied') {
            Alert.alert(
              strings.AccessDenied,
              strings.Pleasegrantpermissiontoaccesscontactssettings,
              [
                {
                  text: strings.Ok,
                  onPress: () => this.props.navigation.goBack(),
                },
              ],
            );
          } else {
            throw err;
          }
        }
        // contacts returned
        this.setState({contacts});
      });
    } else if (Platform.OS === 'android') {
      this.requestContactPermission();
    }
  }
  async requestContactPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
        {
          title: strings.Contacts,
          message: strings.Thisappwouldliketoviewyourcontacts,
          buttonNegative: strings.Cancel,
          buttonPositive: strings.Ok,
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        // console.log('You can use the camera');

        this.getAndroidContacts();
      } else {
        //console.log('Camera permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  addEmergency() {
    this.props.navigation.navigate('AddContacts');
  }

  deleteContact(item) {
    this.props.deleteContactRequest(item._id, this.props.navigation);
  }

  goBack() {
    this.props.navigation.goBack();
  }

  addDriver() {
    this.props.navigation.navigate('AddDriver');
  }

  getEmergencyContactList = () => {
    this.props.contactListRequest(this.props.navigation);
  };

  render() {
    return (
      <View style={styles.container}>
        <RenderHeader
          back={true}
          title={strings.EmergencyContacts}
          navigation={this.props.navigation}
        />

        <View style={styles.gridViewBackground}>
          {this.state.dataSource != null && this.state.dataSource != '' ? (
            <FlatList
              style={styles.listContainer}
              data={this.state.dataSource}
              renderItem={({item}) => (
                <View style={styles.listItem}>
                  <Text style={styles.contact_details}>{`${item.name} `}</Text>
                  <TouchableOpacity
                    onPress={() => this.deleteContact(item)}
                    style={styles.deleteButton}>
                    <Text style={{textAlign: 'center', top: 5, color: 'red'}}>
                      {strings.Remove}
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
              keyExtractor={item => item._id}
            />
          ) : (
            <Text style={styles.txtNoLoads}>{strings.NoEmergencyContacts}</Text>
          )}
        </View>

        <View style={styles.arrowTile}>
          {/* {this.state.dataSource.length < 5 ? ( */}
          <TouchableOpacity
            style={styles.touchableArrow}
            onPress={() => this.addEmergency()}>
            <Image
              resizeMode="contain"
              style={styles.arrowIcon}
              source={Images.addIcon}
            />
          </TouchableOpacity>
          {/* ) : null} */}
        </View>
      </View>
    );
  }
}
