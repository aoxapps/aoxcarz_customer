import {StyleSheet} from 'react-native';
import Fonts from '../../constants/Fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Colors from '../../constants/Colors';
import {RFValue} from 'react-native-responsive-fontsize';
const styles = StyleSheet.create({
  container: {flex: 1, backgroundColor: '#ffffff'},
  title: {
    fontSize: 20,
    color: 'white',
  },
  listContainer: {width: '100%', height: 100},
  listItem: {
    borderBottomWidth: 1,
    borderColor: 'grey',
    flexDirection: 'row',
    height: 55,
    marginTop: 5,
  },
  deleteButton: {
    borderWidth: 1,
    borderColor: 'red',
    borderRadius: 20,
    width: 80,
    height: 30,
    alignItems: 'center',
    top: 10,
    position: 'absolute',
    right: 15,
  },
  gridViewBackground: {
    flex: 1,
    marginTop: 0,
    marginBottom: -50,
    borderColor: 'black',
    borderWidth: 0.0,
    backgroundColor: Colors.White,
  },
  arrowTile: {
    backgroundColor: 'transparent',
    height: wp('16%'),
    position: 'absolute',
    right: wp('5.33%'),
    left: wp('5.33%'),
    bottom: wp('18%'),
    alignItems: 'center',
    flexDirection: 'row',
    borderWidth: 0,
    borderColor: 'blue',
  },
  touchableArrow: {
    backgroundColor: Colors.Black,
    position: 'absolute',
    right: 0,
    height: wp('16%'),
    width: wp('16%'),
    bottom: 0,
    borderRadius: wp('8%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  arrowIcon: {
    width: '100%',
    height: '100%',
  },
  txtNoLoads: {
    marginTop: 150,
    width: '100%',
    textAlign: 'center',
    color: 'gray',
    fontSize: RFValue(16),
  },
  contact_details: {
    fontFamily: 'Arial',
    color: 'black',
    marginVertical: 10,
    marginLeft: 15,
    fontSize: wp('4.8%'),
  },
});
export default styles;
