//import {NavigationActions} from 'react-navigation';
import Emergency from './Emergency';
import {connect} from 'react-redux';
import {contactListRequest} from './../../modules/GetEmergencyContactList/actions';
import {deleteContactRequest} from './../../modules/EmergencyContacts/actions';
const mapStateToProps = state => ({
  contactListData: state.emergencyContactListReducer.contactListData,
});
const mapDispatchToProps = dispatch => ({
  contactListRequest: navigation => dispatch(contactListRequest(navigation)),
  deleteContactRequest: (contactID, navigation) =>
    dispatch(deleteContactRequest(contactID, navigation)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Emergency);
//export default Emergency;
