import {StyleSheet} from 'react-native';
import Fonts from '../../constants/Fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Colors from '../../constants/Colors';

import {moderateScale, scale, verticalScale} from 'react-native-size-matters';
import {RFValue} from 'react-native-responsive-fontsize';

const styles = StyleSheet.create({
  title: {
    fontSize: RFValue(20),
    color: 'white',
  },

  gridViewBackground: {
    flex: 1,
    marginTop: 20,
  },

  tileIcon: {
    width: 20,
    height: 40,
    marginLeft: 20,
    marginTop: -5,
  },
  tile: {
    backgroundColor: 'transparent',
    width: 'auto',
    height: wp('13.33%'),
    marginTop: 0,
    marginHorizontal: 0,
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 1.0,
    borderColor: '#818e97',
  },
  searchTextInput: {
    height: 40,
    width: '100%',
    paddingBottom: 12,
    paddingHorizontal: 20,
    backgroundColor: 'transparent',
    borderColor: 'gray',
    borderRadius: 0,
    fontSize: RFValue(16),
    top: 4,
  },
  arrowIcon: {
    width: '100%',
    height: '100%',
  },
  touchableArrow: {
    backgroundColor: Colors.Black,
    position: 'absolute',
    right: 0,
    height: wp('16%'),
    width: wp('16%'),
    borderRadius: wp('8%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  arrowTile: {
    backgroundColor: 'transparent',
    width: 'auto',
    height: wp('16%'),
    marginTop: 50,
    right: 20,
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 0,
    borderColor: '#818e97',
  },
  bottomView: {position: 'absolute', bottom: 40, width: '100%'},
});

export default styles;
