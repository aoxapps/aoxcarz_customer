import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  FlatList,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  ScrollView,
  Keyboard,
  Alert,
} from 'react-native';
import {DrawerActions} from 'react-navigation';
import {getConfiguration} from '../../utils/configuration';
import Activity from '../../components/ActivityIndicator';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import stripe from 'tipsi-stripe';

import {strings} from '../../constants/languagesString';
import RenderHeader from './../../components/CustomComponent/renderHeader';
import Fonts from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import Images from '../../constants/Images';
import styles from './style';
import {moderateScale, scale, verticalScale} from 'react-native-size-matters';
import {RFValue} from 'react-native-responsive-fontsize';
stripe.setOptions({
  publishableKey:
    'pk_test_51I25y4FhkC8xNc6FW2arKfYZhIRXGOYvv4UXohvmNfBoF7sHsyzfsW7548dIRbuhdQDyoicAS0qaxm61t0Bo466g00kzOQIOad', //'pk_test_INM9aFcqplBpLxivFa68HdMt00LjD5iZ7U'     // test
  androidPayMode: 'test', // Android only
});

export default class AddCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cardNumber: '',
      cardName: '',
      expDate: '',
      cvv: '',
      prevScreen: 'Payment',
    };
    const {navigation} = props;
    this.didFocusListener = navigation.addListener(
      'didFocus',
      this.componentDidFocus,
    );
  }

  componentWillUnmount() {
    this.didFocusListener.remove();
  }
  componentDidFocus = payload => {
    const {params} = payload.action;
    // console.log('payload', payload);
  };

  componentDidMount() {}

  stripePay() {
    const {expDate, cardNumber, cvv, CardName} = this.state;
    var str = expDate;
    var month = str.substring(0, 2);
    var year = str.substring(3, 5);
    //var last41 = this.state.cardNumber.slice(-4);
    const params = {
      number: cardNumber,
      expMonth: parseInt(month, 10),
      expYear: parseInt(year, 10),
      cvc: cvv,
      name: CardName,
    };
    stripe
      .createTokenWithCard(params)
      .then(token => {
        // console.log('token------------------', token);
        const customerid = getConfiguration('user_id');
        this.props.posRequest(
          customerid,
          token.card.brand,
          token.tokenId,
          token.card.last4,
          this.props.navigation,
        );
        this.props.navigation.goBack();
      })
      .catch(e => {
        // error handling
        console.log('error------------------', e.message);
        this.showAlert(e.message, 300);
      });
  }

  showAlert(message, duration) {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      alert(message);
    }, duration);
  }

  goToNextScreen() {
    const {cardNumber, expDate, cvv, cardName} = this.state;
    if (
      cardNumber.trim().length > 14 &&
      expDate.trim().length > 4 &&
      cvv.trim().length > 2 &&
      cardName.trim().length > 0
    ) {
      this.stripePay();
    } else {
      this.showAlert(strings.Pleaseentervaliddetailsofcard, 300);
    }
  }

  goBack() {
    this.props.navigation.goBack();
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: Colors.White}}>
        <RenderHeader
          back={true}
          title={strings.AddCard}
          navigation={this.props.navigation}
        />
        <View style={styles.gridViewBackground}>
          <View style={styles.tile}>
            <TextInput
              style={styles.searchTextInput}
              placeholder={strings.CardName}
              placeholderTextColor={'#818e97'}
              autoCorrect={false}
              maxLength={30}
              onChangeText={cardName => this.setState({cardName})}
              value={this.state.cardName}
            />
          </View>
          <View style={styles.tile}>
            <Image
              resizeMode="contain"
              style={styles.tileIcon}
              source={require('../../../assets/black_card.png')}
            />
            <TextInput
              style={styles.searchTextInput}
              placeholder={strings.CardNumber}
              placeholderTextColor={'#818e97'}
              autoCorrect={false}
              keyboardType="number-pad"
              returnKeyType="done"
              maxLength={16}
              onChangeText={cardNumber => this.setState({cardNumber})}
              value={this.state.cardNumber}
            />
          </View>

          <View style={styles.tile}>
            <View
              style={{
                backgroundColor: 'transparent',
                width: '50%',
                height: '100%',
              }}>
              <TextInput
                style={styles.searchTextInput}
                placeholder={strings.ExpDate}
                placeholderTextColor={'#818e97'}
                autoCorrect={false}
                keyboardType="number-pad"
                returnKeyType="done"
                maxLength={5}
                onChangeText={expDate => {
                  if (expDate.length == 2) {
                    expDate = expDate + '/';
                  } else if (expDate.length == 3) {
                    if (expDate.indexOf('/') !== -1) {
                      expDate = expDate.replace('/', '');
                    } else {
                      expDate = expDate.substr(0, 2) + '/' + expDate.substr(2);
                      // expDate = expDate.insert('1', "/");
                    }
                  }
                  this.setState({expDate: expDate});
                }}
                value={this.state.expDate}
              />
            </View>
            <View
              style={{
                backgroundColor: '#818e97',
                width: 1,
                height: '100%',
              }}
            />

            <View
              style={{
                backgroundColor: 'transparent',
                width: '50%',
                height: '100%',
              }}>
              <TextInput
                style={styles.searchTextInput}
                placeholder={strings.CVV}
                placeholderTextColor={'#818e97'}
                autoCorrect={false}
                keyboardType="number-pad"
                returnKeyType="done"
                maxLength={3}
                onChangeText={cvv => {
                  this.setState({cvv: cvv});
                }}
                value={this.state.cvv}
              />
            </View>
          </View>
          <View style={styles.bottomView}>
            <View style={styles.arrowTile}>
              <TouchableOpacity
                style={styles.touchableArrow}
                onPress={() => this.goToNextScreen()}>
                <Image
                  resizeMode="contain"
                  style={styles.arrowIcon}
                  source={require('../../../assets/right.png')}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
