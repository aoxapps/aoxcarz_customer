import AddCard from './AddCard';
import {connect} from 'react-redux';
import {posRequest} from './../../modules/AddPOS/actions';
const mapDispatchToProps = dispatch => ({
  posRequest: (customerId, name, token, lastd, navigation) =>
    dispatch(posRequest(customerId, name, token, lastd, navigation)),
});
export default connect(null, mapDispatchToProps)(AddCard);
//export default AddCard;
