import Profile from './Profile';
import {connect} from 'react-redux';
import {logoutRequest} from './../../redux/reducers';
const mapStateToProps = state => ({
  loginData: state.loginReducer.loginData,
});
const mapDispatchToProps = dispatch => ({
  // saveData: data => dispatch(loginSuccess(data)),
  logoutRequest: () => dispatch(logoutRequest()),
});
export default connect(mapStateToProps, mapDispatchToProps)(Profile);
//export default Profile;
