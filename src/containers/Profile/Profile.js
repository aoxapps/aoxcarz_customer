import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  ActivityIndicator,
  StyleSheet,
  ImageBackground,
  Alert,
  AsyncStorage,
  Platform,
} from 'react-native';
import {Image as ImageEle} from 'react-native-elements';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {NavigationActions} from 'react-navigation';

import Fonts from '../../constants/Fonts';
import Config from '../../constants/Config';
import Images from '../../constants/Images';
import Colors from '../../constants/Colors';
import {getStatusBarHeight} from '../../utils/IPhoneXHelper';
import {strings} from '../../constants/languagesString';
import {getConfiguration, setConfiguration} from '../../utils/configuration';
import {RFValue} from 'react-native-responsive-fontsize';
import {moderateScale, scale, verticalScale} from 'react-native-size-matters';

export default class Profile extends React.PureComponent {
  LogoutAction() {
    Alert.alert(
      'Alert!',
      'Are you sure you want to logout?',
      [
        {text: 'Ok', onPress: () => this.logoutAgain()},
        {text: 'Cancel', onPress: () => console.log('OK Pressed')},
      ],
      {cancelable: false},
    );
  }
  async logoutAgain() {
    setConfiguration('token', '');
    setConfiguration('user_id', '');
    setConfiguration('defaultPayment', '');
    await AsyncStorage.setItem('user_id', '');
    await AsyncStorage.setItem('token', '');

    this.props.navigation.navigate(Config.Login);
  }

  render() {
    var {name, profileImage, email, mobileNumber, address} = '';
    if (this.props.loginData != null && this.props.loginData != '') {
      name = this.props.loginData.name;
      profileImage = this.props.loginData.profileImage;
      email = this.props.loginData.email;
      mobileNumber =
        this.props.loginData.countryCode +
        '-' +
        this.props.loginData.mobileNumber;
      address = this.props.loginData.address;
    }
    return (
      <View style={{flex: 1, backgroundColor: Colors.White}}>
        <View style={styles.HeaderImage}>
          <ImageBackground
            style={{
              flex: 1,
              paddingTop: Platform.select({
                ios: getStatusBarHeight(),
                android: 0,
              }),
              alignItems: 'center',
              justifyContent: 'center',
            }}
            source={Images.sidemenuBG}
            resizeMode="cover">
            <View style={styles.headerView}>
              <TouchableOpacity
                onPress={() => this.props.navigation.toggleDrawer()}>
                <Image
                  source={Images.menuIcon}
                  style={styles.backIcon}
                  resizeMode="contain"
                />
              </TouchableOpacity>
              <Text style={styles.title}>{strings.profile}</Text>
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.navigate(Config.EditProfile)
                }>
                <Image
                  resizeMode="contain"
                  style={styles.editIcon}
                  source={Images.editProfile}
                />
              </TouchableOpacity>
            </View>
            <View style={styles.profileView}>
              <View style={styles.profileimg}>
                {profileImage == '' ||
                profileImage == 'null' ||
                profileImage == null ||
                profileImage == 'none' ? (
                  <ImageEle
                    resizeMode="stretch"
                    style={{
                      width: '100%',
                      height: '100%',
                      borderRadius: moderateScale(45),
                    }}
                    source={Images.drawerProfileBG}
                    PlaceholderContent={
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <ActivityIndicator />
                      </View>
                    }
                  />
                ) : (
                  <ImageEle
                    resizeMode="stretch"
                    style={{
                      width: '100%',
                      height: '100%',
                      borderRadius: moderateScale(45),
                    }}
                    source={{uri: profileImage}}
                    PlaceholderContent={
                      <View
                        style={{
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <ActivityIndicator style={styles.activityInd} />
                      </View>
                    }
                  />
                )}
              </View>
              <Text style={styles.profileName}>{name}</Text>
            </View>
          </ImageBackground>
        </View>
        <View
          style={{
            marginTop:
              Platform.OS === 'ios' ? moderateScale(40) : moderateScale(40),
          }}>
          <View style={styles.tile}>
            <Image
              resizeMode="contain"
              style={styles.tileIcon}
              source={Images.profileIcon}
            />
            <Text style={styles.inputTxt}>{name}</Text>
          </View>
          <View style={styles.tile}>
            <Image
              resizeMode="contain"
              style={styles.tileIcon}
              source={Images.callIcon}
            />
            <Text style={styles.inputTxt}>{mobileNumber}</Text>
          </View>
          <View style={styles.tile}>
            <Image
              resizeMode="contain"
              style={styles.tileIcon}
              source={Images.emailIcon}
            />
            <Text style={styles.inputTxt}>{email}</Text>
          </View>
          <View style={styles.tile}>
            <Image
              resizeMode="contain"
              style={styles.tileIcon}
              source={Images.addressIcon}
            />
            <Text style={styles.inputTxt}>{address}</Text>
          </View>
        </View>
        <View style={styles.arrowTile}>
          <TouchableOpacity
            style={styles.buttonFirst}
            onPress={() =>
              this.props.navigation.navigate(Config.ChangePasswordProfile)
            }>
            <Text style={[{color: Colors.Black, fontSize: RFValue(16)}]}>
              {strings.ChangePassword}
            </Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.buttonSecond}
            onPress={() => this.LogoutAction()}>
            <Image
              resizeMode="contain"
              style={{width: 30, height: 30}}
              source={Images.logout}
            />
            <Text style={[styles.title2, {marginLeft: 10}]}>
              {strings.Logout}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  HeaderImage: {
    width: '100%',
    backgroundColor: Colors.White,
    height: '32%',
  },
  headerView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    alignItems: 'center',
    marginTop: moderateScale(-20),
  },
  backIcon: {
    width: moderateScale(55),
    height: moderateScale(55),
    marginLeft: moderateScale(5),
  },
  editIcon: {
    width: moderateScale(30),
    height: moderateScale(22),
    marginRight: moderateScale(15),
  },
  profileView: {
    width: '100%',
    alignItems: 'center',
    marginTop: moderateScale(30),
  },
  profileimg: {
    height: moderateScale(90),
    overflow: 'hidden',
    width: moderateScale(90),
    elevation: 1,
    borderColor: Colors.White,
    borderRadius: moderateScale(45),
  },
  profileName: {
    color: Colors.White,
    fontFamily: Fonts.Regular,
    fontSize: RFValue(18),
    marginTop: moderateScale(10),
  },

  logout: {
    position: 'absolute',
    width: 60,
    height: 50,
    top: 0,
    backgroundColor: 'transparent',
    right: 0,
  },

  inputTxt: {
    color: 'black',
    fontSize: RFValue(16),
    height: moderateScale(25),
    fontFamily: Fonts.Regular,
  },
  buttonFirst: {
    width: '45%',
    height: moderateScale(45),
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: moderateScale(10),
    backgroundColor: Colors.primary1,
    borderColor: Colors.Black,
    borderRadius: moderateScale(22.5),
  },
  buttonSecond: {
    width: '40%',
    height: wp('13%'),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    marginHorizontal: moderateScale(10),
    backgroundColor: 'transparent',
  },
  title2: {
    fontSize: RFValue(18),
    color: 'red',
    fontFamily: Fonts.Regular,
  },
  title: {
    fontSize: RFValue(18),
    color: 'white',
    fontFamily: Fonts.Semibold,
  },
  tile: {
    width: '100%',
    height: moderateScale(55),
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderColor: Colors.bottomBorder,
    paddingHorizontal: moderateScale(15),
  },
  tileIcon: {
    width: moderateScale(18),
    height: moderateScale(40),
    marginRight: moderateScale(10),
    tintColor: Colors.placeholderColor,
  },
  arrowTile: {
    position: 'absolute',
    backgroundColor: 'transparent',
    width: '100%',
    height: wp('16%'),
    bottom: Platform.OS === 'ios' ? moderateScale(20) : moderateScale(20),
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-end',
    borderBottomWidth: 0,
    borderColor: '#818e97',
  },

  button: {
    color: Colors.Black,
    fontFamily: Fonts.Semibold,
    fontSize: RFValue(16),
  },
  bottom: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    backgroundColor: Colors.primary1,
    height: moderateScale(45),
    marginHorizontal: moderateScale(20),
    borderRadius: moderateScale(30),
    bottom: moderateScale(25),
    width: '90%',
  },
});
