import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  Keyboard,
  StatusBar,
  ScrollView,
  SafeAreaView,
  BackHandler,
  Alert,
  Linking,
} from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Platform} from 'react-native';
//import {fetchLocation} from '../../utils/LocationSetup';
import PhoneInput from 'react-native-phone-input';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import SplashScreen from 'react-native-splash-screen';
import DummySplash from '../../components/DummySplash';
// import Fonts from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import Images from '../../constants/Images';
import Config from '../../constants/Config';
import Placeholder from '../../constants/Placeholder';
import ValidationMessage from '../../constants/ValidationMessage';
import styles from './style';

import {getConfiguration, setConfiguration} from '../../utils/configuration';
import {AsyncStorage} from 'react-native';
import {getStatusBarHeight} from '../../utils/IPhoneXHelper';
import Fonts from '../../constants/Fonts';
import {strings} from '../../constants/languagesString';

//import firebase from 'react-native-firebase';
//import {bindActionCreators} from 'redux';
import DeviceInfo from 'react-native-device-info';
// import appleAuth, {
//   AppleButton,
//   AppleAuthError,
//   AppleAuthRequestScope,
//   AppleAuthRealUserStatus,
//   AppleAuthCredentialState,
//   AppleAuthRequestOperation,
// } from '@invertase/react-native-apple-authentication';
import {
  LoginButton,
  AccessToken,
  LoginManager,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
// import {GoogleSignin, GoogleSigninButton} from 'react-native-google-signin';
import {moderateScale, scale} from 'react-native-size-matters';
import {RFValue} from 'react-native-responsive-fontsize';
export default class Login extends React.Component {
  that = this;
  constructor(props) {
    super(props);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.backHandler = null;
    this.state = {
      phone: '',
      autoLogin: false,
      countryCode: '+1',
      googleId: '',
      facebookId: '',
      appleId: '',
      mainViewTop: 0,
      marginTopImg: 0,
      socialData: '',
      photo: '',
      countryName: '',
      majorVersionIOS: 0,
      appVersion: '',
      forceUpdateVisible: false,
      appUrl: '',
      forceUpdateEnable: false,
      isMount: false,
    };
    const {navigation} = props;
    this.didFocusListener = navigation.addListener(
      'didFocus',
      this.componentDidFocus,
    );
  }
  focus = () => {
    this.phone.focus();
  };

  componentDidFocus = payload => {
    // this.props.settingRequest(this.props.navigation);
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.profileData !== this.props.profileData) {
      this.setState({autoLogin: false});
    }
    // if (prevProps.socialData !== this.props.socialData) {
    //   this.setState({socialData: this.props.socialData}, () => {
    //     if (
    //       this.state.socialData != '' &&
    //       this.state.socialData != 'null' &&
    //       this.state.socialData != null
    //     ) {
    //       var data = this.state.socialData;
    //       // console.log(data);
    //       this.afterSocialLogin(data);
    //     } else {
    //     }
    //   });
    // }
    // if (prevProps.settingData !== this.props.settingData) {
    //   if (Platform.OS == 'android') {
    //     this.setState(
    //       {
    //         forceUpdateEnable: this.props.settingData.data.appForceUpdate
    //           .Android_User_App_Force_Update,
    //         appVersion: this.props.settingData.data.appVersion
    //           .Android_User_App_Force_Version,
    //         appUrl: this.props.settingData.App_Url.Android_App_URL
    //           .Android_Client_App_URL,
    //         isMount: true,
    //       },
    //       () => {
    //         this.getData();
    //       },
    //     );
    //   } else {
    //     this.setState(
    //       {
    //         forceUpdateEnable: this.props.settingData.data.appForceUpdate
    //           .IOS_User_App_Force_Update,
    //         appVersion: this.props.settingData.data.appVersion
    //           .IOS_User_App_Force_Version,
    //         appUrl: this.props.settingData.App_Url.IOS_App_URL
    //           .IOS_Client_App_URL,
    //         isMount: true,
    //       },
    //       () => {
    //         this.getData();
    //       },
    //     );
    //   }
    // }
  }

  afterSocialLogin = async data => {
    let isExist = data;
    if (data.exist) {
      var token = data.data.token;
      setConfiguration('token', token);
      var user_id = data.data._id;
      setConfiguration('user_id', user_id);
      await AsyncStorage.setItem('user_id', user_id);
      await AsyncStorage.setItem('token', token);
      this.getData();
    } else {
      this.props.navigation.navigate('Register', {
        googleId: this.state.googleId,
        facebookId: this.state.facebookId,
        appleId: this.state.appleId,
        name: this.state.name,
        email: this.state.email,
        countryCode: this.state.countryCode,
        photo: this.state.photo,
        registerType: 'Social',
      });
    }
  };
  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // user has a device token
        setConfiguration('fcmToken', fcmToken);
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    } else {
      setConfiguration('fcmToken', fcmToken);
    }
    //  setConfiguration('fcmToken', fcmToken);
  }
  verifyPhone = () => {
    const {phone} = this.state;
    if (phone.length === 0) {
      alert(ValidationMessage.mobileRequired);
      return false;
    }
    if (phone.length < 9) {
      alert(ValidationMessage.mobileValid);
      return false;
    }
    return true;
  };

  afterLogin = async () => {
    // await this.getToken();

    setConfiguration('token', '');
    setConfiguration('user_id', '');
    //setConfiguration('findingDriver', false);
    const {countryCode, countryName, phone} = this.state;

    if (this.verifyPhone()) {
      this.props.mobileRequest(
        countryCode,
        phone,
        countryName,
        this.props.navigation,
      );
    }
  };

  componentDidMount() {
    // GoogleSignin.configure({
    //   webClientId:
    //     '120084917363-k6jsl9eoehc8gjpg7a526r36hehssf9v.apps.googleusercontent.com',

    //   offlineAccess: true,
    //   iosClientId:
    //     '120084917363-6j90gmlderhh9qp0m69674s5qp6me94m.apps.googleusercontent.com',
    // });

    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow.bind(this),
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide.bind(this),
    );
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButtonClick,
    );

    if (Platform.OS === 'ios') {
      // this.checkAppleAvailability();
    }
  }
  handleBackButtonClick = () => {
    if (this.props.navigation.isFocused()) {
      Alert.alert(
        'Exit',
        'Are you sure you want to Exit App?',
        [
          {text: 'Cancel', onPress: () => {}, style: 'cancel'},
          {text: 'Exit', onPress: () => BackHandler.exitApp()},
        ],
        {cancelable: false},
      );
      return true;
    } else {
      return false;
    }
  };

  checkAppleAvailability() {
    const majorVersionIOS = parseInt(Platform.Version, 10);
    // console.log('iOS version ' + majorVersionIOS);
    this.setState({majorVersionIOS: majorVersionIOS});

    if (Platform.OS != 'android' && this.state.majorVersionIOS >= 13) {
      if (appleAuth.isSupported) {
        this.authCredentialListener = appleAuth.onCredentialRevoked(
          async () => {
            /// console.log('Credential Revoked');
            this.fetchAndUpdateCredentialState().catch(error =>
              this.setState({credentialStateForUser: `Error: ${error.code}`}),
            );
          },
        );
        this.fetchAndUpdateCredentialState()
          .then(res => this.setState({credentialStateForUser: res}))
          .catch(error =>
            this.setState({credentialStateForUser: `Error: ${error.code}`}),
          );
      } else {
        // console.log('Apple Login is not Supported');
      }
    }
  }

  socialLogin(fbId, gId, aId) {
    setConfiguration('token', '');
    setConfiguration('user_id', '');
    let data = {
      fbid: fbId,
      gid: gId,
      appleid: aId,
    };
    this.props.loginWithSocialRequest(data, this.props.navigation);
    // .then(() => this.afterSocialLogin())
    // .catch(e => this.showAlert(e.message, 300));
  }

  googleAuth() {
    // this.googleLogin();
    //
    // return;
    //
    //

    GoogleSignin.signIn()
      .then(user => {
        // console.log('google user: --- ', user);
        var fullname = user.user.givenName + ' ' + user.user.familyName;

        this.socialLogin('', user.user.id);
        this.setState({
          googleId: user.user.id,
          facebookId: '',
          appleId: '',
          name: fullname,
          email: user.user.email,
          photo: user.user.photo,
        });

        GoogleSignin.signOut();
        //this.googleClick();
      })
      .catch(err => {
        // console.log('WRONG SIGNIN', err);
      })
      .done();
  }

  fetchAndUpdateCredentialState = async () => {
    if (this.user === null) {
      this.setState({credentialStateForUser: 'N/A'});
    } else {
      const credentialState = await appleAuth.getCredentialStateForUser(
        this.user,
      );
      if (credentialState === AppleAuthCredentialState.AUTHORIZED) {
        this.setState({credentialStateForUser: 'AUTHORIZED'});
      } else {
        this.setState({credentialStateForUser: credentialState});
      }
    }
  };

  signInApple = async () => {
    //  console.warn('Beginning Apple Authentication');
    try {
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: AppleAuthRequestOperation.LOGIN,
        requestedScopes: [
          AppleAuthRequestScope.EMAIL,
          AppleAuthRequestScope.FULL_NAME,
        ],
      });

      //  console.log('appleAuthRequestResponse', appleAuthRequestResponse);
      this.setState({
        googleId: '',
        facebookId: '',
        appleId: appleAuthRequestResponse.user,
        name:
          appleAuthRequestResponse.fullName.givenName +
          ' ' +
          appleAuthRequestResponse.fullName.familyName,
        email: appleAuthRequestResponse.email,
        photo: '',
      });
      this.socialLogin('', '', appleAuthRequestResponse.user);
      const {
        user: newUser,
        email,
        nonce,
        identityToken,
        realUserStatus /* etc */,
      } = appleAuthRequestResponse;

      this.user = newUser;
      this.fetchAndUpdateCredentialState()
        .then(res => this.setState({credentialStateForUser: res}))
        .catch(error =>
          this.setState({credentialStateForUser: `Error: ${error.code}`}),
        );

      if (identityToken) {
        // e.g. sign in with Firebase Auth using `nonce` & `identityToken`
        //console.log(nonce, identityToken);
      } else {
        // no token - failed sign-in?
      }

      if (realUserStatus === AppleAuthRealUserStatus.LIKELY_REAL) {
        //console.log("I'm a real person!");
      }
      //  console.warn(`Apple Authentication Completed, ${this.user}, ${email}`);
    } catch (error) {
      if (error.code === AppleAuthError.CANCELED) {
        //  console.log('User canceled Apple Sign in.');
      } else {
        // console.log(error);
      }
    }
  };

  handleFacebookLogin() {
    var that2 = this;
    LoginManager.logInWithPermissions(['public_profile', 'email']).then(
      function(result) {
        if (result.isCancelled) {
          // console.log('Login cancelled');
        } else {
          //console.log('resuttttttt', result);
          // console.log(
          //   'Login success with permissions: ' +
          //     result.grantedPermissions.toString(),
          // );

          AccessToken.getCurrentAccessToken().then(data => {
            let accessToken = data.accessToken;
            //alert(accessToken.toString())

            const responseInfoCallback = (error, result) => {
              if (error) {
                // console.log(error);
                alert('Error fetching data: ' + error.toString());
              } else {
                // console.log(result);
                //alert('Success fetching data: ' + result.toString());

                // var user = data.credentials;
                var api = `https://graph.facebook.com/v2.3/${
                  result.id
                }?fields=name,email,picture&access_token=${accessToken}`;

                fetch(api)
                  .then(response => response.json())
                  .then(responseData => {
                    // console.log(
                    //   '------------responseData-------------------: ',
                    //   responseData,
                    // );
                    that2.setState({
                      googleId: '',
                      facebookId: responseData.id,
                      appleId: '',
                      name: responseData.name,
                      email: responseData.email,
                      photo: responseData.picture.data.url,
                    });
                    that2.socialLogin(responseData.id, '', '');
                    // that2.props.navigation.navigate('GoogleLogin');
                  });
                // LoginManager.logout((data) => {
                //   //do something

                // })
              }
            };

            const infoRequest = new GraphRequest(
              '/me',
              {
                accessToken: accessToken,
                parameters: {
                  fields: {
                    string: 'email,name,first_name,middle_name,last_name',
                  },
                },
              },
              responseInfoCallback,
            );

            // Start the graph request.
            new GraphRequestManager().addRequest(infoRequest).start();
          });
        }
      },
      function(error) {
        // console.log('Login fail with error: ' + error);
      },
    );
  }

  componentWillMount() {
    //this.setState({autoLogin: true});
    this.getData();
  }

  // async getData() {
  //   try {
  //     this.setState({autoLogin: true});
  //     const us = await AsyncStorage.getItem('user_id');
  //     const tok = await AsyncStorage.getItem('token');
  //     setConfiguration('language', 'en');
  //     if (us) {
  //       setConfiguration('user_id', us);
  //       setConfiguration('token', tok);
  //       setTimeout(() => this.getProfile(), 2000);
  //       // this.setState({autoLogin: false});
  //     } else {
  //       this.setState({autoLogin: false});
  //       setConfiguration('user_id', '');
  //       setConfiguration('token', '');
  //       // this.getLanguages();
  //     }
  //   } catch (e) {
  //     this.setState({autoLogin: false});
  //   }
  // }

  getData = async () => {
    try {
      this.setState({autoLogin: true});
      const value = await AsyncStorage.getItem('user_id');
      const token = await AsyncStorage.getItem('token');
      setConfiguration('language', 'en');

      if (value !== null && value != '' && token != null && token != '') {
        setConfiguration('user_id', value);
        setConfiguration('token', token);
        if (this.state.forceUpdateEnable) {
          this.forceUpdate(true);
        } else {
          // if (this.state.isMount) {
          setTimeout(() => this.getProfile(), 2000);
          // }
        }
      } else {
        this.setState({autoLogin: false});
        setConfiguration('user_id', '');
        setConfiguration('token', '');
        if (this.state.forceUpdateEnable) {
          this.forceUpdate(false);
        }
      }
    } catch (e) {
      this.setState({autoLogin: false});
    }
  };

  updateApp = () => {
    //   console.log('goToPlayStore', this.state.appUrl);
    // var aa = this.state.appUrl;
    //alert('hello');
    ///debugger;
    Linking.openURL(this.state.appUrl);
  };

  forceUpdate = async isLogin => {
    const currentVersion = parseFloat(this.state.appVersion);

    // const currentVersion = 2;
    const buildVersion = DeviceInfo.getVersion();
    var appVersion = parseFloat(buildVersion);
    //debugger;
    if (appVersion < currentVersion) {
      this.setState({forceUpdateVisible: true});
    } else {
      if (isLogin) {
        if (this.state.isMount) {
          setTimeout(() => this.getProfile(), 2000);
        }
      } else {
      }
    }
  };

  getProfile() {
    this.props.profileRequest(this.props.navigation);
  }

  _keyboardDidShow() {
    this.setState({
      mainViewTop: moderateScale(-200),
      marginTopImg: hp('30%'),
    });
  }

  _keyboardDidHide() {
    this.setState({
      mainViewTop: 0,
      marginTopImg: 0,
    });
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
    // this.didFocusListener.remove();
  }
  selectCountry(country) {
    this.phone.selectCountry(country.iso2);
  }

  getCountryCode() {
    var country = this.phone.getCountryCode();
    var name = this.phone.getISOCode();
    this.setState({countryCode: '+' + country, countryName: name});
  }

  render() {
    if (this.state.forceUpdateVisible) {
      return (
        <View style={styles.containerBlack}>
          <ImageBackground
            style={{
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              backgroundColor: Colors.PrimaryColor,
            }}
            // source={Images.loginBGplan}
            resizeMode="stretch">
            <View
              style={{
                width: '100%',
                height: 'auto',

                alignItems: 'center',
                marginBottom: 100,
              }}>
              <Image
                style={{height: 150, width: 150}}
                source={Images.inovation}
                resizeMode="contain"
              />

              <Text
                style={{
                  color: 'white',
                  fontFamily: Fonts.Semibold,
                  fontSize: 30,
                }}>
                Better and faster
              </Text>
              <Text
                style={{
                  color: 'white',
                  fontFamily: Fonts.Semibold,
                  fontSize: 30,
                  marginBottom: 5,
                }}>
                Version Available
              </Text>
              <Text
                style={{
                  color: 'white',
                  fontFamily: Fonts.Regular,
                  fontSize: 16,
                  textAlign: 'center',
                }}>
                An update with new features and fixes is available. It typically
                takes less then 1 minute.
              </Text>
            </View>

            <TouchableOpacity
              onPress={() => this.updateApp()}
              style={{
                backgroundColor: 'white',
                width: '80%',
                alignSelf: 'center',
                height: 40,
                borderRadius: 20,
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontSize: 20,
                  fontFamily: Fonts.Semibold,
                  alignSelf: 'center',
                }}>
                Update Now
              </Text>
            </TouchableOpacity>
          </ImageBackground>
        </View>
      );
    } else {
      return (
        <View
          style={{
            flex: 1,
            marginTop: this.state.mainViewTop,
            backgroundColor: Colors.White,
          }}>
          <StatusBar
            backgroundColor={Colors.PrimaryColor}
            barStyle="dark-content"
          />

          <View style={styles.HeaderImage}>
            <ImageBackground
              style={{
                flex: 1,
                paddingTop: Platform.select({
                  ios: getStatusBarHeight(),
                  android: 0,
                }),
                alignItems: 'center',
                justifyContent: 'center',
                //marginBottom: hp('3%'),
                marginTop: this.state.marginTopImg,
              }}
              // source={Images.loginBGMain}
              // source={Platform.OS === 'ios' ? Images.loginPei : Images.loginPei}
              resizeMode="cover">
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginBottom: moderateScale(40),
                  marginTop: moderateScale(-40),
                }}>
                <Text style={styles.welcomeMain}> {strings.login_signup} </Text>
              </View>
              <Image
                resizeMode="contain"
                style={{
                  width: '55%',
                  height: '55%',
                  backgroundColor: 'transparent',
                }}
                source={Images.loginBG}
              />
            </ImageBackground>
          </View>
          <KeyboardAwareScrollView
            style={{backgroundColor: Colors.background, flex: 1}}>
            <View style={styles.lowerView}>
              <View style={styles.headingBG}>
                <Text style={styles.welcome}>{strings.get_started} </Text>
              </View>
              <View style={styles.tile}>
                <Text style={styles.countryCode}>{this.state.countryCode}</Text>
                <View style={styles.phoneInput} />
                <PhoneInput
                  ref={ref => {
                    this.phone = ref;
                  }}
                  initialCountry={'us'}
                  returnKeyType={'done'}
                  textProps={{
                    returnKeyType: 'done',
                    placeholder: strings.mobile_no,
                    value: this.state.phone,
                  }}
                  textStyle={styles.searchTextInput}
                  onChangePhoneNumber={phone => this.setState({phone})}
                  onSelectCountry={countryCode => this.getCountryCode()}
                  offset={70}
                />
              </View>

              <TouchableOpacity
                style={styles.bottom}
                onPress={() => this.afterLogin()}>
                <Text
                  style={{
                    color: Colors.Black,
                    fontFamily: Fonts.Semibold,
                    fontSize: RFValue(16),
                  }}>
                  {strings.next}
                </Text>
              </TouchableOpacity>

              {/* <View style={styles.orTile}>
                <Text style={styles.orText}>or connect with</Text>
              </View>
              <View style={styles.socialBtnContainer}>
                <View>
                  <TouchableOpacity onPress={() => this.handleFacebookLogin()}>
                    <Image
                      resizeMode="contain"
                      style={styles.socialBtn}
                      source={Images.fbIcon}
                    />
                  </TouchableOpacity>
                </View>

                <View>
                  <TouchableOpacity onPress={() => this.googleAuth()}>
                    <Image
                      resizeMode="contain"
                      style={styles.socialBtn}
                      source={Images.googleIcon}
                    />
                  </TouchableOpacity>
                </View>
                <View>
                  {Platform.OS === 'ios' && this.state.majorVersionIOS >= 13 ? (
                    <TouchableOpacity onPress={() => this.signInApple()}>
                      <Image
                        resizeMode="contain"
                        style={styles.socialBtn}
                        source={Images.appleIcon}
                      />
                    </TouchableOpacity>
                  ) : null}
                </View>
              </View> */}
            </View>
          </KeyboardAwareScrollView>
          {this.state.autoLogin ? <DummySplash /> : null}
        </View>
      );
    }
  }
}
