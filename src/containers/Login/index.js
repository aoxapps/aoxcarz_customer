import Login from './Login';
import {connect} from 'react-redux';
import {
  mobileRequest,
  loginWithSocialRequest,
} from './../../modules/Login/actions';
import {profileRequest} from './../../modules/GetProfile/actions';
import {settingRequest} from './../../modules/BasicSetting/actions';

const mapStateToProps = state => ({
  socialData: state.mobileReducer.socialData,
  profileData: state.GetProfileReducer.profileData,
  settingData: state.basicSettingReducer.settingData,
});

const mapDispatchToProps = dispatch => ({
  mobileRequest: (countryCode, mobileNumber, countryName, navigation) =>
    dispatch(mobileRequest(countryCode, mobileNumber, countryName, navigation)),
  loginWithSocialRequest: (data, navigation) =>
    dispatch(loginWithSocialRequest(data, navigation)),
  profileRequest: navigation => dispatch(profileRequest(navigation)),
  settingRequest: navigation => dispatch(settingRequest(navigation)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);

//export default Login;
