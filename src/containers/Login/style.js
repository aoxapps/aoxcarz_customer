import {StyleSheet} from 'react-native';
import Fonts from '../../constants/Fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Colors from '../../constants/Colors';
import {moderateScale, scale, verticalScale} from 'react-native-size-matters';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcome: {
    fontFamily: Fonts.Semibold,
    color: 'black',
    fontSize: scale(24),
  },
  containerBlack: {
    flex: 1,
    // backgroundColor: 'black',
    width: wp('100%'),
    height: wp('100%'),
    justifyContent: 'center',
  },
  welcomeMain: {
    color: Colors.PrimaryColor,
    fontFamily: Fonts.Semibold,
    fontSize: scale(18),
  },
  HeaderImage: {
    flex: 1,
    //width: '100%',
    backgroundColor: '#ffffff',
    //height: '50%',
    //  marginTop: 30,
  },
  searchTextInput: {
    height: moderateScale(40),
    backgroundColor: 'transparent',
    borderColor: 'gray',
    width: '100%',
    borderRadius: 0,
    fontSize: scale(14),
    fontFamily: Fonts.Regular,
  },
  tile: {
    backgroundColor: Colors.background1,
    width: 'auto',
    height: moderateScale(45),
    marginTop: wp('5%'),
    marginHorizontal: moderateScale(30),
    alignItems: 'center',
    flexDirection: 'row',
    //borderBottomWidth: 1.0,
    borderWidth: 0.5,
    borderRadius: moderateScale(16),
    borderColor: 'gray',
  },
  countryCode: {
    marginLeft: moderateScale(10),
    position: 'absolute',
    left: moderateScale(40),
    //top: 6,
    color: 'black',
    //marginTop: wp('2.5%'),
    fontFamily: Fonts.Regular,
    fontSize: scale(16),
  },
  phoneInput: {
    backgroundColor: 'gray',
    // height: '70%',
    width: 1,
    // position: 'absolute',
    //left: 85,
    //top: 8,
    marginLeft: moderateScale(10),
  },
  bottom: {
    backgroundColor: Colors.primary1,
    height: moderateScale(45),
    marginTop: wp('7.5%'),
    marginHorizontal: moderateScale(30),
    borderRadius: moderateScale(30),
    justifyContent: 'center',
    alignItems: 'center',
  },
  lowerView: {
    flex: 1,
    marginTop: 0,
  },
  headingBG: {
    width: '100%',
    height: 'auto',
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },

  socialBtnContainer: {
    backgroundColor: 'transparent',
    width: '100%',
    height: moderateScale(50),
    marginTop: Platform.OS === 'ios' ? wp('6%') : wp('5%'),
    marginHorizontal: moderateScale(-10),
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    // marginBottom: 40,
  },
  socialBtn: {marginLeft: wp('5%'), width: wp('12%'), height: '100%'},
  orTile: {
    backgroundColor: 'transparent',
    width: 'auto',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  orText: {
    fontFamily: Fonts.Light,
    color: 'gray',
    fontSize: wp('4.8%'),
  },
});
export default styles;
