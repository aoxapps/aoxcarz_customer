import {StyleSheet} from 'react-native';
import Fonts from '../../constants/Fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Colors from '../../constants/Colors';
import {moderateScale, scale} from 'react-native-size-matters';
import {RFValue} from 'react-native-responsive-fontsize';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.White,
  },
  forgot: {
    color: '#000000',
    fontSize: moderateScale(18),
    fontFamily: Fonts.Regular,
  },
  lowerView: {
    flex: 1,
    marginTop: 0,
  },
  tileIcon: {
    width: moderateScale(18),
    height: moderateScale(40),
    marginTop: moderateScale(-5),
    marginLeft: 0,
  },
  tile: {
    backgroundColor: Colors.background1,
    width: 'auto',
    marginTop: moderateScale(30),
    marginBottom: moderateScale(10),
    paddingLeft: wp('6%'),
    alignItems: 'center',
    flexDirection: 'row',
    borderWidth: 0.8,
    height: moderateScale(45),
    marginHorizontal: moderateScale(20),
    borderRadius: moderateScale(16),
    borderColor: Colors.bottomBorder,
  },
  searchTextInput: {
    width: '100%',
    backgroundColor: 'transparent',
    marginLeft: moderateScale(10),
    fontSize: RFValue(16),
    fontFamily: Fonts.Regular,
  },
  forgotTile: {
    backgroundColor: 'transparent',
    width: 'auto',
    height: moderateScale(40),
    marginTop: moderateScale(30),
    marginHorizontal: moderateScale(20),
    alignItems: 'center',
    flexDirection: 'row',
    borderColor: '#818e97',
  },
  touchableForgotPassword: {
    backgroundColor: 'transparent',
    position: 'absolute',
    right: 0,
    height: moderateScale(30),
  },

  title: {
    fontSize: RFValue(18),
    color: 'white',
    fontFamily: Fonts.Semibold,
  },
  bottom: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    backgroundColor: Colors.primary1,
    height: moderateScale(45),
    marginHorizontal: moderateScale(20),
    borderRadius: moderateScale(30),
    bottom: moderateScale(35),
    width: '90%',
  },
});
export default styles;
