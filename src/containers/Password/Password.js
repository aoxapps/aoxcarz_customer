import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Platform} from 'react-native';

import RenderHeader from './../../components/CustomComponent/renderHeader';
import Fonts from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import Config from '../../constants/Config';
import Images from '../../constants/Images';
import styles from './style';
import {AsyncStorage} from 'react-native';
import {moderateScale, scale} from 'react-native-size-matters';
import {RFValue} from 'react-native-responsive-fontsize';
import strings from '../../constants/languagesString';
export default class Password extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      fcmToken: 'none',
    };
  }
  async componentDidMount() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    this.setState({
      fcmToken: fcmToken,
    });
    //console.log('----fcm-----', fcmToken);
  }
  forgotPassword() {
    this.props.navigation.navigate(Config.VerifyOTP, {
      mobileNumber: this.props.navigation.getParam('mobileNumber', ''),
      forgot: true,
      countryCode: this.props.navigation.getParam('countryCode', ''),
    });
  }

  goBack() {
    this.props.navigation.goBack();
  }
  onLogin = async () => {
    const {password} = this.state;
    var fcmToken = await AsyncStorage.getItem('fcmToken');
    this.props.loginRequest(
      this.props.navigation.getParam('countryCode'),
      this.props.navigation.getParam('mobileNumber'),
      password,
      fcmToken,
      this.props.navigation,
    );
  };
  render() {
    const {loginRequest, navigation} = this.props;
    //const {password, fcmToken} = this.state;

    return (
      <View style={styles.container}>
        <RenderHeader
          back={true}
          title={'Enter Password'}
          navigation={this.props.navigation}
        />
        <View style={styles.lowerView}>
          <View style={styles.tile}>
            <Image
              resizeMode="contain"
              style={styles.tileIcon}
              source={require('../../../assets/password.png')}
            />
            <TextInput
              style={styles.searchTextInput}
              placeholder="Enter Password"
              placeholderTextColor={'#818e97'}
              autoCorrect={false}
              autoFocus={true}
              secureTextEntry={true}
              onChangeText={password => this.setState({password})}
              value={this.state.password}
            />
          </View>
          <View style={styles.forgotTile}>
            <TouchableOpacity
              style={styles.touchableForgotPassword}
              onPress={() => this.forgotPassword()}>
              <Text style={styles.forgot}> Forgot Password? </Text>
            </TouchableOpacity>
          </View>

          <TouchableOpacity
            style={styles.bottom}
            onPress={() => this.onLogin()}>
            <Text
              style={{
                color: Colors.Black,
                fontFamily: Fonts.Semibold,
                fontSize: RFValue(16),
              }}>
              {strings.Submit}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
