import Password from './Password';
import {connect} from 'react-redux';
import {loginRequest} from './../../modules/Password/actions';
const mapStateToProps = state => ({
  loginData: state.loginReducer.loginData,
  isBusy: state.loginReducer.isBusy,
});
const mapDispatchToProps = dispatch => ({
  loginRequest: (countryCode, mobileNumber, password, fcmToken, navigation) =>
    dispatch(
      loginRequest(countryCode, mobileNumber, password, fcmToken, navigation),
    ),
});
export default connect(mapStateToProps, mapDispatchToProps)(Password);
//export default Password;
