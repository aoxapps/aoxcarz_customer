import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  Platform,
  ImageBackground,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
  Modal,
  TouchableHighlight,
} from 'react-native';

import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ImageChooser from 'react-native-image-picker';
import { isEmpty } from '../../utils/CommonFunctions';
const regEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
import { Image as ImageEle } from 'react-native-elements';
import VectorIcon from '../../utils/vectorIcons';
import { getStatusBarHeight } from '../../utils/IPhoneXHelper';
import ImageResizer from 'react-native-image-resizer';
import Fonts from '../../constants/Fonts';
import Config from '../../constants/Config';
import Images from '../../constants/Images';
import Colors from '../../constants/Colors';
import styles from './style';
import { strings } from '../../constants/languagesString';
import { getConfiguration } from '../../utils/configuration';
import { moderateScale, scale, verticalScale } from 'react-native-size-matters';
import { RFValue } from 'react-native-responsive-fontsize';

var firstTime = false;
export default class EditProfile extends React.Component {
  constructor(props) {
    super(props);
    firstTime = true;
    const {
      name,
      email,
      mobileNumber,
      profileImage,
      address,
      countryCode,
    } = this.props.loginData;

    this.state = {
      name: name,
      mobileNumber: mobileNumber,
      email: email,
      address: address,
      filePath: profileImage,
      fileName: '',
      countryCode: countryCode + '-',
      predictions: [],
      showSuggestion: false,
      sourceLocation: '',
      addressModel: false,
      modalVisible: false,
      curLatitude: '',
      curLongitude: '',
    };
    const { navigation } = this.props;
    this.didFocusListener = navigation.addListener(
      'didFocus',
      this.componentDidFocus,
    );
  }
  componentDidFocus = payload => {
    //  this.updateProfileData();
    // setTimeout(() => this.getCurrentLocation(), 2000);
  };
  goBack() {
    this.props.navigation.goBack();
  }

  goToNextScreen = () => {
    this.saveProfileData();
  };

  componentDidMount() {
    this.getCurrentLocation();
    this.updateProfileData();
    // this.onLoad();
    // firstTime = false;
  }

  componentWillUnmount = () => {
    this.didFocusListener.remove();
  };
  getCurrentLocation = async () => {
    const lat = getConfiguration('lat');
    const long = getConfiguration('long');
    if (lat && long) {
      this.setState({
        curLatitude: lat,
        curLongitude: long,
      });
    }
  };

  updateProfileData = () => {
    const {
      name,
      email,
      mobileNumber,
      profileImage,
      address,
      countryCode,
    } = this.props.loginData;

    this.setState({
      name: name,
      mobileNumber: mobileNumber,
      countryCode: countryCode + '-',
      email: email,
      address: address,
      filePath: profileImage,
      fileName: '',
    });
  };

  onLoad = () => {
    // setTimeout(() => this.getCurrentLocation(), 2000);
    !firstTime && this.updateProfileData();
  };

  saveImage = data => {
    const { updateProfile } = this.props;
    const profiledata = new FormData();
    profiledata.append('profileImage', {
      uri:
        Platform.OS === 'android' ? data.uri : data.uri.replace('file://', ''),
      type: 'image/jpeg',
      name: 'profile',
    });
    updateProfile(profiledata, this.props.navigation);
  };

  profileImagePck = () => {
    try {
      const options = {
        quality: 0.5,
        maxWidth: 500,
        maxHeight: 500,
        storageOptions: {
          skipBackup: true,
        },
      };
      ImageChooser.showImagePicker(options, response => {
        if (response.didCancel) {
          // console.log('User cancelled image picker');
        } else if (response.error) {
          // console.log('ImagePicker Error: ', response.error);
        } else if (response.customButton) {
          // console.log('User tapped custom button: ', response.customButton);
        } else {
          // const source = {uri: response.uri};

          // You can also display the image using data:
          // const source = { uri: 'data:image/jpeg;base64,' + response.data };

          // this.setState({
          //   filePath: source.uri,
          // });
          this.resize(response.uri);
          // this.saveImage(source);
        }
      });
    } catch (error) {
      console.log(JSON.stringify(error));
    }
  };

  resize = sourceURI => {
    ImageResizer.createResizedImage(sourceURI, 300, 300, 'PNG', 80)
      .then(({ uri }) => {
        const source = {
          uri: uri,
          imageName: 'profile',
        };
        this.setState({
          filePath: source.uri,
        });
        this.saveImage(source);
      })
      .catch(err => {
        console.log(err);
        return Alert.alert(
          'Unable to upload photo',
          // 'Check the console for full the error message',
        );
      });
  };

  verifyCredentials = () => {
    const { name, email, mobileNumber, address } = this.state;
    if (isEmpty(name.trim())) {
      alert('Please enter name!!');
      return false;
    } else if (isEmpty(email.trim())) {
      alert('Please enter email!!');
      return false;
    } else if (!regEmail.test(email.trim())) {
      alert('Please enter valid email!!');
      return false;
    } else if (isEmpty(mobileNumber.trim())) {
      alert(strings.mobileNumberAlert);
      return false;
    } else if (isEmpty(address.trim())) {
      alert('Please enter address!!');
      return false;
    }
    return true;
  };

  saveProfileData = () => {
    if (this.verifyCredentials()) {
      const { email, name, mobileNumber, address } = this.state;

      var profileData = {
        name: name,
        email: email,
        mobileNumber: mobileNumber,
        address: address,
      };
      var data = {
        profileData: profileData,
        isRegister: false,
      };
      this.props.updateProfileData(data, this.props.navigation);
    }
  };

  renderTextInput = (imageLeft, placeHolderText, value, key) => {
    return (
      <View style={styles.tile}>
        <Image
          resizeMode="contain"
          style={[
            styles.tileIcon,
            {
              marginLeft:
                key == 'mobileNumber' ? moderateScale(70) : moderateScale(15),
            },
          ]}
          source={imageLeft}
        />
        {key == 'mobileNumber' ? (
          <TextInput style={styles.searchTextInput2}>
            {this.state.countryCode}
          </TextInput>
        ) : null}
        <TextInput
          onTouchStart={key == 'address' ? () => this.openAddressModel() : null}
          style={
            key == 'mobileNumber'
              ? styles.searchTextInput1
              : styles.searchTextInput
          }
          placeholder={placeHolderText}
          placeholderTextColor={'#818e97'}
          autoCorrect={false}
          editable={key == 'mobileNumber' ? false : true}
          onChangeText={value => this.setState({ [key]: value })}
          value={value}
          returnKeyType="next"
        />
      </View>
    );
  };
  openDrawerClick() {
    this.setState({ modalVisible: false });
  }
  openAddressModel(visible) {
    this.setState({ modalVisible: visible });
  }
  googleAutocomplete = async (Location, curLat, curLong) => {
    const apiUrl =
      'https://maps.googleapis.com/maps/api/place/autocomplete/json?input=' +
      Location +
      '&key=' +
      Config.GOOGLE_MAPS_APIKEY +
      // '&components=country:in|country:ca' +
      '&location=' +
      curLat +
      ',' +
      curLong +
      '&radius=' +
      1000;
    const result = await fetch(apiUrl);
    const json = await result.json();
    return json;
  };

  async onChangeSource(sourceLocation) {
    this.setState({
      tentativePrice: '',
      showSuggestionDest: false,
      sourceLocation: sourceLocation,
    });
    var json = await this.googleAutocomplete(
      sourceLocation,
      this.state.curLatitude,
      this.state.curLongitude,
    );
    try {
      this.setState({
        predictions: json.predictions,
        showSuggestion: true,
      });
      var adress_data = json.predictions;

      this.setState({
        myaddress_list: adress_data,
      });

      if (json.predictions.length == 0) {
        this.setState({
          showSuggestion: false,
        });
      }
    } catch (err) {
      this.showAlert(err.message, 300);
    }
  }

  showAlert(message, duration) {
    this.timer = setTimeout(() => {
      alert(message);
    }, duration);
  }

  setSourceLocation = (placeId, description) => {
    this.setState({
      sourceLocation: description,
      showSuggestion: false,
      selSourcePlaceId: placeId,
      address: description,
      modalVisible: false,
    });
  };
  render() {
    const { name, mobileNumber, email, filePath, address } = this.state;
    const predictions = this.state.predictions.map(prediction => (
      <TouchableHighlight
        style={{
          paddingVertical: moderateScale(5),
          borderBottomWidth: 1,
          borderColor: 'gray',
          backgroundColor: 'white',
          height: 'auto',
        }}
        onPress={() =>
          this.setSourceLocation(prediction.place_id, prediction.description)
        }>
        <Text style={{ margin: 10 }} key={prediction.id}>
          {prediction.description}
        </Text>
      </TouchableHighlight>
    ));
    return (
      <View style={{ flex: 1, backgroundColor: Colors.White }}>
        <View style={styles.HeaderImage}>
          <ImageBackground
            style={{
              flex: 1,
              paddingTop: Platform.select({
                ios: getStatusBarHeight(),
                android: 0,
              }),
              alignItems: 'center',
              justifyContent: 'center',
            }}
            source={Images.sidemenuBG}
            resizeMode="cover">
            <View style={styles.headerView}>
              <View style={{ flex: 1 }}>
                <TouchableOpacity onPress={() => this.goBack()}>
                  <Image
                    resizeMode="contain"
                    source={Images.backIcon}
                    style={styles.backIcon}
                  />
                </TouchableOpacity>
              </View>
              <Text style={styles.title}>Edit Profile</Text>
              <View style={{ flex: 1 }} />
            </View>
            <View style={styles.profileView}>
              <TouchableOpacity onPress={this.profileImagePck}>
                <View style={styles.profileimg}>
                  {filePath == '' ||
                    filePath == 'null' ||
                    filePath == null ||
                    filePath == 'none' ? (
                    <ImageEle
                      resizeMode="cover"
                      style={{
                        width: '100%',
                        height: '100%',
                        borderRadius: moderateScale(45),
                      }}
                      source={Images.drawerProfileBG}
                      PlaceholderContent={
                        <View
                          style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <ActivityIndicator />
                        </View>
                      }
                    />
                  ) : (
                    <ImageEle
                      resizeMode="cover"
                      style={{
                        width: '100%',
                        height: '100%',
                        borderRadius: moderateScale(45),
                      }}
                      source={{ uri: filePath }}
                      PlaceholderContent={
                        <View
                          style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <ActivityIndicator />
                        </View>
                      }
                    />
                  )}
                </View>
              </TouchableOpacity>
              <Text style={styles.profileName}>{name}</Text>
            </View>
          </ImageBackground>
        </View>
        <View style={styles.lowerView}>
          <KeyboardAwareScrollView>
            {this.renderTextInput(
              Images.profileIcon,
              strings.Name,
              name,
              'name',
            )}
            {this.renderTextInput(
              Images.callIcon,
              strings.Phone,
              mobileNumber,
              'mobileNumber',
            )}
            {this.renderTextInput(
              Images.emailIcon,
              strings.Email,
              email,
              'email',
            )}
            {this.renderTextInput(
              Images.addressIcon,
              strings.Address,
              address,
              'address',
            )}
            <View style={{ height: '75%', width: '90%' }}>
              <Modal
                animationType="slide"
                transparent={false}
                visible={this.state.modalVisible}
                onRequestClose={() => {
                  this.openAddressModel(false);
                  // Alert.alert('Modal has been closed.');
                }}>
                <ImageBackground
                  style={{
                    height: Platform.select({
                      ios: moderateScale(40) + getStatusBarHeight(),
                      android: moderateScale(50),
                    }),
                    width: '100%',
                    alignItems: 'center',
                    paddingTop: Platform.select({
                      ios: getStatusBarHeight(),
                      android: 0,
                    }),
                    flexDirection: 'row',
                  }}

                >
                  <TouchableOpacity
                    style={{ flex: 2 }}
                    onPress={() => this.openDrawerClick()}>
                    <Image
                      source={Images.backIcon}
                      style={{
                        width: moderateScale(55),
                        height: moderateScale(55),
                        marginLeft: moderateScale(5),
                      }}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      fontSize: RFValue(18),
                      color: Colors.Black,
                      flex: 6,
                      fontFamily: Fonts.Semibold,
                      textAlign: 'center',
                    }}>
                    {strings.enter_address}
                  </Text>
                  <View style={{ flex: 2 }} />
                </ImageBackground>
                <View>
                  <View style={styles.tile2}>
                    <Image
                      resizeMode="contain"
                      style={styles.tileIcon}
                      source={Images.addressIcon}
                    />
                    <TextInput
                      onTouchStart={() => this.openAddressModel()}
                      placeholder="Address"
                      placeholderTextColor="grey"
                      onChangeText={sourceLocation =>
                        this.onChangeSource(sourceLocation)
                      }
                      value={this.state.sourceLocation}
                      style={styles.searchTextInput}
                    />
                  </View>

                  {this.state.showSuggestion ? (
                    <View
                      style={{
                        height: wp('55%'),
                        width: wp('100%'),
                        backgroundColor: 'white',
                      }}>
                      {predictions}
                    </View>
                  ) : null}
                </View>
              </Modal>
            </View>
          </KeyboardAwareScrollView>

          <TouchableOpacity
            style={styles.bottom}
            onPress={() => this.goToNextScreen()}>
            <Text style={styles.button}>{strings.save}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
