import {StyleSheet, Platform} from 'react-native';
import Fonts from '../../constants/Fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Colors from '../../constants/Colors';
import {RFValue} from 'react-native-responsive-fontsize';
import {moderateScale, scale, verticalScale} from 'react-native-size-matters';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
  },
  HeaderImage: {
    width: '100%',
    backgroundColor: Colors.White,
    height: '32%',
  },
  headerView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    // marginTop: moderateScale(-20),
  },
  backIcon: {
    width: moderateScale(55),
    height: moderateScale(55),
    marginLeft: moderateScale(5),
  },
  profileView: {
    width: '100%',
    alignItems: 'center',
    marginTop: moderateScale(30),
    paddingBottom: moderateScale(24),
  },
  profileimg: {
    height: moderateScale(90),
    overflow: 'hidden',
    width: moderateScale(90),
    elevation: 1,
    borderColor: Colors.White,
    borderRadius: moderateScale(45),
  },
  tile2: {
    backgroundColor: Colors.background1,
    width: 'auto',
    height: moderateScale(45),
    marginTop: moderateScale(20),
    alignItems: 'center',
    flexDirection: 'row',
    borderWidth: 0.5,
    borderColor: Colors.bottomBorder,
    borderRadius: moderateScale(16),
    marginHorizontal: moderateScale(20),
  },
  // activityInd: {
  //   marginRight: 4,
  //   marginBottom: 4,
  //   backgroundColor: 'transparent',
  // },

  profileName: {
    color: Colors.White,
    fontFamily: Fonts.Regular,
    fontSize: RFValue(18),
    marginTop: moderateScale(10),
  },
  searchTextInput: {
    width: '100%',
    paddingHorizontal: moderateScale(20),
    borderColor: 'gray',
    fontSize: RFValue(16),
    fontFamily: Fonts.Regular,
  },
  searchTextInput1: {
    width: '100%',
    color: 'grey',
    paddingHorizontal: moderateScale(20),
    borderColor: 'gray',
    fontSize: RFValue(16),
    fontFamily: Fonts.Regular,
  },
  searchTextInput2: {
    color: 'grey',
    paddingLeft: wp('4.5%'),
    borderColor: 'gray',
    fontSize: RFValue(16),
    fontFamily: 'CharlieDisplay-Regular',
  },

  arrowIcon: {
    width: '100%',
    height: '100%',
  },
  lowerView: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? moderateScale(40) : moderateScale(40),
  },

  tileIcon: {
    width: moderateScale(18),
    height: moderateScale(30),
    marginLeft: moderateScale(20),
    tintColor: Colors.placeholderColor,
  },
  tile: {
    backgroundColor: 'transparent',
    width: 'auto',
    height: moderateScale(55),
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    borderColor: Colors.bottomBorder,
    paddingHorizontal: moderateScale(15),
  },
  title: {
    fontSize: RFValue(18),
    color: 'white',
    fontFamily: Fonts.Semibold,
  },
  button: {
    color: Colors.Black,
    fontFamily: Fonts.Semibold,
    fontSize: RFValue(16),
  },
  bottom: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    backgroundColor: Colors.primary1,
    height: moderateScale(45),
    marginHorizontal: moderateScale(20),
    borderRadius: moderateScale(30),
    bottom: moderateScale(25),
    width: '90%',
  },
});
export default styles;
