import EditProfile from './EditProfile';
import {
  profileImRequest,
  profileRequest,
} from './../../modules/EditProfile/actions';
import {connect} from 'react-redux';
const mapStateToProps = state => ({
  loginData: state.loginReducer.loginData,
});
const mapDispatchToProps = dispatch => ({
  updateProfileData: (data, navigation) =>
    dispatch(profileRequest(data, navigation)),
  updateProfile: (data, navigation) =>
    dispatch(profileImRequest(data, navigation)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
//export default EditProfile;
