import Wallet from './Wallet';
import {addWalletBalanceRequest} from './../../modules/AddWallet/actions';
import {connect} from 'react-redux';
const mapStateToProps = state => ({
  PaymentListData: state.paymentListReducer.paymentMethodData,
  ProfileData: state.GetProfileReducer.profileData,
  addWalletBalanceData: state.addWalletBalanceReducer.addWalletBalanceData,
});

const mapDispatchToProps = dispatch => ({
  addWalletBalanceRequest: (count, item_id, navigation) =>
    dispatch(addWalletBalanceRequest(count, item_id, navigation)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Wallet);
//export default Wallet;
