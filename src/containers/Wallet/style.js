import {StyleSheet, Platform} from 'react-native';
import Fonts from '../../constants/Fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Colors from '../../constants/Colors';
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
  },
  HeaderImage: {
    flex: 1,
    //width: '100%',
    backgroundColor: '#ffffff',
    // height: '50%',
    //  marginTop: 30
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },

  forgot: {
    color: '#D81C03',
    fontSize: wp('4.8%'),
    fontWeight: 'bold',
  },

  lowerView: {
    flex: 1,
    //  marginTop: 0,
    // backgroundColor: 'green',
  },
  headingBG: {
    marginTop: wp('8%'),
    width: '100%',
    height: 'auto',
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  tileIcon: {
    width: 18,
    height: 40,
    marginTop: -5,
    marginLeft: 0,
  },
  tile: {
    backgroundColor: 'transparent',
    width: 'auto',
    height: 40,
    marginTop: wp('4.33%'),
    marginHorizontal: 20,
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 0,
    justifyContent: 'center',
    marginBottom: 10,
    // backgroundColor: 'red',
    //borderColor: '#E8E7E8'
    // borderBottomColor: "#E8E7E8",
  },
  searchTextInput: {
    height: 35,
    width: 100,
    paddingBottom: 0,
    paddingHorizontal: 0,
    backgroundColor: 'transparent',
    borderColor: 'gray',
    borderRadius: 0,
    borderBottomWidth: 1,
    fontSize: 15,
    marginLeft: 15,
    textAlign: 'center',
  },
  forgotTile: {
    backgroundColor: 'transparent',
    width: 'auto',
    height: 40,
    marginTop: 25,
    marginHorizontal: 20,
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 0,
    borderColor: '#818e97',
  },

  arrowTile: {
    //position: 'absolute',
    width: '60%',
    height: 45,
    borderRadius: 20,
    backgroundColor: '#000000',
    //bottom: 40,

    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  touchableArrow: {
    backgroundColor: '#D92104',
    position: 'absolute',
    right: 0,
    height: wp('16%'),
    width: wp('16%'),
    borderRadius: wp('8%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  arrowIcon: {
    width: wp('9.33%'),
    height: wp('9.33%'),
  },
  title: {
    fontSize: wp('4.8%'),
    color: 'white',
  },
  backTouchable: {
    position: 'absolute',
    width: 60,
    height: 50,
    top: 0,
    backgroundColor: 'transparent',
    left: 0,
  },
  headerView: {
    height: 40,
    width: '100%',
    backgroundColor: 'transparent',
    borderColor: '#0082cb',
    borderWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    left: 0,
    top: Platform.OS === 'ios' ? 30 : 10,
  },

  backIcon: {
    position: 'absolute',
    width: 25,
    height: 25,
    top: 10,
    left: 15,
    backgroundColor: 'transparent',
  },
  bottomView: {
    //position: 'absolute',
    // bottom: Platform.OS === 'ios' ? 20 : 0,
    flex: 1,
    width: '100%',
    marginTop: Platform.OS === 'ios' ? 60 : 30,
  },
  loadDetail: {
    backgroundColor: '#E9E9E9',
    borderColor: '#E9E9E9',
    marginBottom: 6,
    marginLeft: '3%',
    width: '94%',
    marginTop: 5,
    borderBottomWidth: 0.0,
  },
});
export default styles;
