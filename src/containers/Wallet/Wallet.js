import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  ScrollView,
  Keyboard,
  FlatList,
  ImageBackground,
  Alert,
} from 'react-native';
import {DrawerActions} from 'react-navigation-drawer';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {getStatusBarHeight} from '../../utils/IPhoneXHelper';
import {strings} from '../../constants/languagesString';
import styles from './style';
import RenderHeader from './../../components/CustomComponent/renderHeaderMain';
import Fonts from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import Images from '../../constants/Images';
import Activity from '../../components/ActivityIndicator';
import {SUCCESS} from '../../constants/Config';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class Wallet extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      dataSourcePast: [],
      selTab: '0',
      underLineLeft: 0,
      walletText: '',
      walletBalance: '0',
      paymentMethodList: [],
      addPay: false,
    };
    const {navigation} = props;
    this.didFocusListener = navigation.addListener(
      'didFocus',
      this.componentDidFocus,
    );
  }
  componentWillUnmount() {
    this.didFocusListener.remove();
  }
  componentDidFocus = payload => {
    // const {params} = payload.action;
  };

  componentDidMount() {
    this.getWalletBalance();
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.ProfileData !== this.props.ProfileData) {
      this.setState({walletBalance: this.props.ProfileData.walletCredit});
    }
    if (prevProps.addWalletBalanceData !== this.props.addWalletBalanceData) {
      var aa = this.props.addWalletBalanceData;
      //   console.log('asdad');
      if (this.props.addWalletBalanceData.status === SUCCESS) {
        //  console.log('asdsasdada');
        //this.showAlert('Money added successfuly to your wallet', 3000);
        setTimeout(() => {
          Alert.alert(
            strings.Success,
            'Money added successfuly to your wallet',
            [{text: strings.Ok, onPress: () => {}}],
            {cancelable: false},
          );
        }, 600);
      }
    }
  }

  getWalletBalance = () => {
    this.setState({
      walletBalance: this.props.ProfileData.walletCredit,
    });
  };

  goBack() {
    this.props.navigation.goBack();
  }
  openDrawerClick() {
    this.props.navigation.dispatch(DrawerActions.openDrawer());
  }

  addCardAction() {
    this.props.navigation.navigate('PaymentMethod');
  }

  deleteCard(cardId) {
    Alert.alert('Alert!', 'Are you sure you want to remove this card', [
      {text: 'OK', onPress: () => this.deleteCard1(cardId)},
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
    ]);
  }

  showAlert(message, duration) {
    //clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      alert(message);
    }, 300);
  }
  addTwentyFive() {
    this.setState({walletText: '25'});
  }

  addFifty() {
    this.setState({walletText: '50'});
  }

  addSeventyFive() {
    this.setState({walletText: '75'});
  }
  addhundred() {
    this.setState({walletText: '100'});
  }
  cancelPayAction() {
    this.setState({addPay: false});
  }
  addButtonAction() {
    if (this.state.walletText == '') {
      Alert.alert('Enter money to add');
    } else {
      this.setState({addPay: true});
    }
  }
  AddNewCard = () => {
    this.setState({addPay: false}, () => {
      this.props.navigation.navigate('AccountDetailScreen');
    });
  };
  ListEmptyView = () => {
    return (
      <View
        style={{
          justifyContent: 'center',
          flex: 1,
          flexDirection: 'column',
          alignContent: 'center',
          alignSelf: 'center',
          // backgroundColor: 'red',
          marginTop: 30,
        }}>
        <Text
          style={{
            textAlign: 'center',
            color: 'black',
            fontFamily: Fonts.Semibold,
            fontSize: 18,
          }}>
          No card found
        </Text>
        <TouchableOpacity
          onPress={() => this.AddNewCard()}
          style={{
            width: 170,
            height: 40,
            backgroundColor: Colors.PrimaryColor,
            alignItems: 'center',
            justifyContent: 'center',
            borderRadius: 25,
            marginTop: 50,
            // position: 'absolute',
            //bottom: 20,
          }}>
          <Text
            style={{
              fontWeight: 'bold',
              color: Colors.White,
              fontSize: 16,
            }}>
            {strings.AddNewCard}
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  addPaymentAction(item) {
    this.setState({addPay: false});
    var count = Number(this.state.walletText);
    this.props.addWalletBalanceRequest(count, item._id, this.props.navigation);
    this.setState({walletText: ''});
  }

  // afterAddPaymentAction() {
  //   this.getWalletBalance();
  //   this.showAlert('Money added successfuly to your wallet', 3000);
  // }

  render() {
    return (
      <View
        style={{
          flex: 1,
          // marginTop: this.state.mainViewTop,

          backgroundColor: '#ffffff',
        }}>
        <View style={styles.HeaderImage}>
          <ImageBackground
            // style={{
            //   flex: 1,
            //   alignItems: 'center',
            //   justifyContent: 'center',
            // }}
            style={{
              flex: 1,
              paddingTop: Platform.select({
                ios: getStatusBarHeight(),
                android: 0,
              }),
              alignItems: 'center',
              justifyContent: 'center',
              marginBottom: hp('3%'),
            }}
            source={Images.loginBGMain}
            resizeMode="cover">
            <View style={styles.headerView}>
              <TouchableOpacity
                style={styles.backTouchable}
                onPress={() => this.openDrawerClick()}>
                <Image
                  resizeMode="contain"
                  style={styles.backIcon}
                  source={Images.menuWhite}
                />
              </TouchableOpacity>
            </View>
            <Image
              resizeMode="contain"
              style={{
                width: '52%',
                height: '52%',
                backgroundColor: 'transparent',
              }}
              source={Images.logo}
            />
          </ImageBackground>
        </View>
        <View style={styles.lowerView}>
          <KeyboardAwareScrollView>
            <View
              style={{
                //marginTop: this.state.bgViewTop,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <View
                style={{
                  width: '100%',
                  //  marginTop: 20,
                  backgroundColor: 'transparent',
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Text
                  style={{
                    fontSize: 28,
                    fontFamily: Fonts.Regular,
                    color: Colors.PrimaryColor,
                  }}>
                  Wallet Amount
                </Text>
                <Text
                  style={{
                    fontSize: 28,
                    fontFamily: Fonts.Regular,
                    color: Colors.PrimaryColor,
                  }}>
                  $ {this.state.walletBalance}
                </Text>
              </View>
            </View>

            <View style={styles.tile}>
              <View
                style={{
                  flex: 1,
                  backgroundColor: 'white',
                  flexDirection: 'row',
                  justifyContent: 'center',
                }}>
                <TouchableOpacity
                  onPress={() => this.addTwentyFive()}
                  style={{
                    width: '16%',
                    height: 35,
                    backgroundColor: Colors.PrimaryColor,
                    //marginLeft: 10,
                    borderWidth: 0.5,
                    borderColor: 'grey',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{fontSize: 15, fontWeight: '500', color: '#ffffff'}}>
                    $ 25
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.addFifty()}
                  style={{
                    width: '16%',
                    height: 35,
                    backgroundColor: Colors.PrimaryColor,
                    marginLeft: 15,
                    borderWidth: 0.5,
                    borderColor: 'grey',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{fontSize: 15, fontWeight: '500', color: '#ffffff'}}>
                    $ 50
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.addSeventyFive()}
                  style={{
                    width: '16%',
                    height: 35,
                    backgroundColor: Colors.PrimaryColor,
                    marginLeft: 15,
                    borderWidth: 0.5,
                    borderColor: 'grey',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{fontSize: 15, fontWeight: '500', color: '#ffffff'}}>
                    $ 75
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => this.addhundred()}
                  style={{
                    width: '16%',
                    height: 35,
                    backgroundColor: Colors.PrimaryColor,
                    marginLeft: 15,
                    borderWidth: 0.5,
                    borderColor: 'grey',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}>
                  <Text
                    style={{fontSize: 15, fontWeight: '500', color: '#ffffff'}}>
                    $ 100
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.tile}>
              <TextInput
                style={styles.searchTextInput}
                placeholder=""
                placeholderTextColor={'#818e97'}
                autoCorrect={false}
                secureTextEntry={false}
                onChangeText={value => this.setState({walletText: value})}
                value={this.state.walletText}
                keyboardType="number-pad"
                returnKeyType="done"
              />
            </View>
            <View style={styles.bottomView}>
              <TouchableOpacity
                onPress={() => this.addButtonAction()}
                style={styles.arrowTile}>
                <Text style={{fontSize: 18, fontWeight: '400', color: 'white'}}>
                  {strings.addMoney}
                </Text>
              </TouchableOpacity>
            </View>
          </KeyboardAwareScrollView>
        </View>

        {this.state.addPay == true ? (
          <View
            style={{
              width: '100%',
              height: '100%',
              backgroundColor: 'transparent',
              position: 'absolute',
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <View
              style={{
                width: '80%',
                height: 250,
                backgroundColor: 'white',
                borderColor: 'grey',
                borderWidth: 0.5,
                borderRadius: 5,
              }}>
              <TouchableOpacity
                onPress={() => this.cancelPayAction()}
                style={{
                  width: 60,
                  height: 60,
                  backgroundColor: 'transparent',
                  position: 'absolute',
                  right: -20,
                  top: -20,
                  alignItems: 'center',
                  justifyContent: 'center',
                }}>
                <Image
                  style={{
                    width: 30,
                    height: 30,
                    borderRadius: 20,
                    marginLeft: 10,
                    marginTop: -10,
                    backgroundColor: '#000000',
                  }}
                  source={Images.cancelNew}
                />
              </TouchableOpacity>
              {this.props.PaymentListData.length > 1 ? (
                <Text
                  style={{
                    fontSize: 15,
                    fontWeight: 'bold',
                    alignSelf: 'center',
                    marginTop: 10,
                  }}>
                  Select card for payment
                </Text>
              ) : null}

              <View style={{justifyContent: 'center', flex: 1, margin: 10}}>
                <FlatList
                  data={this.props.PaymentListData}
                  showsVerticalScrollIndicator={false}
                  renderItem={
                    this.props.PaymentListData.length > 1
                      ? ({item}) =>
                          // <React.Fragment key={item.id}>
                          item.type === 'Card' ? (
                            <TouchableOpacity
                              onPress={() => this.addPaymentAction(item)}
                              style={styles.loadDetail}>
                              <View
                                style={{
                                  flexDirection: 'row',
                                  margin: 10,
                                  alignContent: 'center',
                                  justifyContent: 'space-between',
                                }}>
                                <View style={{width: '20%'}}>
                                  <Image
                                    resizeMode="contain"
                                    style={{height: 50, width: 50}}
                                    source={{uri: item.logo}}
                                  />
                                </View>
                                <View>
                                  <Text style={{top: 20}}>
                                    ***********{item.lastd}
                                  </Text>
                                </View>
                              </View>
                            </TouchableOpacity>
                          ) : null
                      : this.ListEmptyView
                    // </React.Fragment>
                  }
                  keyExtractor={item => item._id}
                  ListEmptyComponent={this.ListEmptyView}
                />
              </View>
            </View>
          </View>
        ) : null}
      </View>
    );
  }
}
