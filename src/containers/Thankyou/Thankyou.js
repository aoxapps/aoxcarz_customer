import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  Keyboard,
  Platform,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default class Thankyou extends React.Component {
  that = this;
  constructor(props) {
    super(props);
    this.state = {};

    const {navigation} = props;

    this.didFocusListener = navigation.addListener(
      'didFocus',
      this.componentDidFocus,
    );
  }
  componentWillUnmount() {
    this.didFocusListener.remove();
  }
  render() {
    return (
      <View>
        <Text>Thankyou</Text>
      </View>
    );
  }
}
