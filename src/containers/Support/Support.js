import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  FlatList,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  ScrollView,
  Keyboard,
} from 'react-native';
import {DrawerActions} from 'react-navigation-drawer';
import {WebView} from 'react-native-webview';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
// import { CachedImage } from 'react-native-cached-image';
import {strings} from '../../constants/languagesString';
import RenderHeader from './../../components/CustomComponent/renderHeaderMain';
import Fonts from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import Images from '../../constants/Images';
import {moderateScale, scale} from 'react-native-size-matters';
import {RFValue} from 'react-native-responsive-fontsize';

export default class Support extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
    };
    const {navigation} = props;
    this.didFocusListener = navigation.addListener(
      'didFocus',
      this.componentDidFocus,
    );
  }
  componentWillUnmount() {
    this.didFocusListener.remove();
  }
  componentDidFocus = payload => {
    //const {params} = payload.action;
    // console.log('hurrayyyyyy');
    this.getData();
    // this.favouritesAPI();
  };

  getData() {
    // var responseGetProfile= this.props.responseGetProfile.response.data;
    // console.log('responseGetProfile data', responseGetProfile);
    //
    //
    // var drivers = responseGetProfile.drivers;
    //
    // console.log('drivers data', drivers);
    // var temp = [{ key: 1, name: 'Instagram', image: require('../../../assets/insta.png') }, { key: 2, name: 'Twitter', image: require('../../../assets/twitter.png') }];
    // this.setState({
    //   dataSource: temp
    // });
  }

  componentDidMount() {}

  goBack() {
    this.props.navigation.goBack();
  }
  openDrawerClick() {
    this.props.navigation.dispatch(DrawerActions.openDrawer());
  }
  sendMessage() {
    this.props.navigation.navigate('SupportMessage');
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: '#ffffff'}}>
        <RenderHeader
          back={true}
          title={strings.HelpSupport}
          navigation={this.props.navigation}
        />

        <View style={styles.gridViewBackground}>
          <View
            style={{
              width: '100%',
              flex: 1,
              overflow: 'hidden',
              marginBottom: 100,
              backgroundColor: 'white',
            }}>
            <WebView
              source={{
                uri: 'http://3.21.59.245:3001/admin/helpandsupport',
              }}
              style={{
                width: '100%',
                height: '100%',
                backgroundColor: 'white',
              }}
            />
          </View>

          <View style={styles.arrowTile}>
            <View>
              <Text style={{fontWeight: 'bold', marginBottom: 20}}>
                Still stuck ? Help is a mail away
              </Text>
            </View>
            <TouchableOpacity
              onPress={() => this.sendMessage()}
              style={{
                width: '90%',
                height: 45,
                marginTop: 0,
                alignItems: 'center',
                justifyContent: 'center',
                marginHorizontal: 10,
                borderRadius: 27,
                marginBottom: 30,
                backgroundColor: Colors.primary1,
              }}>
              <Text style={styles.title}> {strings.SENDAMESSAGE} </Text>
            </TouchableOpacity>
          </View>
        </View>
        {/* <TouchableOpacity
          style={styles.bottom}
          onPress={() => this.goToNextScreen()}>
          <Text style={styles.button}>
            {' '}
            <Text style={styles.title}> {strings.SENDAMESSAGE} </Text>
          </Text>
        </TouchableOpacity> */}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: RFValue(20),
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  headerView: {
    height: 60,
    width: '100%',
    backgroundColor: '#1c1c1a',
    borderColor: '#0082cb',
    borderWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: RFValue(16),
    color: Colors.Black,
  },

  gridViewBackground: {
    flex: 1,
    marginTop: 0,
    marginBottom: 0,
    borderColor: 'white',
    borderWidth: 0.0,
    backgroundColor: 'white',
  },
  backTouchable: {
    position: 'absolute',
    width: 60,
    height: 60,
    top: 0,
    left: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backIcon: {
    width: 22,
    height: 22,
    top: 0,
    left: 0,
    backgroundColor: 'transparent',
  },
  loadDetail: {
    width: 'auto',
    height: wp('18.66%'),
    backgroundColor: 'white',
    marginVertical: 7,
    marginHorizontal: 5,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    borderColor: 'gray',
    borderBottomWidth: 0.6,
  },
  sourceAddressBG: {
    marginTop: 20,
    marginHorizontal: 20,
    height: 30,
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  destAddressBG: {
    marginTop: 0,
    marginHorizontal: 20,
    height: 30,
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  sourceIcon: {
    width: 15,
    height: 15,
  },
  equipmentIcon: {
    width: 0,
    height: 0,
  },
  arrowTile: {
    backgroundColor: 'white',
    width: '100%',
    position: 'absolute',
    left: 0,
    bottom: 80,
    justifyContent: 'center',
    alignItems: 'center',
    height: wp('16%'),
    marginBottom: -50,
    borderBottomWidth: 0,
    borderColor: '#818e97',
  },
  touchableArrow: {
    backgroundColor: '#2a4359',
    position: 'absolute',
    right: 0,
    height: wp('16%'),
    width: wp('16%'),
    bottom: 0,
    borderRadius: wp('8%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  arrowIcon: {
    width: wp('6.66%'),
    height: wp('6.66%'),
  },
  txtNoLoads: {
    marginTop: 50,
    width: '100%',
    textAlign: 'center',
    fontSize: wp('5.86%'),
    fontWeight: 'bold',
  },
  button: {
    color: Colors.Black,
    fontFamily: Fonts.Semibold,
    fontSize: RFValue(16),
  },
  bottom: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    backgroundColor: Colors.primary1,
    height: moderateScale(45),
    marginHorizontal: moderateScale(20),
    borderRadius: moderateScale(30),
    bottom: moderateScale(25),
    width: '90%',
  },
});
