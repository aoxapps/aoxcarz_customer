import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  Keyboard,
  ImageBackground,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import axios from 'axios';
import Activity from '../../components/ActivityIndicator';
import {Platform} from 'react-native';
import {getConfiguration} from '../../utils/configuration';

import {isEmpty} from '../../utils/CommonFunctions';
import {strings} from '../../constants/languagesString';
import RenderHeader from './../../components/CustomComponent/renderHeaderMain';
import Fonts from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import Images from '../../constants/Images';
import styles from './style';
import {getStatusBarHeight} from '../../utils/IPhoneXHelper';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class ChangePasswordProfile extends React.Component {
  that = this;
  constructor(props) {
    super(props);
    this.state = {
      password: '',
      confirmPassword: '',
      oldPassword: '',
      fcmToken: '',
    };
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }
  _keyboardDidShow() {
    this.setState({
      mainViewTop: -200,
    });
  }
  _keyboardDidHide() {
    this.setState({
      mainViewTop: 0,
    });
  }

  componentWillMount() {
    this.setState({autoLogin: true});
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow.bind(this),
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide.bind(this),
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  showAlert(message, duration) {
    this.setState({autoLogin: false});
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      alert(message);
    }, duration);
  }

  goBack() {
    this.props.navigation.goBack();
  }

  goToNextScreen() {
    this.changePasswordAPI();
  }

  verifyCredentials = () => {
    const {oldPassword, password, confirmPassword} = this.state;
    if (isEmpty(oldPassword)) {
      alert('Please enter current password!!');
      return false;
    } else if (isEmpty(password)) {
      alert('Please enter password!!');
      return false;
    } else if (isEmpty(confirmPassword)) {
      alert('Please enter confirm password!!');
      return false;
    } else if (password !== confirmPassword) {
      alert('Password do not match!!');
      return false;
    }
    return true;
  };

  changePasswordAPI = () => {
    const {navigation, changeRequest} = this.props;
    const {password, oldPassword} = this.state;
    if (this.verifyCredentials()) {
      changeRequest(password, oldPassword, navigation);
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.HeaderImage}>
          <ImageBackground
            style={{
              flex: 1,
              paddingTop: Platform.select({
                ios: getStatusBarHeight(),
                android: 0,
              }),
              alignItems: 'center',
              justifyContent: 'center',
            }}
            source={Images.sidemenuBG}
            resizeMode="cover">
            <View style={styles.headerView}>
              <View style={{flex: 1}}>
                <TouchableOpacity onPress={() => this.goBack()}>
                  <Image
                    resizeMode="contain"
                    style={styles.backIcon}
                    source={Images.backIcon}
                  />
                </TouchableOpacity>
              </View>
              <Text style={styles.title}>{strings.change_password}</Text>
              <View style={{flex: 1}} />
            </View>
          </ImageBackground>
        </View>

        <View style={styles.lowerView}>
          <KeyboardAwareScrollView>
            <View style={styles.tile}>
              <Image
                resizeMode="contain"
                style={styles.tileIcon}
                source={Images.passwordIcon}
              />
              <TextInput
                style={styles.searchTextInput}
                placeholder="Old Password"
                placeholderTextColor={'#818e97'}
                autoCorrect={false}
                secureTextEntry={true}
                returnKeyType="next"
                onChangeText={oldPassword => this.setState({oldPassword})}
                onSubmitEditing={() => this.password.focus()}
                value={this.state.oldPassword}
              />
            </View>
            <View style={styles.tile}>
              <Image
                resizeMode="contain"
                style={styles.tileIcon}
                source={Images.passwordIcon}
              />
              <TextInput
                style={styles.searchTextInput}
                placeholder="New Password"
                placeholderTextColor={'#818e97'}
                autoCorrect={false}
                secureTextEntry={true}
                onChangeText={password => this.setState({password})}
                returnKeyType="next"
                onSubmitEditing={() => this.confirmPassword.focus()}
                ref={password => (this.password = password)}
                value={this.state.password}
              />
            </View>
            <View style={styles.tile}>
              <Image
                resizeMode="contain"
                style={styles.tileIcon}
                source={Images.passwordIcon}
              />
              <TextInput
                style={styles.searchTextInput}
                placeholder="Confirm Password"
                placeholderTextColor={'#818e97'}
                autoCorrect={false}
                secureTextEntry={true}
                returnKeyType="next"
                onChangeText={confirmPassword =>
                  this.setState({confirmPassword})
                }
                ref={confirmPassword =>
                  (this.confirmPassword = confirmPassword)
                }
                value={this.state.confirmPassword}
              />
            </View>
          </KeyboardAwareScrollView>

          <TouchableOpacity
            style={styles.bottom}
            onPress={() => this.goToNextScreen()}>
            <Text style={styles.button}>{strings.save}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
