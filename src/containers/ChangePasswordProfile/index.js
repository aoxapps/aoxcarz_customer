import ChangePasswordProfile from './ChangePasswordProfile';
import {connect} from 'react-redux';
import {
  resetPRequest,
  changeRequest,
} from './../../modules/ChangePassword/actions';
const mapStateToProps = state => ({
  resetData: state.changePReducer.data,
});
const mapDispatchToProps = dispatch => ({
  changeRequest: (password, oldPassword, navigation) =>
    dispatch(changeRequest(password, oldPassword, navigation)),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChangePasswordProfile);
//export default ChangePasswordProfile;
