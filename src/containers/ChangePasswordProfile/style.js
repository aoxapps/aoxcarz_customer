import {StyleSheet, Platform} from 'react-native';
import Fonts from '../../constants/Fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Colors from '../../constants/Colors';
import {moderateScale, scale, verticalScale} from 'react-native-size-matters';
import {RFValue} from 'react-native-responsive-fontsize';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.White,
  },
  HeaderImage: {
    width: '100%',
    backgroundColor: Colors.White,
    height: '32%',
  },
  headerView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    alignItems: 'center',
    marginTop: moderateScale(-170),
  },
  backIcon: {
    width: moderateScale(55),
    height: moderateScale(55),
    marginLeft: moderateScale(5),
  },
  img: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'transparent',
  },
  lowerView: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? moderateScale(40) : moderateScale(40),
  },
  tileIcon: {
    width: moderateScale(18),
    height: moderateScale(30),
    tintColor: Colors.placeholderColor,
    marginTop: moderateScale(-5),
    marginLeft: moderateScale(20),
  },
  tile: {
    backgroundColor: 'transparent',
    width: 'auto',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    borderBottomWidth: 0.5,
    paddingHorizontal: moderateScale(20),
    borderColor: Colors.bottomBorder,
  },
  searchTextInput: {
    height: moderateScale(55),
    width: '100%',
    paddingHorizontal: 20,
    backgroundColor: 'transparent',
    borderColor: 'gray',
    borderRadius: 0,
    fontSize: RFValue(16),
    fontFamily: Fonts.Regular,
  },

  arrowTile: {
    backgroundColor: 'transparent',
    width: 'auto',
    height: wp('16%'),
    marginTop: wp('8%'),
    marginBottom: 10,
    alignItems: 'center',
    flexDirection: 'row',
    marginHorizontal: wp('5.33%'),
    borderBottomWidth: 0,
    borderColor: '#818e97',
  },
  touchableArrow: {
    backgroundColor: Colors.Black,
    position: 'absolute',
    right: 0,
    height: wp('16%'),
    width: wp('16%'),
    borderRadius: wp('8%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  arrowIcon: {
    width: 30,
    height: 30,
  },
  title: {
    fontSize: RFValue(18),
    color: 'white',
    fontFamily: Fonts.Semibold,
  },

  button: {
    color: Colors.Black,
    fontFamily: Fonts.Semibold,
    fontSize: RFValue(16),
  },
  bottom: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    backgroundColor: Colors.primary1,
    height: moderateScale(45),
    marginHorizontal: moderateScale(20),
    borderRadius: moderateScale(30),
    bottom: moderateScale(25),
    width: '90%',
  },
});

export default styles;
