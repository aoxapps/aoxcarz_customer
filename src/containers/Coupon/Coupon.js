import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  ScrollView,
  Keyboard,
  FlatList,
} from 'react-native';
import {DrawerActions} from 'react-navigation-drawer';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {getConfiguration} from '../../utils/configuration';
import moment from 'moment';
import {strings} from '../../constants/languagesString';
import RenderHeader from './../../components/CustomComponent/renderHeaderMain';
import Fonts from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import Images from '../../constants/Images';

export default class Coupon extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      dataSourcePast: [],
      selTab: '0',
      underLineLeft: 0,
    };
    const {navigation} = props;
    this.didFocusListener = navigation.addListener(
      'didFocus',
      this.componentDidFocus,
    );
  }

  componentWillUnmount() {
    this.didFocusListener.remove();
  }
  componentDidFocus = payload => {
    this.getData();
  };

  componentDidMount() {}

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.promoCodesData !== this.props.promoCodesData) {
      var data = this.props.promoCodesData;
      this.afterGetData(data);
    }
  }
  afterGetData = data => {
    var aa = data;
    this.setState({
      dataSource: data,
    });
  };

  getData = () => {
    this.props.promoListRequest();
  };

  getExpiryDate(dat) {
    // var aa = dat;
    // var newDate = moment(dat).format('DD-MM-YYYY');
    var newDate = moment(dat).format('MM-DD-YYYY');
    var newDate1 = 'Valid Upto ' + newDate;
    return newDate1;
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <RenderHeader
          back={true}
          title={strings.Promotions}
          navigation={this.props.navigation}
        />

        <View style={{flex: 1, backgroundColor: Colors.White, marginTop: 2}}>
          <FlatList
            data={this.state.dataSource}
            showsVerticalScrollIndicator={false}
            renderItem={({item}) => (
              <TouchableOpacity style={styles.loadDetail}>
                <View
                  style={{
                    flexDirection: 'row',
                    //justifyContent: 'center',
                    alignContent: 'space-between',
                    width: '100%',
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      margin: 10,
                      width: '75%',
                    }}>
                    <View>
                      <Image
                        resizeMode="contain"
                        style={{height: 50, width: 50}}
                        source={require('../../../assets/couponRide.png')}
                      />
                    </View>
                    <View style={{marginLeft: 15}}>
                      <Text>{item.promocode}</Text>
                      {item.type === 'Flat' ? (
                        <Text
                          style={{
                            color: '#BEBDC2',
                            //  marginTop: 2,
                            fontWeight: 'bold',
                          }}>
                          Flat ${item.amount} off on ride
                        </Text>
                      ) : (
                        <Text
                          style={{
                            color: '#BEBDC2',
                            //  marginTop: 2,
                            fontWeight: 'bold',
                          }}>
                          {item.maxAmount}% off on ride
                        </Text>
                      )}
                      {/* <Text
                        style={{
                          color: '#BEBDC2',
                          //  marginTop: 2,
                          fontWeight: 'bold',
                        }}>
                        {item.detials}
                      </Text> */}
                      <Text
                        style={{
                          color: '#BEBDC2',
                          // marginTop: 2,
                          fontWeight: 'bold',
                        }}>
                        {this.getExpiryDate(item.upto)}
                      </Text>
                    </View>
                  </View>

                  <View
                    style={{
                      // flexDirection: 'column',
                      alignItems: 'center',
                      margin: 10,
                      width: '25%',
                      paddingRight: 20,
                      // marginTop: 5,
                      // alignItems: 'stretch',
                      alignSelf: 'center',
                    }}>
                    <View style={{flexDirection: 'row'}}>
                      <Text
                        style={{
                          color: '#BEBDC2',
                          // marginTop: 2,
                          fontWeight: 'bold',
                        }}>
                        Used:
                      </Text>
                      <Text
                        style={{
                          color: '#BEBDC2',
                          //marginTop: 2,
                          fontWeight: 'bold',
                        }}>
                        {item.used}
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <Text
                        style={{
                          color: '#BEBDC2',
                          // marginTop: 2,
                          fontWeight: 'bold',
                        }}>
                        Limit:
                      </Text>
                      <Text
                        style={{
                          color: '#BEBDC2',
                          //marginTop: 2,
                          fontWeight: 'bold',
                        }}>
                        {item.limit}
                      </Text>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            )}
            keyExtractor={item => item.id}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  headerView: {
    height: 60,
    width: '100%',
    backgroundColor: '#1c1c1a',
    borderColor: '#0082cb',
    borderWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    color: 'white',
  },

  gridViewBackground: {
    flex: 1,
    marginTop: 0,
    marginBottom: -50,
    borderColor: 'black',
    borderWidth: 0.0,
    backgroundColor: 'white',
  },
  backTouchable: {
    position: 'absolute',
    width: 60,
    height: 60,
    top: 0,
    left: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backIcon: {
    // position: 'absolute',
    width: 22,
    height: 22,
    top: 0,
    left: 0,
    backgroundColor: 'transparent',
  },
  loadDetail: {
    backgroundColor: '#ffff',
    borderColor: 'gray',
    marginBottom: 6,
    marginLeft: '2%',
    width: '96%',
    marginTop: 5,
    borderRadius: 5,
    padding: 6,
    shadowRadius: 5,
    borderBottomWidth: 0.0,
  },
  sourceAddressBG: {
    marginTop: 20,
    marginHorizontal: 20,
    height: 30,
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  destAddressBG: {
    marginTop: 0,
    marginHorizontal: 20,
    height: 30,
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  sourceIcon: {
    width: 15,
    height: 15,
  },
  equipmentIcon: {
    width: 0,
    height: 0,
  },
  arrowTile: {
    backgroundColor: 'transparent',
    height: wp('16%'),
    position: 'absolute',
    right: wp('5.33%'),
    left: wp('5.33%'),
    bottom: wp('18%'),
    alignItems: 'center',
    flexDirection: 'row',
    borderWidth: 0,
    borderColor: 'blue',
  },
  touchableArrow: {
    backgroundColor: '#2a4359',
    position: 'absolute',
    right: 0,
    height: wp('16%'),
    width: wp('16%'),
    bottom: 0,
    borderRadius: wp('8%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  arrowIcon: {
    width: wp('6.66%'),
    height: wp('6.66%'),
  },
  txtNoLoads: {
    marginTop: 50,
    width: '100%',
    textAlign: 'center',

    fontSize: wp('5.86%'),
  },
  tabViewBG: {
    flexDirection: 'row',
    marginHorizontal: 0,
    backgroundColor: 'black',
    height: wp('10.66%'),
    width: '100%',
  },
  tab1BG: {
    backgroundColor: 'transparent',
    height: '100%',
    width: '50%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tab2BG: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
    height: '100%',
    width: '50%',
  },
  txtUpcoming: {
    fontSize: wp('4.8%'),
    color: 'white',
    fontWeight: 'bold',
  },
  list: {
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  cards: {
    padding: 30,
    backgroundColor: '#d9d9d9',
    shadowColor: '#000000',
    shadowOpacity: 2,
    shadowRadius: 2,
    margin: 12,
    shadowOffset: {
      height: 1,
      width: 1,
    },
  },
});
