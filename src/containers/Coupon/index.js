import Coupon from './Coupon';

import {promoListRequest} from '../../modules/PromoCodes/actions';
import {connect} from 'react-redux';
const mapStateToProps = state => ({
  promoCodesData: state.promoCodesReducer.promoListData,
});
const mapDispatchToProps = dispatch => ({
  promoListRequest: navigation => dispatch(promoListRequest(navigation)),
});
export default connect(mapStateToProps, mapDispatchToProps)(Coupon);
