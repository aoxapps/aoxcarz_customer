import {StyleSheet} from 'react-native';
import Fonts from '../../constants/Fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Colors from '../../constants/Colors';
const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    color: 'white',
  },
  loadDetail: {
    backgroundColor: '#F5F5F5',
    borderColor: 'gray',
    marginBottom: 6,
    marginLeft: '3%',
    width: '94.5%',
    marginTop: 8,
    borderRadius: 5,
    padding: 6,
    shadowRadius: 5,
    borderBottomWidth: 0.0,
  },
  listItem: {
    flexDirection: 'row',
    margin: 5,
    alignContent: 'center',
    justifyContent: 'space-between',
  },
  itemDelete: {
    // borderWidth: 1,
    //  borderColor: 'red',
    // borderRadius: 20,
    width: 30,
    height: 30,
    alignItems: 'center',
    top: 10,
  },
  addCard: {
    position: 'absolute',
    bottom: 50,
    right: 35,
    backgroundColor: Colors.Black,
    width: 60,
    height: 60,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
export default styles;
