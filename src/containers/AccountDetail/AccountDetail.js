import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  ScrollView,
  Keyboard,
  FlatList,
  Alert,
} from 'react-native';
import {DrawerActions} from 'react-navigation-drawer';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Activity from '../../components/ActivityIndicator';
import {getConfiguration} from '../../utils/configuration';
import {strings} from '../../constants/languagesString';
import RenderHeader from './../../components/CustomComponent/renderHeaderMain';
import Fonts from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import Images from '../../constants/Images';
import {bindActionCreators} from 'redux';
import styles from './style';
//import {SQIPCardEntry, SQIPCore} from 'react-native-square-in-app-payments';
import Modal from 'react-native-modal';
// import {
//   SQUARE_APP_ID,
//   // SQUARE_LOCATION_ID,
//   // CHARGE_SERVER_HOST,
//   // GOOGLE_PAY_LOCATION_ID,
//   // APPLE_PAY_MERCHANT_ID,
//   // CUSTOMER_ID
// } from '../../constants/SquareConstants';

var isMount = false;
export default class AccountDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
    };
    const {navigation} = props;
    this.didFocusListener = navigation.addListener(
      'didFocus',
      this.componentDidFocus,
    );
    this.onStartCardEntry = this.onStartCardEntry.bind(this);
    this.onCardNonceRequestSuccess = this.onCardNonceRequestSuccess.bind(this);
  }
  componentWillUnmount() {
    this.didFocusListener.remove();
  }
  async componentWillMount() {
    if (Platform.OS === 'ios') {
      await SQIPCardEntry.setIOSCardEntryTheme({
        saveButtonFont: {
          size: 25,
        },
        saveButtonTitle: 'Order 💳 ',
        keyboardAppearance: 'Light',
        saveButtonTextColor: {
          r: 255,
          g: 0,
          b: 125,
          a: 0.5,
        },
      });
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.PaymentListData !== this.props.PaymentListData) {
      this.setState({dataSource: this.props.PaymentListData}, () => {
        this.afterGetPaymentMethodList();
      });
    }
  }
  componentDidFocus = payload => {
    this.getPaymentMethodsList();
  };
  getPaymentMethodsList = () => {
    // var aa = this.props.PaymentListData;
    //console.log('aa');
    this.props.PaymentMethodListRequest(this.props.navigation);
    // this.setState({dataSource: this.props.PaymentListData});
  };
  componentDidMount = async () => {
    await SQIPCore.setSquareApplicationId(SQUARE_APP_ID);
  };

  onCardEntryComplete = cardDetails => {
    const customerid = getConfiguration('user_id');
    this.props.posRequest(
      customerid,
      cardDetails.card.brand,
      cardDetails.nonce,
      cardDetails.card.lastFourDigits,
      cardDetails.card.postalCode,
      this.props.navigation,
    );
  };

  onCardNonceRequestSuccess = async cardDetails => {
    try {
      await SQIPCardEntry.completeCardEntry(
        this.onCardEntryComplete(cardDetails),
      );
    } catch (ex) {
      // var errorMessage = 'Invalid card details';
      await SQIPCardEntry.showCardNonceProcessingError(ex.message);
    }
  };

  onCardEntryCancel = () => {};

  onStartCardEntry = async () => {
    const cardEntryConfig = {
      collectPostalCode: true,
    };
    await SQIPCardEntry.startCardEntryFlow(
      cardEntryConfig,
      this.onCardNonceRequestSuccess,
      this.onCardEntryCancel,
    );
  };

  afterGetPaymentMethodList() {
    this.setState({
      dataSource: this.props.PaymentListData,
    });
  }

  goBack() {
    this.props.navigation.goBack();
  }

  addCardAction() {
    this.props.navigation.navigate('AddCard');
  }

  deleteCard(cardId) {
    Alert.alert(strings.Alert, strings.Areyousureyouwanttoremovethiscard, [
      {text: strings.Ok, onPress: () => this.deleteCard1(cardId)},
      {
        text: strings.Cancel,
        onPress: () => console.log('Cancel Pressed'),
        style: strings.Cancel,
      },
    ]);
  }

  deleteCard1(cardId) {
    this.props.posDeleteRequest(cardId, this.props.navigation);
  }

  showAlert(message, duration) {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      alert(message);
    }, duration);
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <RenderHeader
          back={true}
          title={strings.PaymentMethod}
          navigation={this.props.navigation}
        />
        <View style={{flex: 1, marginTop: 6}}>
          <FlatList
            data={this.state.dataSource}
            showsVerticalScrollIndicator={false}
            renderItem={({item}) => (
              <View style={styles.loadDetail}>
                <View style={styles.listItem}>
                  <View style={{width: '20%'}}>
                    <Image
                      resizeMode="contain"
                      style={{height: 40, width: 40}}
                      source={{uri: item.logo}}
                    />
                  </View>
                  {item.type === 'Cash' ? (
                    <View>
                      <Text style={{top: 20}}>{item.type}</Text>
                    </View>
                  ) : item.type === 'Wallet' ? (
                    <View>
                      <Text style={{top: 20}}>{item.type}</Text>
                    </View>
                  ) : (
                    <View style={{flexDirection: 'row'}}>
                      <View>
                        <Text style={{top: 20, marginRight: 20}}>
                          ***********{item.lastd}
                        </Text>
                      </View>
                      <TouchableOpacity
                        onPress={() => this.deleteCard(item._id)}
                        style={styles.itemDelete}>
                        <Image
                          resizeMode="contain"
                          style={{width: '80%', height: '80%'}}
                          source={require('../../../assets/delete.png')}
                        />
                      </TouchableOpacity>
                    </View>
                  )}
                </View>
              </View>
            )}
            keyExtractor={item => item._id}
          />
        </View>
        <TouchableOpacity
          onPress={() => this.addCardAction()}
          // onPress={() => this.onStartCardEntry()}
          style={styles.addCard}>
          <Image
            resizeMode="contain"
            style={{height: 60, width: 60}}
            source={Images.addIcon}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
