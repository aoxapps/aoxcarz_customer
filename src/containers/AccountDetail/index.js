import AccountDetail from './AccountDetail';
import {connect} from 'react-redux';
import {PaymentMethodListRequest} from './../../modules/GetPaymentMethodList/actions';
import {posDeleteRequest} from './../../modules/DeletePOS/actions';
import {posRequest} from './../../modules/AddPOS/actions';

const mapStateToProps = state => ({
  PaymentListData: state.paymentListReducer.paymentMethodData,
});
const mapDispatchToProps = dispatch => ({
  PaymentMethodListRequest: navigation =>
    dispatch(PaymentMethodListRequest(navigation)),
  posDeleteRequest: (posID, navigation) =>
    dispatch(posDeleteRequest(posID, navigation)),
  posRequest: (customerId, name, token, lastd, postalCode, navigation) =>
    dispatch(
      posRequest(customerId, name, token, lastd, postalCode, navigation),
    ),
});
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AccountDetail);

//export default AccountDetail;
