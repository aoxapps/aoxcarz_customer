import PreferredDriver from './PreferredDriver';
import {preferredRequest} from './../../modules/PreferDriver/actions';
import {connect} from 'react-redux';
const mapStateToProps = state => ({
  preferredDriverList: state.preferredDriverReducer.data,
});

const mapDispatchToProps = dispatch => ({
  preferredRequest: () => dispatch(preferredRequest()),
});
export default connect(mapStateToProps, mapDispatchToProps)(PreferredDriver);
//export default PreferredDriver;
