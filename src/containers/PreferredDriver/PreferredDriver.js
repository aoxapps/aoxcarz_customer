import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  FlatList,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  ScrollView,
  Keyboard,
  Platform,
  Alert,
} from 'react-native';
import {DrawerActions} from 'react-navigation-drawer';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Activity from '../../components/ActivityIndicator';

import {strings} from '../../constants/languagesString';
import RenderHeader from './../../components/CustomComponent/renderHeaderMain';
import Fonts from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import Images from '../../constants/Images';
import Config from '../../constants/Config';
import {moderateScale, scale} from 'react-native-size-matters';
import {RFValue} from 'react-native-responsive-fontsize';

export default class PreferredDriver extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataSource: [],
      calling: this.props.navigation.getParam('calling', ''),
      preferredDriverList: '',
    };
    const {navigation} = props;
    this.didFocusListener = navigation.addListener(
      'didFocus',
      this.componentDidFocus,
    );
  }

  componentDidFocus = payload => {
    // const {params} = payload.action;
    // console.log('-----hurrayyyyyy------');
    // console.log('did focus main enter kr gya hai');
    // console.log('-------------adadadadadada-----------------');
    this.getData();
    // this.favouritesAPI();
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.preferredDriverList !== this.props.preferredDriverList) {
      this.setState(
        {preferredDriverList: this.props.preferredDriverList[0]},
        () => {
          var data = this.state.preferredDriverList.prefferedDriver;
          this.afterPreferDriverList(data);
        },
      );
    }
  }
  getData() {
    this.props.preferredRequest();
  }

  afterPreferDriverList = data => {
    if (data.length > 0) {
      var PreferData = data;
      this.setState({
        dataSource: PreferData,
      });
    }
  };

  bookNow(driverId) {
    this.props.navigation.navigate(Config.MapSearchScreen, {
      driverId: driverId,
      prefDriverRide: 'yes',
    });
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'black'}}>
        <RenderHeader
          back={true}
          title={strings.PreferredDriver}
          navigation={this.props.navigation}
        />

        <View style={styles.gridViewBackground}>
          {this.state.dataSource && this.state.dataSource.length > 0 ? (
            <FlatList
              style={{marginTop: 10}}
              data={this.state.dataSource}
              renderItem={({item}) => (
                <View
                  style={{
                    flex: 1,
                    borderBottomWidth: 1.0,
                    borderColor: '#818e97',
                    paddingBottom: wp('7%'),
                    flexDirection: 'row',
                    width: '100%',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                  }}>
                  <View style={{width: '25%'}}>
                    {item.profileImage == '' ||
                    item.profileImage == 'null' ||
                    item.profileImage == null ? (
                      <Image
                        resizeMode="contain"
                        style={{
                          width: wp('25%'),
                          height: wp('15%'),
                          marginTop: wp('3%'),
                        }}
                        source={require('../../../assets/dummyUser.png')}
                      />
                    ) : (
                      <Image
                        resizeMode="contain"
                        style={{
                          width: wp('15%'),
                          height: wp('15%'),
                          marginTop: wp('3%'),
                        }}
                        source={{uri: item.profileImage}}
                      />
                    )}
                  </View>
                  <View style={{width: '50%'}}>
                    <View style={{width: '100%'}}>
                      <Text
                        style={{
                          fontFamily: Fonts.Semibold,
                          fontSize: wp('4.3%'),
                        }}>
                        {item.name}
                      </Text>
                    </View>
                    <View style={{flexDirection: 'row', width: '100%'}}>
                      <Text
                        style={{
                          //fontWeight: 'bold',
                          width: '50%',
                          fontSize: wp('3.5%'),
                          color: '#000000',
                        }}>
                        {strings.CarNumber}:
                      </Text>
                      <Text
                        style={{
                          fontWeight: 'bold',
                          width: '50%',
                          fontSize: wp('3.5%'),
                          color: '#439cba',
                          top: 2,
                        }}>
                        {item.selectedCar.plateNumber}
                      </Text>
                    </View>
                    <View style={{width: '100%'}}>
                      {item.driverStatus == Config.DRIVER_FINDING_TRIPS ? (
                        <Text
                          style={{
                            fontWeight: 'bold',
                            color: '#0A7C06',
                            fontSize: wp('3.3%'),
                            marginTop: 3,
                          }}>
                          {strings.Available}
                        </Text>
                      ) : (
                        <Text style={{fontWeight: 'bold', fontSize: wp('3%')}}>
                          {' '}
                          {strings.NotAvailable}
                        </Text>
                      )}
                    </View>
                  </View>
                  <View style={{width: '22%', right: 10}}>
                    {item.driverStatus == Config.DRIVER_FINDING_TRIPS ? (
                      <TouchableOpacity
                        onPress={() => this.bookNow(item._id)}
                        style={{
                          width: '100%',
                          height: wp('6%'),
                          alignItems: 'center',
                          justifyContent: 'center',
                          borderRadius: 20,
                          backgroundColor: Colors.primary1,
                        }}>
                        <Text style={{color: Colors.Black, fontWeight: 'bold'}}>
                          {' '}
                          {strings.BookNow}
                        </Text>
                      </TouchableOpacity>
                    ) : (
                      <Text> </Text>
                    )}
                  </View>
                </View>
              )}
            />
          ) : (
            <Text style={styles.txtNoLoads}> {strings.NoPreferredDriver} </Text>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  headerView: {
    height: 40,
    width: '100%',
    backgroundColor: 'black',
    borderColor: '#0082cb',
    borderWidth: 0,
  },
  title: {
    fontSize: 18,
    color: 'white',
    marginTop: wp('2%'),
    alignSelf: 'center',
  },

  gridViewBackground: {
    flex: 1,
    marginTop: 0,
    marginBottom: -50,
    borderColor: 'black',
    borderWidth: 0.0,
    backgroundColor: 'white',
  },
  backTouchable: {
    position: 'absolute',
    width: 60,
    height: 50,
    top: 0,
    left: 0,
  },
  backIcon: {
    position: 'absolute',
    width: 22,
    height: 22,
    top: 10,
    left: 15,
    backgroundColor: 'transparent',
  },
  loadDetail: {
    width: 'auto',
    height: wp('18.66%'),
    backgroundColor: 'white',
    borderRadius: 5,
    marginVertical: 7,
    marginHorizontal: 5,
    alignItems: 'center',
    flexDirection: 'row',
    borderColor: 'gray',
    borderBottomWidth: 0.0,
    // shadowColor: '#000000',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    //shadowRadius: 1,
    // shadowOpacity: 0.6,
  },
  sourceAddressBG: {
    marginTop: 20,
    marginHorizontal: 20,
    height: 30,
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  destAddressBG: {
    marginTop: 0,
    marginHorizontal: 20,
    height: 30,
    backgroundColor: 'white',
    flexDirection: 'row',
  },
  sourceIcon: {
    width: 15,
    height: 15,
  },
  equipmentIcon: {
    width: 0,
    height: 0,
  },
  arrowTile: {
    backgroundColor: 'transparent',
    height: wp('16%'),
    position: 'absolute',
    right: wp('5.33%'),
    left: wp('5.33%'),
    bottom: wp('18%'),
    alignItems: 'center',
    flexDirection: 'row',
    borderWidth: 0,
    borderColor: 'blue',
  },
  touchableArrow: {
    // backgroundColor: '#f0579a',
    position: 'absolute',
    right: 0,
    height: wp('16%'),
    width: wp('16%'),
    bottom: 0,
    borderRadius: wp('8%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  arrowIcon: {
    width: '100%',
    height: '100%',
  },
  txtNoLoads: {
    marginTop: 150,
    width: '100%',
    textAlign: 'center',
    color: 'gray',
    fontSize: RFValue(16),
  },
});
