import {StyleSheet} from 'react-native';
import Fonts from '../../constants/Fonts';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Colors from '../../constants/Colors';
import {moderateScale, scale} from 'react-native-size-matters';
import {RFValue} from 'react-native-responsive-fontsize';

const styles = StyleSheet.create({
  lowerView: {
    flex: 1,
    marginTop: 0,
  },

  tileIcon: {
    width: moderateScale(18),
    height: moderateScale(40),
    marginTop: moderateScale(-5),
    marginLeft: moderateScale(20),
  },
  tile: {
    backgroundColor: Colors.background1,
    width: 'auto',
    marginTop: wp('8%'),
    // marginHorizontal: 20,
    alignItems: 'center',
    flexDirection: 'row',
    height: moderateScale(45),
    marginHorizontal: moderateScale(20),
    borderRadius: moderateScale(16),
    borderColor: Colors.bottomBorder,
  },
  searchTextInput: {
    height: moderateScale(40),
    width: '100%',
    // paddingBottom: 5,
    paddingHorizontal: moderateScale(20),
    backgroundColor: 'transparent',
    borderColor: 'gray',
    borderRadius: 0,
    fontSize: wp('4.8%'),
    fontFamily: 'CharlieDisplay-Regular',
  },

  title: {
    fontSize: wp('4.8%'),
    color: 'white',
    fontFamily: 'CharlieDisplay-Semibold',
  },
  bottom: {
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    backgroundColor: Colors.primary1,
    height: moderateScale(45),
    marginHorizontal: moderateScale(20),
    borderRadius: moderateScale(30),
    bottom: moderateScale(35),
    width: '90%',
  },
});
export default styles;
