import {NavigationActions} from 'react-navigation';
import ChangePassword from './ChangePassword';
import {connect} from 'react-redux';
import {resetPRequest} from './../../modules/ChangePassword/actions';

const mapStateToProps = state => ({
  resetData: state.changePReducer.data,
});
const mapDispatchToProps = dispatch => ({
  resetRequest: (password, navigation) =>
    dispatch(resetPRequest(password, navigation)),
});
export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword);
