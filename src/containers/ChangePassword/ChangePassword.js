import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {Platform} from 'react-native';

import {isEmpty} from '../../utils/CommonFunctions';
import RenderHeader from './../../components/CustomComponent/renderHeader';
import Fonts from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import Config from '../../constants/Config';
import Images from '../../constants/Images';
import styles from './style';
import {moderateScale, scale} from 'react-native-size-matters';
import {RFValue} from 'react-native-responsive-fontsize';
import strings from '../../constants/languagesString';

export default class ChangePassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPassword: '',
      password: '',
      confirmPassword: '',
    };
  }

  goBack() {
    this.props.navigation.goBack();
  }

  verifyCredentials = () => {
    const {password, confirmPassword} = this.state;
    if (isEmpty(password)) {
      alert('Please enter password!!');
      return false;
    } else if (isEmpty(confirmPassword)) {
      alert('Please enter confirm password!!');
      return false;
    } else if (password !== confirmPassword) {
      alert('Password do not match!!');
      return false;
    }
    return true;
  };

  requestAPI = () => {
    const {navigation, resetRequest} = this.props;
    const {password} = this.state;
    if (this.verifyCredentials()) {
      resetRequest(password, navigation);
    }
  };

  render() {
    const {navigation} = this.props;
    return (
      <View
        style={{
          flex: 1,
          marginTop: this.state.mainViewTop,
          backgroundColor: '#ffffff',
        }}>
        <RenderHeader
          back={true}
          title={'Forget Password'}
          navigation={this.props.navigation}
        />
        <View style={styles.lowerView}>
          <View style={styles.tile}>
            <Image
              resizeMode="contain"
              style={styles.tileIcon}
              source={Images.passwordIcon}
            />
            <TextInput
              style={styles.searchTextInput}
              placeholder="New Password"
              placeholderTextColor={'#818e97'}
              autoCorrect={false}
              secureTextEntry={true}
              onChangeText={password => this.setState({password})}
              value={this.state.password}
            />
          </View>

          <View style={styles.tile}>
            <Image
              resizeMode="contain"
              style={styles.tileIcon}
              source={Images.passwordIcon}
            />
            <TextInput
              style={styles.searchTextInput}
              placeholder="Confirm Password"
              placeholderTextColor={'#818e97'}
              autoCorrect={false}
              secureTextEntry={true}
              onChangeText={confirmPassword => this.setState({confirmPassword})}
              value={this.state.confirmPassword}
            />
          </View>
          {/* <View style={styles.bottomView}>
            <View style={styles.arrowTile}>
              <TouchableOpacity
                style={styles.touchableArrow}
                onPress={this.requestAPI}>
                <Image
                  resizeMode="contain"
                  style={styles.arrowIcon}
                  source={Images.nextIcon}
                />
              </TouchableOpacity>
            </View>
          </View> */}

          <View style={styles.bottom}>
            <TouchableOpacity onPress={() => this.requestAPI()}>
              <Text
                style={{
                  color: Colors.Black,
                  fontFamily: Fonts.Semibold,
                  fontSize: RFValue(16),
                }}>
                {strings.Submit}
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
