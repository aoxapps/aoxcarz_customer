import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Image,
  Text,
  TextInput,
  FlatList,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  ScrollView,
  Keyboard,
  Alert,
  StatusBar,
} from 'react-native';
import {WebView} from 'react-native-webview';
import {DrawerActions} from 'react-navigation-drawer';
import Activity from '../../components/ActivityIndicator';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
//import stripe from 'tipsi-stripe'
import {strings} from '../../constants/languagesString';
import Fonts from '../../constants/Fonts';
import Colors from '../../constants/Colors';
import Images from '../../constants/Images';
import RenderHeader from './../../components/CustomComponent/renderHeaderMain';

export default class About extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      message: '',
    };
  }

  componentDidMount() {}

  showAlert(message, duration) {
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      alert(message);
    }, duration);
  }

  afterSupportAPI() {
    this.props.navigation.goBack();
  }

  sendMessage() {
    this.props
      .supportAPI(this.state.name, this.state.message)
      .then(() => this.afterSupportAPI())
      .catch(e => this.showAlert(e.message, 300));
  }

  showAlert(message, duration) {
    this.setState({autoLogin: false});
    clearTimeout(this.timer);
    this.timer = setTimeout(() => {
      alert(message);
    }, duration);
  }

  openDrawerClick() {
    this.props.navigation.dispatch(DrawerActions.openDrawer());
  }

  render() {
    //StatusBar.setHidden(true, 'none');
    console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];
    console.disableYellowBox = true;
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <RenderHeader
          back={true}
          title={strings.AboutUs}
          navigation={this.props.navigation}
        />

        <View style={styles.gridViewBackground}>
          <View
            style={{
              width: '100%',
              height: hp('33%'),
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'transparent',
            }}>
            <Image
              resizeMode="contain"
              style={{width: 300, height: 300}}
              source={require('../../../assets/About.png')}
            />
          </View>

          <View
            style={{
              width: '100%',
              height: 'auto',
              flexDirection: 'row',
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: 'transparent',
            }}>
            <Text style={{fontSize: wp('7%'), fontFamily: Fonts.Semibold}}>
              {strings.WelcomeTo + '   '}
            </Text>
            <Text
              style={{
                color: Colors.PrimaryColor,
                fontFamily: Fonts.Semibold,
                fontSize: wp('7%'),
              }}>
              {strings.Fare}
            </Text>
          </View>

          {/* <View
            style={{
              width: '85%',
              height: 'auto',
              marginLeft: '7%',
              marginTop: 25,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{textAlign: 'center', color: '#C7C7C7', fontSize: 15}}>
              It is a long established fact that a reader will be distracted by
              the readable content of a page when looking at its layout.
            </Text>
          </View> */}
          <WebView
            source={{
              uri: 'http://3.12.72.231:5004/api/v1/admin/aboutus',
            }}
            style={{
              width: '100%',
              height: '100%',
              backgroundColor: 'white',
              textAlign: 'center',

              // color: '#C7C7C7',
            }}
          />
          {/* <View
            style={{
              width: '85%',
              height: 'auto',
              marginLeft: '7%',
              marginTop: 20,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{textAlign: 'center', color: '#C7C7C7', fontSize: 15}}>
              It is a long established fact that a reader will be distracted by
              the readable content of a page when looking at its layout.
            </Text>
          </View> */}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },

  headerView: {
    height: 60,
    width: '100%',
    backgroundColor: '#1c1c1a',
    borderColor: '#0082cb',
    borderWidth: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 20,
    color: 'white',
  },

  gridViewBackground: {
    height: '100%',
    width: '100%',
    marginTop: 10,
    borderColor: '#f5f6f8',
    borderWidth: 0,
    backgroundColor: 'white',
  },
  backTouchable: {
    position: 'absolute',
    width: 50,
    height: 60,
    top: 0,
    backgroundColor: 'transparent',
    left: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  backIcon: {
    width: 22,
    height: 22,
    top: 0,
    left: 0,
    backgroundColor: 'transparent',
  },

  tileIcon: {
    width: 20,
    height: 40,
    marginLeft: 20,
    marginTop: -5,
  },
  tile: {
    backgroundColor: 'transparent',
    width: 'auto',
    height: wp('13.33%'),
    marginTop: 0,
    marginHorizontal: 20,
    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 1.0,
    borderColor: '#818e97',
  },
  searchTextInput: {
    height: 'auto',
    width: '100%',
    paddingHorizontal: 20,
    backgroundColor: 'transparent',
    borderColor: 'gray',
    borderRadius: 0,
    fontSize: wp('4.8%'),
  },
  arrowIcon: {
    width: '100%',
    height: '100%',
  },
  touchableArrow: {
    backgroundColor: '#375064',
    position: 'absolute',
    right: 0,
    height: wp('16%'),
    width: wp('16%'),
    borderRadius: wp('8%'),
    justifyContent: 'center',
    alignItems: 'center',
  },
  arrowTile: {
    backgroundColor: 'transparent',
    width: '100%',
    height: 50,
    marginTop: 20,

    alignItems: 'center',
    flexDirection: 'row',
    borderBottomWidth: 0,
    borderColor: '#818e97',
  },

  tileFeedback: {
    backgroundColor: 'transparent',
    width: 'auto',
    height: 120,
    marginTop: 30,
    marginHorizontal: 20,

    borderBottomWidth: 1.0,
    borderColor: '#818e97',
    borderRadius: 5,
  },
});
